# AutoHotKey Emoji Text Expansion

use emoji text codes, `:shrug:`, to insert emojis into your text field.

A super hack so I could use this basic functionality, since I'm used to [Rocket for Mac](https://matthewpalmer.net/rocket/)


# how to use
1. Download the [emoji auto hot key script](./src/emoji.ahk) file (or just clone the repo)
1. Download [AutoHotKey](https://www.autohotkey.com/download/)
1. Double click the emoji.ahk script from the first step. 
1. type `:shrug:` into a text field
1. Profit

# Disclaimer it works 90% of the time 60% of the time. YMMV.
# Another Disclaimer it uses the offical emoji names so some times they aren't exactly what you expect. 🤷‍♀️