﻿;1F600
:*::happy-face::
:*::smiley-face::
:*::grinning-face::
Send,😀
return

;1F603

:*::grinning-face-with-big-eyes::
Send,😃
return

;1F604

:*::grinning-face-with-smiling-eyes::
Send,😄
return

;1F601
:*::big-smile::
:*::cheesy-smile::
:*::beaming-face-with-smiling-eyes::
Send,😁
return

;1F606

:*::grinning-squinting-face::
Send,😆
return

;1F605

:*::grinning-face-with-sweat::
Send,😅
return

;1F923
:*::rofl::
:*::side-cry-laugh::
:*::rolling-on-the-floor-laughing::
Send,🤣
return

;1F602
:*::laughing::
:*::laughing-crying::
:*::laughing-tears::
:*::lol::
:*::face-with-tears-of-joy::
Send,😂
return

;1F642
:*::slightly-happy::
:*::this-is-fine::
:*::slightly-smiling-face::
Send,🙂
return

;1F643
:*::sarcasm::
:*::silly::
:*::upside-down-face::
Send,🙃
return

;1F609
:*::wink::
:*::wink-face::
:*::winky-face::
:*::winking-face::
Send,😉
return

;1F60A
:*::^^::
:*::happy-face::
:*::smile::
:*::smiley-face::
:*::smiling-face-with-smiling-eyes::
Send,😊
return

;1F607
:*::angel::
:*::halo::
:*::smiling-face-with-halo::
Send,😇
return

;1F970

:*::smiling-face-with-hearts::
Send,🥰
return

;1F60D

:*::smiling-face-with-heart-eyes::
Send,😍
return

;1F929

:*::star-struck::
Send,🤩
return

;1F618

:*::face-blowing-a-kiss::
Send,😘
return

;1F617
:*:::-*::
:*::duck-face::
:*::kissy-face::
:*::whistling::
:*::kissing-face::
Send,😗
return

;263A FE0F

:*::smiling-face::
Send,☺️
return

;263A

:*::smiling-face::
Send,☺
return

;1F61A
:*::kiss-face::
:*::kissy-face::
:*::kissing-face-with-closed-eyes::
Send,😚
return

;1F619
:*::kiss-face::
:*::kissy::
:*::whistle::
:*::whistling::
:*::kissing-face-with-smiling-eyes::
Send,😙
return

;1F60B

:*::face-savoring-food::
Send,😋
return

;1F61B

:*::face-with-tongue::
Send,😛
return

;1F61C

:*::winking-face-with-tongue::
Send,😜
return

;1F92A

:*::zany-face::
Send,🤪
return

;1F61D

:*::squinting-face-with-tongue::
Send,😝
return

;1F911
:*::dollar-sign-eyes::
:*::money-face::
:*::rich::
:*::money-mouth-face::
Send,🤑
return

;1F917
:*::hug::
:*::hugging::
:*::hugs::
:*::hugging-face::
Send,🤗
return

;1F92D

:*::face-with-hand-over-mouth::
Send,🤭
return

;1F92B

:*::shushing-face::
Send,🤫
return

;1F914
:*::chin-thumb::
:*::thinker::
:*::throwing-shade::
:*::thinking-face::
Send,🤔
return

;1F910
:*::lips-sealed::
:*::sealed-lips::
:*::zip-it::
:*::zipper-mouth-face::
Send,🤐
return

;1F928

:*::face-with-raised-eyebrow::
Send,🤨
return

;1F610
:*::face-with-straight-mouth::
:*::straight-faced::
:*::neutral-face::
Send,😐
return

;1F611
:*::face-with-straight-mouth::
:*::straight-face::
:*::expressionless-face::
Send,😑
return

;1F636
:*::blank-face::
:*::mouthless::
:*::silence::
:*::silent::
:*::face-without-mouth::
Send,😶
return

;1F60F
:*::flirting::
:*::sexual-face::
:*::smug-face::
:*::suggestive-smile::
:*::smirking-face::
Send,😏
return

;1F612
:*::dissatisfied::
:*::meh::
:*::side-eye::
:*::unimpressed::
:*::unamused-face::
Send,😒
return

;1F644
:*::eye-roll::
:*::face-with-rolling-eyes::
Send,🙄
return

;1F62C
:*::awkward::
:*::eek::
:*::foot-in-mouth::
:*::nervous::
:*::snapchat-mutual-#1 best-friend::
:*::grimacing-face::
Send,😬
return

;1F925
:*::liar::
:*::long-nose::
:*::pinocchio::
:*::lying-face::
Send,🤥
return

;1F60C
:*::content::
:*::pleased::
:*::relieved-face::
Send,😌
return

;1F614
:*::pensive::
:*::sad::
:*::sadface::
:*::sorrowful::
:*::pensive-face::
Send,😔
return

;1F62A
:*::side-tear::
:*::snot-bubble::
:*::sleepy-face::
Send,😪
return

;1F924
:*::drool::
:*::drooling-face::
Send,🤤
return

;1F634
:*::sleep-face::
:*::snoring::
:*::zzz-face::
:*::sleeping-face::
Send,😴
return

;1F637
:*::bird-flu::
:*::doctor::
:*::mask-face::
:*::sars::
:*::sick::
:*::surgical-mask::
:*::face-with-medical-mask::
Send,😷
return

;1F912
:*::ill::
:*::sick::
:*::face-with-thermometer::
Send,🤒
return

;1F915
:*::bandaged-head::
:*::clumsy::
:*::injured::
:*::face-with-head-bandage::
Send,🤕
return

;1F922
:*::disgust::
:*::green face::
:*::vomit::
:*::nauseated-face::
Send,🤢
return

;1F92E

:*::face-vomiting::
Send,🤮
return

;1F927
:*::gesundheit::
:*::sneezing-face::
Send,🤧
return

;1F975

:*::hot-face::
Send,🥵
return

;1F976

:*::cold-face::
Send,🥶
return

;1F974

:*::woozy-face::
Send,🥴
return

;1F635
:*::cross-eyes-face::
:*::spiral eyes-face::
:*::dizzy-face::
Send,😵
return

;1F92F

:*::exploding-head::
Send,🤯
return

;1F920

:*::cowboy-hat-face::
Send,🤠
return

;1F973

:*::partying-face::
Send,🥳
return

;1F60E
:*::cool::
:*::mutual-best-friends-(snapchat)::
:*::sunglasses::
:*::smiling-face-with-sunglasses::
Send,😎
return

;1F913
:*::nerdy::
:*::nerd-face::
Send,🤓
return

;1F9D0

:*::face-with-monocle::
Send,🧐
return

;1F615
:*::nonplussed::
:*::puzzled::
:*:::s::
:*::confused-face::
Send,😕
return

;1F61F
:*::sad::
:*::sadface::
:*::worried-face::
Send,😟
return

;1F641
:*::slightly-sad::
:*::slightly-frowning-face::
Send,🙁
return

;2639 FE0F

:*::frowning-face::
Send,☹️
return

;2639

:*::frowning-face::
Send,☹
return

;1F62E
:*::open-mouth::
:*::surprised::
:*::face-with-open-mouth::
Send,😮
return

;1F62F
:*::surprise::
:*::surprised-face::
:*::hushed-face::
Send,😯
return

;1F632
:*::drunk face::
:*::gasping-face::
:*::shocked-face::
:*::astonished-face::
Send,😲
return

;1F633
:*:::$::
:*::blushing-face::
:*::embarrassed::
:*::shame::
:*::flushed-face::
Send,😳
return

;1F97A

:*::pleading-face::
Send,🥺
return

;1F626
:*::yawning::
:*::frowning-face-with-open-mouth::
Send,😦
return

;1F627
:*::pained-face::
:*::anguished-face::
Send,😧
return

;1F628
:*::scared::
:*::surprised::
:*::fearful-face::
Send,😨
return

;1F630

:*::anxious-face-with-sweat::
Send,😰
return

;1F625

:*::sad-but-relieved-face::
Send,😥
return

;1F622
:*::crying::
:*::tear::
:*::crying-face::
Send,😢
return

;1F62D
:*::bawling::
:*::crying::
:*::sad-tears::
:*::sobbing::
:*::loudly-crying-face::
Send,😭
return

;1F631
:*::home-alone::
:*::scream::
:*::screaming-face::
:*::face-screaming-in-fear::
Send,😱
return

;1F616
:*::quivering-mouth::
:*::scrunched-face::
:*::confounded-face::
Send,😖
return

;1F623
:*::helpless-face::
:*::scrunched-eyes::
:*::persevering-face::
Send,😣
return

;1F61E
:*:::(::
:*::sad::
:*::sadface::
:*::disappointed-face::
Send,😞
return

;1F613

:*::downcast-face-with-sweat::
Send,😓
return

;1F629
:*::distraught-face::
:*::wailing::
:*::weary-face::
Send,😩
return

;1F62B
:*::exhausted::
:*::fed-up::
:*::tired-face::
Send,😫
return

;1F971

:*::yawning-face::
Send,🥱
return

;1F624

:*::face-with-steam-from-nose::
Send,😤
return

;1F621
:*::angry-face::
:*::grumpy-face::
:*::mad-face::
:*::red-face::
:*::pouting-face::
Send,😡
return

;1F620
:*::angry::
:*::grumpy-face::
:*::angry-face::
Send,😠
return

;1F92C

:*::face-with-symbols-on-mouth::
Send,🤬
return

;1F608
:*::devil::
:*::devil-horns::
:*::happy-devil::
:*::purple-devil::
:*::red devil::
:*::smiling-face-with-horns::
Send,😈
return

;1F47F

:*::angry-face-with-horns::
Send,👿
return

;1F480
:*::death::
:*::grey-skull::
:*::skeleton::
:*::skull::
Send,💀
return

;2620 FE0F

:*::skull-and-crossbones::
Send,☠️
return

;2620

:*::skull-and-crossbones::
Send,☠
return

;1F4A9
:*::dog-dirt::
:*::smiling-poop::
:*::pile-of-poo::
Send,💩
return

;1F921
:*::creepy-clown::
:*::evil-clown::
:*::scary-clown::
:*::clown-face::
Send,🤡
return

;1F479

:*::ogre::
Send,👹
return

;1F47A

:*::goblin::
Send,👺
return

;1F47B
:*::ghoul::
:*::halloween::
:*::ghost::
Send,👻
return

;1F47D

:*::alien::
Send,👽
return

;1F47E
:*::space-invader::
:*::video-game-monster::
:*::alien-monster::
Send,👾
return

;1F916

:*::robot::
Send,🤖
return

;1F63A

:*::grinning-cat::
Send,😺
return

;1F638

:*::grinning-cat-with-smiling-eyes::
Send,😸
return

;1F639

:*::cat-with-tears-of-joy::
Send,😹
return

;1F63B

:*::smiling-cat-with-heart-eyes::
Send,😻
return

;1F63C

:*::cat-with-wry-smile::
Send,😼
return

;1F63D

:*::kissing-cat::
Send,😽
return

;1F640

:*::weary-cat::
Send,🙀
return

;1F63F

:*::crying-cat::
Send,😿
return

;1F63E

:*::pouting-cat::
Send,😾
return

;1F648
:*::mizaru::
:*::monkey-covering-eyes::
:*::see-no-evil-monkey::
Send,🙈
return

;1F649
:*::kikazaru::
:*::monkey-covering-ears::
:*::hear-no-evil-monkey::
Send,🙉
return

;1F64A
:*::iwazaru::
:*::monkey-covering-mouth::
:*::no speaking::
:*::speak-no-evil-monkey::
Send,🙊
return

;1F48B
:*::kissing-lips::
:*::kiss-mark::
Send,💋
return

;1F48C
:*::heart-envelope::
:*::love-note::
:*::love-letter::
Send,💌
return

;1F498
:*::cupid-arrow::
:*::lovestruck::
:*::heart-with-arrow::
Send,💘
return

;1F49D
:*::chocolate-box::
:*::gift-box::
:*::gift-heart::
:*::heart-with-ribbon::
Send,💝
return

;1F496
:*::sparkle-heart::
:*::sparkly-heart::
:*::stars heart::
:*::sparkling-heart::
Send,💖
return

;1F497
:*::multiple-heart::
:*::triple-heart::
:*::growing-heart::
Send,💗
return

;1F493
:*::heart-alarm::
:*::heartbeat::
:*::wifi-heart::
:*::beating-heart::
Send,💓
return

;1F49E
:*::two-hearts::
:*::revolving-hearts::
Send,💞
return

;1F495
:*::small-hearts::
:*::two-pink-hearts::
:*::two-hearts::
Send,💕
return

;1F49F

:*::heart-decoration::
Send,💟
return

;2763 FE0F

:*::heart-exclamation::
Send,❣️
return

;2763

:*::heart-exclamation::
Send,❣
return

;1F494
:*::breaking-heart::
:*::brokenhearted::
:*::heart-broken::
:*::broken-heart::
Send,💔
return

;2764 FE0F

:*::red-heart::
Send,❤️
return

;2764

:*::red-heart::
Send,❤
return

;1F9E1

:*::orange-heart::
Send,🧡
return

;1F49B
:*::#1-bf-snapchat::
:*::gold-heart::
:*::yellow-heart::
Send,💛
return

;1F49A
:*::jealous-heart::
:*::green-heart::
Send,💚
return

;1F499

:*::blue-heart::
Send,💙
return

;1F49C

:*::purple-heart::
Send,💜
return

;1F90E

:*::brown-heart::
Send,🤎
return

;1F5A4
:*::dark-heart::
:*::black-heart::
Send,🖤
return

;1F90D

:*::white-heart::
Send,🤍
return

;1F4AF

:*::hundred-points::
Send,💯
return

;1F4A2
:*::anger-sign::
:*::vein-pop::
:*::anger-symbol::
Send,💢
return

;1F4A5

:*::collision::
Send,💥
return

;1F4AB

:*::dizzy::
Send,💫
return

;1F4A6

:*::sweat-droplets::
Send,💦
return

;1F4A8

:*::dashing-away::
Send,💨
return

;1F573 FE0F

:*::hole::
Send,🕳️
return

;1F573

:*::hole::
Send,🕳
return

;1F4A3

:*::bomb::
Send,💣
return

;1F4AC
:*::chat-bubble::
:*::speech-bubble::
:*::speech-balloon::
Send,💬
return

;1F441 FE0F 200D 1F5E8 FE0F
:*::i-am-a-witness::
:*::eye-in-speech-bubble::
Send,👁️‍🗨️
return

;1F441 200D 1F5E8 FE0F
:*::i-am-a-witness::
:*::eye-in-speech-bubble::
Send,👁‍🗨️
return

;1F441 FE0F 200D 1F5E8
:*::i-am-a-witness::
:*::eye-in-speech-bubble::
Send,👁️‍🗨
return

;1F441 200D 1F5E8
:*::i-am-a-witness::
:*::eye-in-speech-bubble::
Send,👁‍🗨
return

;1F5E8 FE0F

:*::left-speech-bubble::
Send,🗨️
return

;1F5E8

:*::left-speech-bubble::
Send,🗨
return

;1F5EF FE0F
:*::zig-zag-bubble::
:*::right-anger-bubble::
Send,🗯️
return

;1F5EF
:*::zig-zag-bubble::
:*::right-anger-bubble::
Send,🗯
return

;1F4AD
:*::thinking-bubble::
:*::thought-bubble::
:*::thought-balloon::
Send,💭
return

;1F4A4

:*::zzz::
Send,💤
return

;1F44B

:*::waving-hand::
Send,👋
return

;1F44B 1F3FB

:*::waving-hand:-light-skin-tone::
Send,👋🏻
return

;1F44B 1F3FC

:*::waving-hand:-medium-light-skin-tone::
Send,👋🏼
return

;1F44B 1F3FD

:*::waving-hand:-medium-skin-tone::
Send,👋🏽
return

;1F44B 1F3FE

:*::waving-hand:-medium-dark-skin-tone::
Send,👋🏾
return

;1F44B 1F3FF

:*::waving-hand:-dark-skin-tone::
Send,👋🏿
return

;1F91A
:*::backhand::
:*::raised-back-of-hand::
Send,🤚
return

;1F91A 1F3FB

:*::raised-back-of-hand:-light-skin-tone::
Send,🤚🏻
return

;1F91A 1F3FC

:*::raised-back-of-hand:-medium-light-skin-::
Send,🤚🏼
return

;1F91A 1F3FD

:*::raised-back-of-hand:-medium-skin-tone::
Send,🤚🏽
return

;1F91A 1F3FE

:*::raised-back-of-hand:-medium-dark-skin-t::
Send,🤚🏾
return

;1F91A 1F3FF

:*::raised-back-of-hand:-dark-skin-tone::
Send,🤚🏿
return

;1F590 FE0F

:*::hand-with-fingers-splayed::
Send,🖐️
return

;1F590

:*::hand-with-fingers-splayed::
Send,🖐
return

;1F590 1F3FB

:*::hand-with-fingers-splayed:-light-skin-t::
Send,🖐🏻
return

;1F590 1F3FC

:*::hand-with-fingers-splayed:-medium-light::
Send,🖐🏼
return

;1F590 1F3FD

:*::hand-with-fingers-splayed:-medium-skin-::
Send,🖐🏽
return

;1F590 1F3FE

:*::hand-with-fingers-splayed:-medium-dark-::
Send,🖐🏾
return

;1F590 1F3FF

:*::hand-with-fingers-splayed:-dark-skin-to::
Send,🖐🏿
return

;270B
:*::high-five::
:*::stop::
:*::raised-hand::
Send,✋
return

;270B 1F3FB

:*::raised-hand:-light-skin-tone::
Send,✋🏻
return

;270B 1F3FC

:*::raised-hand:-medium-light-skin-tone::
Send,✋🏼
return

;270B 1F3FD

:*::raised-hand:-medium-skin-tone::
Send,✋🏽
return

;270B 1F3FE

:*::raised-hand:-medium-dark-skin-tone::
Send,✋🏾
return

;270B 1F3FF

:*::raised-hand:-dark-skin-tone::
Send,✋🏿
return

;1F596

:*::vulcan-salute::
Send,🖖
return

;1F596 1F3FB

:*::vulcan-salute:-light-skin-tone::
Send,🖖🏻
return

;1F596 1F3FC

:*::vulcan-salute:-medium-light-skin-tone::
Send,🖖🏼
return

;1F596 1F3FD

:*::vulcan-salute:-medium-skin-tone::
Send,🖖🏽
return

;1F596 1F3FE

:*::vulcan-salute:-medium-dark-skin-tone::
Send,🖖🏾
return

;1F596 1F3FF

:*::vulcan-salute:-dark-skin-tone::
Send,🖖🏿
return

;1F44C

:*::ok-hand::
Send,👌
return

;1F44C 1F3FB

:*::ok-hand:-light-skin-tone::
Send,👌🏻
return

;1F44C 1F3FC

:*::ok-hand:-medium-light-skin-tone::
Send,👌🏼
return

;1F44C 1F3FD

:*::ok-hand:-medium-skin-tone::
Send,👌🏽
return

;1F44C 1F3FE

:*::ok-hand:-medium-dark-skin-tone::
Send,👌🏾
return

;1F44C 1F3FF

:*::ok-hand:-dark-skin-tone::
Send,👌🏿
return

;1F90F

:*::pinching-hand::
Send,🤏
return

;1F90F 1F3FB

:*::pinching-hand:-light-skin-tone::
Send,🤏🏻
return

;1F90F 1F3FC

:*::pinching-hand:-medium-light-skin-tone::
Send,🤏🏼
return

;1F90F 1F3FD

:*::pinching-hand:-medium-skin-tone::
Send,🤏🏽
return

;1F90F 1F3FE

:*::pinching-hand:-medium-dark-skin-tone::
Send,🤏🏾
return

;1F90F 1F3FF

:*::pinching-hand:-dark-skin-tone::
Send,🤏🏿
return

;270C FE0F
:*::air-quotes::
:*::peace::
:*::peace-sign::
:*::v-sign::
:*::victory-hand::
Send,✌️
return

;270C
:*::air-quotes::
:*::peace::
:*::peace-sign::
:*::v-sign::
:*::victory-hand::
Send,✌
return

;270C 1F3FB

:*::victory-hand:-light-skin-tone::
Send,✌🏻
return

;270C 1F3FC

:*::victory-hand:-medium-light-skin-tone::
Send,✌🏼
return

;270C 1F3FD

:*::victory-hand:-medium-skin-tone::
Send,✌🏽
return

;270C 1F3FE

:*::victory-hand:-medium-dark-skin-tone::
Send,✌🏾
return

;270C 1F3FF

:*::victory-hand:-dark-skin-tone::
Send,✌🏿
return

;1F91E

:*::crossed-fingers::
Send,🤞
return

;1F91E 1F3FB

:*::crossed-fingers:-light-skin-tone::
Send,🤞🏻
return

;1F91E 1F3FC

:*::crossed-fingers:-medium-light-skin-tone::
Send,🤞🏼
return

;1F91E 1F3FD

:*::crossed-fingers:-medium-skin-tone::
Send,🤞🏽
return

;1F91E 1F3FE

:*::crossed-fingers:-medium-dark-skin-tone::
Send,🤞🏾
return

;1F91E 1F3FF

:*::crossed-fingers:-dark-skin-tone::
Send,🤞🏿
return

;1F91F

:*::love-you-gesture::
Send,🤟
return

;1F91F 1F3FB

:*::love-you-gesture:-light-skin-tone::
Send,🤟🏻
return

;1F91F 1F3FC

:*::love-you-gesture:-medium-light-skin-ton::
Send,🤟🏼
return

;1F91F 1F3FD

:*::love-you-gesture:-medium-skin-tone::
Send,🤟🏽
return

;1F91F 1F3FE

:*::love-you-gesture:-medium-dark-skin-tone::
Send,🤟🏾
return

;1F91F 1F3FF

:*::love-you-gesture:-dark-skin-tone::
Send,🤟🏿
return

;1F918
:*::devil-fingers::
:*::heavy-metal::
:*::rock-on::
:*::sign-of-the-horns::
Send,🤘
return

;1F918 1F3FB

:*::sign-of-the-horns:-light-skin-tone::
Send,🤘🏻
return

;1F918 1F3FC

:*::sign-of-the-horns:-medium-light-skin-to::
Send,🤘🏼
return

;1F918 1F3FD

:*::sign-of-the-horns:-medium-skin-tone::
Send,🤘🏽
return

;1F918 1F3FE

:*::sign-of-the-horns:-medium-dark-skin-ton::
Send,🤘🏾
return

;1F918 1F3FF

:*::sign-of-the-horns:-dark-skin-tone::
Send,🤘🏿
return

;1F919
:*::phone-hand::
:*::shaka::
:*::call-me-hand::
Send,🤙
return

;1F919 1F3FB

:*::call-me-hand:-light-skin-tone::
Send,🤙🏻
return

;1F919 1F3FC

:*::call-me-hand:-medium-light-skin-tone::
Send,🤙🏼
return

;1F919 1F3FD

:*::call-me-hand:-medium-skin-tone::
Send,🤙🏽
return

;1F919 1F3FE

:*::call-me-hand:-medium-dark-skin-tone::
Send,🤙🏾
return

;1F919 1F3FF

:*::call-me-hand:-dark-skin-tone::
Send,🤙🏿
return

;1F448

:*::backhand-index-pointing-left::
Send,👈
return

;1F448 1F3FB

:*::backhand-index-pointing-left:-light-ski::
Send,👈🏻
return

;1F448 1F3FC

:*::backhand-index-pointing-left:-medium-li::
Send,👈🏼
return

;1F448 1F3FD

:*::backhand-index-pointing-left:-medium-sk::
Send,👈🏽
return

;1F448 1F3FE

:*::backhand-index-pointing-left:-medium-da::
Send,👈🏾
return

;1F448 1F3FF

:*::backhand-index-pointing-left:-dark-skin::
Send,👈🏿
return

;1F449

:*::backhand-index-pointing-right::
Send,👉
return

;1F449 1F3FB

:*::backhand-index-pointing-right:-light-sk::
Send,👉🏻
return

;1F449 1F3FC

:*::backhand-index-pointing-right:-medium-l::
Send,👉🏼
return

;1F449 1F3FD

:*::backhand-index-pointing-right:-medium-s::
Send,👉🏽
return

;1F449 1F3FE

:*::backhand-index-pointing-right:-medium-d::
Send,👉🏾
return

;1F449 1F3FF

:*::backhand-index-pointing-right:-dark-ski::
Send,👉🏿
return

;1F446

:*::backhand-index-pointing-up::
Send,👆
return

;1F446 1F3FB

:*::backhand-index-pointing-up:-light-skin-::
Send,👆🏻
return

;1F446 1F3FC

:*::backhand-index-pointing-up:-medium-ligh::
Send,👆🏼
return

;1F446 1F3FD

:*::backhand-index-pointing-up:-medium-skin::
Send,👆🏽
return

;1F446 1F3FE

:*::backhand-index-pointing-up:-medium-dark::
Send,👆🏾
return

;1F446 1F3FF

:*::backhand-index-pointing-up:-dark-skin-t::
Send,👆🏿
return

;1F595

:*::middle-finger::
Send,🖕
return

;1F595 1F3FB

:*::middle-finger:-light-skin-tone::
Send,🖕🏻
return

;1F595 1F3FC

:*::middle-finger:-medium-light-skin-tone::
Send,🖕🏼
return

;1F595 1F3FD

:*::middle-finger:-medium-skin-tone::
Send,🖕🏽
return

;1F595 1F3FE

:*::middle-finger:-medium-dark-skin-tone::
Send,🖕🏾
return

;1F595 1F3FF

:*::middle-finger:-dark-skin-tone::
Send,🖕🏿
return

;1F447

:*::backhand-index-pointing-down::
Send,👇
return

;1F447 1F3FB

:*::backhand-index-pointing-down:-light-ski::
Send,👇🏻
return

;1F447 1F3FC

:*::backhand-index-pointing-down:-medium-li::
Send,👇🏼
return

;1F447 1F3FD

:*::backhand-index-pointing-down:-medium-sk::
Send,👇🏽
return

;1F447 1F3FE

:*::backhand-index-pointing-down:-medium-da::
Send,👇🏾
return

;1F447 1F3FF

:*::backhand-index-pointing-down:-dark-skin::
Send,👇🏿
return

;261D FE0F

:*::index-pointing-up::
Send,☝️
return

;261D

:*::index-pointing-up::
Send,☝
return

;261D 1F3FB

:*::index-pointing-up:-light-skin-tone::
Send,☝🏻
return

;261D 1F3FC

:*::index-pointing-up:-medium-light-skin-to::
Send,☝🏼
return

;261D 1F3FD

:*::index-pointing-up:-medium-skin-tone::
Send,☝🏽
return

;261D 1F3FE

:*::index-pointing-up:-medium-dark-skin-ton::
Send,☝🏾
return

;261D 1F3FF

:*::index-pointing-up:-dark-skin-tone::
Send,☝🏿
return

;1F44D

:*::thumbs-up::
Send,👍
return

;1F44D 1F3FB

:*::thumbs-up:-light-skin-tone::
Send,👍🏻
return

;1F44D 1F3FC

:*::thumbs-up:-medium-light-skin-tone::
Send,👍🏼
return

;1F44D 1F3FD

:*::thumbs-up:-medium-skin-tone::
Send,👍🏽
return

;1F44D 1F3FE

:*::thumbs-up:-medium-dark-skin-tone::
Send,👍🏾
return

;1F44D 1F3FF

:*::thumbs-up:-dark-skin-tone::
Send,👍🏿
return

;1F44E

:*::thumbs-down::
Send,👎
return

;1F44E 1F3FB

:*::thumbs-down:-light-skin-tone::
Send,👎🏻
return

;1F44E 1F3FC

:*::thumbs-down:-medium-light-skin-tone::
Send,👎🏼
return

;1F44E 1F3FD

:*::thumbs-down:-medium-skin-tone::
Send,👎🏽
return

;1F44E 1F3FE

:*::thumbs-down:-medium-dark-skin-tone::
Send,👎🏾
return

;1F44E 1F3FF

:*::thumbs-down:-dark-skin-tone::
Send,👎🏿
return

;270A
:*::fist-pump::
:*::raised-fist::
Send,✊
return

;270A 1F3FB

:*::raised-fist:-light-skin-tone::
Send,✊🏻
return

;270A 1F3FC

:*::raised-fist:-medium-light-skin-tone::
Send,✊🏼
return

;270A 1F3FD

:*::raised-fist:-medium-skin-tone::
Send,✊🏽
return

;270A 1F3FE

:*::raised-fist:-medium-dark-skin-tone::
Send,✊🏾
return

;270A 1F3FF

:*::raised-fist:-dark-skin-tone::
Send,✊🏿
return

;1F44A

:*::oncoming-fist::
Send,👊
return

;1F44A 1F3FB

:*::oncoming-fist:-light-skin-tone::
Send,👊🏻
return

;1F44A 1F3FC

:*::oncoming-fist:-medium-light-skin-tone::
Send,👊🏼
return

;1F44A 1F3FD

:*::oncoming-fist:-medium-skin-tone::
Send,👊🏽
return

;1F44A 1F3FE

:*::oncoming-fist:-medium-dark-skin-tone::
Send,👊🏾
return

;1F44A 1F3FF

:*::oncoming-fist:-dark-skin-tone::
Send,👊🏿
return

;1F91B
:*::left-fist-bump::
:*::left-facing-fist::
Send,🤛
return

;1F91B 1F3FB

:*::left-facing-fist:-light-skin-tone::
Send,🤛🏻
return

;1F91B 1F3FC

:*::left-facing-fist:-medium-light-skin-ton::
Send,🤛🏼
return

;1F91B 1F3FD

:*::left-facing-fist:-medium-skin-tone::
Send,🤛🏽
return

;1F91B 1F3FE

:*::left-facing-fist:-medium-dark-skin-tone::
Send,🤛🏾
return

;1F91B 1F3FF

:*::left-facing-fist:-dark-skin-tone::
Send,🤛🏿
return

;1F91C
:*::right fist-bump::
:*::right-facing-fist::
Send,🤜
return

;1F91C 1F3FB

:*::right-facing-fist:-light-skin-tone::
Send,🤜🏻
return

;1F91C 1F3FC

:*::right-facing-fist:-medium-light-skin-to::
Send,🤜🏼
return

;1F91C 1F3FD

:*::right-facing-fist:-medium-skin-tone::
Send,🤜🏽
return

;1F91C 1F3FE

:*::right-facing-fist:-medium-dark-skin-ton::
Send,🤜🏾
return

;1F91C 1F3FF

:*::right-facing-fist:-dark-skin-tone::
Send,🤜🏿
return

;1F44F

:*::clapping-hands::
Send,👏
return

;1F44F 1F3FB

:*::clapping-hands:-light-skin-tone::
Send,👏🏻
return

;1F44F 1F3FC

:*::clapping-hands:-medium-light-skin-tone::
Send,👏🏼
return

;1F44F 1F3FD

:*::clapping-hands:-medium-skin-tone::
Send,👏🏽
return

;1F44F 1F3FE

:*::clapping-hands:-medium-dark-skin-tone::
Send,👏🏾
return

;1F44F 1F3FF

:*::clapping-hands:-dark-skin-tone::
Send,👏🏿
return

;1F64C

:*::raising-hands::
Send,🙌
return

;1F64C 1F3FB

:*::raising-hands:-light-skin-tone::
Send,🙌🏻
return

;1F64C 1F3FC

:*::raising-hands:-medium-light-skin-tone::
Send,🙌🏼
return

;1F64C 1F3FD

:*::raising-hands:-medium-skin-tone::
Send,🙌🏽
return

;1F64C 1F3FE

:*::raising-hands:-medium-dark-skin-tone::
Send,🙌🏾
return

;1F64C 1F3FF

:*::raising-hands:-dark-skin-tone::
Send,🙌🏿
return

;1F450

:*::open-hands::
Send,👐
return

;1F450 1F3FB

:*::open-hands:-light-skin-tone::
Send,👐🏻
return

;1F450 1F3FC

:*::open-hands:-medium-light-skin-tone::
Send,👐🏼
return

;1F450 1F3FD

:*::open-hands:-medium-skin-tone::
Send,👐🏽
return

;1F450 1F3FE

:*::open-hands:-medium-dark-skin-tone::
Send,👐🏾
return

;1F450 1F3FF

:*::open-hands:-dark-skin-tone::
Send,👐🏿
return

;1F932
:*::dua::
:*::palms-up-together::
Send,🤲
return

;1F932 1F3FB

:*::palms-up-together:-light-skin-tone::
Send,🤲🏻
return

;1F932 1F3FC

:*::palms-up-together:-medium-light-skin-to::
Send,🤲🏼
return

;1F932 1F3FD

:*::palms-up-together:-medium-skin-tone::
Send,🤲🏽
return

;1F932 1F3FE

:*::palms-up-together:-medium-dark-skin-ton::
Send,🤲🏾
return

;1F932 1F3FF

:*::palms-up-together:-dark-skin-tone::
Send,🤲🏿
return

;1F91D
:*::shaking-hands::
:*::handshake::
Send,🤝
return

;1F64F

:*::folded-hands::
Send,🙏
return

;1F64F 1F3FB

:*::folded-hands:-light-skin-tone::
Send,🙏🏻
return

;1F64F 1F3FC

:*::folded-hands:-medium-light-skin-tone::
Send,🙏🏼
return

;1F64F 1F3FD

:*::folded-hands:-medium-skin-tone::
Send,🙏🏽
return

;1F64F 1F3FE

:*::folded-hands:-medium-dark-skin-tone::
Send,🙏🏾
return

;1F64F 1F3FF

:*::folded-hands:-dark-skin-tone::
Send,🙏🏿
return

;270D FE0F

:*::writing-hand::
Send,✍️
return

;270D

:*::writing-hand::
Send,✍
return

;270D 1F3FB

:*::writing-hand:-light-skin-tone::
Send,✍🏻
return

;270D 1F3FC

:*::writing-hand:-medium-light-skin-tone::
Send,✍🏼
return

;270D 1F3FD

:*::writing-hand:-medium-skin-tone::
Send,✍🏽
return

;270D 1F3FE

:*::writing-hand:-medium-dark-skin-tone::
Send,✍🏾
return

;270D 1F3FF

:*::writing-hand:-dark-skin-tone::
Send,✍🏿
return

;1F485
:*::fingers::
:*::manicure::
:*::nonchalant::
:*::nail-polish::
Send,💅
return

;1F485 1F3FB

:*::nail-polish:-light-skin-tone::
Send,💅🏻
return

;1F485 1F3FC

:*::nail-polish:-medium-light-skin-tone::
Send,💅🏼
return

;1F485 1F3FD

:*::nail-polish:-medium-skin-tone::
Send,💅🏽
return

;1F485 1F3FE

:*::nail-polish:-medium-dark-skin-tone::
Send,💅🏾
return

;1F485 1F3FF

:*::nail-polish:-dark-skin-tone::
Send,💅🏿
return

;1F933
:*::phone-camera::
:*::selfie-hand::
:*::selfie::
Send,🤳
return

;1F933 1F3FB

:*::selfie:-light-skin-tone::
Send,🤳🏻
return

;1F933 1F3FC

:*::selfie:-medium-light-skin-tone::
Send,🤳🏼
return

;1F933 1F3FD

:*::selfie:-medium-skin-tone::
Send,🤳🏽
return

;1F933 1F3FE

:*::selfie:-medium-dark-skin-tone::
Send,🤳🏾
return

;1F933 1F3FF

:*::selfie:-dark-skin-tone::
Send,🤳🏿
return

;1F4AA
:*::feats-of-strength::
:*::flexing-arm-muscles::
:*::muscle::
:*::strong::
:*::flexed-biceps::
Send,💪
return

;1F4AA 1F3FB

:*::flexed-biceps:-light-skin-tone::
Send,💪🏻
return

;1F4AA 1F3FC

:*::flexed-biceps:-medium-light-skin-tone::
Send,💪🏼
return

;1F4AA 1F3FD

:*::flexed-biceps:-medium-skin-tone::
Send,💪🏽
return

;1F4AA 1F3FE

:*::flexed-biceps:-medium-dark-skin-tone::
Send,💪🏾
return

;1F4AA 1F3FF

:*::flexed-biceps:-dark-skin-tone::
Send,💪🏿
return

;1F9BE

:*::mechanical-arm::
Send,🦾
return

;1F9BF

:*::mechanical-leg::
Send,🦿
return

;1F9B5

:*::leg::
Send,🦵
return

;1F9B5 1F3FB

:*::leg:-light-skin-tone::
Send,🦵🏻
return

;1F9B5 1F3FC

:*::leg:-medium-light-skin-tone::
Send,🦵🏼
return

;1F9B5 1F3FD

:*::leg:-medium-skin-tone::
Send,🦵🏽
return

;1F9B5 1F3FE

:*::leg:-medium-dark-skin-tone::
Send,🦵🏾
return

;1F9B5 1F3FF

:*::leg:-dark-skin-tone::
Send,🦵🏿
return

;1F9B6

:*::foot::
Send,🦶
return

;1F9B6 1F3FB

:*::foot:-light-skin-tone::
Send,🦶🏻
return

;1F9B6 1F3FC

:*::foot:-medium-light-skin-tone::
Send,🦶🏼
return

;1F9B6 1F3FD

:*::foot:-medium-skin-tone::
Send,🦶🏽
return

;1F9B6 1F3FE

:*::foot:-medium-dark-skin-tone::
Send,🦶🏾
return

;1F9B6 1F3FF

:*::foot:-dark-skin-tone::
Send,🦶🏿
return

;1F442
:*::ears::
:*::hearing::
:*::listening::
:*::ear::
Send,👂
return

;1F442 1F3FB

:*::ear:-light-skin-tone::
Send,👂🏻
return

;1F442 1F3FC

:*::ear:-medium-light-skin-tone::
Send,👂🏼
return

;1F442 1F3FD

:*::ear:-medium-skin-tone::
Send,👂🏽
return

;1F442 1F3FE

:*::ear:-medium-dark-skin-tone::
Send,👂🏾
return

;1F442 1F3FF

:*::ear:-dark-skin-tone::
Send,👂🏿
return

;1F9BB

:*::ear-with-hearing-aid::
Send,🦻
return

;1F9BB 1F3FB

:*::ear-with-hearing-aid:-light-skin-tone::
Send,🦻🏻
return

;1F9BB 1F3FC

:*::ear-with-hearing-aid:-medium-light-skin::
Send,🦻🏼
return

;1F9BB 1F3FD

:*::ear-with-hearing-aid:-medium-skin-tone::
Send,🦻🏽
return

;1F9BB 1F3FE

:*::ear-with-hearing-aid:-medium-dark-skin-::
Send,🦻🏾
return

;1F9BB 1F3FF

:*::ear-with-hearing-aid:-dark-skin-tone::
Send,🦻🏿
return

;1F443
:*::smelling::
:*::sniffing::
:*::stinky::
:*::nose::
Send,👃
return

;1F443 1F3FB

:*::nose:-light-skin-tone::
Send,👃🏻
return

;1F443 1F3FC

:*::nose:-medium-light-skin-tone::
Send,👃🏼
return

;1F443 1F3FD

:*::nose:-medium-skin-tone::
Send,👃🏽
return

;1F443 1F3FE

:*::nose:-medium-dark-skin-tone::
Send,👃🏾
return

;1F443 1F3FF

:*::nose:-dark-skin-tone::
Send,👃🏿
return

;1F9E0

:*::brain::
Send,🧠
return

;1F9B7

:*::tooth::
Send,🦷
return

;1F9B4

:*::bone::
Send,🦴
return

;1F440
:*::eyeballs::
:*::shifty-eyes::
:*::wide-eyes::
:*::eyes::
Send,👀
return

;1F441 FE0F
:*::single-eye::
:*::eye::
Send,👁️
return

;1F441
:*::single-eye::
:*::eye::
Send,👁
return

;1F445
:*::tongue-out::
:*::tongue::
Send,👅
return

;1F444
:*::kissing-lips::
:*::lips::
:*::mouth::
Send,👄
return

;1F476
:*::child::
:*::toddler::
:*::baby::
Send,👶
return

;1F476 1F3FB

:*::baby:-light-skin-tone::
Send,👶🏻
return

;1F476 1F3FC

:*::baby:-medium-light-skin-tone::
Send,👶🏼
return

;1F476 1F3FD

:*::baby:-medium-skin-tone::
Send,👶🏽
return

;1F476 1F3FE

:*::baby:-medium-dark-skin-tone::
Send,👶🏾
return

;1F476 1F3FF

:*::baby:-dark-skin-tone::
Send,👶🏿
return

;1F9D2
:*::gender-neutral-child::
:*::child::
Send,🧒
return

;1F9D2 1F3FB

:*::child:-light-skin-tone::
Send,🧒🏻
return

;1F9D2 1F3FC

:*::child:-medium-light-skin-tone::
Send,🧒🏼
return

;1F9D2 1F3FD

:*::child:-medium-skin-tone::
Send,🧒🏽
return

;1F9D2 1F3FE

:*::child:-medium-dark-skin-tone::
Send,🧒🏾
return

;1F9D2 1F3FF

:*::child:-dark-skin-tone::
Send,🧒🏿
return

;1F466

:*::boy::
Send,👦
return

;1F466 1F3FB

:*::boy:-light-skin-tone::
Send,👦🏻
return

;1F466 1F3FC

:*::boy:-medium-light-skin-tone::
Send,👦🏼
return

;1F466 1F3FD

:*::boy:-medium-skin-tone::
Send,👦🏽
return

;1F466 1F3FE

:*::boy:-medium-dark-skin-tone::
Send,👦🏾
return

;1F466 1F3FF

:*::boy:-dark-skin-tone::
Send,👦🏿
return

;1F467

:*::girl::
Send,👧
return

;1F467 1F3FB

:*::girl:-light-skin-tone::
Send,👧🏻
return

;1F467 1F3FC

:*::girl:-medium-light-skin-tone::
Send,👧🏼
return

;1F467 1F3FD

:*::girl:-medium-skin-tone::
Send,👧🏽
return

;1F467 1F3FE

:*::girl:-medium-dark-skin-tone::
Send,👧🏾
return

;1F467 1F3FF

:*::girl:-dark-skin-tone::
Send,👧🏿
return

;1F9D1

:*::person::
Send,🧑
return

;1F9D1 1F3FB

:*::person:-light-skin-tone::
Send,🧑🏻
return

;1F9D1 1F3FC

:*::person:-medium-light-skin-tone::
Send,🧑🏼
return

;1F9D1 1F3FD

:*::person:-medium-skin-tone::
Send,🧑🏽
return

;1F9D1 1F3FE

:*::person:-medium-dark-skin-tone::
Send,🧑🏾
return

;1F9D1 1F3FF

:*::person:-dark-skin-tone::
Send,🧑🏿
return

;1F471

:*::person:-blond-hair::
Send,👱
return

;1F471 1F3FB

:*::person:-light-skin-tone,-blond-hair::
Send,👱🏻
return

;1F471 1F3FC

:*::person:-medium-light-skin-tone,-blond-h::
Send,👱🏼
return

;1F471 1F3FD

:*::person:-medium-skin-tone,-blond-hair::
Send,👱🏽
return

;1F471 1F3FE

:*::person:-medium-dark-skin-tone,-blond-ha::
Send,👱🏾
return

;1F471 1F3FF

:*::person:-dark-skin-tone,-blond-hair::
Send,👱🏿
return

;1F468
:*::male::
:*::moustache-man::
:*::man::
Send,👨
return

;1F468 1F3FB

:*::man:-light-skin-tone::
Send,👨🏻
return

;1F468 1F3FC

:*::man:-medium-light-skin-tone::
Send,👨🏼
return

;1F468 1F3FD

:*::man:-medium-skin-tone::
Send,👨🏽
return

;1F468 1F3FE

:*::man:-medium-dark-skin-tone::
Send,👨🏾
return

;1F468 1F3FF

:*::man:-dark-skin-tone::
Send,👨🏿
return

;1F9D4

:*::man:-beard::
Send,🧔
return

;1F9D4 1F3FB

:*::man:-light-skin-tone,-beard::
Send,🧔🏻
return

;1F9D4 1F3FC

:*::man:-medium-light-skin-tone,-beard::
Send,🧔🏼
return

;1F9D4 1F3FD

:*::man:-medium-skin-tone,-beard::
Send,🧔🏽
return

;1F9D4 1F3FE

:*::man:-medium-dark-skin-tone,-beard::
Send,🧔🏾
return

;1F9D4 1F3FF

:*::man:-dark-skin-tone,-beard::
Send,🧔🏿
return

;1F471 200D 2642 FE0F

:*::man:-blond-hair::
Send,👱‍♂️
return

;1F471 200D 2642

:*::man:-blond-hair::
Send,👱‍♂
return

;1F471 1F3FB 200D 2642 FE0F

:*::man:-light-skin-tone,-blond-hair::
Send,👱🏻‍♂️
return

;1F471 1F3FB 200D 2642

:*::man:-light-skin-tone,-blond-hair::
Send,👱🏻‍♂
return

;1F471 1F3FC 200D 2642 FE0F

:*::man:-medium-light-skin-tone,-blond-hair::
Send,👱🏼‍♂️
return

;1F471 1F3FC 200D 2642

:*::man:-medium-light-skin-tone,-blond-hair::
Send,👱🏼‍♂
return

;1F471 1F3FD 200D 2642 FE0F

:*::man:-medium-skin-tone,-blond-hair::
Send,👱🏽‍♂️
return

;1F471 1F3FD 200D 2642

:*::man:-medium-skin-tone,-blond-hair::
Send,👱🏽‍♂
return

;1F471 1F3FE 200D 2642 FE0F

:*::man:-medium-dark-skin-tone,-blond-hair::
Send,👱🏾‍♂️
return

;1F471 1F3FE 200D 2642

:*::man:-medium-dark-skin-tone,-blond-hair::
Send,👱🏾‍♂
return

;1F471 1F3FF 200D 2642 FE0F

:*::man:-dark-skin-tone,-blond-hair::
Send,👱🏿‍♂️
return

;1F471 1F3FF 200D 2642

:*::man:-dark-skin-tone,-blond-hair::
Send,👱🏿‍♂
return

;1F468 200D 1F9B0

:*::man:-red-hair::
Send,👨‍🦰
return

;1F468 1F3FB 200D 1F9B0

:*::man:-light-skin-tone,-red-hair::
Send,👨🏻‍🦰
return

;1F468 1F3FC 200D 1F9B0

:*::man:-medium-light-skin-tone,-red-hair::
Send,👨🏼‍🦰
return

;1F468 1F3FD 200D 1F9B0

:*::man:-medium-skin-tone,-red-hair::
Send,👨🏽‍🦰
return

;1F468 1F3FE 200D 1F9B0

:*::man:-medium-dark-skin-tone,-red-hair::
Send,👨🏾‍🦰
return

;1F468 1F3FF 200D 1F9B0

:*::man:-dark-skin-tone,-red-hair::
Send,👨🏿‍🦰
return

;1F468 200D 1F9B1

:*::man:-curly-hair::
Send,👨‍🦱
return

;1F468 1F3FB 200D 1F9B1

:*::man:-light-skin-tone,-curly-hair::
Send,👨🏻‍🦱
return

;1F468 1F3FC 200D 1F9B1

:*::man:-medium-light-skin-tone,-curly-hair::
Send,👨🏼‍🦱
return

;1F468 1F3FD 200D 1F9B1

:*::man:-medium-skin-tone,-curly-hair::
Send,👨🏽‍🦱
return

;1F468 1F3FE 200D 1F9B1

:*::man:-medium-dark-skin-tone,-curly-hair::
Send,👨🏾‍🦱
return

;1F468 1F3FF 200D 1F9B1

:*::man:-dark-skin-tone,-curly-hair::
Send,👨🏿‍🦱
return

;1F468 200D 1F9B3

:*::man:-white-hair::
Send,👨‍🦳
return

;1F468 1F3FB 200D 1F9B3

:*::man:-light-skin-tone,-white-hair::
Send,👨🏻‍🦳
return

;1F468 1F3FC 200D 1F9B3

:*::man:-medium-light-skin-tone,-white-hair::
Send,👨🏼‍🦳
return

;1F468 1F3FD 200D 1F9B3

:*::man:-medium-skin-tone,-white-hair::
Send,👨🏽‍🦳
return

;1F468 1F3FE 200D 1F9B3

:*::man:-medium-dark-skin-tone,-white-hair::
Send,👨🏾‍🦳
return

;1F468 1F3FF 200D 1F9B3

:*::man:-dark-skin-tone,-white-hair::
Send,👨🏿‍🦳
return

;1F468 200D 1F9B2

:*::man:-bald::
Send,👨‍🦲
return

;1F468 1F3FB 200D 1F9B2

:*::man:-light-skin-tone,-bald::
Send,👨🏻‍🦲
return

;1F468 1F3FC 200D 1F9B2

:*::man:-medium-light-skin-tone,-bald::
Send,👨🏼‍🦲
return

;1F468 1F3FD 200D 1F9B2

:*::man:-medium-skin-tone,-bald::
Send,👨🏽‍🦲
return

;1F468 1F3FE 200D 1F9B2

:*::man:-medium-dark-skin-tone,-bald::
Send,👨🏾‍🦲
return

;1F468 1F3FF 200D 1F9B2

:*::man:-dark-skin-tone,-bald::
Send,👨🏿‍🦲
return

;1F469
:*::female::
:*::lady::
:*::yellow-woman::
:*::woman::
Send,👩
return

;1F469 1F3FB

:*::woman:-light-skin-tone::
Send,👩🏻
return

;1F469 1F3FC

:*::woman:-medium-light-skin-tone::
Send,👩🏼
return

;1F469 1F3FD

:*::woman:-medium-skin-tone::
Send,👩🏽
return

;1F469 1F3FE

:*::woman:-medium-dark-skin-tone::
Send,👩🏾
return

;1F469 1F3FF

:*::woman:-dark-skin-tone::
Send,👩🏿
return

;1F471 200D 2640 FE0F

:*::woman:-blond-hair::
Send,👱‍♀️
return

;1F471 200D 2640

:*::woman:-blond-hair::
Send,👱‍♀
return

;1F471 1F3FB 200D 2640 FE0F

:*::woman:-light-skin-tone,-blond-hair::
Send,👱🏻‍♀️
return

;1F471 1F3FB 200D 2640

:*::woman:-light-skin-tone,-blond-hair::
Send,👱🏻‍♀
return

;1F471 1F3FC 200D 2640 FE0F

:*::woman:-medium-light-skin-tone,-blond-ha::
Send,👱🏼‍♀️
return

;1F471 1F3FC 200D 2640

:*::woman:-medium-light-skin-tone,-blond-ha::
Send,👱🏼‍♀
return

;1F471 1F3FD 200D 2640 FE0F

:*::woman:-medium-skin-tone,-blond-hair::
Send,👱🏽‍♀️
return

;1F471 1F3FD 200D 2640

:*::woman:-medium-skin-tone,-blond-hair::
Send,👱🏽‍♀
return

;1F471 1F3FE 200D 2640 FE0F

:*::woman:-medium-dark-skin-tone,-blond-hai::
Send,👱🏾‍♀️
return

;1F471 1F3FE 200D 2640

:*::woman:-medium-dark-skin-tone,-blond-hai::
Send,👱🏾‍♀
return

;1F471 1F3FF 200D 2640 FE0F

:*::woman:-dark-skin-tone,-blond-hair::
Send,👱🏿‍♀️
return

;1F471 1F3FF 200D 2640

:*::woman:-dark-skin-tone,-blond-hair::
Send,👱🏿‍♀
return

;1F469 200D 1F9B0

:*::woman:-red-hair::
Send,👩‍🦰
return

;1F469 1F3FB 200D 1F9B0

:*::woman:-light-skin-tone,-red-hair::
Send,👩🏻‍🦰
return

;1F469 1F3FC 200D 1F9B0

:*::woman:-medium-light-skin-tone,-red-hair::
Send,👩🏼‍🦰
return

;1F469 1F3FD 200D 1F9B0

:*::woman:-medium-skin-tone,-red-hair::
Send,👩🏽‍🦰
return

;1F469 1F3FE 200D 1F9B0

:*::woman:-medium-dark-skin-tone,-red-hair::
Send,👩🏾‍🦰
return

;1F469 1F3FF 200D 1F9B0

:*::woman:-dark-skin-tone,-red-hair::
Send,👩🏿‍🦰
return

;1F469 200D 1F9B1

:*::woman:-curly-hair::
Send,👩‍🦱
return

;1F469 1F3FB 200D 1F9B1

:*::woman:-light-skin-tone,-curly-hair::
Send,👩🏻‍🦱
return

;1F469 1F3FC 200D 1F9B1

:*::woman:-medium-light-skin-tone,-curly-ha::
Send,👩🏼‍🦱
return

;1F469 1F3FD 200D 1F9B1

:*::woman:-medium-skin-tone,-curly-hair::
Send,👩🏽‍🦱
return

;1F469 1F3FE 200D 1F9B1

:*::woman:-medium-dark-skin-tone,-curly-hai::
Send,👩🏾‍🦱
return

;1F469 1F3FF 200D 1F9B1

:*::woman:-dark-skin-tone,-curly-hair::
Send,👩🏿‍🦱
return

;1F469 200D 1F9B3

:*::woman:-white-hair::
Send,👩‍🦳
return

;1F469 1F3FB 200D 1F9B3

:*::woman:-light-skin-tone,-white-hair::
Send,👩🏻‍🦳
return

;1F469 1F3FC 200D 1F9B3

:*::woman:-medium-light-skin-tone,-white-ha::
Send,👩🏼‍🦳
return

;1F469 1F3FD 200D 1F9B3

:*::woman:-medium-skin-tone,-white-hair::
Send,👩🏽‍🦳
return

;1F469 1F3FE 200D 1F9B3

:*::woman:-medium-dark-skin-tone,-white-hai::
Send,👩🏾‍🦳
return

;1F469 1F3FF 200D 1F9B3

:*::woman:-dark-skin-tone,-white-hair::
Send,👩🏿‍🦳
return

;1F469 200D 1F9B2

:*::woman:-bald::
Send,👩‍🦲
return

;1F469 1F3FB 200D 1F9B2

:*::woman:-light-skin-tone,-bald::
Send,👩🏻‍🦲
return

;1F469 1F3FC 200D 1F9B2

:*::woman:-medium-light-skin-tone,-bald::
Send,👩🏼‍🦲
return

;1F469 1F3FD 200D 1F9B2

:*::woman:-medium-skin-tone,-bald::
Send,👩🏽‍🦲
return

;1F469 1F3FE 200D 1F9B2

:*::woman:-medium-dark-skin-tone,-bald::
Send,👩🏾‍🦲
return

;1F469 1F3FF 200D 1F9B2

:*::woman:-dark-skin-tone,-bald::
Send,👩🏿‍🦲
return

;1F9D3

:*::older-person::
Send,🧓
return

;1F9D3 1F3FB

:*::older-person:-light-skin-tone::
Send,🧓🏻
return

;1F9D3 1F3FC

:*::older-person:-medium-light-skin-tone::
Send,🧓🏼
return

;1F9D3 1F3FD

:*::older-person:-medium-skin-tone::
Send,🧓🏽
return

;1F9D3 1F3FE

:*::older-person:-medium-dark-skin-tone::
Send,🧓🏾
return

;1F9D3 1F3FF

:*::older-person:-dark-skin-tone::
Send,🧓🏿
return

;1F474

:*::old-man::
Send,👴
return

;1F474 1F3FB

:*::old-man:-light-skin-tone::
Send,👴🏻
return

;1F474 1F3FC

:*::old-man:-medium-light-skin-tone::
Send,👴🏼
return

;1F474 1F3FD

:*::old-man:-medium-skin-tone::
Send,👴🏽
return

;1F474 1F3FE

:*::old-man:-medium-dark-skin-tone::
Send,👴🏾
return

;1F474 1F3FF

:*::old-man:-dark-skin-tone::
Send,👴🏿
return

;1F475

:*::old-woman::
Send,👵
return

;1F475 1F3FB

:*::old-woman:-light-skin-tone::
Send,👵🏻
return

;1F475 1F3FC

:*::old-woman:-medium-light-skin-tone::
Send,👵🏼
return

;1F475 1F3FD

:*::old-woman:-medium-skin-tone::
Send,👵🏽
return

;1F475 1F3FE

:*::old-woman:-medium-dark-skin-tone::
Send,👵🏾
return

;1F475 1F3FF

:*::old-woman:-dark-skin-tone::
Send,👵🏿
return

;1F64D
:*::sad-person::
:*::woman-frowning::
:*::person-frowning::
Send,🙍
return

;1F64D 1F3FB

:*::person-frowning:-light-skin-tone::
Send,🙍🏻
return

;1F64D 1F3FC

:*::person-frowning:-medium-light-skin-tone::
Send,🙍🏼
return

;1F64D 1F3FD

:*::person-frowning:-medium-skin-tone::
Send,🙍🏽
return

;1F64D 1F3FE

:*::person-frowning:-medium-dark-skin-tone::
Send,🙍🏾
return

;1F64D 1F3FF

:*::person-frowning:-dark-skin-tone::
Send,🙍🏿
return

;1F64D 200D 2642 FE0F

:*::man-frowning::
Send,🙍‍♂️
return

;1F64D 200D 2642

:*::man-frowning::
Send,🙍‍♂
return

;1F64D 1F3FB 200D 2642 FE0F

:*::man-frowning:-light-skin-tone::
Send,🙍🏻‍♂️
return

;1F64D 1F3FB 200D 2642

:*::man-frowning:-light-skin-tone::
Send,🙍🏻‍♂
return

;1F64D 1F3FC 200D 2642 FE0F

:*::man-frowning:-medium-light-skin-tone::
Send,🙍🏼‍♂️
return

;1F64D 1F3FC 200D 2642

:*::man-frowning:-medium-light-skin-tone::
Send,🙍🏼‍♂
return

;1F64D 1F3FD 200D 2642 FE0F

:*::man-frowning:-medium-skin-tone::
Send,🙍🏽‍♂️
return

;1F64D 1F3FD 200D 2642

:*::man-frowning:-medium-skin-tone::
Send,🙍🏽‍♂
return

;1F64D 1F3FE 200D 2642 FE0F

:*::man-frowning:-medium-dark-skin-tone::
Send,🙍🏾‍♂️
return

;1F64D 1F3FE 200D 2642

:*::man-frowning:-medium-dark-skin-tone::
Send,🙍🏾‍♂
return

;1F64D 1F3FF 200D 2642 FE0F

:*::man-frowning:-dark-skin-tone::
Send,🙍🏿‍♂️
return

;1F64D 1F3FF 200D 2642

:*::man-frowning:-dark-skin-tone::
Send,🙍🏿‍♂
return

;1F64D 200D 2640 FE0F

:*::woman-frowning::
Send,🙍‍♀️
return

;1F64D 200D 2640

:*::woman-frowning::
Send,🙍‍♀
return

;1F64D 1F3FB 200D 2640 FE0F

:*::woman-frowning:-light-skin-tone::
Send,🙍🏻‍♀️
return

;1F64D 1F3FB 200D 2640

:*::woman-frowning:-light-skin-tone::
Send,🙍🏻‍♀
return

;1F64D 1F3FC 200D 2640 FE0F

:*::woman-frowning:-medium-light-skin-tone::
Send,🙍🏼‍♀️
return

;1F64D 1F3FC 200D 2640

:*::woman-frowning:-medium-light-skin-tone::
Send,🙍🏼‍♀
return

;1F64D 1F3FD 200D 2640 FE0F

:*::woman-frowning:-medium-skin-tone::
Send,🙍🏽‍♀️
return

;1F64D 1F3FD 200D 2640

:*::woman-frowning:-medium-skin-tone::
Send,🙍🏽‍♀
return

;1F64D 1F3FE 200D 2640 FE0F

:*::woman-frowning:-medium-dark-skin-tone::
Send,🙍🏾‍♀️
return

;1F64D 1F3FE 200D 2640

:*::woman-frowning:-medium-dark-skin-tone::
Send,🙍🏾‍♀
return

;1F64D 1F3FF 200D 2640 FE0F

:*::woman-frowning:-dark-skin-tone::
Send,🙍🏿‍♀️
return

;1F64D 1F3FF 200D 2640

:*::woman-frowning:-dark-skin-tone::
Send,🙍🏿‍♀
return

;1F64E

:*::person-pouting::
Send,🙎
return

;1F64E 1F3FB

:*::person-pouting:-light-skin-tone::
Send,🙎🏻
return

;1F64E 1F3FC

:*::person-pouting:-medium-light-skin-tone::
Send,🙎🏼
return

;1F64E 1F3FD

:*::person-pouting:-medium-skin-tone::
Send,🙎🏽
return

;1F64E 1F3FE

:*::person-pouting:-medium-dark-skin-tone::
Send,🙎🏾
return

;1F64E 1F3FF

:*::person-pouting:-dark-skin-tone::
Send,🙎🏿
return

;1F64E 200D 2642 FE0F

:*::man-pouting::
Send,🙎‍♂️
return

;1F64E 200D 2642

:*::man-pouting::
Send,🙎‍♂
return

;1F64E 1F3FB 200D 2642 FE0F

:*::man-pouting:-light-skin-tone::
Send,🙎🏻‍♂️
return

;1F64E 1F3FB 200D 2642

:*::man-pouting:-light-skin-tone::
Send,🙎🏻‍♂
return

;1F64E 1F3FC 200D 2642 FE0F

:*::man-pouting:-medium-light-skin-tone::
Send,🙎🏼‍♂️
return

;1F64E 1F3FC 200D 2642

:*::man-pouting:-medium-light-skin-tone::
Send,🙎🏼‍♂
return

;1F64E 1F3FD 200D 2642 FE0F

:*::man-pouting:-medium-skin-tone::
Send,🙎🏽‍♂️
return

;1F64E 1F3FD 200D 2642

:*::man-pouting:-medium-skin-tone::
Send,🙎🏽‍♂
return

;1F64E 1F3FE 200D 2642 FE0F

:*::man-pouting:-medium-dark-skin-tone::
Send,🙎🏾‍♂️
return

;1F64E 1F3FE 200D 2642

:*::man-pouting:-medium-dark-skin-tone::
Send,🙎🏾‍♂
return

;1F64E 1F3FF 200D 2642 FE0F

:*::man-pouting:-dark-skin-tone::
Send,🙎🏿‍♂️
return

;1F64E 1F3FF 200D 2642

:*::man-pouting:-dark-skin-tone::
Send,🙎🏿‍♂
return

;1F64E 200D 2640 FE0F

:*::woman-pouting::
Send,🙎‍♀️
return

;1F64E 200D 2640

:*::woman-pouting::
Send,🙎‍♀
return

;1F64E 1F3FB 200D 2640 FE0F

:*::woman-pouting:-light-skin-tone::
Send,🙎🏻‍♀️
return

;1F64E 1F3FB 200D 2640

:*::woman-pouting:-light-skin-tone::
Send,🙎🏻‍♀
return

;1F64E 1F3FC 200D 2640 FE0F

:*::woman-pouting:-medium-light-skin-tone::
Send,🙎🏼‍♀️
return

;1F64E 1F3FC 200D 2640

:*::woman-pouting:-medium-light-skin-tone::
Send,🙎🏼‍♀
return

;1F64E 1F3FD 200D 2640 FE0F

:*::woman-pouting:-medium-skin-tone::
Send,🙎🏽‍♀️
return

;1F64E 1F3FD 200D 2640

:*::woman-pouting:-medium-skin-tone::
Send,🙎🏽‍♀
return

;1F64E 1F3FE 200D 2640 FE0F

:*::woman-pouting:-medium-dark-skin-tone::
Send,🙎🏾‍♀️
return

;1F64E 1F3FE 200D 2640

:*::woman-pouting:-medium-dark-skin-tone::
Send,🙎🏾‍♀
return

;1F64E 1F3FF 200D 2640 FE0F

:*::woman-pouting:-dark-skin-tone::
Send,🙎🏿‍♀️
return

;1F64E 1F3FF 200D 2640

:*::woman-pouting:-dark-skin-tone::
Send,🙎🏿‍♀
return

;1F645

:*::person-gesturing-no::
Send,🙅
return

;1F645 1F3FB

:*::person-gesturing-no:-light-skin-tone::
Send,🙅🏻
return

;1F645 1F3FC

:*::person-gesturing-no:-medium-light-skin-::
Send,🙅🏼
return

;1F645 1F3FD

:*::person-gesturing-no:-medium-skin-tone::
Send,🙅🏽
return

;1F645 1F3FE

:*::person-gesturing-no:-medium-dark-skin-t::
Send,🙅🏾
return

;1F645 1F3FF

:*::person-gesturing-no:-dark-skin-tone::
Send,🙅🏿
return

;1F645 200D 2642 FE0F

:*::man-gesturing-no::
Send,🙅‍♂️
return

;1F645 200D 2642

:*::man-gesturing-no::
Send,🙅‍♂
return

;1F645 1F3FB 200D 2642 FE0F

:*::man-gesturing-no:-light-skin-tone::
Send,🙅🏻‍♂️
return

;1F645 1F3FB 200D 2642

:*::man-gesturing-no:-light-skin-tone::
Send,🙅🏻‍♂
return

;1F645 1F3FC 200D 2642 FE0F

:*::man-gesturing-no:-medium-light-skin-ton::
Send,🙅🏼‍♂️
return

;1F645 1F3FC 200D 2642

:*::man-gesturing-no:-medium-light-skin-ton::
Send,🙅🏼‍♂
return

;1F645 1F3FD 200D 2642 FE0F

:*::man-gesturing-no:-medium-skin-tone::
Send,🙅🏽‍♂️
return

;1F645 1F3FD 200D 2642

:*::man-gesturing-no:-medium-skin-tone::
Send,🙅🏽‍♂
return

;1F645 1F3FE 200D 2642 FE0F

:*::man-gesturing-no:-medium-dark-skin-tone::
Send,🙅🏾‍♂️
return

;1F645 1F3FE 200D 2642

:*::man-gesturing-no:-medium-dark-skin-tone::
Send,🙅🏾‍♂
return

;1F645 1F3FF 200D 2642 FE0F

:*::man-gesturing-no:-dark-skin-tone::
Send,🙅🏿‍♂️
return

;1F645 1F3FF 200D 2642

:*::man-gesturing-no:-dark-skin-tone::
Send,🙅🏿‍♂
return

;1F645 200D 2640 FE0F

:*::woman-gesturing-no::
Send,🙅‍♀️
return

;1F645 200D 2640

:*::woman-gesturing-no::
Send,🙅‍♀
return

;1F645 1F3FB 200D 2640 FE0F

:*::woman-gesturing-no:-light-skin-tone::
Send,🙅🏻‍♀️
return

;1F645 1F3FB 200D 2640

:*::woman-gesturing-no:-light-skin-tone::
Send,🙅🏻‍♀
return

;1F645 1F3FC 200D 2640 FE0F

:*::woman-gesturing-no:-medium-light-skin-t::
Send,🙅🏼‍♀️
return

;1F645 1F3FC 200D 2640

:*::woman-gesturing-no:-medium-light-skin-t::
Send,🙅🏼‍♀
return

;1F645 1F3FD 200D 2640 FE0F

:*::woman-gesturing-no:-medium-skin-tone::
Send,🙅🏽‍♀️
return

;1F645 1F3FD 200D 2640

:*::woman-gesturing-no:-medium-skin-tone::
Send,🙅🏽‍♀
return

;1F645 1F3FE 200D 2640 FE0F

:*::woman-gesturing-no:-medium-dark-skin-to::
Send,🙅🏾‍♀️
return

;1F645 1F3FE 200D 2640

:*::woman-gesturing-no:-medium-dark-skin-to::
Send,🙅🏾‍♀
return

;1F645 1F3FF 200D 2640 FE0F

:*::woman-gesturing-no:-dark-skin-tone::
Send,🙅🏿‍♀️
return

;1F645 1F3FF 200D 2640

:*::woman-gesturing-no:-dark-skin-tone::
Send,🙅🏿‍♀
return

;1F646

:*::person-gesturing-ok::
Send,🙆
return

;1F646 1F3FB

:*::person-gesturing-ok:-light-skin-tone::
Send,🙆🏻
return

;1F646 1F3FC

:*::person-gesturing-ok:-medium-light-skin-::
Send,🙆🏼
return

;1F646 1F3FD

:*::person-gesturing-ok:-medium-skin-tone::
Send,🙆🏽
return

;1F646 1F3FE

:*::person-gesturing-ok:-medium-dark-skin-t::
Send,🙆🏾
return

;1F646 1F3FF

:*::person-gesturing-ok:-dark-skin-tone::
Send,🙆🏿
return

;1F646 200D 2642 FE0F

:*::man-gesturing-ok::
Send,🙆‍♂️
return

;1F646 200D 2642

:*::man-gesturing-ok::
Send,🙆‍♂
return

;1F646 1F3FB 200D 2642 FE0F

:*::man-gesturing-ok:-light-skin-tone::
Send,🙆🏻‍♂️
return

;1F646 1F3FB 200D 2642

:*::man-gesturing-ok:-light-skin-tone::
Send,🙆🏻‍♂
return

;1F646 1F3FC 200D 2642 FE0F

:*::man-gesturing-ok:-medium-light-skin-ton::
Send,🙆🏼‍♂️
return

;1F646 1F3FC 200D 2642

:*::man-gesturing-ok:-medium-light-skin-ton::
Send,🙆🏼‍♂
return

;1F646 1F3FD 200D 2642 FE0F

:*::man-gesturing-ok:-medium-skin-tone::
Send,🙆🏽‍♂️
return

;1F646 1F3FD 200D 2642

:*::man-gesturing-ok:-medium-skin-tone::
Send,🙆🏽‍♂
return

;1F646 1F3FE 200D 2642 FE0F

:*::man-gesturing-ok:-medium-dark-skin-tone::
Send,🙆🏾‍♂️
return

;1F646 1F3FE 200D 2642

:*::man-gesturing-ok:-medium-dark-skin-tone::
Send,🙆🏾‍♂
return

;1F646 1F3FF 200D 2642 FE0F

:*::man-gesturing-ok:-dark-skin-tone::
Send,🙆🏿‍♂️
return

;1F646 1F3FF 200D 2642

:*::man-gesturing-ok:-dark-skin-tone::
Send,🙆🏿‍♂
return

;1F646 200D 2640 FE0F

:*::woman-gesturing-ok::
Send,🙆‍♀️
return

;1F646 200D 2640

:*::woman-gesturing-ok::
Send,🙆‍♀
return

;1F646 1F3FB 200D 2640 FE0F

:*::woman-gesturing-ok:-light-skin-tone::
Send,🙆🏻‍♀️
return

;1F646 1F3FB 200D 2640

:*::woman-gesturing-ok:-light-skin-tone::
Send,🙆🏻‍♀
return

;1F646 1F3FC 200D 2640 FE0F

:*::woman-gesturing-ok:-medium-light-skin-t::
Send,🙆🏼‍♀️
return

;1F646 1F3FC 200D 2640

:*::woman-gesturing-ok:-medium-light-skin-t::
Send,🙆🏼‍♀
return

;1F646 1F3FD 200D 2640 FE0F

:*::woman-gesturing-ok:-medium-skin-tone::
Send,🙆🏽‍♀️
return

;1F646 1F3FD 200D 2640

:*::woman-gesturing-ok:-medium-skin-tone::
Send,🙆🏽‍♀
return

;1F646 1F3FE 200D 2640 FE0F

:*::woman-gesturing-ok:-medium-dark-skin-to::
Send,🙆🏾‍♀️
return

;1F646 1F3FE 200D 2640

:*::woman-gesturing-ok:-medium-dark-skin-to::
Send,🙆🏾‍♀
return

;1F646 1F3FF 200D 2640 FE0F

:*::woman-gesturing-ok:-dark-skin-tone::
Send,🙆🏿‍♀️
return

;1F646 1F3FF 200D 2640

:*::woman-gesturing-ok:-dark-skin-tone::
Send,🙆🏿‍♀
return

;1F481

:*::person-tipping-hand::
Send,💁
return

;1F481 1F3FB

:*::person-tipping-hand:-light-skin-tone::
Send,💁🏻
return

;1F481 1F3FC

:*::person-tipping-hand:-medium-light-skin-::
Send,💁🏼
return

;1F481 1F3FD

:*::person-tipping-hand:-medium-skin-tone::
Send,💁🏽
return

;1F481 1F3FE

:*::person-tipping-hand:-medium-dark-skin-t::
Send,💁🏾
return

;1F481 1F3FF

:*::person-tipping-hand:-dark-skin-tone::
Send,💁🏿
return

;1F481 200D 2642 FE0F

:*::man-tipping-hand::
Send,💁‍♂️
return

;1F481 200D 2642

:*::man-tipping-hand::
Send,💁‍♂
return

;1F481 1F3FB 200D 2642 FE0F

:*::man-tipping-hand:-light-skin-tone::
Send,💁🏻‍♂️
return

;1F481 1F3FB 200D 2642

:*::man-tipping-hand:-light-skin-tone::
Send,💁🏻‍♂
return

;1F481 1F3FC 200D 2642 FE0F

:*::man-tipping-hand:-medium-light-skin-ton::
Send,💁🏼‍♂️
return

;1F481 1F3FC 200D 2642

:*::man-tipping-hand:-medium-light-skin-ton::
Send,💁🏼‍♂
return

;1F481 1F3FD 200D 2642 FE0F

:*::man-tipping-hand:-medium-skin-tone::
Send,💁🏽‍♂️
return

;1F481 1F3FD 200D 2642

:*::man-tipping-hand:-medium-skin-tone::
Send,💁🏽‍♂
return

;1F481 1F3FE 200D 2642 FE0F

:*::man-tipping-hand:-medium-dark-skin-tone::
Send,💁🏾‍♂️
return

;1F481 1F3FE 200D 2642

:*::man-tipping-hand:-medium-dark-skin-tone::
Send,💁🏾‍♂
return

;1F481 1F3FF 200D 2642 FE0F

:*::man-tipping-hand:-dark-skin-tone::
Send,💁🏿‍♂️
return

;1F481 1F3FF 200D 2642

:*::man-tipping-hand:-dark-skin-tone::
Send,💁🏿‍♂
return

;1F481 200D 2640 FE0F

:*::woman-tipping-hand::
Send,💁‍♀️
return

;1F481 200D 2640

:*::woman-tipping-hand::
Send,💁‍♀
return

;1F481 1F3FB 200D 2640 FE0F

:*::woman-tipping-hand:-light-skin-tone::
Send,💁🏻‍♀️
return

;1F481 1F3FB 200D 2640

:*::woman-tipping-hand:-light-skin-tone::
Send,💁🏻‍♀
return

;1F481 1F3FC 200D 2640 FE0F

:*::woman-tipping-hand:-medium-light-skin-t::
Send,💁🏼‍♀️
return

;1F481 1F3FC 200D 2640

:*::woman-tipping-hand:-medium-light-skin-t::
Send,💁🏼‍♀
return

;1F481 1F3FD 200D 2640 FE0F

:*::woman-tipping-hand:-medium-skin-tone::
Send,💁🏽‍♀️
return

;1F481 1F3FD 200D 2640

:*::woman-tipping-hand:-medium-skin-tone::
Send,💁🏽‍♀
return

;1F481 1F3FE 200D 2640 FE0F

:*::woman-tipping-hand:-medium-dark-skin-to::
Send,💁🏾‍♀️
return

;1F481 1F3FE 200D 2640

:*::woman-tipping-hand:-medium-dark-skin-to::
Send,💁🏾‍♀
return

;1F481 1F3FF 200D 2640 FE0F

:*::woman-tipping-hand:-dark-skin-tone::
Send,💁🏿‍♀️
return

;1F481 1F3FF 200D 2640

:*::woman-tipping-hand:-dark-skin-tone::
Send,💁🏿‍♀
return

;1F64B

:*::person-raising-hand::
Send,🙋
return

;1F64B 1F3FB

:*::person-raising-hand:-light-skin-tone::
Send,🙋🏻
return

;1F64B 1F3FC

:*::person-raising-hand:-medium-light-skin-::
Send,🙋🏼
return

;1F64B 1F3FD

:*::person-raising-hand:-medium-skin-tone::
Send,🙋🏽
return

;1F64B 1F3FE

:*::person-raising-hand:-medium-dark-skin-t::
Send,🙋🏾
return

;1F64B 1F3FF

:*::person-raising-hand:-dark-skin-tone::
Send,🙋🏿
return

;1F64B 200D 2642 FE0F

:*::man-raising-hand::
Send,🙋‍♂️
return

;1F64B 200D 2642

:*::man-raising-hand::
Send,🙋‍♂
return

;1F64B 1F3FB 200D 2642 FE0F

:*::man-raising-hand:-light-skin-tone::
Send,🙋🏻‍♂️
return

;1F64B 1F3FB 200D 2642

:*::man-raising-hand:-light-skin-tone::
Send,🙋🏻‍♂
return

;1F64B 1F3FC 200D 2642 FE0F

:*::man-raising-hand:-medium-light-skin-ton::
Send,🙋🏼‍♂️
return

;1F64B 1F3FC 200D 2642

:*::man-raising-hand:-medium-light-skin-ton::
Send,🙋🏼‍♂
return

;1F64B 1F3FD 200D 2642 FE0F

:*::man-raising-hand:-medium-skin-tone::
Send,🙋🏽‍♂️
return

;1F64B 1F3FD 200D 2642

:*::man-raising-hand:-medium-skin-tone::
Send,🙋🏽‍♂
return

;1F64B 1F3FE 200D 2642 FE0F

:*::man-raising-hand:-medium-dark-skin-tone::
Send,🙋🏾‍♂️
return

;1F64B 1F3FE 200D 2642

:*::man-raising-hand:-medium-dark-skin-tone::
Send,🙋🏾‍♂
return

;1F64B 1F3FF 200D 2642 FE0F

:*::man-raising-hand:-dark-skin-tone::
Send,🙋🏿‍♂️
return

;1F64B 1F3FF 200D 2642

:*::man-raising-hand:-dark-skin-tone::
Send,🙋🏿‍♂
return

;1F64B 200D 2640 FE0F

:*::woman-raising-hand::
Send,🙋‍♀️
return

;1F64B 200D 2640

:*::woman-raising-hand::
Send,🙋‍♀
return

;1F64B 1F3FB 200D 2640 FE0F

:*::woman-raising-hand:-light-skin-tone::
Send,🙋🏻‍♀️
return

;1F64B 1F3FB 200D 2640

:*::woman-raising-hand:-light-skin-tone::
Send,🙋🏻‍♀
return

;1F64B 1F3FC 200D 2640 FE0F

:*::woman-raising-hand:-medium-light-skin-t::
Send,🙋🏼‍♀️
return

;1F64B 1F3FC 200D 2640

:*::woman-raising-hand:-medium-light-skin-t::
Send,🙋🏼‍♀
return

;1F64B 1F3FD 200D 2640 FE0F

:*::woman-raising-hand:-medium-skin-tone::
Send,🙋🏽‍♀️
return

;1F64B 1F3FD 200D 2640

:*::woman-raising-hand:-medium-skin-tone::
Send,🙋🏽‍♀
return

;1F64B 1F3FE 200D 2640 FE0F

:*::woman-raising-hand:-medium-dark-skin-to::
Send,🙋🏾‍♀️
return

;1F64B 1F3FE 200D 2640

:*::woman-raising-hand:-medium-dark-skin-to::
Send,🙋🏾‍♀
return

;1F64B 1F3FF 200D 2640 FE0F

:*::woman-raising-hand:-dark-skin-tone::
Send,🙋🏿‍♀️
return

;1F64B 1F3FF 200D 2640

:*::woman-raising-hand:-dark-skin-tone::
Send,🙋🏿‍♀
return

;1F9CF

:*::deaf-person::
Send,🧏
return

;1F9CF 1F3FB

:*::deaf-person:-light-skin-tone::
Send,🧏🏻
return

;1F9CF 1F3FC

:*::deaf-person:-medium-light-skin-tone::
Send,🧏🏼
return

;1F9CF 1F3FD

:*::deaf-person:-medium-skin-tone::
Send,🧏🏽
return

;1F9CF 1F3FE

:*::deaf-person:-medium-dark-skin-tone::
Send,🧏🏾
return

;1F9CF 1F3FF

:*::deaf-person:-dark-skin-tone::
Send,🧏🏿
return

;1F9CF 200D 2642 FE0F

:*::deaf-man::
Send,🧏‍♂️
return

;1F9CF 200D 2642

:*::deaf-man::
Send,🧏‍♂
return

;1F9CF 1F3FB 200D 2642 FE0F

:*::deaf-man:-light-skin-tone::
Send,🧏🏻‍♂️
return

;1F9CF 1F3FB 200D 2642

:*::deaf-man:-light-skin-tone::
Send,🧏🏻‍♂
return

;1F9CF 1F3FC 200D 2642 FE0F

:*::deaf-man:-medium-light-skin-tone::
Send,🧏🏼‍♂️
return

;1F9CF 1F3FC 200D 2642

:*::deaf-man:-medium-light-skin-tone::
Send,🧏🏼‍♂
return

;1F9CF 1F3FD 200D 2642 FE0F

:*::deaf-man:-medium-skin-tone::
Send,🧏🏽‍♂️
return

;1F9CF 1F3FD 200D 2642

:*::deaf-man:-medium-skin-tone::
Send,🧏🏽‍♂
return

;1F9CF 1F3FE 200D 2642 FE0F

:*::deaf-man:-medium-dark-skin-tone::
Send,🧏🏾‍♂️
return

;1F9CF 1F3FE 200D 2642

:*::deaf-man:-medium-dark-skin-tone::
Send,🧏🏾‍♂
return

;1F9CF 1F3FF 200D 2642 FE0F

:*::deaf-man:-dark-skin-tone::
Send,🧏🏿‍♂️
return

;1F9CF 1F3FF 200D 2642

:*::deaf-man:-dark-skin-tone::
Send,🧏🏿‍♂
return

;1F9CF 200D 2640 FE0F

:*::deaf-woman::
Send,🧏‍♀️
return

;1F9CF 200D 2640

:*::deaf-woman::
Send,🧏‍♀
return

;1F9CF 1F3FB 200D 2640 FE0F

:*::deaf-woman:-light-skin-tone::
Send,🧏🏻‍♀️
return

;1F9CF 1F3FB 200D 2640

:*::deaf-woman:-light-skin-tone::
Send,🧏🏻‍♀
return

;1F9CF 1F3FC 200D 2640 FE0F

:*::deaf-woman:-medium-light-skin-tone::
Send,🧏🏼‍♀️
return

;1F9CF 1F3FC 200D 2640

:*::deaf-woman:-medium-light-skin-tone::
Send,🧏🏼‍♀
return

;1F9CF 1F3FD 200D 2640 FE0F

:*::deaf-woman:-medium-skin-tone::
Send,🧏🏽‍♀️
return

;1F9CF 1F3FD 200D 2640

:*::deaf-woman:-medium-skin-tone::
Send,🧏🏽‍♀
return

;1F9CF 1F3FE 200D 2640 FE0F

:*::deaf-woman:-medium-dark-skin-tone::
Send,🧏🏾‍♀️
return

;1F9CF 1F3FE 200D 2640

:*::deaf-woman:-medium-dark-skin-tone::
Send,🧏🏾‍♀
return

;1F9CF 1F3FF 200D 2640 FE0F

:*::deaf-woman:-dark-skin-tone::
Send,🧏🏿‍♀️
return

;1F9CF 1F3FF 200D 2640

:*::deaf-woman:-dark-skin-tone::
Send,🧏🏿‍♀
return

;1F647

:*::person-bowing::
Send,🙇
return

;1F647 1F3FB

:*::person-bowing:-light-skin-tone::
Send,🙇🏻
return

;1F647 1F3FC

:*::person-bowing:-medium-light-skin-tone::
Send,🙇🏼
return

;1F647 1F3FD

:*::person-bowing:-medium-skin-tone::
Send,🙇🏽
return

;1F647 1F3FE

:*::person-bowing:-medium-dark-skin-tone::
Send,🙇🏾
return

;1F647 1F3FF

:*::person-bowing:-dark-skin-tone::
Send,🙇🏿
return

;1F647 200D 2642 FE0F

:*::man-bowing::
Send,🙇‍♂️
return

;1F647 200D 2642

:*::man-bowing::
Send,🙇‍♂
return

;1F647 1F3FB 200D 2642 FE0F

:*::man-bowing:-light-skin-tone::
Send,🙇🏻‍♂️
return

;1F647 1F3FB 200D 2642

:*::man-bowing:-light-skin-tone::
Send,🙇🏻‍♂
return

;1F647 1F3FC 200D 2642 FE0F

:*::man-bowing:-medium-light-skin-tone::
Send,🙇🏼‍♂️
return

;1F647 1F3FC 200D 2642

:*::man-bowing:-medium-light-skin-tone::
Send,🙇🏼‍♂
return

;1F647 1F3FD 200D 2642 FE0F

:*::man-bowing:-medium-skin-tone::
Send,🙇🏽‍♂️
return

;1F647 1F3FD 200D 2642

:*::man-bowing:-medium-skin-tone::
Send,🙇🏽‍♂
return

;1F647 1F3FE 200D 2642 FE0F

:*::man-bowing:-medium-dark-skin-tone::
Send,🙇🏾‍♂️
return

;1F647 1F3FE 200D 2642

:*::man-bowing:-medium-dark-skin-tone::
Send,🙇🏾‍♂
return

;1F647 1F3FF 200D 2642 FE0F

:*::man-bowing:-dark-skin-tone::
Send,🙇🏿‍♂️
return

;1F647 1F3FF 200D 2642

:*::man-bowing:-dark-skin-tone::
Send,🙇🏿‍♂
return

;1F647 200D 2640 FE0F

:*::woman-bowing::
Send,🙇‍♀️
return

;1F647 200D 2640

:*::woman-bowing::
Send,🙇‍♀
return

;1F647 1F3FB 200D 2640 FE0F

:*::woman-bowing:-light-skin-tone::
Send,🙇🏻‍♀️
return

;1F647 1F3FB 200D 2640

:*::woman-bowing:-light-skin-tone::
Send,🙇🏻‍♀
return

;1F647 1F3FC 200D 2640 FE0F

:*::woman-bowing:-medium-light-skin-tone::
Send,🙇🏼‍♀️
return

;1F647 1F3FC 200D 2640

:*::woman-bowing:-medium-light-skin-tone::
Send,🙇🏼‍♀
return

;1F647 1F3FD 200D 2640 FE0F

:*::woman-bowing:-medium-skin-tone::
Send,🙇🏽‍♀️
return

;1F647 1F3FD 200D 2640

:*::woman-bowing:-medium-skin-tone::
Send,🙇🏽‍♀
return

;1F647 1F3FE 200D 2640 FE0F

:*::woman-bowing:-medium-dark-skin-tone::
Send,🙇🏾‍♀️
return

;1F647 1F3FE 200D 2640

:*::woman-bowing:-medium-dark-skin-tone::
Send,🙇🏾‍♀
return

;1F647 1F3FF 200D 2640 FE0F

:*::woman-bowing:-dark-skin-tone::
Send,🙇🏿‍♀️
return

;1F647 1F3FF 200D 2640

:*::woman-bowing:-dark-skin-tone::
Send,🙇🏿‍♀
return

;1F926

:*::person-facepalming::
Send,🤦
return

;1F926 1F3FB

:*::person-facepalming:-light-skin-tone::
Send,🤦🏻
return

;1F926 1F3FC

:*::person-facepalming:-medium-light-skin-t::
Send,🤦🏼
return

;1F926 1F3FD

:*::person-facepalming:-medium-skin-tone::
Send,🤦🏽
return

;1F926 1F3FE

:*::person-facepalming:-medium-dark-skin-to::
Send,🤦🏾
return

;1F926 1F3FF

:*::person-facepalming:-dark-skin-tone::
Send,🤦🏿
return

;1F926 200D 2642 FE0F

:*::man-facepalming::
Send,🤦‍♂️
return

;1F926 200D 2642

:*::man-facepalming::
Send,🤦‍♂
return

;1F926 1F3FB 200D 2642 FE0F

:*::man-facepalming:-light-skin-tone::
Send,🤦🏻‍♂️
return

;1F926 1F3FB 200D 2642

:*::man-facepalming:-light-skin-tone::
Send,🤦🏻‍♂
return

;1F926 1F3FC 200D 2642 FE0F

:*::man-facepalming:-medium-light-skin-tone::
Send,🤦🏼‍♂️
return

;1F926 1F3FC 200D 2642

:*::man-facepalming:-medium-light-skin-tone::
Send,🤦🏼‍♂
return

;1F926 1F3FD 200D 2642 FE0F

:*::man-facepalming:-medium-skin-tone::
Send,🤦🏽‍♂️
return

;1F926 1F3FD 200D 2642

:*::man-facepalming:-medium-skin-tone::
Send,🤦🏽‍♂
return

;1F926 1F3FE 200D 2642 FE0F

:*::man-facepalming:-medium-dark-skin-tone::
Send,🤦🏾‍♂️
return

;1F926 1F3FE 200D 2642

:*::man-facepalming:-medium-dark-skin-tone::
Send,🤦🏾‍♂
return

;1F926 1F3FF 200D 2642 FE0F

:*::man-facepalming:-dark-skin-tone::
Send,🤦🏿‍♂️
return

;1F926 1F3FF 200D 2642

:*::man-facepalming:-dark-skin-tone::
Send,🤦🏿‍♂
return

;1F926 200D 2640 FE0F

:*::woman-facepalming::
Send,🤦‍♀️
return

;1F926 200D 2640

:*::woman-facepalming::
Send,🤦‍♀
return

;1F926 1F3FB 200D 2640 FE0F

:*::woman-facepalming:-light-skin-tone::
Send,🤦🏻‍♀️
return

;1F926 1F3FB 200D 2640

:*::woman-facepalming:-light-skin-tone::
Send,🤦🏻‍♀
return

;1F926 1F3FC 200D 2640 FE0F

:*::woman-facepalming:-medium-light-skin-to::
Send,🤦🏼‍♀️
return

;1F926 1F3FC 200D 2640

:*::woman-facepalming:-medium-light-skin-to::
Send,🤦🏼‍♀
return

;1F926 1F3FD 200D 2640 FE0F

:*::woman-facepalming:-medium-skin-tone::
Send,🤦🏽‍♀️
return

;1F926 1F3FD 200D 2640

:*::woman-facepalming:-medium-skin-tone::
Send,🤦🏽‍♀
return

;1F926 1F3FE 200D 2640 FE0F

:*::woman-facepalming:-medium-dark-skin-ton::
Send,🤦🏾‍♀️
return

;1F926 1F3FE 200D 2640

:*::woman-facepalming:-medium-dark-skin-ton::
Send,🤦🏾‍♀
return

;1F926 1F3FF 200D 2640 FE0F

:*::woman-facepalming:-dark-skin-tone::
Send,🤦🏿‍♀️
return

;1F926 1F3FF 200D 2640

:*::woman-facepalming:-dark-skin-tone::
Send,🤦🏿‍♀
return

;1F937

:*::person-shrugging::
Send,🤷
return

;1F937 1F3FB

:*::person-shrugging:-light-skin-tone::
Send,🤷🏻
return

;1F937 1F3FC

:*::person-shrugging:-medium-light-skin-ton::
Send,🤷🏼
return

;1F937 1F3FD

:*::person-shrugging:-medium-skin-tone::
Send,🤷🏽
return

;1F937 1F3FE

:*::person-shrugging:-medium-dark-skin-tone::
Send,🤷🏾
return

;1F937 1F3FF

:*::person-shrugging:-dark-skin-tone::
Send,🤷🏿
return

;1F937 200D 2642 FE0F

:*::man-shrugging::
Send,🤷‍♂️
return

;1F937 200D 2642

:*::man-shrugging::
Send,🤷‍♂
return

;1F937 1F3FB 200D 2642 FE0F

:*::man-shrugging:-light-skin-tone::
Send,🤷🏻‍♂️
return

;1F937 1F3FB 200D 2642

:*::man-shrugging:-light-skin-tone::
Send,🤷🏻‍♂
return

;1F937 1F3FC 200D 2642 FE0F

:*::man-shrugging:-medium-light-skin-tone::
Send,🤷🏼‍♂️
return

;1F937 1F3FC 200D 2642

:*::man-shrugging:-medium-light-skin-tone::
Send,🤷🏼‍♂
return

;1F937 1F3FD 200D 2642 FE0F

:*::man-shrugging:-medium-skin-tone::
Send,🤷🏽‍♂️
return

;1F937 1F3FD 200D 2642

:*::man-shrugging:-medium-skin-tone::
Send,🤷🏽‍♂
return

;1F937 1F3FE 200D 2642 FE0F

:*::man-shrugging:-medium-dark-skin-tone::
Send,🤷🏾‍♂️
return

;1F937 1F3FE 200D 2642

:*::man-shrugging:-medium-dark-skin-tone::
Send,🤷🏾‍♂
return

;1F937 1F3FF 200D 2642 FE0F

:*::man-shrugging:-dark-skin-tone::
Send,🤷🏿‍♂️
return

;1F937 1F3FF 200D 2642

:*::man-shrugging:-dark-skin-tone::
Send,🤷🏿‍♂
return

;1F937 200D 2640 FE0F

:*::woman-shrugging::
Send,🤷‍♀️
return

;1F937 200D 2640

:*::woman-shrugging::
Send,🤷‍♀
return

;1F937 1F3FB 200D 2640 FE0F

:*::woman-shrugging:-light-skin-tone::
Send,🤷🏻‍♀️
return

;1F937 1F3FB 200D 2640

:*::woman-shrugging:-light-skin-tone::
Send,🤷🏻‍♀
return

;1F937 1F3FC 200D 2640 FE0F

:*::woman-shrugging:-medium-light-skin-tone::
Send,🤷🏼‍♀️
return

;1F937 1F3FC 200D 2640

:*::woman-shrugging:-medium-light-skin-tone::
Send,🤷🏼‍♀
return

;1F937 1F3FD 200D 2640 FE0F

:*::woman-shrugging:-medium-skin-tone::
Send,🤷🏽‍♀️
return

;1F937 1F3FD 200D 2640

:*::woman-shrugging:-medium-skin-tone::
Send,🤷🏽‍♀
return

;1F937 1F3FE 200D 2640 FE0F

:*::woman-shrugging:-medium-dark-skin-tone::
Send,🤷🏾‍♀️
return

;1F937 1F3FE 200D 2640

:*::woman-shrugging:-medium-dark-skin-tone::
Send,🤷🏾‍♀
return

;1F937 1F3FF 200D 2640 FE0F

:*::woman-shrugging:-dark-skin-tone::
Send,🤷🏿‍♀️
return

;1F937 1F3FF 200D 2640

:*::woman-shrugging:-dark-skin-tone::
Send,🤷🏿‍♀
return

;1F468 200D 2695 FE0F

:*::man-health-worker::
Send,👨‍⚕️
return

;1F468 200D 2695

:*::man-health-worker::
Send,👨‍⚕
return

;1F468 1F3FB 200D 2695 FE0F

:*::man-health-worker:-light-skin-tone::
Send,👨🏻‍⚕️
return

;1F468 1F3FB 200D 2695

:*::man-health-worker:-light-skin-tone::
Send,👨🏻‍⚕
return

;1F468 1F3FC 200D 2695 FE0F

:*::man-health-worker:-medium-light-skin-to::
Send,👨🏼‍⚕️
return

;1F468 1F3FC 200D 2695

:*::man-health-worker:-medium-light-skin-to::
Send,👨🏼‍⚕
return

;1F468 1F3FD 200D 2695 FE0F

:*::man-health-worker:-medium-skin-tone::
Send,👨🏽‍⚕️
return

;1F468 1F3FD 200D 2695

:*::man-health-worker:-medium-skin-tone::
Send,👨🏽‍⚕
return

;1F468 1F3FE 200D 2695 FE0F

:*::man-health-worker:-medium-dark-skin-ton::
Send,👨🏾‍⚕️
return

;1F468 1F3FE 200D 2695

:*::man-health-worker:-medium-dark-skin-ton::
Send,👨🏾‍⚕
return

;1F468 1F3FF 200D 2695 FE0F

:*::man-health-worker:-dark-skin-tone::
Send,👨🏿‍⚕️
return

;1F468 1F3FF 200D 2695

:*::man-health-worker:-dark-skin-tone::
Send,👨🏿‍⚕
return

;1F469 200D 2695 FE0F

:*::woman-health-worker::
Send,👩‍⚕️
return

;1F469 200D 2695

:*::woman-health-worker::
Send,👩‍⚕
return

;1F469 1F3FB 200D 2695 FE0F

:*::woman-health-worker:-light-skin-tone::
Send,👩🏻‍⚕️
return

;1F469 1F3FB 200D 2695

:*::woman-health-worker:-light-skin-tone::
Send,👩🏻‍⚕
return

;1F469 1F3FC 200D 2695 FE0F

:*::woman-health-worker:-medium-light-skin-::
Send,👩🏼‍⚕️
return

;1F469 1F3FC 200D 2695

:*::woman-health-worker:-medium-light-skin-::
Send,👩🏼‍⚕
return

;1F469 1F3FD 200D 2695 FE0F

:*::woman-health-worker:-medium-skin-tone::
Send,👩🏽‍⚕️
return

;1F469 1F3FD 200D 2695

:*::woman-health-worker:-medium-skin-tone::
Send,👩🏽‍⚕
return

;1F469 1F3FE 200D 2695 FE0F

:*::woman-health-worker:-medium-dark-skin-t::
Send,👩🏾‍⚕️
return

;1F469 1F3FE 200D 2695

:*::woman-health-worker:-medium-dark-skin-t::
Send,👩🏾‍⚕
return

;1F469 1F3FF 200D 2695 FE0F

:*::woman-health-worker:-dark-skin-tone::
Send,👩🏿‍⚕️
return

;1F469 1F3FF 200D 2695

:*::woman-health-worker:-dark-skin-tone::
Send,👩🏿‍⚕
return

;1F468 200D 1F393

:*::man-student::
Send,👨‍🎓
return

;1F468 1F3FB 200D 1F393

:*::man-student:-light-skin-tone::
Send,👨🏻‍🎓
return

;1F468 1F3FC 200D 1F393

:*::man-student:-medium-light-skin-tone::
Send,👨🏼‍🎓
return

;1F468 1F3FD 200D 1F393

:*::man-student:-medium-skin-tone::
Send,👨🏽‍🎓
return

;1F468 1F3FE 200D 1F393

:*::man-student:-medium-dark-skin-tone::
Send,👨🏾‍🎓
return

;1F468 1F3FF 200D 1F393

:*::man-student:-dark-skin-tone::
Send,👨🏿‍🎓
return

;1F469 200D 1F393

:*::woman-student::
Send,👩‍🎓
return

;1F469 1F3FB 200D 1F393

:*::woman-student:-light-skin-tone::
Send,👩🏻‍🎓
return

;1F469 1F3FC 200D 1F393

:*::woman-student:-medium-light-skin-tone::
Send,👩🏼‍🎓
return

;1F469 1F3FD 200D 1F393

:*::woman-student:-medium-skin-tone::
Send,👩🏽‍🎓
return

;1F469 1F3FE 200D 1F393

:*::woman-student:-medium-dark-skin-tone::
Send,👩🏾‍🎓
return

;1F469 1F3FF 200D 1F393

:*::woman-student:-dark-skin-tone::
Send,👩🏿‍🎓
return

;1F468 200D 1F3EB

:*::man-teacher::
Send,👨‍🏫
return

;1F468 1F3FB 200D 1F3EB

:*::man-teacher:-light-skin-tone::
Send,👨🏻‍🏫
return

;1F468 1F3FC 200D 1F3EB

:*::man-teacher:-medium-light-skin-tone::
Send,👨🏼‍🏫
return

;1F468 1F3FD 200D 1F3EB

:*::man-teacher:-medium-skin-tone::
Send,👨🏽‍🏫
return

;1F468 1F3FE 200D 1F3EB

:*::man-teacher:-medium-dark-skin-tone::
Send,👨🏾‍🏫
return

;1F468 1F3FF 200D 1F3EB

:*::man-teacher:-dark-skin-tone::
Send,👨🏿‍🏫
return

;1F469 200D 1F3EB

:*::woman-teacher::
Send,👩‍🏫
return

;1F469 1F3FB 200D 1F3EB

:*::woman-teacher:-light-skin-tone::
Send,👩🏻‍🏫
return

;1F469 1F3FC 200D 1F3EB

:*::woman-teacher:-medium-light-skin-tone::
Send,👩🏼‍🏫
return

;1F469 1F3FD 200D 1F3EB

:*::woman-teacher:-medium-skin-tone::
Send,👩🏽‍🏫
return

;1F469 1F3FE 200D 1F3EB

:*::woman-teacher:-medium-dark-skin-tone::
Send,👩🏾‍🏫
return

;1F469 1F3FF 200D 1F3EB

:*::woman-teacher:-dark-skin-tone::
Send,👩🏿‍🏫
return

;1F468 200D 2696 FE0F

:*::man-judge::
Send,👨‍⚖️
return

;1F468 200D 2696

:*::man-judge::
Send,👨‍⚖
return

;1F468 1F3FB 200D 2696 FE0F

:*::man-judge:-light-skin-tone::
Send,👨🏻‍⚖️
return

;1F468 1F3FB 200D 2696

:*::man-judge:-light-skin-tone::
Send,👨🏻‍⚖
return

;1F468 1F3FC 200D 2696 FE0F

:*::man-judge:-medium-light-skin-tone::
Send,👨🏼‍⚖️
return

;1F468 1F3FC 200D 2696

:*::man-judge:-medium-light-skin-tone::
Send,👨🏼‍⚖
return

;1F468 1F3FD 200D 2696 FE0F

:*::man-judge:-medium-skin-tone::
Send,👨🏽‍⚖️
return

;1F468 1F3FD 200D 2696

:*::man-judge:-medium-skin-tone::
Send,👨🏽‍⚖
return

;1F468 1F3FE 200D 2696 FE0F

:*::man-judge:-medium-dark-skin-tone::
Send,👨🏾‍⚖️
return

;1F468 1F3FE 200D 2696

:*::man-judge:-medium-dark-skin-tone::
Send,👨🏾‍⚖
return

;1F468 1F3FF 200D 2696 FE0F

:*::man-judge:-dark-skin-tone::
Send,👨🏿‍⚖️
return

;1F468 1F3FF 200D 2696

:*::man-judge:-dark-skin-tone::
Send,👨🏿‍⚖
return

;1F469 200D 2696 FE0F

:*::woman-judge::
Send,👩‍⚖️
return

;1F469 200D 2696

:*::woman-judge::
Send,👩‍⚖
return

;1F469 1F3FB 200D 2696 FE0F

:*::woman-judge:-light-skin-tone::
Send,👩🏻‍⚖️
return

;1F469 1F3FB 200D 2696

:*::woman-judge:-light-skin-tone::
Send,👩🏻‍⚖
return

;1F469 1F3FC 200D 2696 FE0F

:*::woman-judge:-medium-light-skin-tone::
Send,👩🏼‍⚖️
return

;1F469 1F3FC 200D 2696

:*::woman-judge:-medium-light-skin-tone::
Send,👩🏼‍⚖
return

;1F469 1F3FD 200D 2696 FE0F

:*::woman-judge:-medium-skin-tone::
Send,👩🏽‍⚖️
return

;1F469 1F3FD 200D 2696

:*::woman-judge:-medium-skin-tone::
Send,👩🏽‍⚖
return

;1F469 1F3FE 200D 2696 FE0F

:*::woman-judge:-medium-dark-skin-tone::
Send,👩🏾‍⚖️
return

;1F469 1F3FE 200D 2696

:*::woman-judge:-medium-dark-skin-tone::
Send,👩🏾‍⚖
return

;1F469 1F3FF 200D 2696 FE0F

:*::woman-judge:-dark-skin-tone::
Send,👩🏿‍⚖️
return

;1F469 1F3FF 200D 2696

:*::woman-judge:-dark-skin-tone::
Send,👩🏿‍⚖
return

;1F468 200D 1F33E

:*::man-farmer::
Send,👨‍🌾
return

;1F468 1F3FB 200D 1F33E

:*::man-farmer:-light-skin-tone::
Send,👨🏻‍🌾
return

;1F468 1F3FC 200D 1F33E

:*::man-farmer:-medium-light-skin-tone::
Send,👨🏼‍🌾
return

;1F468 1F3FD 200D 1F33E

:*::man-farmer:-medium-skin-tone::
Send,👨🏽‍🌾
return

;1F468 1F3FE 200D 1F33E

:*::man-farmer:-medium-dark-skin-tone::
Send,👨🏾‍🌾
return

;1F468 1F3FF 200D 1F33E

:*::man-farmer:-dark-skin-tone::
Send,👨🏿‍🌾
return

;1F469 200D 1F33E

:*::woman-farmer::
Send,👩‍🌾
return

;1F469 1F3FB 200D 1F33E

:*::woman-farmer:-light-skin-tone::
Send,👩🏻‍🌾
return

;1F469 1F3FC 200D 1F33E

:*::woman-farmer:-medium-light-skin-tone::
Send,👩🏼‍🌾
return

;1F469 1F3FD 200D 1F33E

:*::woman-farmer:-medium-skin-tone::
Send,👩🏽‍🌾
return

;1F469 1F3FE 200D 1F33E

:*::woman-farmer:-medium-dark-skin-tone::
Send,👩🏾‍🌾
return

;1F469 1F3FF 200D 1F33E

:*::woman-farmer:-dark-skin-tone::
Send,👩🏿‍🌾
return

;1F468 200D 1F373

:*::man-cook::
Send,👨‍🍳
return

;1F468 1F3FB 200D 1F373

:*::man-cook:-light-skin-tone::
Send,👨🏻‍🍳
return

;1F468 1F3FC 200D 1F373

:*::man-cook:-medium-light-skin-tone::
Send,👨🏼‍🍳
return

;1F468 1F3FD 200D 1F373

:*::man-cook:-medium-skin-tone::
Send,👨🏽‍🍳
return

;1F468 1F3FE 200D 1F373

:*::man-cook:-medium-dark-skin-tone::
Send,👨🏾‍🍳
return

;1F468 1F3FF 200D 1F373

:*::man-cook:-dark-skin-tone::
Send,👨🏿‍🍳
return

;1F469 200D 1F373

:*::woman-cook::
Send,👩‍🍳
return

;1F469 1F3FB 200D 1F373

:*::woman-cook:-light-skin-tone::
Send,👩🏻‍🍳
return

;1F469 1F3FC 200D 1F373

:*::woman-cook:-medium-light-skin-tone::
Send,👩🏼‍🍳
return

;1F469 1F3FD 200D 1F373

:*::woman-cook:-medium-skin-tone::
Send,👩🏽‍🍳
return

;1F469 1F3FE 200D 1F373

:*::woman-cook:-medium-dark-skin-tone::
Send,👩🏾‍🍳
return

;1F469 1F3FF 200D 1F373

:*::woman-cook:-dark-skin-tone::
Send,👩🏿‍🍳
return

;1F468 200D 1F527

:*::man-mechanic::
Send,👨‍🔧
return

;1F468 1F3FB 200D 1F527

:*::man-mechanic:-light-skin-tone::
Send,👨🏻‍🔧
return

;1F468 1F3FC 200D 1F527

:*::man-mechanic:-medium-light-skin-tone::
Send,👨🏼‍🔧
return

;1F468 1F3FD 200D 1F527

:*::man-mechanic:-medium-skin-tone::
Send,👨🏽‍🔧
return

;1F468 1F3FE 200D 1F527

:*::man-mechanic:-medium-dark-skin-tone::
Send,👨🏾‍🔧
return

;1F468 1F3FF 200D 1F527

:*::man-mechanic:-dark-skin-tone::
Send,👨🏿‍🔧
return

;1F469 200D 1F527

:*::woman-mechanic::
Send,👩‍🔧
return

;1F469 1F3FB 200D 1F527

:*::woman-mechanic:-light-skin-tone::
Send,👩🏻‍🔧
return

;1F469 1F3FC 200D 1F527

:*::woman-mechanic:-medium-light-skin-tone::
Send,👩🏼‍🔧
return

;1F469 1F3FD 200D 1F527

:*::woman-mechanic:-medium-skin-tone::
Send,👩🏽‍🔧
return

;1F469 1F3FE 200D 1F527

:*::woman-mechanic:-medium-dark-skin-tone::
Send,👩🏾‍🔧
return

;1F469 1F3FF 200D 1F527

:*::woman-mechanic:-dark-skin-tone::
Send,👩🏿‍🔧
return

;1F468 200D 1F3ED

:*::man-factory-worker::
Send,👨‍🏭
return

;1F468 1F3FB 200D 1F3ED

:*::man-factory-worker:-light-skin-tone::
Send,👨🏻‍🏭
return

;1F468 1F3FC 200D 1F3ED

:*::man-factory-worker:-medium-light-skin-t::
Send,👨🏼‍🏭
return

;1F468 1F3FD 200D 1F3ED

:*::man-factory-worker:-medium-skin-tone::
Send,👨🏽‍🏭
return

;1F468 1F3FE 200D 1F3ED

:*::man-factory-worker:-medium-dark-skin-to::
Send,👨🏾‍🏭
return

;1F468 1F3FF 200D 1F3ED

:*::man-factory-worker:-dark-skin-tone::
Send,👨🏿‍🏭
return

;1F469 200D 1F3ED

:*::woman-factory-worker::
Send,👩‍🏭
return

;1F469 1F3FB 200D 1F3ED

:*::woman-factory-worker:-light-skin-tone::
Send,👩🏻‍🏭
return

;1F469 1F3FC 200D 1F3ED

:*::woman-factory-worker:-medium-light-skin::
Send,👩🏼‍🏭
return

;1F469 1F3FD 200D 1F3ED

:*::woman-factory-worker:-medium-skin-tone::
Send,👩🏽‍🏭
return

;1F469 1F3FE 200D 1F3ED

:*::woman-factory-worker:-medium-dark-skin-::
Send,👩🏾‍🏭
return

;1F469 1F3FF 200D 1F3ED

:*::woman-factory-worker:-dark-skin-tone::
Send,👩🏿‍🏭
return

;1F468 200D 1F4BC

:*::man-office-worker::
Send,👨‍💼
return

;1F468 1F3FB 200D 1F4BC

:*::man-office-worker:-light-skin-tone::
Send,👨🏻‍💼
return

;1F468 1F3FC 200D 1F4BC

:*::man-office-worker:-medium-light-skin-to::
Send,👨🏼‍💼
return

;1F468 1F3FD 200D 1F4BC

:*::man-office-worker:-medium-skin-tone::
Send,👨🏽‍💼
return

;1F468 1F3FE 200D 1F4BC

:*::man-office-worker:-medium-dark-skin-ton::
Send,👨🏾‍💼
return

;1F468 1F3FF 200D 1F4BC

:*::man-office-worker:-dark-skin-tone::
Send,👨🏿‍💼
return

;1F469 200D 1F4BC

:*::woman-office-worker::
Send,👩‍💼
return

;1F469 1F3FB 200D 1F4BC

:*::woman-office-worker:-light-skin-tone::
Send,👩🏻‍💼
return

;1F469 1F3FC 200D 1F4BC

:*::woman-office-worker:-medium-light-skin-::
Send,👩🏼‍💼
return

;1F469 1F3FD 200D 1F4BC

:*::woman-office-worker:-medium-skin-tone::
Send,👩🏽‍💼
return

;1F469 1F3FE 200D 1F4BC

:*::woman-office-worker:-medium-dark-skin-t::
Send,👩🏾‍💼
return

;1F469 1F3FF 200D 1F4BC

:*::woman-office-worker:-dark-skin-tone::
Send,👩🏿‍💼
return

;1F468 200D 1F52C

:*::man-scientist::
Send,👨‍🔬
return

;1F468 1F3FB 200D 1F52C

:*::man-scientist:-light-skin-tone::
Send,👨🏻‍🔬
return

;1F468 1F3FC 200D 1F52C

:*::man-scientist:-medium-light-skin-tone::
Send,👨🏼‍🔬
return

;1F468 1F3FD 200D 1F52C

:*::man-scientist:-medium-skin-tone::
Send,👨🏽‍🔬
return

;1F468 1F3FE 200D 1F52C

:*::man-scientist:-medium-dark-skin-tone::
Send,👨🏾‍🔬
return

;1F468 1F3FF 200D 1F52C

:*::man-scientist:-dark-skin-tone::
Send,👨🏿‍🔬
return

;1F469 200D 1F52C

:*::woman-scientist::
Send,👩‍🔬
return

;1F469 1F3FB 200D 1F52C

:*::woman-scientist:-light-skin-tone::
Send,👩🏻‍🔬
return

;1F469 1F3FC 200D 1F52C

:*::woman-scientist:-medium-light-skin-tone::
Send,👩🏼‍🔬
return

;1F469 1F3FD 200D 1F52C

:*::woman-scientist:-medium-skin-tone::
Send,👩🏽‍🔬
return

;1F469 1F3FE 200D 1F52C

:*::woman-scientist:-medium-dark-skin-tone::
Send,👩🏾‍🔬
return

;1F469 1F3FF 200D 1F52C

:*::woman-scientist:-dark-skin-tone::
Send,👩🏿‍🔬
return

;1F468 200D 1F4BB

:*::man-technologist::
Send,👨‍💻
return

;1F468 1F3FB 200D 1F4BB

:*::man-technologist:-light-skin-tone::
Send,👨🏻‍💻
return

;1F468 1F3FC 200D 1F4BB

:*::man-technologist:-medium-light-skin-ton::
Send,👨🏼‍💻
return

;1F468 1F3FD 200D 1F4BB

:*::man-technologist:-medium-skin-tone::
Send,👨🏽‍💻
return

;1F468 1F3FE 200D 1F4BB

:*::man-technologist:-medium-dark-skin-tone::
Send,👨🏾‍💻
return

;1F468 1F3FF 200D 1F4BB

:*::man-technologist:-dark-skin-tone::
Send,👨🏿‍💻
return

;1F469 200D 1F4BB

:*::woman-technologist::
Send,👩‍💻
return

;1F469 1F3FB 200D 1F4BB

:*::woman-technologist:-light-skin-tone::
Send,👩🏻‍💻
return

;1F469 1F3FC 200D 1F4BB

:*::woman-technologist:-medium-light-skin-t::
Send,👩🏼‍💻
return

;1F469 1F3FD 200D 1F4BB

:*::woman-technologist:-medium-skin-tone::
Send,👩🏽‍💻
return

;1F469 1F3FE 200D 1F4BB

:*::woman-technologist:-medium-dark-skin-to::
Send,👩🏾‍💻
return

;1F469 1F3FF 200D 1F4BB

:*::woman-technologist:-dark-skin-tone::
Send,👩🏿‍💻
return

;1F468 200D 1F3A4

:*::man-singer::
Send,👨‍🎤
return

;1F468 1F3FB 200D 1F3A4

:*::man-singer:-light-skin-tone::
Send,👨🏻‍🎤
return

;1F468 1F3FC 200D 1F3A4

:*::man-singer:-medium-light-skin-tone::
Send,👨🏼‍🎤
return

;1F468 1F3FD 200D 1F3A4

:*::man-singer:-medium-skin-tone::
Send,👨🏽‍🎤
return

;1F468 1F3FE 200D 1F3A4

:*::man-singer:-medium-dark-skin-tone::
Send,👨🏾‍🎤
return

;1F468 1F3FF 200D 1F3A4

:*::man-singer:-dark-skin-tone::
Send,👨🏿‍🎤
return

;1F469 200D 1F3A4

:*::woman-singer::
Send,👩‍🎤
return

;1F469 1F3FB 200D 1F3A4

:*::woman-singer:-light-skin-tone::
Send,👩🏻‍🎤
return

;1F469 1F3FC 200D 1F3A4

:*::woman-singer:-medium-light-skin-tone::
Send,👩🏼‍🎤
return

;1F469 1F3FD 200D 1F3A4

:*::woman-singer:-medium-skin-tone::
Send,👩🏽‍🎤
return

;1F469 1F3FE 200D 1F3A4

:*::woman-singer:-medium-dark-skin-tone::
Send,👩🏾‍🎤
return

;1F469 1F3FF 200D 1F3A4

:*::woman-singer:-dark-skin-tone::
Send,👩🏿‍🎤
return

;1F468 200D 1F3A8

:*::man-artist::
Send,👨‍🎨
return

;1F468 1F3FB 200D 1F3A8

:*::man-artist:-light-skin-tone::
Send,👨🏻‍🎨
return

;1F468 1F3FC 200D 1F3A8

:*::man-artist:-medium-light-skin-tone::
Send,👨🏼‍🎨
return

;1F468 1F3FD 200D 1F3A8

:*::man-artist:-medium-skin-tone::
Send,👨🏽‍🎨
return

;1F468 1F3FE 200D 1F3A8

:*::man-artist:-medium-dark-skin-tone::
Send,👨🏾‍🎨
return

;1F468 1F3FF 200D 1F3A8

:*::man-artist:-dark-skin-tone::
Send,👨🏿‍🎨
return

;1F469 200D 1F3A8

:*::woman-artist::
Send,👩‍🎨
return

;1F469 1F3FB 200D 1F3A8

:*::woman-artist:-light-skin-tone::
Send,👩🏻‍🎨
return

;1F469 1F3FC 200D 1F3A8

:*::woman-artist:-medium-light-skin-tone::
Send,👩🏼‍🎨
return

;1F469 1F3FD 200D 1F3A8

:*::woman-artist:-medium-skin-tone::
Send,👩🏽‍🎨
return

;1F469 1F3FE 200D 1F3A8

:*::woman-artist:-medium-dark-skin-tone::
Send,👩🏾‍🎨
return

;1F469 1F3FF 200D 1F3A8

:*::woman-artist:-dark-skin-tone::
Send,👩🏿‍🎨
return

;1F468 200D 2708 FE0F

:*::man-pilot::
Send,👨‍✈️
return

;1F468 200D 2708

:*::man-pilot::
Send,👨‍✈
return

;1F468 1F3FB 200D 2708 FE0F

:*::man-pilot:-light-skin-tone::
Send,👨🏻‍✈️
return

;1F468 1F3FB 200D 2708

:*::man-pilot:-light-skin-tone::
Send,👨🏻‍✈
return

;1F468 1F3FC 200D 2708 FE0F

:*::man-pilot:-medium-light-skin-tone::
Send,👨🏼‍✈️
return

;1F468 1F3FC 200D 2708

:*::man-pilot:-medium-light-skin-tone::
Send,👨🏼‍✈
return

;1F468 1F3FD 200D 2708 FE0F

:*::man-pilot:-medium-skin-tone::
Send,👨🏽‍✈️
return

;1F468 1F3FD 200D 2708

:*::man-pilot:-medium-skin-tone::
Send,👨🏽‍✈
return

;1F468 1F3FE 200D 2708 FE0F

:*::man-pilot:-medium-dark-skin-tone::
Send,👨🏾‍✈️
return

;1F468 1F3FE 200D 2708

:*::man-pilot:-medium-dark-skin-tone::
Send,👨🏾‍✈
return

;1F468 1F3FF 200D 2708 FE0F

:*::man-pilot:-dark-skin-tone::
Send,👨🏿‍✈️
return

;1F468 1F3FF 200D 2708

:*::man-pilot:-dark-skin-tone::
Send,👨🏿‍✈
return

;1F469 200D 2708 FE0F

:*::woman-pilot::
Send,👩‍✈️
return

;1F469 200D 2708

:*::woman-pilot::
Send,👩‍✈
return

;1F469 1F3FB 200D 2708 FE0F

:*::woman-pilot:-light-skin-tone::
Send,👩🏻‍✈️
return

;1F469 1F3FB 200D 2708

:*::woman-pilot:-light-skin-tone::
Send,👩🏻‍✈
return

;1F469 1F3FC 200D 2708 FE0F

:*::woman-pilot:-medium-light-skin-tone::
Send,👩🏼‍✈️
return

;1F469 1F3FC 200D 2708

:*::woman-pilot:-medium-light-skin-tone::
Send,👩🏼‍✈
return

;1F469 1F3FD 200D 2708 FE0F

:*::woman-pilot:-medium-skin-tone::
Send,👩🏽‍✈️
return

;1F469 1F3FD 200D 2708

:*::woman-pilot:-medium-skin-tone::
Send,👩🏽‍✈
return

;1F469 1F3FE 200D 2708 FE0F

:*::woman-pilot:-medium-dark-skin-tone::
Send,👩🏾‍✈️
return

;1F469 1F3FE 200D 2708

:*::woman-pilot:-medium-dark-skin-tone::
Send,👩🏾‍✈
return

;1F469 1F3FF 200D 2708 FE0F

:*::woman-pilot:-dark-skin-tone::
Send,👩🏿‍✈️
return

;1F469 1F3FF 200D 2708

:*::woman-pilot:-dark-skin-tone::
Send,👩🏿‍✈
return

;1F468 200D 1F680

:*::man-astronaut::
Send,👨‍🚀
return

;1F468 1F3FB 200D 1F680

:*::man-astronaut:-light-skin-tone::
Send,👨🏻‍🚀
return

;1F468 1F3FC 200D 1F680

:*::man-astronaut:-medium-light-skin-tone::
Send,👨🏼‍🚀
return

;1F468 1F3FD 200D 1F680

:*::man-astronaut:-medium-skin-tone::
Send,👨🏽‍🚀
return

;1F468 1F3FE 200D 1F680

:*::man-astronaut:-medium-dark-skin-tone::
Send,👨🏾‍🚀
return

;1F468 1F3FF 200D 1F680

:*::man-astronaut:-dark-skin-tone::
Send,👨🏿‍🚀
return

;1F469 200D 1F680

:*::woman-astronaut::
Send,👩‍🚀
return

;1F469 1F3FB 200D 1F680

:*::woman-astronaut:-light-skin-tone::
Send,👩🏻‍🚀
return

;1F469 1F3FC 200D 1F680

:*::woman-astronaut:-medium-light-skin-tone::
Send,👩🏼‍🚀
return

;1F469 1F3FD 200D 1F680

:*::woman-astronaut:-medium-skin-tone::
Send,👩🏽‍🚀
return

;1F469 1F3FE 200D 1F680

:*::woman-astronaut:-medium-dark-skin-tone::
Send,👩🏾‍🚀
return

;1F469 1F3FF 200D 1F680

:*::woman-astronaut:-dark-skin-tone::
Send,👩🏿‍🚀
return

;1F468 200D 1F692

:*::man-firefighter::
Send,👨‍🚒
return

;1F468 1F3FB 200D 1F692

:*::man-firefighter:-light-skin-tone::
Send,👨🏻‍🚒
return

;1F468 1F3FC 200D 1F692

:*::man-firefighter:-medium-light-skin-tone::
Send,👨🏼‍🚒
return

;1F468 1F3FD 200D 1F692

:*::man-firefighter:-medium-skin-tone::
Send,👨🏽‍🚒
return

;1F468 1F3FE 200D 1F692

:*::man-firefighter:-medium-dark-skin-tone::
Send,👨🏾‍🚒
return

;1F468 1F3FF 200D 1F692

:*::man-firefighter:-dark-skin-tone::
Send,👨🏿‍🚒
return

;1F469 200D 1F692

:*::woman-firefighter::
Send,👩‍🚒
return

;1F469 1F3FB 200D 1F692

:*::woman-firefighter:-light-skin-tone::
Send,👩🏻‍🚒
return

;1F469 1F3FC 200D 1F692

:*::woman-firefighter:-medium-light-skin-to::
Send,👩🏼‍🚒
return

;1F469 1F3FD 200D 1F692

:*::woman-firefighter:-medium-skin-tone::
Send,👩🏽‍🚒
return

;1F469 1F3FE 200D 1F692

:*::woman-firefighter:-medium-dark-skin-ton::
Send,👩🏾‍🚒
return

;1F469 1F3FF 200D 1F692

:*::woman-firefighter:-dark-skin-tone::
Send,👩🏿‍🚒
return

;1F46E
:*::cop::
:*::police::
:*::policeman::
:*::policewoman::
:*::police-officer::
Send,👮
return

;1F46E 1F3FB

:*::police-officer:-light-skin-tone::
Send,👮🏻
return

;1F46E 1F3FC

:*::police-officer:-medium-light-skin-tone::
Send,👮🏼
return

;1F46E 1F3FD

:*::police-officer:-medium-skin-tone::
Send,👮🏽
return

;1F46E 1F3FE

:*::police-officer:-medium-dark-skin-tone::
Send,👮🏾
return

;1F46E 1F3FF

:*::police-officer:-dark-skin-tone::
Send,👮🏿
return

;1F46E 200D 2642 FE0F

:*::man-police-officer::
Send,👮‍♂️
return

;1F46E 200D 2642

:*::man-police-officer::
Send,👮‍♂
return

;1F46E 1F3FB 200D 2642 FE0F

:*::man-police-officer:-light-skin-tone::
Send,👮🏻‍♂️
return

;1F46E 1F3FB 200D 2642

:*::man-police-officer:-light-skin-tone::
Send,👮🏻‍♂
return

;1F46E 1F3FC 200D 2642 FE0F

:*::man-police-officer:-medium-light-skin-t::
Send,👮🏼‍♂️
return

;1F46E 1F3FC 200D 2642

:*::man-police-officer:-medium-light-skin-t::
Send,👮🏼‍♂
return

;1F46E 1F3FD 200D 2642 FE0F

:*::man-police-officer:-medium-skin-tone::
Send,👮🏽‍♂️
return

;1F46E 1F3FD 200D 2642

:*::man-police-officer:-medium-skin-tone::
Send,👮🏽‍♂
return

;1F46E 1F3FE 200D 2642 FE0F

:*::man-police-officer:-medium-dark-skin-to::
Send,👮🏾‍♂️
return

;1F46E 1F3FE 200D 2642

:*::man-police-officer:-medium-dark-skin-to::
Send,👮🏾‍♂
return

;1F46E 1F3FF 200D 2642 FE0F

:*::man-police-officer:-dark-skin-tone::
Send,👮🏿‍♂️
return

;1F46E 1F3FF 200D 2642

:*::man-police-officer:-dark-skin-tone::
Send,👮🏿‍♂
return

;1F46E 200D 2640 FE0F

:*::woman-police-officer::
Send,👮‍♀️
return

;1F46E 200D 2640

:*::woman-police-officer::
Send,👮‍♀
return

;1F46E 1F3FB 200D 2640 FE0F

:*::woman-police-officer:-light-skin-tone::
Send,👮🏻‍♀️
return

;1F46E 1F3FB 200D 2640

:*::woman-police-officer:-light-skin-tone::
Send,👮🏻‍♀
return

;1F46E 1F3FC 200D 2640 FE0F

:*::woman-police-officer:-medium-light-skin::
Send,👮🏼‍♀️
return

;1F46E 1F3FC 200D 2640

:*::woman-police-officer:-medium-light-skin::
Send,👮🏼‍♀
return

;1F46E 1F3FD 200D 2640 FE0F

:*::woman-police-officer:-medium-skin-tone::
Send,👮🏽‍♀️
return

;1F46E 1F3FD 200D 2640

:*::woman-police-officer:-medium-skin-tone::
Send,👮🏽‍♀
return

;1F46E 1F3FE 200D 2640 FE0F

:*::woman-police-officer:-medium-dark-skin-::
Send,👮🏾‍♀️
return

;1F46E 1F3FE 200D 2640

:*::woman-police-officer:-medium-dark-skin-::
Send,👮🏾‍♀
return

;1F46E 1F3FF 200D 2640 FE0F

:*::woman-police-officer:-dark-skin-tone::
Send,👮🏿‍♀️
return

;1F46E 1F3FF 200D 2640

:*::woman-police-officer:-dark-skin-tone::
Send,👮🏿‍♀
return

;1F575 FE0F

:*::detective::
Send,🕵️
return

;1F575

:*::detective::
Send,🕵
return

;1F575 1F3FB

:*::detective:-light-skin-tone::
Send,🕵🏻
return

;1F575 1F3FC

:*::detective:-medium-light-skin-tone::
Send,🕵🏼
return

;1F575 1F3FD

:*::detective:-medium-skin-tone::
Send,🕵🏽
return

;1F575 1F3FE

:*::detective:-medium-dark-skin-tone::
Send,🕵🏾
return

;1F575 1F3FF

:*::detective:-dark-skin-tone::
Send,🕵🏿
return

;1F575 FE0F 200D 2642 FE0F

:*::man-detective::
Send,🕵️‍♂️
return

;1F575 200D 2642 FE0F

:*::man-detective::
Send,🕵‍♂️
return

;1F575 FE0F 200D 2642

:*::man-detective::
Send,🕵️‍♂
return

;1F575 200D 2642

:*::man-detective::
Send,🕵‍♂
return

;1F575 1F3FB 200D 2642 FE0F

:*::man-detective:-light-skin-tone::
Send,🕵🏻‍♂️
return

;1F575 1F3FB 200D 2642

:*::man-detective:-light-skin-tone::
Send,🕵🏻‍♂
return

;1F575 1F3FC 200D 2642 FE0F

:*::man-detective:-medium-light-skin-tone::
Send,🕵🏼‍♂️
return

;1F575 1F3FC 200D 2642

:*::man-detective:-medium-light-skin-tone::
Send,🕵🏼‍♂
return

;1F575 1F3FD 200D 2642 FE0F

:*::man-detective:-medium-skin-tone::
Send,🕵🏽‍♂️
return

;1F575 1F3FD 200D 2642

:*::man-detective:-medium-skin-tone::
Send,🕵🏽‍♂
return

;1F575 1F3FE 200D 2642 FE0F

:*::man-detective:-medium-dark-skin-tone::
Send,🕵🏾‍♂️
return

;1F575 1F3FE 200D 2642

:*::man-detective:-medium-dark-skin-tone::
Send,🕵🏾‍♂
return

;1F575 1F3FF 200D 2642 FE0F

:*::man-detective:-dark-skin-tone::
Send,🕵🏿‍♂️
return

;1F575 1F3FF 200D 2642

:*::man-detective:-dark-skin-tone::
Send,🕵🏿‍♂
return

;1F575 FE0F 200D 2640 FE0F

:*::woman-detective::
Send,🕵️‍♀️
return

;1F575 200D 2640 FE0F

:*::woman-detective::
Send,🕵‍♀️
return

;1F575 FE0F 200D 2640

:*::woman-detective::
Send,🕵️‍♀
return

;1F575 200D 2640

:*::woman-detective::
Send,🕵‍♀
return

;1F575 1F3FB 200D 2640 FE0F

:*::woman-detective:-light-skin-tone::
Send,🕵🏻‍♀️
return

;1F575 1F3FB 200D 2640

:*::woman-detective:-light-skin-tone::
Send,🕵🏻‍♀
return

;1F575 1F3FC 200D 2640 FE0F

:*::woman-detective:-medium-light-skin-tone::
Send,🕵🏼‍♀️
return

;1F575 1F3FC 200D 2640

:*::woman-detective:-medium-light-skin-tone::
Send,🕵🏼‍♀
return

;1F575 1F3FD 200D 2640 FE0F

:*::woman-detective:-medium-skin-tone::
Send,🕵🏽‍♀️
return

;1F575 1F3FD 200D 2640

:*::woman-detective:-medium-skin-tone::
Send,🕵🏽‍♀
return

;1F575 1F3FE 200D 2640 FE0F

:*::woman-detective:-medium-dark-skin-tone::
Send,🕵🏾‍♀️
return

;1F575 1F3FE 200D 2640

:*::woman-detective:-medium-dark-skin-tone::
Send,🕵🏾‍♀
return

;1F575 1F3FF 200D 2640 FE0F

:*::woman-detective:-dark-skin-tone::
Send,🕵🏿‍♀️
return

;1F575 1F3FF 200D 2640

:*::woman-detective:-dark-skin-tone::
Send,🕵🏿‍♀
return

;1F482

:*::guard::
Send,💂
return

;1F482 1F3FB

:*::guard:-light-skin-tone::
Send,💂🏻
return

;1F482 1F3FC

:*::guard:-medium-light-skin-tone::
Send,💂🏼
return

;1F482 1F3FD

:*::guard:-medium-skin-tone::
Send,💂🏽
return

;1F482 1F3FE

:*::guard:-medium-dark-skin-tone::
Send,💂🏾
return

;1F482 1F3FF

:*::guard:-dark-skin-tone::
Send,💂🏿
return

;1F482 200D 2642 FE0F

:*::man-guard::
Send,💂‍♂️
return

;1F482 200D 2642

:*::man-guard::
Send,💂‍♂
return

;1F482 1F3FB 200D 2642 FE0F

:*::man-guard:-light-skin-tone::
Send,💂🏻‍♂️
return

;1F482 1F3FB 200D 2642

:*::man-guard:-light-skin-tone::
Send,💂🏻‍♂
return

;1F482 1F3FC 200D 2642 FE0F

:*::man-guard:-medium-light-skin-tone::
Send,💂🏼‍♂️
return

;1F482 1F3FC 200D 2642

:*::man-guard:-medium-light-skin-tone::
Send,💂🏼‍♂
return

;1F482 1F3FD 200D 2642 FE0F

:*::man-guard:-medium-skin-tone::
Send,💂🏽‍♂️
return

;1F482 1F3FD 200D 2642

:*::man-guard:-medium-skin-tone::
Send,💂🏽‍♂
return

;1F482 1F3FE 200D 2642 FE0F

:*::man-guard:-medium-dark-skin-tone::
Send,💂🏾‍♂️
return

;1F482 1F3FE 200D 2642

:*::man-guard:-medium-dark-skin-tone::
Send,💂🏾‍♂
return

;1F482 1F3FF 200D 2642 FE0F

:*::man-guard:-dark-skin-tone::
Send,💂🏿‍♂️
return

;1F482 1F3FF 200D 2642

:*::man-guard:-dark-skin-tone::
Send,💂🏿‍♂
return

;1F482 200D 2640 FE0F

:*::woman-guard::
Send,💂‍♀️
return

;1F482 200D 2640

:*::woman-guard::
Send,💂‍♀
return

;1F482 1F3FB 200D 2640 FE0F

:*::woman-guard:-light-skin-tone::
Send,💂🏻‍♀️
return

;1F482 1F3FB 200D 2640

:*::woman-guard:-light-skin-tone::
Send,💂🏻‍♀
return

;1F482 1F3FC 200D 2640 FE0F

:*::woman-guard:-medium-light-skin-tone::
Send,💂🏼‍♀️
return

;1F482 1F3FC 200D 2640

:*::woman-guard:-medium-light-skin-tone::
Send,💂🏼‍♀
return

;1F482 1F3FD 200D 2640 FE0F

:*::woman-guard:-medium-skin-tone::
Send,💂🏽‍♀️
return

;1F482 1F3FD 200D 2640

:*::woman-guard:-medium-skin-tone::
Send,💂🏽‍♀
return

;1F482 1F3FE 200D 2640 FE0F

:*::woman-guard:-medium-dark-skin-tone::
Send,💂🏾‍♀️
return

;1F482 1F3FE 200D 2640

:*::woman-guard:-medium-dark-skin-tone::
Send,💂🏾‍♀
return

;1F482 1F3FF 200D 2640 FE0F

:*::woman-guard:-dark-skin-tone::
Send,💂🏿‍♀️
return

;1F482 1F3FF 200D 2640

:*::woman-guard:-dark-skin-tone::
Send,💂🏿‍♀
return

;1F477
:*::builder::
:*::face-with-hat::
:*::hard-hat::
:*::safety-helmet::
:*::construction-worker::
Send,👷
return

;1F477 1F3FB

:*::construction-worker:-light-skin-tone::
Send,👷🏻
return

;1F477 1F3FC

:*::construction-worker:-medium-light-skin-::
Send,👷🏼
return

;1F477 1F3FD

:*::construction-worker:-medium-skin-tone::
Send,👷🏽
return

;1F477 1F3FE

:*::construction-worker:-medium-dark-skin-t::
Send,👷🏾
return

;1F477 1F3FF

:*::construction-worker:-dark-skin-tone::
Send,👷🏿
return

;1F477 200D 2642 FE0F

:*::man-construction-worker::
Send,👷‍♂️
return

;1F477 200D 2642

:*::man-construction-worker::
Send,👷‍♂
return

;1F477 1F3FB 200D 2642 FE0F

:*::man-construction-worker:-light-skin-ton::
Send,👷🏻‍♂️
return

;1F477 1F3FB 200D 2642

:*::man-construction-worker:-light-skin-ton::
Send,👷🏻‍♂
return

;1F477 1F3FC 200D 2642 FE0F

:*::man-construction-worker:-medium-light-s::
Send,👷🏼‍♂️
return

;1F477 1F3FC 200D 2642

:*::man-construction-worker:-medium-light-s::
Send,👷🏼‍♂
return

;1F477 1F3FD 200D 2642 FE0F

:*::man-construction-worker:-medium-skin-to::
Send,👷🏽‍♂️
return

;1F477 1F3FD 200D 2642

:*::man-construction-worker:-medium-skin-to::
Send,👷🏽‍♂
return

;1F477 1F3FE 200D 2642 FE0F

:*::man-construction-worker:-medium-dark-sk::
Send,👷🏾‍♂️
return

;1F477 1F3FE 200D 2642

:*::man-construction-worker:-medium-dark-sk::
Send,👷🏾‍♂
return

;1F477 1F3FF 200D 2642 FE0F

:*::man-construction-worker:-dark-skin-tone::
Send,👷🏿‍♂️
return

;1F477 1F3FF 200D 2642

:*::man-construction-worker:-dark-skin-tone::
Send,👷🏿‍♂
return

;1F477 200D 2640 FE0F

:*::woman-construction-worker::
Send,👷‍♀️
return

;1F477 200D 2640

:*::woman-construction-worker::
Send,👷‍♀
return

;1F477 1F3FB 200D 2640 FE0F

:*::woman-construction-worker:-light-skin-t::
Send,👷🏻‍♀️
return

;1F477 1F3FB 200D 2640

:*::woman-construction-worker:-light-skin-t::
Send,👷🏻‍♀
return

;1F477 1F3FC 200D 2640 FE0F

:*::woman-construction-worker:-medium-light::
Send,👷🏼‍♀️
return

;1F477 1F3FC 200D 2640

:*::woman-construction-worker:-medium-light::
Send,👷🏼‍♀
return

;1F477 1F3FD 200D 2640 FE0F

:*::woman-construction-worker:-medium-skin-::
Send,👷🏽‍♀️
return

;1F477 1F3FD 200D 2640

:*::woman-construction-worker:-medium-skin-::
Send,👷🏽‍♀
return

;1F477 1F3FE 200D 2640 FE0F

:*::woman-construction-worker:-medium-dark-::
Send,👷🏾‍♀️
return

;1F477 1F3FE 200D 2640

:*::woman-construction-worker:-medium-dark-::
Send,👷🏾‍♀
return

;1F477 1F3FF 200D 2640 FE0F

:*::woman-construction-worker:-dark-skin-to::
Send,👷🏿‍♀️
return

;1F477 1F3FF 200D 2640

:*::woman-construction-worker:-dark-skin-to::
Send,👷🏿‍♀
return

;1F934

:*::prince::
Send,🤴
return

;1F934 1F3FB

:*::prince:-light-skin-tone::
Send,🤴🏻
return

;1F934 1F3FC

:*::prince:-medium-light-skin-tone::
Send,🤴🏼
return

;1F934 1F3FD

:*::prince:-medium-skin-tone::
Send,🤴🏽
return

;1F934 1F3FE

:*::prince:-medium-dark-skin-tone::
Send,🤴🏾
return

;1F934 1F3FF

:*::prince:-dark-skin-tone::
Send,🤴🏿
return

;1F478
:*::blonde-girl::
:*::girl-with-crown::
:*::girl-with-tiara::
:*::princess::
Send,👸
return

;1F478 1F3FB

:*::princess:-light-skin-tone::
Send,👸🏻
return

;1F478 1F3FC

:*::princess:-medium-light-skin-tone::
Send,👸🏼
return

;1F478 1F3FD

:*::princess:-medium-skin-tone::
Send,👸🏽
return

;1F478 1F3FE

:*::princess:-medium-dark-skin-tone::
Send,👸🏾
return

;1F478 1F3FF

:*::princess:-dark-skin-tone::
Send,👸🏿
return

;1F473

:*::person-wearing-turban::
Send,👳
return

;1F473 1F3FB

:*::person-wearing-turban:-light-skin-tone::
Send,👳🏻
return

;1F473 1F3FC

:*::person-wearing-turban:-medium-light-ski::
Send,👳🏼
return

;1F473 1F3FD

:*::person-wearing-turban:-medium-skin-tone::
Send,👳🏽
return

;1F473 1F3FE

:*::person-wearing-turban:-medium-dark-skin::
Send,👳🏾
return

;1F473 1F3FF

:*::person-wearing-turban:-dark-skin-tone::
Send,👳🏿
return

;1F473 200D 2642 FE0F

:*::man-wearing-turban::
Send,👳‍♂️
return

;1F473 200D 2642

:*::man-wearing-turban::
Send,👳‍♂
return

;1F473 1F3FB 200D 2642 FE0F

:*::man-wearing-turban:-light-skin-tone::
Send,👳🏻‍♂️
return

;1F473 1F3FB 200D 2642

:*::man-wearing-turban:-light-skin-tone::
Send,👳🏻‍♂
return

;1F473 1F3FC 200D 2642 FE0F

:*::man-wearing-turban:-medium-light-skin-t::
Send,👳🏼‍♂️
return

;1F473 1F3FC 200D 2642

:*::man-wearing-turban:-medium-light-skin-t::
Send,👳🏼‍♂
return

;1F473 1F3FD 200D 2642 FE0F

:*::man-wearing-turban:-medium-skin-tone::
Send,👳🏽‍♂️
return

;1F473 1F3FD 200D 2642

:*::man-wearing-turban:-medium-skin-tone::
Send,👳🏽‍♂
return

;1F473 1F3FE 200D 2642 FE0F

:*::man-wearing-turban:-medium-dark-skin-to::
Send,👳🏾‍♂️
return

;1F473 1F3FE 200D 2642

:*::man-wearing-turban:-medium-dark-skin-to::
Send,👳🏾‍♂
return

;1F473 1F3FF 200D 2642 FE0F

:*::man-wearing-turban:-dark-skin-tone::
Send,👳🏿‍♂️
return

;1F473 1F3FF 200D 2642

:*::man-wearing-turban:-dark-skin-tone::
Send,👳🏿‍♂
return

;1F473 200D 2640 FE0F

:*::woman-wearing-turban::
Send,👳‍♀️
return

;1F473 200D 2640

:*::woman-wearing-turban::
Send,👳‍♀
return

;1F473 1F3FB 200D 2640 FE0F

:*::woman-wearing-turban:-light-skin-tone::
Send,👳🏻‍♀️
return

;1F473 1F3FB 200D 2640

:*::woman-wearing-turban:-light-skin-tone::
Send,👳🏻‍♀
return

;1F473 1F3FC 200D 2640 FE0F

:*::woman-wearing-turban:-medium-light-skin::
Send,👳🏼‍♀️
return

;1F473 1F3FC 200D 2640

:*::woman-wearing-turban:-medium-light-skin::
Send,👳🏼‍♀
return

;1F473 1F3FD 200D 2640 FE0F

:*::woman-wearing-turban:-medium-skin-tone::
Send,👳🏽‍♀️
return

;1F473 1F3FD 200D 2640

:*::woman-wearing-turban:-medium-skin-tone::
Send,👳🏽‍♀
return

;1F473 1F3FE 200D 2640 FE0F

:*::woman-wearing-turban:-medium-dark-skin-::
Send,👳🏾‍♀️
return

;1F473 1F3FE 200D 2640

:*::woman-wearing-turban:-medium-dark-skin-::
Send,👳🏾‍♀
return

;1F473 1F3FF 200D 2640 FE0F

:*::woman-wearing-turban:-dark-skin-tone::
Send,👳🏿‍♀️
return

;1F473 1F3FF 200D 2640

:*::woman-wearing-turban:-dark-skin-tone::
Send,👳🏿‍♀
return

;1F472

:*::man-with-chinese-cap::
Send,👲
return

;1F472 1F3FB

:*::man-with-chinese-cap:-light-skin-tone::
Send,👲🏻
return

;1F472 1F3FC

:*::man-with-chinese-cap:-medium-light-skin::
Send,👲🏼
return

;1F472 1F3FD

:*::man-with-chinese-cap:-medium-skin-tone::
Send,👲🏽
return

;1F472 1F3FE

:*::man-with-chinese-cap:-medium-dark-skin-::
Send,👲🏾
return

;1F472 1F3FF

:*::man-with-chinese-cap:-dark-skin-tone::
Send,👲🏿
return

;1F9D5

:*::woman-with-headscarf::
Send,🧕
return

;1F9D5 1F3FB

:*::woman-with-headscarf:-light-skin-tone::
Send,🧕🏻
return

;1F9D5 1F3FC

:*::woman-with-headscarf:-medium-light-skin::
Send,🧕🏼
return

;1F9D5 1F3FD

:*::woman-with-headscarf:-medium-skin-tone::
Send,🧕🏽
return

;1F9D5 1F3FE

:*::woman-with-headscarf:-medium-dark-skin-::
Send,🧕🏾
return

;1F9D5 1F3FF

:*::woman-with-headscarf:-dark-skin-tone::
Send,🧕🏿
return

;1F935
:*::groom::
:*::man-in-suit::
:*::man-in-tuxedo::
Send,🤵
return

;1F935 1F3FB

:*::man-in-tuxedo:-light-skin-tone::
Send,🤵🏻
return

;1F935 1F3FC

:*::man-in-tuxedo:-medium-light-skin-tone::
Send,🤵🏼
return

;1F935 1F3FD

:*::man-in-tuxedo:-medium-skin-tone::
Send,🤵🏽
return

;1F935 1F3FE

:*::man-in-tuxedo:-medium-dark-skin-tone::
Send,🤵🏾
return

;1F935 1F3FF

:*::man-in-tuxedo:-dark-skin-tone::
Send,🤵🏿
return

;1F470
:*::marriage::
:*::wedding::
:*::bride-with-veil::
Send,👰
return

;1F470 1F3FB

:*::bride-with-veil:-light-skin-tone::
Send,👰🏻
return

;1F470 1F3FC

:*::bride-with-veil:-medium-light-skin-tone::
Send,👰🏼
return

;1F470 1F3FD

:*::bride-with-veil:-medium-skin-tone::
Send,👰🏽
return

;1F470 1F3FE

:*::bride-with-veil:-medium-dark-skin-tone::
Send,👰🏾
return

;1F470 1F3FF

:*::bride-with-veil:-dark-skin-tone::
Send,👰🏿
return

;1F930
:*::pregnancy::
:*::pregnant lady::
:*::pregnant-woman::
Send,🤰
return

;1F930 1F3FB

:*::pregnant-woman:-light-skin-tone::
Send,🤰🏻
return

;1F930 1F3FC

:*::pregnant-woman:-medium-light-skin-tone::
Send,🤰🏼
return

;1F930 1F3FD

:*::pregnant-woman:-medium-skin-tone::
Send,🤰🏽
return

;1F930 1F3FE

:*::pregnant-woman:-medium-dark-skin-tone::
Send,🤰🏾
return

;1F930 1F3FF

:*::pregnant-woman:-dark-skin-tone::
Send,🤰🏿
return

;1F931
:*::breastfeeding::
:*::breast-feeding::
Send,🤱
return

;1F931 1F3FB

:*::breast-feeding:-light-skin-tone::
Send,🤱🏻
return

;1F931 1F3FC

:*::breast-feeding:-medium-light-skin-tone::
Send,🤱🏼
return

;1F931 1F3FD

:*::breast-feeding:-medium-skin-tone::
Send,🤱🏽
return

;1F931 1F3FE

:*::breast-feeding:-medium-dark-skin-tone::
Send,🤱🏾
return

;1F931 1F3FF

:*::breast-feeding:-dark-skin-tone::
Send,🤱🏿
return

;1F47C
:*::angel::
:*::cherub::
:*::cupid::
:*::putto::
:*::baby-angel::
Send,👼
return

;1F47C 1F3FB

:*::baby-angel:-light-skin-tone::
Send,👼🏻
return

;1F47C 1F3FC

:*::baby-angel:-medium-light-skin-tone::
Send,👼🏼
return

;1F47C 1F3FD

:*::baby-angel:-medium-skin-tone::
Send,👼🏽
return

;1F47C 1F3FE

:*::baby-angel:-medium-dark-skin-tone::
Send,👼🏾
return

;1F47C 1F3FF

:*::baby-angel:-dark-skin-tone::
Send,👼🏿
return

;1F385

:*::santa-claus::
Send,🎅
return

;1F385 1F3FB

:*::santa-claus:-light-skin-tone::
Send,🎅🏻
return

;1F385 1F3FC

:*::santa-claus:-medium-light-skin-tone::
Send,🎅🏼
return

;1F385 1F3FD

:*::santa-claus:-medium-skin-tone::
Send,🎅🏽
return

;1F385 1F3FE

:*::santa-claus:-medium-dark-skin-tone::
Send,🎅🏾
return

;1F385 1F3FF

:*::santa-claus:-dark-skin-tone::
Send,🎅🏿
return

;1F936

:*::mrs.-claus::
Send,🤶
return

;1F936 1F3FB

:*::mrs.-claus:-light-skin-tone::
Send,🤶🏻
return

;1F936 1F3FC

:*::mrs.-claus:-medium-light-skin-tone::
Send,🤶🏼
return

;1F936 1F3FD

:*::mrs.-claus:-medium-skin-tone::
Send,🤶🏽
return

;1F936 1F3FE

:*::mrs.-claus:-medium-dark-skin-tone::
Send,🤶🏾
return

;1F936 1F3FF

:*::mrs.-claus:-dark-skin-tone::
Send,🤶🏿
return

;1F9B8

:*::superhero::
Send,🦸
return

;1F9B8 1F3FB

:*::superhero:-light-skin-tone::
Send,🦸🏻
return

;1F9B8 1F3FC

:*::superhero:-medium-light-skin-tone::
Send,🦸🏼
return

;1F9B8 1F3FD

:*::superhero:-medium-skin-tone::
Send,🦸🏽
return

;1F9B8 1F3FE

:*::superhero:-medium-dark-skin-tone::
Send,🦸🏾
return

;1F9B8 1F3FF

:*::superhero:-dark-skin-tone::
Send,🦸🏿
return

;1F9B8 200D 2642 FE0F

:*::man-superhero::
Send,🦸‍♂️
return

;1F9B8 200D 2642

:*::man-superhero::
Send,🦸‍♂
return

;1F9B8 1F3FB 200D 2642 FE0F

:*::man-superhero:-light-skin-tone::
Send,🦸🏻‍♂️
return

;1F9B8 1F3FB 200D 2642

:*::man-superhero:-light-skin-tone::
Send,🦸🏻‍♂
return

;1F9B8 1F3FC 200D 2642 FE0F

:*::man-superhero:-medium-light-skin-tone::
Send,🦸🏼‍♂️
return

;1F9B8 1F3FC 200D 2642

:*::man-superhero:-medium-light-skin-tone::
Send,🦸🏼‍♂
return

;1F9B8 1F3FD 200D 2642 FE0F

:*::man-superhero:-medium-skin-tone::
Send,🦸🏽‍♂️
return

;1F9B8 1F3FD 200D 2642

:*::man-superhero:-medium-skin-tone::
Send,🦸🏽‍♂
return

;1F9B8 1F3FE 200D 2642 FE0F

:*::man-superhero:-medium-dark-skin-tone::
Send,🦸🏾‍♂️
return

;1F9B8 1F3FE 200D 2642

:*::man-superhero:-medium-dark-skin-tone::
Send,🦸🏾‍♂
return

;1F9B8 1F3FF 200D 2642 FE0F

:*::man-superhero:-dark-skin-tone::
Send,🦸🏿‍♂️
return

;1F9B8 1F3FF 200D 2642

:*::man-superhero:-dark-skin-tone::
Send,🦸🏿‍♂
return

;1F9B8 200D 2640 FE0F

:*::woman-superhero::
Send,🦸‍♀️
return

;1F9B8 200D 2640

:*::woman-superhero::
Send,🦸‍♀
return

;1F9B8 1F3FB 200D 2640 FE0F

:*::woman-superhero:-light-skin-tone::
Send,🦸🏻‍♀️
return

;1F9B8 1F3FB 200D 2640

:*::woman-superhero:-light-skin-tone::
Send,🦸🏻‍♀
return

;1F9B8 1F3FC 200D 2640 FE0F

:*::woman-superhero:-medium-light-skin-tone::
Send,🦸🏼‍♀️
return

;1F9B8 1F3FC 200D 2640

:*::woman-superhero:-medium-light-skin-tone::
Send,🦸🏼‍♀
return

;1F9B8 1F3FD 200D 2640 FE0F

:*::woman-superhero:-medium-skin-tone::
Send,🦸🏽‍♀️
return

;1F9B8 1F3FD 200D 2640

:*::woman-superhero:-medium-skin-tone::
Send,🦸🏽‍♀
return

;1F9B8 1F3FE 200D 2640 FE0F

:*::woman-superhero:-medium-dark-skin-tone::
Send,🦸🏾‍♀️
return

;1F9B8 1F3FE 200D 2640

:*::woman-superhero:-medium-dark-skin-tone::
Send,🦸🏾‍♀
return

;1F9B8 1F3FF 200D 2640 FE0F

:*::woman-superhero:-dark-skin-tone::
Send,🦸🏿‍♀️
return

;1F9B8 1F3FF 200D 2640

:*::woman-superhero:-dark-skin-tone::
Send,🦸🏿‍♀
return

;1F9B9

:*::supervillain::
Send,🦹
return

;1F9B9 1F3FB

:*::supervillain:-light-skin-tone::
Send,🦹🏻
return

;1F9B9 1F3FC

:*::supervillain:-medium-light-skin-tone::
Send,🦹🏼
return

;1F9B9 1F3FD

:*::supervillain:-medium-skin-tone::
Send,🦹🏽
return

;1F9B9 1F3FE

:*::supervillain:-medium-dark-skin-tone::
Send,🦹🏾
return

;1F9B9 1F3FF

:*::supervillain:-dark-skin-tone::
Send,🦹🏿
return

;1F9B9 200D 2642 FE0F

:*::man-supervillain::
Send,🦹‍♂️
return

;1F9B9 200D 2642

:*::man-supervillain::
Send,🦹‍♂
return

;1F9B9 1F3FB 200D 2642 FE0F

:*::man-supervillain:-light-skin-tone::
Send,🦹🏻‍♂️
return

;1F9B9 1F3FB 200D 2642

:*::man-supervillain:-light-skin-tone::
Send,🦹🏻‍♂
return

;1F9B9 1F3FC 200D 2642 FE0F

:*::man-supervillain:-medium-light-skin-ton::
Send,🦹🏼‍♂️
return

;1F9B9 1F3FC 200D 2642

:*::man-supervillain:-medium-light-skin-ton::
Send,🦹🏼‍♂
return

;1F9B9 1F3FD 200D 2642 FE0F

:*::man-supervillain:-medium-skin-tone::
Send,🦹🏽‍♂️
return

;1F9B9 1F3FD 200D 2642

:*::man-supervillain:-medium-skin-tone::
Send,🦹🏽‍♂
return

;1F9B9 1F3FE 200D 2642 FE0F

:*::man-supervillain:-medium-dark-skin-tone::
Send,🦹🏾‍♂️
return

;1F9B9 1F3FE 200D 2642

:*::man-supervillain:-medium-dark-skin-tone::
Send,🦹🏾‍♂
return

;1F9B9 1F3FF 200D 2642 FE0F

:*::man-supervillain:-dark-skin-tone::
Send,🦹🏿‍♂️
return

;1F9B9 1F3FF 200D 2642

:*::man-supervillain:-dark-skin-tone::
Send,🦹🏿‍♂
return

;1F9B9 200D 2640 FE0F

:*::woman-supervillain::
Send,🦹‍♀️
return

;1F9B9 200D 2640

:*::woman-supervillain::
Send,🦹‍♀
return

;1F9B9 1F3FB 200D 2640 FE0F

:*::woman-supervillain:-light-skin-tone::
Send,🦹🏻‍♀️
return

;1F9B9 1F3FB 200D 2640

:*::woman-supervillain:-light-skin-tone::
Send,🦹🏻‍♀
return

;1F9B9 1F3FC 200D 2640 FE0F

:*::woman-supervillain:-medium-light-skin-t::
Send,🦹🏼‍♀️
return

;1F9B9 1F3FC 200D 2640

:*::woman-supervillain:-medium-light-skin-t::
Send,🦹🏼‍♀
return

;1F9B9 1F3FD 200D 2640 FE0F

:*::woman-supervillain:-medium-skin-tone::
Send,🦹🏽‍♀️
return

;1F9B9 1F3FD 200D 2640

:*::woman-supervillain:-medium-skin-tone::
Send,🦹🏽‍♀
return

;1F9B9 1F3FE 200D 2640 FE0F

:*::woman-supervillain:-medium-dark-skin-to::
Send,🦹🏾‍♀️
return

;1F9B9 1F3FE 200D 2640

:*::woman-supervillain:-medium-dark-skin-to::
Send,🦹🏾‍♀
return

;1F9B9 1F3FF 200D 2640 FE0F

:*::woman-supervillain:-dark-skin-tone::
Send,🦹🏿‍♀️
return

;1F9B9 1F3FF 200D 2640

:*::woman-supervillain:-dark-skin-tone::
Send,🦹🏿‍♀
return

;1F9D9
:*::sorcerer::
:*::sorceress::
:*::witch::
:*::wizard::
:*::mage::
Send,🧙
return

;1F9D9 1F3FB

:*::mage:-light-skin-tone::
Send,🧙🏻
return

;1F9D9 1F3FC

:*::mage:-medium-light-skin-tone::
Send,🧙🏼
return

;1F9D9 1F3FD

:*::mage:-medium-skin-tone::
Send,🧙🏽
return

;1F9D9 1F3FE

:*::mage:-medium-dark-skin-tone::
Send,🧙🏾
return

;1F9D9 1F3FF

:*::mage:-dark-skin-tone::
Send,🧙🏿
return

;1F9D9 200D 2642 FE0F
:*::wizard::
:*::man-mage::
Send,🧙‍♂️
return

;1F9D9 200D 2642
:*::wizard::
:*::man-mage::
Send,🧙‍♂
return

;1F9D9 1F3FB 200D 2642 FE0F

:*::man-mage:-light-skin-tone::
Send,🧙🏻‍♂️
return

;1F9D9 1F3FB 200D 2642

:*::man-mage:-light-skin-tone::
Send,🧙🏻‍♂
return

;1F9D9 1F3FC 200D 2642 FE0F

:*::man-mage:-medium-light-skin-tone::
Send,🧙🏼‍♂️
return

;1F9D9 1F3FC 200D 2642

:*::man-mage:-medium-light-skin-tone::
Send,🧙🏼‍♂
return

;1F9D9 1F3FD 200D 2642 FE0F

:*::man-mage:-medium-skin-tone::
Send,🧙🏽‍♂️
return

;1F9D9 1F3FD 200D 2642

:*::man-mage:-medium-skin-tone::
Send,🧙🏽‍♂
return

;1F9D9 1F3FE 200D 2642 FE0F

:*::man-mage:-medium-dark-skin-tone::
Send,🧙🏾‍♂️
return

;1F9D9 1F3FE 200D 2642

:*::man-mage:-medium-dark-skin-tone::
Send,🧙🏾‍♂
return

;1F9D9 1F3FF 200D 2642 FE0F

:*::man-mage:-dark-skin-tone::
Send,🧙🏿‍♂️
return

;1F9D9 1F3FF 200D 2642

:*::man-mage:-dark-skin-tone::
Send,🧙🏿‍♂
return

;1F9D9 200D 2640 FE0F
:*::witch::
:*::woman-mage::
Send,🧙‍♀️
return

;1F9D9 200D 2640
:*::witch::
:*::woman-mage::
Send,🧙‍♀
return

;1F9D9 1F3FB 200D 2640 FE0F

:*::woman-mage:-light-skin-tone::
Send,🧙🏻‍♀️
return

;1F9D9 1F3FB 200D 2640

:*::woman-mage:-light-skin-tone::
Send,🧙🏻‍♀
return

;1F9D9 1F3FC 200D 2640 FE0F

:*::woman-mage:-medium-light-skin-tone::
Send,🧙🏼‍♀️
return

;1F9D9 1F3FC 200D 2640

:*::woman-mage:-medium-light-skin-tone::
Send,🧙🏼‍♀
return

;1F9D9 1F3FD 200D 2640 FE0F

:*::woman-mage:-medium-skin-tone::
Send,🧙🏽‍♀️
return

;1F9D9 1F3FD 200D 2640

:*::woman-mage:-medium-skin-tone::
Send,🧙🏽‍♀
return

;1F9D9 1F3FE 200D 2640 FE0F

:*::woman-mage:-medium-dark-skin-tone::
Send,🧙🏾‍♀️
return

;1F9D9 1F3FE 200D 2640

:*::woman-mage:-medium-dark-skin-tone::
Send,🧙🏾‍♀
return

;1F9D9 1F3FF 200D 2640 FE0F

:*::woman-mage:-dark-skin-tone::
Send,🧙🏿‍♀️
return

;1F9D9 1F3FF 200D 2640

:*::woman-mage:-dark-skin-tone::
Send,🧙🏿‍♀
return

;1F9DA

:*::fairy::
Send,🧚
return

;1F9DA 1F3FB

:*::fairy:-light-skin-tone::
Send,🧚🏻
return

;1F9DA 1F3FC

:*::fairy:-medium-light-skin-tone::
Send,🧚🏼
return

;1F9DA 1F3FD

:*::fairy:-medium-skin-tone::
Send,🧚🏽
return

;1F9DA 1F3FE

:*::fairy:-medium-dark-skin-tone::
Send,🧚🏾
return

;1F9DA 1F3FF

:*::fairy:-dark-skin-tone::
Send,🧚🏿
return

;1F9DA 200D 2642 FE0F

:*::man-fairy::
Send,🧚‍♂️
return

;1F9DA 200D 2642

:*::man-fairy::
Send,🧚‍♂
return

;1F9DA 1F3FB 200D 2642 FE0F

:*::man-fairy:-light-skin-tone::
Send,🧚🏻‍♂️
return

;1F9DA 1F3FB 200D 2642

:*::man-fairy:-light-skin-tone::
Send,🧚🏻‍♂
return

;1F9DA 1F3FC 200D 2642 FE0F

:*::man-fairy:-medium-light-skin-tone::
Send,🧚🏼‍♂️
return

;1F9DA 1F3FC 200D 2642

:*::man-fairy:-medium-light-skin-tone::
Send,🧚🏼‍♂
return

;1F9DA 1F3FD 200D 2642 FE0F

:*::man-fairy:-medium-skin-tone::
Send,🧚🏽‍♂️
return

;1F9DA 1F3FD 200D 2642

:*::man-fairy:-medium-skin-tone::
Send,🧚🏽‍♂
return

;1F9DA 1F3FE 200D 2642 FE0F

:*::man-fairy:-medium-dark-skin-tone::
Send,🧚🏾‍♂️
return

;1F9DA 1F3FE 200D 2642

:*::man-fairy:-medium-dark-skin-tone::
Send,🧚🏾‍♂
return

;1F9DA 1F3FF 200D 2642 FE0F

:*::man-fairy:-dark-skin-tone::
Send,🧚🏿‍♂️
return

;1F9DA 1F3FF 200D 2642

:*::man-fairy:-dark-skin-tone::
Send,🧚🏿‍♂
return

;1F9DA 200D 2640 FE0F

:*::woman-fairy::
Send,🧚‍♀️
return

;1F9DA 200D 2640

:*::woman-fairy::
Send,🧚‍♀
return

;1F9DA 1F3FB 200D 2640 FE0F

:*::woman-fairy:-light-skin-tone::
Send,🧚🏻‍♀️
return

;1F9DA 1F3FB 200D 2640

:*::woman-fairy:-light-skin-tone::
Send,🧚🏻‍♀
return

;1F9DA 1F3FC 200D 2640 FE0F

:*::woman-fairy:-medium-light-skin-tone::
Send,🧚🏼‍♀️
return

;1F9DA 1F3FC 200D 2640

:*::woman-fairy:-medium-light-skin-tone::
Send,🧚🏼‍♀
return

;1F9DA 1F3FD 200D 2640 FE0F

:*::woman-fairy:-medium-skin-tone::
Send,🧚🏽‍♀️
return

;1F9DA 1F3FD 200D 2640

:*::woman-fairy:-medium-skin-tone::
Send,🧚🏽‍♀
return

;1F9DA 1F3FE 200D 2640 FE0F

:*::woman-fairy:-medium-dark-skin-tone::
Send,🧚🏾‍♀️
return

;1F9DA 1F3FE 200D 2640

:*::woman-fairy:-medium-dark-skin-tone::
Send,🧚🏾‍♀
return

;1F9DA 1F3FF 200D 2640 FE0F

:*::woman-fairy:-dark-skin-tone::
Send,🧚🏿‍♀️
return

;1F9DA 1F3FF 200D 2640

:*::woman-fairy:-dark-skin-tone::
Send,🧚🏿‍♀
return

;1F9DB
:*::dracula::
:*::vampire::
Send,🧛
return

;1F9DB 1F3FB

:*::vampire:-light-skin-tone::
Send,🧛🏻
return

;1F9DB 1F3FC

:*::vampire:-medium-light-skin-tone::
Send,🧛🏼
return

;1F9DB 1F3FD

:*::vampire:-medium-skin-tone::
Send,🧛🏽
return

;1F9DB 1F3FE

:*::vampire:-medium-dark-skin-tone::
Send,🧛🏾
return

;1F9DB 1F3FF

:*::vampire:-dark-skin-tone::
Send,🧛🏿
return

;1F9DB 200D 2642 FE0F
:*::dracula::
:*::man-vampire::
Send,🧛‍♂️
return

;1F9DB 200D 2642
:*::dracula::
:*::man-vampire::
Send,🧛‍♂
return

;1F9DB 1F3FB 200D 2642 FE0F

:*::man-vampire:-light-skin-tone::
Send,🧛🏻‍♂️
return

;1F9DB 1F3FB 200D 2642

:*::man-vampire:-light-skin-tone::
Send,🧛🏻‍♂
return

;1F9DB 1F3FC 200D 2642 FE0F

:*::man-vampire:-medium-light-skin-tone::
Send,🧛🏼‍♂️
return

;1F9DB 1F3FC 200D 2642

:*::man-vampire:-medium-light-skin-tone::
Send,🧛🏼‍♂
return

;1F9DB 1F3FD 200D 2642 FE0F

:*::man-vampire:-medium-skin-tone::
Send,🧛🏽‍♂️
return

;1F9DB 1F3FD 200D 2642

:*::man-vampire:-medium-skin-tone::
Send,🧛🏽‍♂
return

;1F9DB 1F3FE 200D 2642 FE0F

:*::man-vampire:-medium-dark-skin-tone::
Send,🧛🏾‍♂️
return

;1F9DB 1F3FE 200D 2642

:*::man-vampire:-medium-dark-skin-tone::
Send,🧛🏾‍♂
return

;1F9DB 1F3FF 200D 2642 FE0F

:*::man-vampire:-dark-skin-tone::
Send,🧛🏿‍♂️
return

;1F9DB 1F3FF 200D 2642

:*::man-vampire:-dark-skin-tone::
Send,🧛🏿‍♂
return

;1F9DB 200D 2640 FE0F

:*::woman-vampire::
Send,🧛‍♀️
return

;1F9DB 200D 2640

:*::woman-vampire::
Send,🧛‍♀
return

;1F9DB 1F3FB 200D 2640 FE0F

:*::woman-vampire:-light-skin-tone::
Send,🧛🏻‍♀️
return

;1F9DB 1F3FB 200D 2640

:*::woman-vampire:-light-skin-tone::
Send,🧛🏻‍♀
return

;1F9DB 1F3FC 200D 2640 FE0F

:*::woman-vampire:-medium-light-skin-tone::
Send,🧛🏼‍♀️
return

;1F9DB 1F3FC 200D 2640

:*::woman-vampire:-medium-light-skin-tone::
Send,🧛🏼‍♀
return

;1F9DB 1F3FD 200D 2640 FE0F

:*::woman-vampire:-medium-skin-tone::
Send,🧛🏽‍♀️
return

;1F9DB 1F3FD 200D 2640

:*::woman-vampire:-medium-skin-tone::
Send,🧛🏽‍♀
return

;1F9DB 1F3FE 200D 2640 FE0F

:*::woman-vampire:-medium-dark-skin-tone::
Send,🧛🏾‍♀️
return

;1F9DB 1F3FE 200D 2640

:*::woman-vampire:-medium-dark-skin-tone::
Send,🧛🏾‍♀
return

;1F9DB 1F3FF 200D 2640 FE0F

:*::woman-vampire:-dark-skin-tone::
Send,🧛🏿‍♀️
return

;1F9DB 1F3FF 200D 2640

:*::woman-vampire:-dark-skin-tone::
Send,🧛🏿‍♀
return

;1F9DC
:*::merboy::
:*::mergirl::
:*::mermaid::
:*::merman::
:*::merperson::
Send,🧜
return

;1F9DC 1F3FB

:*::merperson:-light-skin-tone::
Send,🧜🏻
return

;1F9DC 1F3FC

:*::merperson:-medium-light-skin-tone::
Send,🧜🏼
return

;1F9DC 1F3FD

:*::merperson:-medium-skin-tone::
Send,🧜🏽
return

;1F9DC 1F3FE

:*::merperson:-medium-dark-skin-tone::
Send,🧜🏾
return

;1F9DC 1F3FF

:*::merperson:-dark-skin-tone::
Send,🧜🏿
return

;1F9DC 200D 2642 FE0F

:*::merman::
Send,🧜‍♂️
return

;1F9DC 200D 2642

:*::merman::
Send,🧜‍♂
return

;1F9DC 1F3FB 200D 2642 FE0F

:*::merman:-light-skin-tone::
Send,🧜🏻‍♂️
return

;1F9DC 1F3FB 200D 2642

:*::merman:-light-skin-tone::
Send,🧜🏻‍♂
return

;1F9DC 1F3FC 200D 2642 FE0F

:*::merman:-medium-light-skin-tone::
Send,🧜🏼‍♂️
return

;1F9DC 1F3FC 200D 2642

:*::merman:-medium-light-skin-tone::
Send,🧜🏼‍♂
return

;1F9DC 1F3FD 200D 2642 FE0F

:*::merman:-medium-skin-tone::
Send,🧜🏽‍♂️
return

;1F9DC 1F3FD 200D 2642

:*::merman:-medium-skin-tone::
Send,🧜🏽‍♂
return

;1F9DC 1F3FE 200D 2642 FE0F

:*::merman:-medium-dark-skin-tone::
Send,🧜🏾‍♂️
return

;1F9DC 1F3FE 200D 2642

:*::merman:-medium-dark-skin-tone::
Send,🧜🏾‍♂
return

;1F9DC 1F3FF 200D 2642 FE0F

:*::merman:-dark-skin-tone::
Send,🧜🏿‍♂️
return

;1F9DC 1F3FF 200D 2642

:*::merman:-dark-skin-tone::
Send,🧜🏿‍♂
return

;1F9DC 200D 2640 FE0F

:*::mermaid::
Send,🧜‍♀️
return

;1F9DC 200D 2640

:*::mermaid::
Send,🧜‍♀
return

;1F9DC 1F3FB 200D 2640 FE0F

:*::mermaid:-light-skin-tone::
Send,🧜🏻‍♀️
return

;1F9DC 1F3FB 200D 2640

:*::mermaid:-light-skin-tone::
Send,🧜🏻‍♀
return

;1F9DC 1F3FC 200D 2640 FE0F

:*::mermaid:-medium-light-skin-tone::
Send,🧜🏼‍♀️
return

;1F9DC 1F3FC 200D 2640

:*::mermaid:-medium-light-skin-tone::
Send,🧜🏼‍♀
return

;1F9DC 1F3FD 200D 2640 FE0F

:*::mermaid:-medium-skin-tone::
Send,🧜🏽‍♀️
return

;1F9DC 1F3FD 200D 2640

:*::mermaid:-medium-skin-tone::
Send,🧜🏽‍♀
return

;1F9DC 1F3FE 200D 2640 FE0F

:*::mermaid:-medium-dark-skin-tone::
Send,🧜🏾‍♀️
return

;1F9DC 1F3FE 200D 2640

:*::mermaid:-medium-dark-skin-tone::
Send,🧜🏾‍♀
return

;1F9DC 1F3FF 200D 2640 FE0F

:*::mermaid:-dark-skin-tone::
Send,🧜🏿‍♀️
return

;1F9DC 1F3FF 200D 2640

:*::mermaid:-dark-skin-tone::
Send,🧜🏿‍♀
return

;1F9DD
:*::legolas::
:*::elf::
Send,🧝
return

;1F9DD 1F3FB

:*::elf:-light-skin-tone::
Send,🧝🏻
return

;1F9DD 1F3FC

:*::elf:-medium-light-skin-tone::
Send,🧝🏼
return

;1F9DD 1F3FD

:*::elf:-medium-skin-tone::
Send,🧝🏽
return

;1F9DD 1F3FE

:*::elf:-medium-dark-skin-tone::
Send,🧝🏾
return

;1F9DD 1F3FF

:*::elf:-dark-skin-tone::
Send,🧝🏿
return

;1F9DD 200D 2642 FE0F

:*::man-elf::
Send,🧝‍♂️
return

;1F9DD 200D 2642

:*::man-elf::
Send,🧝‍♂
return

;1F9DD 1F3FB 200D 2642 FE0F

:*::man-elf:-light-skin-tone::
Send,🧝🏻‍♂️
return

;1F9DD 1F3FB 200D 2642

:*::man-elf:-light-skin-tone::
Send,🧝🏻‍♂
return

;1F9DD 1F3FC 200D 2642 FE0F

:*::man-elf:-medium-light-skin-tone::
Send,🧝🏼‍♂️
return

;1F9DD 1F3FC 200D 2642

:*::man-elf:-medium-light-skin-tone::
Send,🧝🏼‍♂
return

;1F9DD 1F3FD 200D 2642 FE0F

:*::man-elf:-medium-skin-tone::
Send,🧝🏽‍♂️
return

;1F9DD 1F3FD 200D 2642

:*::man-elf:-medium-skin-tone::
Send,🧝🏽‍♂
return

;1F9DD 1F3FE 200D 2642 FE0F

:*::man-elf:-medium-dark-skin-tone::
Send,🧝🏾‍♂️
return

;1F9DD 1F3FE 200D 2642

:*::man-elf:-medium-dark-skin-tone::
Send,🧝🏾‍♂
return

;1F9DD 1F3FF 200D 2642 FE0F

:*::man-elf:-dark-skin-tone::
Send,🧝🏿‍♂️
return

;1F9DD 1F3FF 200D 2642

:*::man-elf:-dark-skin-tone::
Send,🧝🏿‍♂
return

;1F9DD 200D 2640 FE0F

:*::woman-elf::
Send,🧝‍♀️
return

;1F9DD 200D 2640

:*::woman-elf::
Send,🧝‍♀
return

;1F9DD 1F3FB 200D 2640 FE0F

:*::woman-elf:-light-skin-tone::
Send,🧝🏻‍♀️
return

;1F9DD 1F3FB 200D 2640

:*::woman-elf:-light-skin-tone::
Send,🧝🏻‍♀
return

;1F9DD 1F3FC 200D 2640 FE0F

:*::woman-elf:-medium-light-skin-tone::
Send,🧝🏼‍♀️
return

;1F9DD 1F3FC 200D 2640

:*::woman-elf:-medium-light-skin-tone::
Send,🧝🏼‍♀
return

;1F9DD 1F3FD 200D 2640 FE0F

:*::woman-elf:-medium-skin-tone::
Send,🧝🏽‍♀️
return

;1F9DD 1F3FD 200D 2640

:*::woman-elf:-medium-skin-tone::
Send,🧝🏽‍♀
return

;1F9DD 1F3FE 200D 2640 FE0F

:*::woman-elf:-medium-dark-skin-tone::
Send,🧝🏾‍♀️
return

;1F9DD 1F3FE 200D 2640

:*::woman-elf:-medium-dark-skin-tone::
Send,🧝🏾‍♀
return

;1F9DD 1F3FF 200D 2640 FE0F

:*::woman-elf:-dark-skin-tone::
Send,🧝🏿‍♀️
return

;1F9DD 1F3FF 200D 2640

:*::woman-elf:-dark-skin-tone::
Send,🧝🏿‍♀
return

;1F9DE
:*::djinni::
:*::jinni::
:*::genie::
Send,🧞
return

;1F9DE 200D 2642 FE0F

:*::man-genie::
Send,🧞‍♂️
return

;1F9DE 200D 2642

:*::man-genie::
Send,🧞‍♂
return

;1F9DE 200D 2640 FE0F

:*::woman-genie::
Send,🧞‍♀️
return

;1F9DE 200D 2640

:*::woman-genie::
Send,🧞‍♀
return

;1F9DF

:*::zombie::
Send,🧟
return

;1F9DF 200D 2642 FE0F

:*::man-zombie::
Send,🧟‍♂️
return

;1F9DF 200D 2642

:*::man-zombie::
Send,🧟‍♂
return

;1F9DF 200D 2640 FE0F

:*::woman-zombie::
Send,🧟‍♀️
return

;1F9DF 200D 2640

:*::woman-zombie::
Send,🧟‍♀
return

;1F486

:*::person-getting-massage::
Send,💆
return

;1F486 1F3FB

:*::person-getting-massage:-light-skin-tone::
Send,💆🏻
return

;1F486 1F3FC

:*::person-getting-massage:-medium-light-sk::
Send,💆🏼
return

;1F486 1F3FD

:*::person-getting-massage:-medium-skin-ton::
Send,💆🏽
return

;1F486 1F3FE

:*::person-getting-massage:-medium-dark-ski::
Send,💆🏾
return

;1F486 1F3FF

:*::person-getting-massage:-dark-skin-tone::
Send,💆🏿
return

;1F486 200D 2642 FE0F

:*::man-getting-massage::
Send,💆‍♂️
return

;1F486 200D 2642

:*::man-getting-massage::
Send,💆‍♂
return

;1F486 1F3FB 200D 2642 FE0F

:*::man-getting-massage:-light-skin-tone::
Send,💆🏻‍♂️
return

;1F486 1F3FB 200D 2642

:*::man-getting-massage:-light-skin-tone::
Send,💆🏻‍♂
return

;1F486 1F3FC 200D 2642 FE0F

:*::man-getting-massage:-medium-light-skin-::
Send,💆🏼‍♂️
return

;1F486 1F3FC 200D 2642

:*::man-getting-massage:-medium-light-skin-::
Send,💆🏼‍♂
return

;1F486 1F3FD 200D 2642 FE0F

:*::man-getting-massage:-medium-skin-tone::
Send,💆🏽‍♂️
return

;1F486 1F3FD 200D 2642

:*::man-getting-massage:-medium-skin-tone::
Send,💆🏽‍♂
return

;1F486 1F3FE 200D 2642 FE0F

:*::man-getting-massage:-medium-dark-skin-t::
Send,💆🏾‍♂️
return

;1F486 1F3FE 200D 2642

:*::man-getting-massage:-medium-dark-skin-t::
Send,💆🏾‍♂
return

;1F486 1F3FF 200D 2642 FE0F

:*::man-getting-massage:-dark-skin-tone::
Send,💆🏿‍♂️
return

;1F486 1F3FF 200D 2642

:*::man-getting-massage:-dark-skin-tone::
Send,💆🏿‍♂
return

;1F486 200D 2640 FE0F

:*::woman-getting-massage::
Send,💆‍♀️
return

;1F486 200D 2640

:*::woman-getting-massage::
Send,💆‍♀
return

;1F486 1F3FB 200D 2640 FE0F

:*::woman-getting-massage:-light-skin-tone::
Send,💆🏻‍♀️
return

;1F486 1F3FB 200D 2640

:*::woman-getting-massage:-light-skin-tone::
Send,💆🏻‍♀
return

;1F486 1F3FC 200D 2640 FE0F

:*::woman-getting-massage:-medium-light-ski::
Send,💆🏼‍♀️
return

;1F486 1F3FC 200D 2640

:*::woman-getting-massage:-medium-light-ski::
Send,💆🏼‍♀
return

;1F486 1F3FD 200D 2640 FE0F

:*::woman-getting-massage:-medium-skin-tone::
Send,💆🏽‍♀️
return

;1F486 1F3FD 200D 2640

:*::woman-getting-massage:-medium-skin-tone::
Send,💆🏽‍♀
return

;1F486 1F3FE 200D 2640 FE0F

:*::woman-getting-massage:-medium-dark-skin::
Send,💆🏾‍♀️
return

;1F486 1F3FE 200D 2640

:*::woman-getting-massage:-medium-dark-skin::
Send,💆🏾‍♀
return

;1F486 1F3FF 200D 2640 FE0F

:*::woman-getting-massage:-dark-skin-tone::
Send,💆🏿‍♀️
return

;1F486 1F3FF 200D 2640

:*::woman-getting-massage:-dark-skin-tone::
Send,💆🏿‍♀
return

;1F487

:*::person-getting-haircut::
Send,💇
return

;1F487 1F3FB

:*::person-getting-haircut:-light-skin-tone::
Send,💇🏻
return

;1F487 1F3FC

:*::person-getting-haircut:-medium-light-sk::
Send,💇🏼
return

;1F487 1F3FD

:*::person-getting-haircut:-medium-skin-ton::
Send,💇🏽
return

;1F487 1F3FE

:*::person-getting-haircut:-medium-dark-ski::
Send,💇🏾
return

;1F487 1F3FF

:*::person-getting-haircut:-dark-skin-tone::
Send,💇🏿
return

;1F487 200D 2642 FE0F

:*::man-getting-haircut::
Send,💇‍♂️
return

;1F487 200D 2642

:*::man-getting-haircut::
Send,💇‍♂
return

;1F487 1F3FB 200D 2642 FE0F

:*::man-getting-haircut:-light-skin-tone::
Send,💇🏻‍♂️
return

;1F487 1F3FB 200D 2642

:*::man-getting-haircut:-light-skin-tone::
Send,💇🏻‍♂
return

;1F487 1F3FC 200D 2642 FE0F

:*::man-getting-haircut:-medium-light-skin-::
Send,💇🏼‍♂️
return

;1F487 1F3FC 200D 2642

:*::man-getting-haircut:-medium-light-skin-::
Send,💇🏼‍♂
return

;1F487 1F3FD 200D 2642 FE0F

:*::man-getting-haircut:-medium-skin-tone::
Send,💇🏽‍♂️
return

;1F487 1F3FD 200D 2642

:*::man-getting-haircut:-medium-skin-tone::
Send,💇🏽‍♂
return

;1F487 1F3FE 200D 2642 FE0F

:*::man-getting-haircut:-medium-dark-skin-t::
Send,💇🏾‍♂️
return

;1F487 1F3FE 200D 2642

:*::man-getting-haircut:-medium-dark-skin-t::
Send,💇🏾‍♂
return

;1F487 1F3FF 200D 2642 FE0F

:*::man-getting-haircut:-dark-skin-tone::
Send,💇🏿‍♂️
return

;1F487 1F3FF 200D 2642

:*::man-getting-haircut:-dark-skin-tone::
Send,💇🏿‍♂
return

;1F487 200D 2640 FE0F

:*::woman-getting-haircut::
Send,💇‍♀️
return

;1F487 200D 2640

:*::woman-getting-haircut::
Send,💇‍♀
return

;1F487 1F3FB 200D 2640 FE0F

:*::woman-getting-haircut:-light-skin-tone::
Send,💇🏻‍♀️
return

;1F487 1F3FB 200D 2640

:*::woman-getting-haircut:-light-skin-tone::
Send,💇🏻‍♀
return

;1F487 1F3FC 200D 2640 FE0F

:*::woman-getting-haircut:-medium-light-ski::
Send,💇🏼‍♀️
return

;1F487 1F3FC 200D 2640

:*::woman-getting-haircut:-medium-light-ski::
Send,💇🏼‍♀
return

;1F487 1F3FD 200D 2640 FE0F

:*::woman-getting-haircut:-medium-skin-tone::
Send,💇🏽‍♀️
return

;1F487 1F3FD 200D 2640

:*::woman-getting-haircut:-medium-skin-tone::
Send,💇🏽‍♀
return

;1F487 1F3FE 200D 2640 FE0F

:*::woman-getting-haircut:-medium-dark-skin::
Send,💇🏾‍♀️
return

;1F487 1F3FE 200D 2640

:*::woman-getting-haircut:-medium-dark-skin::
Send,💇🏾‍♀
return

;1F487 1F3FF 200D 2640 FE0F

:*::woman-getting-haircut:-dark-skin-tone::
Send,💇🏿‍♀️
return

;1F487 1F3FF 200D 2640

:*::woman-getting-haircut:-dark-skin-tone::
Send,💇🏿‍♀
return

;1F6B6

:*::person-walking::
Send,🚶
return

;1F6B6 1F3FB

:*::person-walking:-light-skin-tone::
Send,🚶🏻
return

;1F6B6 1F3FC

:*::person-walking:-medium-light-skin-tone::
Send,🚶🏼
return

;1F6B6 1F3FD

:*::person-walking:-medium-skin-tone::
Send,🚶🏽
return

;1F6B6 1F3FE

:*::person-walking:-medium-dark-skin-tone::
Send,🚶🏾
return

;1F6B6 1F3FF

:*::person-walking:-dark-skin-tone::
Send,🚶🏿
return

;1F6B6 200D 2642 FE0F

:*::man-walking::
Send,🚶‍♂️
return

;1F6B6 200D 2642

:*::man-walking::
Send,🚶‍♂
return

;1F6B6 1F3FB 200D 2642 FE0F

:*::man-walking:-light-skin-tone::
Send,🚶🏻‍♂️
return

;1F6B6 1F3FB 200D 2642

:*::man-walking:-light-skin-tone::
Send,🚶🏻‍♂
return

;1F6B6 1F3FC 200D 2642 FE0F

:*::man-walking:-medium-light-skin-tone::
Send,🚶🏼‍♂️
return

;1F6B6 1F3FC 200D 2642

:*::man-walking:-medium-light-skin-tone::
Send,🚶🏼‍♂
return

;1F6B6 1F3FD 200D 2642 FE0F

:*::man-walking:-medium-skin-tone::
Send,🚶🏽‍♂️
return

;1F6B6 1F3FD 200D 2642

:*::man-walking:-medium-skin-tone::
Send,🚶🏽‍♂
return

;1F6B6 1F3FE 200D 2642 FE0F

:*::man-walking:-medium-dark-skin-tone::
Send,🚶🏾‍♂️
return

;1F6B6 1F3FE 200D 2642

:*::man-walking:-medium-dark-skin-tone::
Send,🚶🏾‍♂
return

;1F6B6 1F3FF 200D 2642 FE0F

:*::man-walking:-dark-skin-tone::
Send,🚶🏿‍♂️
return

;1F6B6 1F3FF 200D 2642

:*::man-walking:-dark-skin-tone::
Send,🚶🏿‍♂
return

;1F6B6 200D 2640 FE0F

:*::woman-walking::
Send,🚶‍♀️
return

;1F6B6 200D 2640

:*::woman-walking::
Send,🚶‍♀
return

;1F6B6 1F3FB 200D 2640 FE0F

:*::woman-walking:-light-skin-tone::
Send,🚶🏻‍♀️
return

;1F6B6 1F3FB 200D 2640

:*::woman-walking:-light-skin-tone::
Send,🚶🏻‍♀
return

;1F6B6 1F3FC 200D 2640 FE0F

:*::woman-walking:-medium-light-skin-tone::
Send,🚶🏼‍♀️
return

;1F6B6 1F3FC 200D 2640

:*::woman-walking:-medium-light-skin-tone::
Send,🚶🏼‍♀
return

;1F6B6 1F3FD 200D 2640 FE0F

:*::woman-walking:-medium-skin-tone::
Send,🚶🏽‍♀️
return

;1F6B6 1F3FD 200D 2640

:*::woman-walking:-medium-skin-tone::
Send,🚶🏽‍♀
return

;1F6B6 1F3FE 200D 2640 FE0F

:*::woman-walking:-medium-dark-skin-tone::
Send,🚶🏾‍♀️
return

;1F6B6 1F3FE 200D 2640

:*::woman-walking:-medium-dark-skin-tone::
Send,🚶🏾‍♀
return

;1F6B6 1F3FF 200D 2640 FE0F

:*::woman-walking:-dark-skin-tone::
Send,🚶🏿‍♀️
return

;1F6B6 1F3FF 200D 2640

:*::woman-walking:-dark-skin-tone::
Send,🚶🏿‍♀
return

;1F9CD

:*::person-standing::
Send,🧍
return

;1F9CD 1F3FB

:*::person-standing:-light-skin-tone::
Send,🧍🏻
return

;1F9CD 1F3FC

:*::person-standing:-medium-light-skin-tone::
Send,🧍🏼
return

;1F9CD 1F3FD

:*::person-standing:-medium-skin-tone::
Send,🧍🏽
return

;1F9CD 1F3FE

:*::person-standing:-medium-dark-skin-tone::
Send,🧍🏾
return

;1F9CD 1F3FF

:*::person-standing:-dark-skin-tone::
Send,🧍🏿
return

;1F9CD 200D 2642 FE0F

:*::man-standing::
Send,🧍‍♂️
return

;1F9CD 200D 2642

:*::man-standing::
Send,🧍‍♂
return

;1F9CD 1F3FB 200D 2642 FE0F

:*::man-standing:-light-skin-tone::
Send,🧍🏻‍♂️
return

;1F9CD 1F3FB 200D 2642

:*::man-standing:-light-skin-tone::
Send,🧍🏻‍♂
return

;1F9CD 1F3FC 200D 2642 FE0F

:*::man-standing:-medium-light-skin-tone::
Send,🧍🏼‍♂️
return

;1F9CD 1F3FC 200D 2642

:*::man-standing:-medium-light-skin-tone::
Send,🧍🏼‍♂
return

;1F9CD 1F3FD 200D 2642 FE0F

:*::man-standing:-medium-skin-tone::
Send,🧍🏽‍♂️
return

;1F9CD 1F3FD 200D 2642

:*::man-standing:-medium-skin-tone::
Send,🧍🏽‍♂
return

;1F9CD 1F3FE 200D 2642 FE0F

:*::man-standing:-medium-dark-skin-tone::
Send,🧍🏾‍♂️
return

;1F9CD 1F3FE 200D 2642

:*::man-standing:-medium-dark-skin-tone::
Send,🧍🏾‍♂
return

;1F9CD 1F3FF 200D 2642 FE0F

:*::man-standing:-dark-skin-tone::
Send,🧍🏿‍♂️
return

;1F9CD 1F3FF 200D 2642

:*::man-standing:-dark-skin-tone::
Send,🧍🏿‍♂
return

;1F9CD 200D 2640 FE0F

:*::woman-standing::
Send,🧍‍♀️
return

;1F9CD 200D 2640

:*::woman-standing::
Send,🧍‍♀
return

;1F9CD 1F3FB 200D 2640 FE0F

:*::woman-standing:-light-skin-tone::
Send,🧍🏻‍♀️
return

;1F9CD 1F3FB 200D 2640

:*::woman-standing:-light-skin-tone::
Send,🧍🏻‍♀
return

;1F9CD 1F3FC 200D 2640 FE0F

:*::woman-standing:-medium-light-skin-tone::
Send,🧍🏼‍♀️
return

;1F9CD 1F3FC 200D 2640

:*::woman-standing:-medium-light-skin-tone::
Send,🧍🏼‍♀
return

;1F9CD 1F3FD 200D 2640 FE0F

:*::woman-standing:-medium-skin-tone::
Send,🧍🏽‍♀️
return

;1F9CD 1F3FD 200D 2640

:*::woman-standing:-medium-skin-tone::
Send,🧍🏽‍♀
return

;1F9CD 1F3FE 200D 2640 FE0F

:*::woman-standing:-medium-dark-skin-tone::
Send,🧍🏾‍♀️
return

;1F9CD 1F3FE 200D 2640

:*::woman-standing:-medium-dark-skin-tone::
Send,🧍🏾‍♀
return

;1F9CD 1F3FF 200D 2640 FE0F

:*::woman-standing:-dark-skin-tone::
Send,🧍🏿‍♀️
return

;1F9CD 1F3FF 200D 2640

:*::woman-standing:-dark-skin-tone::
Send,🧍🏿‍♀
return

;1F9CE

:*::person-kneeling::
Send,🧎
return

;1F9CE 1F3FB

:*::person-kneeling:-light-skin-tone::
Send,🧎🏻
return

;1F9CE 1F3FC

:*::person-kneeling:-medium-light-skin-tone::
Send,🧎🏼
return

;1F9CE 1F3FD

:*::person-kneeling:-medium-skin-tone::
Send,🧎🏽
return

;1F9CE 1F3FE

:*::person-kneeling:-medium-dark-skin-tone::
Send,🧎🏾
return

;1F9CE 1F3FF

:*::person-kneeling:-dark-skin-tone::
Send,🧎🏿
return

;1F9CE 200D 2642 FE0F

:*::man-kneeling::
Send,🧎‍♂️
return

;1F9CE 200D 2642

:*::man-kneeling::
Send,🧎‍♂
return

;1F9CE 1F3FB 200D 2642 FE0F

:*::man-kneeling:-light-skin-tone::
Send,🧎🏻‍♂️
return

;1F9CE 1F3FB 200D 2642

:*::man-kneeling:-light-skin-tone::
Send,🧎🏻‍♂
return

;1F9CE 1F3FC 200D 2642 FE0F

:*::man-kneeling:-medium-light-skin-tone::
Send,🧎🏼‍♂️
return

;1F9CE 1F3FC 200D 2642

:*::man-kneeling:-medium-light-skin-tone::
Send,🧎🏼‍♂
return

;1F9CE 1F3FD 200D 2642 FE0F

:*::man-kneeling:-medium-skin-tone::
Send,🧎🏽‍♂️
return

;1F9CE 1F3FD 200D 2642

:*::man-kneeling:-medium-skin-tone::
Send,🧎🏽‍♂
return

;1F9CE 1F3FE 200D 2642 FE0F

:*::man-kneeling:-medium-dark-skin-tone::
Send,🧎🏾‍♂️
return

;1F9CE 1F3FE 200D 2642

:*::man-kneeling:-medium-dark-skin-tone::
Send,🧎🏾‍♂
return

;1F9CE 1F3FF 200D 2642 FE0F

:*::man-kneeling:-dark-skin-tone::
Send,🧎🏿‍♂️
return

;1F9CE 1F3FF 200D 2642

:*::man-kneeling:-dark-skin-tone::
Send,🧎🏿‍♂
return

;1F9CE 200D 2640 FE0F

:*::woman-kneeling::
Send,🧎‍♀️
return

;1F9CE 200D 2640

:*::woman-kneeling::
Send,🧎‍♀
return

;1F9CE 1F3FB 200D 2640 FE0F

:*::woman-kneeling:-light-skin-tone::
Send,🧎🏻‍♀️
return

;1F9CE 1F3FB 200D 2640

:*::woman-kneeling:-light-skin-tone::
Send,🧎🏻‍♀
return

;1F9CE 1F3FC 200D 2640 FE0F

:*::woman-kneeling:-medium-light-skin-tone::
Send,🧎🏼‍♀️
return

;1F9CE 1F3FC 200D 2640

:*::woman-kneeling:-medium-light-skin-tone::
Send,🧎🏼‍♀
return

;1F9CE 1F3FD 200D 2640 FE0F

:*::woman-kneeling:-medium-skin-tone::
Send,🧎🏽‍♀️
return

;1F9CE 1F3FD 200D 2640

:*::woman-kneeling:-medium-skin-tone::
Send,🧎🏽‍♀
return

;1F9CE 1F3FE 200D 2640 FE0F

:*::woman-kneeling:-medium-dark-skin-tone::
Send,🧎🏾‍♀️
return

;1F9CE 1F3FE 200D 2640

:*::woman-kneeling:-medium-dark-skin-tone::
Send,🧎🏾‍♀
return

;1F9CE 1F3FF 200D 2640 FE0F

:*::woman-kneeling:-dark-skin-tone::
Send,🧎🏿‍♀️
return

;1F9CE 1F3FF 200D 2640

:*::woman-kneeling:-dark-skin-tone::
Send,🧎🏿‍♀
return

;1F468 200D 1F9AF

:*::man-with-probing-cane::
Send,👨‍🦯
return

;1F468 1F3FB 200D 1F9AF

:*::man-with-probing-cane:-light-skin-tone::
Send,👨🏻‍🦯
return

;1F468 1F3FC 200D 1F9AF

:*::man-with-probing-cane:-medium-light-ski::
Send,👨🏼‍🦯
return

;1F468 1F3FD 200D 1F9AF

:*::man-with-probing-cane:-medium-skin-tone::
Send,👨🏽‍🦯
return

;1F468 1F3FE 200D 1F9AF

:*::man-with-probing-cane:-medium-dark-skin::
Send,👨🏾‍🦯
return

;1F468 1F3FF 200D 1F9AF

:*::man-with-probing-cane:-dark-skin-tone::
Send,👨🏿‍🦯
return

;1F469 200D 1F9AF

:*::woman-with-probing-cane::
Send,👩‍🦯
return

;1F469 1F3FB 200D 1F9AF

:*::woman-with-probing-cane:-light-skin-ton::
Send,👩🏻‍🦯
return

;1F469 1F3FC 200D 1F9AF

:*::woman-with-probing-cane:-medium-light-s::
Send,👩🏼‍🦯
return

;1F469 1F3FD 200D 1F9AF

:*::woman-with-probing-cane:-medium-skin-to::
Send,👩🏽‍🦯
return

;1F469 1F3FE 200D 1F9AF

:*::woman-with-probing-cane:-medium-dark-sk::
Send,👩🏾‍🦯
return

;1F469 1F3FF 200D 1F9AF

:*::woman-with-probing-cane:-dark-skin-tone::
Send,👩🏿‍🦯
return

;1F468 200D 1F9BC

:*::man-in-motorized-wheelchair::
Send,👨‍🦼
return

;1F468 1F3FB 200D 1F9BC

:*::man-in-motorized-wheelchair:-light-skin::
Send,👨🏻‍🦼
return

;1F468 1F3FC 200D 1F9BC

:*::man-in-motorized-wheelchair:-medium-lig::
Send,👨🏼‍🦼
return

;1F468 1F3FD 200D 1F9BC

:*::man-in-motorized-wheelchair:-medium-ski::
Send,👨🏽‍🦼
return

;1F468 1F3FE 200D 1F9BC

:*::man-in-motorized-wheelchair:-medium-dar::
Send,👨🏾‍🦼
return

;1F468 1F3FF 200D 1F9BC

:*::man-in-motorized-wheelchair:-dark-skin-::
Send,👨🏿‍🦼
return

;1F469 200D 1F9BC

:*::woman-in-motorized-wheelchair::
Send,👩‍🦼
return

;1F469 1F3FB 200D 1F9BC

:*::woman-in-motorized-wheelchair:-light-sk::
Send,👩🏻‍🦼
return

;1F469 1F3FC 200D 1F9BC

:*::woman-in-motorized-wheelchair:-medium-l::
Send,👩🏼‍🦼
return

;1F469 1F3FD 200D 1F9BC

:*::woman-in-motorized-wheelchair:-medium-s::
Send,👩🏽‍🦼
return

;1F469 1F3FE 200D 1F9BC

:*::woman-in-motorized-wheelchair:-medium-d::
Send,👩🏾‍🦼
return

;1F469 1F3FF 200D 1F9BC

:*::woman-in-motorized-wheelchair:-dark-ski::
Send,👩🏿‍🦼
return

;1F468 200D 1F9BD

:*::man-in-manual-wheelchair::
Send,👨‍🦽
return

;1F468 1F3FB 200D 1F9BD

:*::man-in-manual-wheelchair:-light-skin-to::
Send,👨🏻‍🦽
return

;1F468 1F3FC 200D 1F9BD

:*::man-in-manual-wheelchair:-medium-light-::
Send,👨🏼‍🦽
return

;1F468 1F3FD 200D 1F9BD

:*::man-in-manual-wheelchair:-medium-skin-t::
Send,👨🏽‍🦽
return

;1F468 1F3FE 200D 1F9BD

:*::man-in-manual-wheelchair:-medium-dark-s::
Send,👨🏾‍🦽
return

;1F468 1F3FF 200D 1F9BD

:*::man-in-manual-wheelchair:-dark-skin-ton::
Send,👨🏿‍🦽
return

;1F469 200D 1F9BD

:*::woman-in-manual-wheelchair::
Send,👩‍🦽
return

;1F469 1F3FB 200D 1F9BD

:*::woman-in-manual-wheelchair:-light-skin-::
Send,👩🏻‍🦽
return

;1F469 1F3FC 200D 1F9BD

:*::woman-in-manual-wheelchair:-medium-ligh::
Send,👩🏼‍🦽
return

;1F469 1F3FD 200D 1F9BD

:*::woman-in-manual-wheelchair:-medium-skin::
Send,👩🏽‍🦽
return

;1F469 1F3FE 200D 1F9BD

:*::woman-in-manual-wheelchair:-medium-dark::
Send,👩🏾‍🦽
return

;1F469 1F3FF 200D 1F9BD

:*::woman-in-manual-wheelchair:-dark-skin-t::
Send,👩🏿‍🦽
return

;1F3C3

:*::person-running::
Send,🏃
return

;1F3C3 1F3FB

:*::person-running:-light-skin-tone::
Send,🏃🏻
return

;1F3C3 1F3FC

:*::person-running:-medium-light-skin-tone::
Send,🏃🏼
return

;1F3C3 1F3FD

:*::person-running:-medium-skin-tone::
Send,🏃🏽
return

;1F3C3 1F3FE

:*::person-running:-medium-dark-skin-tone::
Send,🏃🏾
return

;1F3C3 1F3FF

:*::person-running:-dark-skin-tone::
Send,🏃🏿
return

;1F3C3 200D 2642 FE0F

:*::man-running::
Send,🏃‍♂️
return

;1F3C3 200D 2642

:*::man-running::
Send,🏃‍♂
return

;1F3C3 1F3FB 200D 2642 FE0F

:*::man-running:-light-skin-tone::
Send,🏃🏻‍♂️
return

;1F3C3 1F3FB 200D 2642

:*::man-running:-light-skin-tone::
Send,🏃🏻‍♂
return

;1F3C3 1F3FC 200D 2642 FE0F

:*::man-running:-medium-light-skin-tone::
Send,🏃🏼‍♂️
return

;1F3C3 1F3FC 200D 2642

:*::man-running:-medium-light-skin-tone::
Send,🏃🏼‍♂
return

;1F3C3 1F3FD 200D 2642 FE0F

:*::man-running:-medium-skin-tone::
Send,🏃🏽‍♂️
return

;1F3C3 1F3FD 200D 2642

:*::man-running:-medium-skin-tone::
Send,🏃🏽‍♂
return

;1F3C3 1F3FE 200D 2642 FE0F

:*::man-running:-medium-dark-skin-tone::
Send,🏃🏾‍♂️
return

;1F3C3 1F3FE 200D 2642

:*::man-running:-medium-dark-skin-tone::
Send,🏃🏾‍♂
return

;1F3C3 1F3FF 200D 2642 FE0F

:*::man-running:-dark-skin-tone::
Send,🏃🏿‍♂️
return

;1F3C3 1F3FF 200D 2642

:*::man-running:-dark-skin-tone::
Send,🏃🏿‍♂
return

;1F3C3 200D 2640 FE0F
:*::boy-runner::
:*::woman-running::
Send,🏃‍♀️
return

;1F3C3 200D 2640
:*::boy-runner::
:*::woman-running::
Send,🏃‍♀
return

;1F3C3 1F3FB 200D 2640 FE0F

:*::woman-running:-light-skin-tone::
Send,🏃🏻‍♀️
return

;1F3C3 1F3FB 200D 2640

:*::woman-running:-light-skin-tone::
Send,🏃🏻‍♀
return

;1F3C3 1F3FC 200D 2640 FE0F

:*::woman-running:-medium-light-skin-tone::
Send,🏃🏼‍♀️
return

;1F3C3 1F3FC 200D 2640

:*::woman-running:-medium-light-skin-tone::
Send,🏃🏼‍♀
return

;1F3C3 1F3FD 200D 2640 FE0F

:*::woman-running:-medium-skin-tone::
Send,🏃🏽‍♀️
return

;1F3C3 1F3FD 200D 2640

:*::woman-running:-medium-skin-tone::
Send,🏃🏽‍♀
return

;1F3C3 1F3FE 200D 2640 FE0F

:*::woman-running:-medium-dark-skin-tone::
Send,🏃🏾‍♀️
return

;1F3C3 1F3FE 200D 2640

:*::woman-running:-medium-dark-skin-tone::
Send,🏃🏾‍♀
return

;1F3C3 1F3FF 200D 2640 FE0F

:*::woman-running:-dark-skin-tone::
Send,🏃🏿‍♀️
return

;1F3C3 1F3FF 200D 2640

:*::woman-running:-dark-skin-tone::
Send,🏃🏿‍♀
return

;1F483

:*::woman-dancing::
Send,💃
return

;1F483 1F3FB

:*::woman-dancing:-light-skin-tone::
Send,💃🏻
return

;1F483 1F3FC

:*::woman-dancing:-medium-light-skin-tone::
Send,💃🏼
return

;1F483 1F3FD

:*::woman-dancing:-medium-skin-tone::
Send,💃🏽
return

;1F483 1F3FE

:*::woman-dancing:-medium-dark-skin-tone::
Send,💃🏾
return

;1F483 1F3FF

:*::woman-dancing:-dark-skin-tone::
Send,💃🏿
return

;1F57A
:*::disco-dancer::
:*::male-dancer::
:*::man-dancing::
Send,🕺
return

;1F57A 1F3FB

:*::man-dancing:-light-skin-tone::
Send,🕺🏻
return

;1F57A 1F3FC

:*::man-dancing:-medium-light-skin-tone::
Send,🕺🏼
return

;1F57A 1F3FD

:*::man-dancing:-medium-skin-tone::
Send,🕺🏽
return

;1F57A 1F3FE

:*::man-dancing:-medium-dark-skin-tone::
Send,🕺🏾
return

;1F57A 1F3FF

:*::man-dancing:-dark-skin-tone::
Send,🕺🏿
return

;1F574 FE0F

:*::man-in-suit-levitating::
Send,🕴️
return

;1F574

:*::man-in-suit-levitating::
Send,🕴
return

;1F574 1F3FB

:*::man-in-suit-levitating:-light-skin-tone::
Send,🕴🏻
return

;1F574 1F3FC

:*::man-in-suit-levitating:-medium-light-sk::
Send,🕴🏼
return

;1F574 1F3FD

:*::man-in-suit-levitating:-medium-skin-ton::
Send,🕴🏽
return

;1F574 1F3FE

:*::man-in-suit-levitating:-medium-dark-ski::
Send,🕴🏾
return

;1F574 1F3FF

:*::man-in-suit-levitating:-dark-skin-tone::
Send,🕴🏿
return

;1F46F

:*::people-with-bunny-ears::
Send,👯
return

;1F46F 200D 2642 FE0F

:*::men-with-bunny-ears::
Send,👯‍♂️
return

;1F46F 200D 2642

:*::men-with-bunny-ears::
Send,👯‍♂
return

;1F46F 200D 2640 FE0F

:*::women-with-bunny-ears::
Send,👯‍♀️
return

;1F46F 200D 2640

:*::women-with-bunny-ears::
Send,👯‍♀
return

;1F9D6
:*::sauna::
:*::person-in-steamy-room::
Send,🧖
return

;1F9D6 1F3FB

:*::person-in-steamy-room:-light-skin-tone::
Send,🧖🏻
return

;1F9D6 1F3FC

:*::person-in-steamy-room:-medium-light-ski::
Send,🧖🏼
return

;1F9D6 1F3FD

:*::person-in-steamy-room:-medium-skin-tone::
Send,🧖🏽
return

;1F9D6 1F3FE

:*::person-in-steamy-room:-medium-dark-skin::
Send,🧖🏾
return

;1F9D6 1F3FF

:*::person-in-steamy-room:-dark-skin-tone::
Send,🧖🏿
return

;1F9D6 200D 2642 FE0F

:*::man-in-steamy-room::
Send,🧖‍♂️
return

;1F9D6 200D 2642

:*::man-in-steamy-room::
Send,🧖‍♂
return

;1F9D6 1F3FB 200D 2642 FE0F

:*::man-in-steamy-room:-light-skin-tone::
Send,🧖🏻‍♂️
return

;1F9D6 1F3FB 200D 2642

:*::man-in-steamy-room:-light-skin-tone::
Send,🧖🏻‍♂
return

;1F9D6 1F3FC 200D 2642 FE0F

:*::man-in-steamy-room:-medium-light-skin-t::
Send,🧖🏼‍♂️
return

;1F9D6 1F3FC 200D 2642

:*::man-in-steamy-room:-medium-light-skin-t::
Send,🧖🏼‍♂
return

;1F9D6 1F3FD 200D 2642 FE0F

:*::man-in-steamy-room:-medium-skin-tone::
Send,🧖🏽‍♂️
return

;1F9D6 1F3FD 200D 2642

:*::man-in-steamy-room:-medium-skin-tone::
Send,🧖🏽‍♂
return

;1F9D6 1F3FE 200D 2642 FE0F

:*::man-in-steamy-room:-medium-dark-skin-to::
Send,🧖🏾‍♂️
return

;1F9D6 1F3FE 200D 2642

:*::man-in-steamy-room:-medium-dark-skin-to::
Send,🧖🏾‍♂
return

;1F9D6 1F3FF 200D 2642 FE0F

:*::man-in-steamy-room:-dark-skin-tone::
Send,🧖🏿‍♂️
return

;1F9D6 1F3FF 200D 2642

:*::man-in-steamy-room:-dark-skin-tone::
Send,🧖🏿‍♂
return

;1F9D6 200D 2640 FE0F

:*::woman-in-steamy-room::
Send,🧖‍♀️
return

;1F9D6 200D 2640

:*::woman-in-steamy-room::
Send,🧖‍♀
return

;1F9D6 1F3FB 200D 2640 FE0F

:*::woman-in-steamy-room:-light-skin-tone::
Send,🧖🏻‍♀️
return

;1F9D6 1F3FB 200D 2640

:*::woman-in-steamy-room:-light-skin-tone::
Send,🧖🏻‍♀
return

;1F9D6 1F3FC 200D 2640 FE0F

:*::woman-in-steamy-room:-medium-light-skin::
Send,🧖🏼‍♀️
return

;1F9D6 1F3FC 200D 2640

:*::woman-in-steamy-room:-medium-light-skin::
Send,🧖🏼‍♀
return

;1F9D6 1F3FD 200D 2640 FE0F

:*::woman-in-steamy-room:-medium-skin-tone::
Send,🧖🏽‍♀️
return

;1F9D6 1F3FD 200D 2640

:*::woman-in-steamy-room:-medium-skin-tone::
Send,🧖🏽‍♀
return

;1F9D6 1F3FE 200D 2640 FE0F

:*::woman-in-steamy-room:-medium-dark-skin-::
Send,🧖🏾‍♀️
return

;1F9D6 1F3FE 200D 2640

:*::woman-in-steamy-room:-medium-dark-skin-::
Send,🧖🏾‍♀
return

;1F9D6 1F3FF 200D 2640 FE0F

:*::woman-in-steamy-room:-dark-skin-tone::
Send,🧖🏿‍♀️
return

;1F9D6 1F3FF 200D 2640

:*::woman-in-steamy-room:-dark-skin-tone::
Send,🧖🏿‍♀
return

;1F9D7
:*::climber::
:*::rock-climbing::
:*::person-climbing::
Send,🧗
return

;1F9D7 1F3FB

:*::person-climbing:-light-skin-tone::
Send,🧗🏻
return

;1F9D7 1F3FC

:*::person-climbing:-medium-light-skin-tone::
Send,🧗🏼
return

;1F9D7 1F3FD

:*::person-climbing:-medium-skin-tone::
Send,🧗🏽
return

;1F9D7 1F3FE

:*::person-climbing:-medium-dark-skin-tone::
Send,🧗🏾
return

;1F9D7 1F3FF

:*::person-climbing:-dark-skin-tone::
Send,🧗🏿
return

;1F9D7 200D 2642 FE0F
:*::male-rock-climber::
:*::man-climber::
:*::man-climbing::
Send,🧗‍♂️
return

;1F9D7 200D 2642
:*::male-rock-climber::
:*::man-climber::
:*::man-climbing::
Send,🧗‍♂
return

;1F9D7 1F3FB 200D 2642 FE0F

:*::man-climbing:-light-skin-tone::
Send,🧗🏻‍♂️
return

;1F9D7 1F3FB 200D 2642

:*::man-climbing:-light-skin-tone::
Send,🧗🏻‍♂
return

;1F9D7 1F3FC 200D 2642 FE0F

:*::man-climbing:-medium-light-skin-tone::
Send,🧗🏼‍♂️
return

;1F9D7 1F3FC 200D 2642

:*::man-climbing:-medium-light-skin-tone::
Send,🧗🏼‍♂
return

;1F9D7 1F3FD 200D 2642 FE0F

:*::man-climbing:-medium-skin-tone::
Send,🧗🏽‍♂️
return

;1F9D7 1F3FD 200D 2642

:*::man-climbing:-medium-skin-tone::
Send,🧗🏽‍♂
return

;1F9D7 1F3FE 200D 2642 FE0F

:*::man-climbing:-medium-dark-skin-tone::
Send,🧗🏾‍♂️
return

;1F9D7 1F3FE 200D 2642

:*::man-climbing:-medium-dark-skin-tone::
Send,🧗🏾‍♂
return

;1F9D7 1F3FF 200D 2642 FE0F

:*::man-climbing:-dark-skin-tone::
Send,🧗🏿‍♂️
return

;1F9D7 1F3FF 200D 2642

:*::man-climbing:-dark-skin-tone::
Send,🧗🏿‍♂
return

;1F9D7 200D 2640 FE0F
:*::female-rock-climber::
:*::woman-climber::
:*::woman-climbing::
Send,🧗‍♀️
return

;1F9D7 200D 2640
:*::female-rock-climber::
:*::woman-climber::
:*::woman-climbing::
Send,🧗‍♀
return

;1F9D7 1F3FB 200D 2640 FE0F

:*::woman-climbing:-light-skin-tone::
Send,🧗🏻‍♀️
return

;1F9D7 1F3FB 200D 2640

:*::woman-climbing:-light-skin-tone::
Send,🧗🏻‍♀
return

;1F9D7 1F3FC 200D 2640 FE0F

:*::woman-climbing:-medium-light-skin-tone::
Send,🧗🏼‍♀️
return

;1F9D7 1F3FC 200D 2640

:*::woman-climbing:-medium-light-skin-tone::
Send,🧗🏼‍♀
return

;1F9D7 1F3FD 200D 2640 FE0F

:*::woman-climbing:-medium-skin-tone::
Send,🧗🏽‍♀️
return

;1F9D7 1F3FD 200D 2640

:*::woman-climbing:-medium-skin-tone::
Send,🧗🏽‍♀
return

;1F9D7 1F3FE 200D 2640 FE0F

:*::woman-climbing:-medium-dark-skin-tone::
Send,🧗🏾‍♀️
return

;1F9D7 1F3FE 200D 2640

:*::woman-climbing:-medium-dark-skin-tone::
Send,🧗🏾‍♀
return

;1F9D7 1F3FF 200D 2640 FE0F

:*::woman-climbing:-dark-skin-tone::
Send,🧗🏿‍♀️
return

;1F9D7 1F3FF 200D 2640

:*::woman-climbing:-dark-skin-tone::
Send,🧗🏿‍♀
return

;1F93A

:*::person-fencing::
Send,🤺
return

;1F3C7
:*::horse-race::
:*::jockey::
:*::horse-racing::
Send,🏇
return

;1F3C7 1F3FB

:*::horse-racing:-light-skin-tone::
Send,🏇🏻
return

;1F3C7 1F3FC

:*::horse-racing:-medium-light-skin-tone::
Send,🏇🏼
return

;1F3C7 1F3FD

:*::horse-racing:-medium-skin-tone::
Send,🏇🏽
return

;1F3C7 1F3FE

:*::horse-racing:-medium-dark-skin-tone::
Send,🏇🏾
return

;1F3C7 1F3FF

:*::horse-racing:-dark-skin-tone::
Send,🏇🏿
return

;26F7 FE0F

:*::skier::
Send,⛷️
return

;26F7

:*::skier::
Send,⛷
return

;1F3C2
:*::snowboard::
:*::snowboarding::
:*::snowboarder::
Send,🏂
return

;1F3C2 1F3FB

:*::snowboarder:-light-skin-tone::
Send,🏂🏻
return

;1F3C2 1F3FC

:*::snowboarder:-medium-light-skin-tone::
Send,🏂🏼
return

;1F3C2 1F3FD

:*::snowboarder:-medium-skin-tone::
Send,🏂🏽
return

;1F3C2 1F3FE

:*::snowboarder:-medium-dark-skin-tone::
Send,🏂🏾
return

;1F3C2 1F3FF

:*::snowboarder:-dark-skin-tone::
Send,🏂🏿
return

;1F3CC FE0F

:*::person-golfing::
Send,🏌️
return

;1F3CC

:*::person-golfing::
Send,🏌
return

;1F3CC 1F3FB

:*::person-golfing:-light-skin-tone::
Send,🏌🏻
return

;1F3CC 1F3FC

:*::person-golfing:-medium-light-skin-tone::
Send,🏌🏼
return

;1F3CC 1F3FD

:*::person-golfing:-medium-skin-tone::
Send,🏌🏽
return

;1F3CC 1F3FE

:*::person-golfing:-medium-dark-skin-tone::
Send,🏌🏾
return

;1F3CC 1F3FF

:*::person-golfing:-dark-skin-tone::
Send,🏌🏿
return

;1F3CC FE0F 200D 2642 FE0F

:*::man-golfing::
Send,🏌️‍♂️
return

;1F3CC 200D 2642 FE0F

:*::man-golfing::
Send,🏌‍♂️
return

;1F3CC FE0F 200D 2642

:*::man-golfing::
Send,🏌️‍♂
return

;1F3CC 200D 2642

:*::man-golfing::
Send,🏌‍♂
return

;1F3CC 1F3FB 200D 2642 FE0F

:*::man-golfing:-light-skin-tone::
Send,🏌🏻‍♂️
return

;1F3CC 1F3FB 200D 2642

:*::man-golfing:-light-skin-tone::
Send,🏌🏻‍♂
return

;1F3CC 1F3FC 200D 2642 FE0F

:*::man-golfing:-medium-light-skin-tone::
Send,🏌🏼‍♂️
return

;1F3CC 1F3FC 200D 2642

:*::man-golfing:-medium-light-skin-tone::
Send,🏌🏼‍♂
return

;1F3CC 1F3FD 200D 2642 FE0F

:*::man-golfing:-medium-skin-tone::
Send,🏌🏽‍♂️
return

;1F3CC 1F3FD 200D 2642

:*::man-golfing:-medium-skin-tone::
Send,🏌🏽‍♂
return

;1F3CC 1F3FE 200D 2642 FE0F

:*::man-golfing:-medium-dark-skin-tone::
Send,🏌🏾‍♂️
return

;1F3CC 1F3FE 200D 2642

:*::man-golfing:-medium-dark-skin-tone::
Send,🏌🏾‍♂
return

;1F3CC 1F3FF 200D 2642 FE0F

:*::man-golfing:-dark-skin-tone::
Send,🏌🏿‍♂️
return

;1F3CC 1F3FF 200D 2642

:*::man-golfing:-dark-skin-tone::
Send,🏌🏿‍♂
return

;1F3CC FE0F 200D 2640 FE0F

:*::woman-golfing::
Send,🏌️‍♀️
return

;1F3CC 200D 2640 FE0F

:*::woman-golfing::
Send,🏌‍♀️
return

;1F3CC FE0F 200D 2640

:*::woman-golfing::
Send,🏌️‍♀
return

;1F3CC 200D 2640

:*::woman-golfing::
Send,🏌‍♀
return

;1F3CC 1F3FB 200D 2640 FE0F

:*::woman-golfing:-light-skin-tone::
Send,🏌🏻‍♀️
return

;1F3CC 1F3FB 200D 2640

:*::woman-golfing:-light-skin-tone::
Send,🏌🏻‍♀
return

;1F3CC 1F3FC 200D 2640 FE0F

:*::woman-golfing:-medium-light-skin-tone::
Send,🏌🏼‍♀️
return

;1F3CC 1F3FC 200D 2640

:*::woman-golfing:-medium-light-skin-tone::
Send,🏌🏼‍♀
return

;1F3CC 1F3FD 200D 2640 FE0F

:*::woman-golfing:-medium-skin-tone::
Send,🏌🏽‍♀️
return

;1F3CC 1F3FD 200D 2640

:*::woman-golfing:-medium-skin-tone::
Send,🏌🏽‍♀
return

;1F3CC 1F3FE 200D 2640 FE0F

:*::woman-golfing:-medium-dark-skin-tone::
Send,🏌🏾‍♀️
return

;1F3CC 1F3FE 200D 2640

:*::woman-golfing:-medium-dark-skin-tone::
Send,🏌🏾‍♀
return

;1F3CC 1F3FF 200D 2640 FE0F

:*::woman-golfing:-dark-skin-tone::
Send,🏌🏿‍♀️
return

;1F3CC 1F3FF 200D 2640

:*::woman-golfing:-dark-skin-tone::
Send,🏌🏿‍♀
return

;1F3C4

:*::person-surfing::
Send,🏄
return

;1F3C4 1F3FB

:*::person-surfing:-light-skin-tone::
Send,🏄🏻
return

;1F3C4 1F3FC

:*::person-surfing:-medium-light-skin-tone::
Send,🏄🏼
return

;1F3C4 1F3FD

:*::person-surfing:-medium-skin-tone::
Send,🏄🏽
return

;1F3C4 1F3FE

:*::person-surfing:-medium-dark-skin-tone::
Send,🏄🏾
return

;1F3C4 1F3FF

:*::person-surfing:-dark-skin-tone::
Send,🏄🏿
return

;1F3C4 200D 2642 FE0F

:*::man-surfing::
Send,🏄‍♂️
return

;1F3C4 200D 2642

:*::man-surfing::
Send,🏄‍♂
return

;1F3C4 1F3FB 200D 2642 FE0F

:*::man-surfing:-light-skin-tone::
Send,🏄🏻‍♂️
return

;1F3C4 1F3FB 200D 2642

:*::man-surfing:-light-skin-tone::
Send,🏄🏻‍♂
return

;1F3C4 1F3FC 200D 2642 FE0F

:*::man-surfing:-medium-light-skin-tone::
Send,🏄🏼‍♂️
return

;1F3C4 1F3FC 200D 2642

:*::man-surfing:-medium-light-skin-tone::
Send,🏄🏼‍♂
return

;1F3C4 1F3FD 200D 2642 FE0F

:*::man-surfing:-medium-skin-tone::
Send,🏄🏽‍♂️
return

;1F3C4 1F3FD 200D 2642

:*::man-surfing:-medium-skin-tone::
Send,🏄🏽‍♂
return

;1F3C4 1F3FE 200D 2642 FE0F

:*::man-surfing:-medium-dark-skin-tone::
Send,🏄🏾‍♂️
return

;1F3C4 1F3FE 200D 2642

:*::man-surfing:-medium-dark-skin-tone::
Send,🏄🏾‍♂
return

;1F3C4 1F3FF 200D 2642 FE0F

:*::man-surfing:-dark-skin-tone::
Send,🏄🏿‍♂️
return

;1F3C4 1F3FF 200D 2642

:*::man-surfing:-dark-skin-tone::
Send,🏄🏿‍♂
return

;1F3C4 200D 2640 FE0F

:*::woman-surfing::
Send,🏄‍♀️
return

;1F3C4 200D 2640

:*::woman-surfing::
Send,🏄‍♀
return

;1F3C4 1F3FB 200D 2640 FE0F

:*::woman-surfing:-light-skin-tone::
Send,🏄🏻‍♀️
return

;1F3C4 1F3FB 200D 2640

:*::woman-surfing:-light-skin-tone::
Send,🏄🏻‍♀
return

;1F3C4 1F3FC 200D 2640 FE0F

:*::woman-surfing:-medium-light-skin-tone::
Send,🏄🏼‍♀️
return

;1F3C4 1F3FC 200D 2640

:*::woman-surfing:-medium-light-skin-tone::
Send,🏄🏼‍♀
return

;1F3C4 1F3FD 200D 2640 FE0F

:*::woman-surfing:-medium-skin-tone::
Send,🏄🏽‍♀️
return

;1F3C4 1F3FD 200D 2640

:*::woman-surfing:-medium-skin-tone::
Send,🏄🏽‍♀
return

;1F3C4 1F3FE 200D 2640 FE0F

:*::woman-surfing:-medium-dark-skin-tone::
Send,🏄🏾‍♀️
return

;1F3C4 1F3FE 200D 2640

:*::woman-surfing:-medium-dark-skin-tone::
Send,🏄🏾‍♀
return

;1F3C4 1F3FF 200D 2640 FE0F

:*::woman-surfing:-dark-skin-tone::
Send,🏄🏿‍♀️
return

;1F3C4 1F3FF 200D 2640

:*::woman-surfing:-dark-skin-tone::
Send,🏄🏿‍♀
return

;1F6A3

:*::person-rowing-boat::
Send,🚣
return

;1F6A3 1F3FB

:*::person-rowing-boat:-light-skin-tone::
Send,🚣🏻
return

;1F6A3 1F3FC

:*::person-rowing-boat:-medium-light-skin-t::
Send,🚣🏼
return

;1F6A3 1F3FD

:*::person-rowing-boat:-medium-skin-tone::
Send,🚣🏽
return

;1F6A3 1F3FE

:*::person-rowing-boat:-medium-dark-skin-to::
Send,🚣🏾
return

;1F6A3 1F3FF

:*::person-rowing-boat:-dark-skin-tone::
Send,🚣🏿
return

;1F6A3 200D 2642 FE0F

:*::man-rowing-boat::
Send,🚣‍♂️
return

;1F6A3 200D 2642

:*::man-rowing-boat::
Send,🚣‍♂
return

;1F6A3 1F3FB 200D 2642 FE0F

:*::man-rowing-boat:-light-skin-tone::
Send,🚣🏻‍♂️
return

;1F6A3 1F3FB 200D 2642

:*::man-rowing-boat:-light-skin-tone::
Send,🚣🏻‍♂
return

;1F6A3 1F3FC 200D 2642 FE0F

:*::man-rowing-boat:-medium-light-skin-tone::
Send,🚣🏼‍♂️
return

;1F6A3 1F3FC 200D 2642

:*::man-rowing-boat:-medium-light-skin-tone::
Send,🚣🏼‍♂
return

;1F6A3 1F3FD 200D 2642 FE0F

:*::man-rowing-boat:-medium-skin-tone::
Send,🚣🏽‍♂️
return

;1F6A3 1F3FD 200D 2642

:*::man-rowing-boat:-medium-skin-tone::
Send,🚣🏽‍♂
return

;1F6A3 1F3FE 200D 2642 FE0F

:*::man-rowing-boat:-medium-dark-skin-tone::
Send,🚣🏾‍♂️
return

;1F6A3 1F3FE 200D 2642

:*::man-rowing-boat:-medium-dark-skin-tone::
Send,🚣🏾‍♂
return

;1F6A3 1F3FF 200D 2642 FE0F

:*::man-rowing-boat:-dark-skin-tone::
Send,🚣🏿‍♂️
return

;1F6A3 1F3FF 200D 2642

:*::man-rowing-boat:-dark-skin-tone::
Send,🚣🏿‍♂
return

;1F6A3 200D 2640 FE0F

:*::woman-rowing-boat::
Send,🚣‍♀️
return

;1F6A3 200D 2640

:*::woman-rowing-boat::
Send,🚣‍♀
return

;1F6A3 1F3FB 200D 2640 FE0F

:*::woman-rowing-boat:-light-skin-tone::
Send,🚣🏻‍♀️
return

;1F6A3 1F3FB 200D 2640

:*::woman-rowing-boat:-light-skin-tone::
Send,🚣🏻‍♀
return

;1F6A3 1F3FC 200D 2640 FE0F

:*::woman-rowing-boat:-medium-light-skin-to::
Send,🚣🏼‍♀️
return

;1F6A3 1F3FC 200D 2640

:*::woman-rowing-boat:-medium-light-skin-to::
Send,🚣🏼‍♀
return

;1F6A3 1F3FD 200D 2640 FE0F

:*::woman-rowing-boat:-medium-skin-tone::
Send,🚣🏽‍♀️
return

;1F6A3 1F3FD 200D 2640

:*::woman-rowing-boat:-medium-skin-tone::
Send,🚣🏽‍♀
return

;1F6A3 1F3FE 200D 2640 FE0F

:*::woman-rowing-boat:-medium-dark-skin-ton::
Send,🚣🏾‍♀️
return

;1F6A3 1F3FE 200D 2640

:*::woman-rowing-boat:-medium-dark-skin-ton::
Send,🚣🏾‍♀
return

;1F6A3 1F3FF 200D 2640 FE0F

:*::woman-rowing-boat:-dark-skin-tone::
Send,🚣🏿‍♀️
return

;1F6A3 1F3FF 200D 2640

:*::woman-rowing-boat:-dark-skin-tone::
Send,🚣🏿‍♀
return

;1F3CA

:*::person-swimming::
Send,🏊
return

;1F3CA 1F3FB

:*::person-swimming:-light-skin-tone::
Send,🏊🏻
return

;1F3CA 1F3FC

:*::person-swimming:-medium-light-skin-tone::
Send,🏊🏼
return

;1F3CA 1F3FD

:*::person-swimming:-medium-skin-tone::
Send,🏊🏽
return

;1F3CA 1F3FE

:*::person-swimming:-medium-dark-skin-tone::
Send,🏊🏾
return

;1F3CA 1F3FF

:*::person-swimming:-dark-skin-tone::
Send,🏊🏿
return

;1F3CA 200D 2642 FE0F

:*::man-swimming::
Send,🏊‍♂️
return

;1F3CA 200D 2642

:*::man-swimming::
Send,🏊‍♂
return

;1F3CA 1F3FB 200D 2642 FE0F

:*::man-swimming:-light-skin-tone::
Send,🏊🏻‍♂️
return

;1F3CA 1F3FB 200D 2642

:*::man-swimming:-light-skin-tone::
Send,🏊🏻‍♂
return

;1F3CA 1F3FC 200D 2642 FE0F

:*::man-swimming:-medium-light-skin-tone::
Send,🏊🏼‍♂️
return

;1F3CA 1F3FC 200D 2642

:*::man-swimming:-medium-light-skin-tone::
Send,🏊🏼‍♂
return

;1F3CA 1F3FD 200D 2642 FE0F

:*::man-swimming:-medium-skin-tone::
Send,🏊🏽‍♂️
return

;1F3CA 1F3FD 200D 2642

:*::man-swimming:-medium-skin-tone::
Send,🏊🏽‍♂
return

;1F3CA 1F3FE 200D 2642 FE0F

:*::man-swimming:-medium-dark-skin-tone::
Send,🏊🏾‍♂️
return

;1F3CA 1F3FE 200D 2642

:*::man-swimming:-medium-dark-skin-tone::
Send,🏊🏾‍♂
return

;1F3CA 1F3FF 200D 2642 FE0F

:*::man-swimming:-dark-skin-tone::
Send,🏊🏿‍♂️
return

;1F3CA 1F3FF 200D 2642

:*::man-swimming:-dark-skin-tone::
Send,🏊🏿‍♂
return

;1F3CA 200D 2640 FE0F

:*::woman-swimming::
Send,🏊‍♀️
return

;1F3CA 200D 2640

:*::woman-swimming::
Send,🏊‍♀
return

;1F3CA 1F3FB 200D 2640 FE0F

:*::woman-swimming:-light-skin-tone::
Send,🏊🏻‍♀️
return

;1F3CA 1F3FB 200D 2640

:*::woman-swimming:-light-skin-tone::
Send,🏊🏻‍♀
return

;1F3CA 1F3FC 200D 2640 FE0F

:*::woman-swimming:-medium-light-skin-tone::
Send,🏊🏼‍♀️
return

;1F3CA 1F3FC 200D 2640

:*::woman-swimming:-medium-light-skin-tone::
Send,🏊🏼‍♀
return

;1F3CA 1F3FD 200D 2640 FE0F

:*::woman-swimming:-medium-skin-tone::
Send,🏊🏽‍♀️
return

;1F3CA 1F3FD 200D 2640

:*::woman-swimming:-medium-skin-tone::
Send,🏊🏽‍♀
return

;1F3CA 1F3FE 200D 2640 FE0F

:*::woman-swimming:-medium-dark-skin-tone::
Send,🏊🏾‍♀️
return

;1F3CA 1F3FE 200D 2640

:*::woman-swimming:-medium-dark-skin-tone::
Send,🏊🏾‍♀
return

;1F3CA 1F3FF 200D 2640 FE0F

:*::woman-swimming:-dark-skin-tone::
Send,🏊🏿‍♀️
return

;1F3CA 1F3FF 200D 2640

:*::woman-swimming:-dark-skin-tone::
Send,🏊🏿‍♀
return

;26F9 FE0F

:*::person-bouncing-ball::
Send,⛹️
return

;26F9

:*::person-bouncing-ball::
Send,⛹
return

;26F9 1F3FB

:*::person-bouncing-ball:-light-skin-tone::
Send,⛹🏻
return

;26F9 1F3FC

:*::person-bouncing-ball:-medium-light-skin::
Send,⛹🏼
return

;26F9 1F3FD

:*::person-bouncing-ball:-medium-skin-tone::
Send,⛹🏽
return

;26F9 1F3FE

:*::person-bouncing-ball:-medium-dark-skin-::
Send,⛹🏾
return

;26F9 1F3FF

:*::person-bouncing-ball:-dark-skin-tone::
Send,⛹🏿
return

;26F9 FE0F 200D 2642 FE0F

:*::man-bouncing-ball::
Send,⛹️‍♂️
return

;26F9 200D 2642 FE0F

:*::man-bouncing-ball::
Send,⛹‍♂️
return

;26F9 FE0F 200D 2642

:*::man-bouncing-ball::
Send,⛹️‍♂
return

;26F9 200D 2642

:*::man-bouncing-ball::
Send,⛹‍♂
return

;26F9 1F3FB 200D 2642 FE0F

:*::man-bouncing-ball:-light-skin-tone::
Send,⛹🏻‍♂️
return

;26F9 1F3FB 200D 2642

:*::man-bouncing-ball:-light-skin-tone::
Send,⛹🏻‍♂
return

;26F9 1F3FC 200D 2642 FE0F

:*::man-bouncing-ball:-medium-light-skin-to::
Send,⛹🏼‍♂️
return

;26F9 1F3FC 200D 2642

:*::man-bouncing-ball:-medium-light-skin-to::
Send,⛹🏼‍♂
return

;26F9 1F3FD 200D 2642 FE0F

:*::man-bouncing-ball:-medium-skin-tone::
Send,⛹🏽‍♂️
return

;26F9 1F3FD 200D 2642

:*::man-bouncing-ball:-medium-skin-tone::
Send,⛹🏽‍♂
return

;26F9 1F3FE 200D 2642 FE0F

:*::man-bouncing-ball:-medium-dark-skin-ton::
Send,⛹🏾‍♂️
return

;26F9 1F3FE 200D 2642

:*::man-bouncing-ball:-medium-dark-skin-ton::
Send,⛹🏾‍♂
return

;26F9 1F3FF 200D 2642 FE0F

:*::man-bouncing-ball:-dark-skin-tone::
Send,⛹🏿‍♂️
return

;26F9 1F3FF 200D 2642

:*::man-bouncing-ball:-dark-skin-tone::
Send,⛹🏿‍♂
return

;26F9 FE0F 200D 2640 FE0F

:*::woman-bouncing-ball::
Send,⛹️‍♀️
return

;26F9 200D 2640 FE0F

:*::woman-bouncing-ball::
Send,⛹‍♀️
return

;26F9 FE0F 200D 2640

:*::woman-bouncing-ball::
Send,⛹️‍♀
return

;26F9 200D 2640

:*::woman-bouncing-ball::
Send,⛹‍♀
return

;26F9 1F3FB 200D 2640 FE0F

:*::woman-bouncing-ball:-light-skin-tone::
Send,⛹🏻‍♀️
return

;26F9 1F3FB 200D 2640

:*::woman-bouncing-ball:-light-skin-tone::
Send,⛹🏻‍♀
return

;26F9 1F3FC 200D 2640 FE0F

:*::woman-bouncing-ball:-medium-light-skin-::
Send,⛹🏼‍♀️
return

;26F9 1F3FC 200D 2640

:*::woman-bouncing-ball:-medium-light-skin-::
Send,⛹🏼‍♀
return

;26F9 1F3FD 200D 2640 FE0F

:*::woman-bouncing-ball:-medium-skin-tone::
Send,⛹🏽‍♀️
return

;26F9 1F3FD 200D 2640

:*::woman-bouncing-ball:-medium-skin-tone::
Send,⛹🏽‍♀
return

;26F9 1F3FE 200D 2640 FE0F

:*::woman-bouncing-ball:-medium-dark-skin-t::
Send,⛹🏾‍♀️
return

;26F9 1F3FE 200D 2640

:*::woman-bouncing-ball:-medium-dark-skin-t::
Send,⛹🏾‍♀
return

;26F9 1F3FF 200D 2640 FE0F

:*::woman-bouncing-ball:-dark-skin-tone::
Send,⛹🏿‍♀️
return

;26F9 1F3FF 200D 2640

:*::woman-bouncing-ball:-dark-skin-tone::
Send,⛹🏿‍♀
return

;1F3CB FE0F

:*::person-lifting-weights::
Send,🏋️
return

;1F3CB

:*::person-lifting-weights::
Send,🏋
return

;1F3CB 1F3FB

:*::person-lifting-weights:-light-skin-tone::
Send,🏋🏻
return

;1F3CB 1F3FC

:*::person-lifting-weights:-medium-light-sk::
Send,🏋🏼
return

;1F3CB 1F3FD

:*::person-lifting-weights:-medium-skin-ton::
Send,🏋🏽
return

;1F3CB 1F3FE

:*::person-lifting-weights:-medium-dark-ski::
Send,🏋🏾
return

;1F3CB 1F3FF

:*::person-lifting-weights:-dark-skin-tone::
Send,🏋🏿
return

;1F3CB FE0F 200D 2642 FE0F

:*::man-lifting-weights::
Send,🏋️‍♂️
return

;1F3CB 200D 2642 FE0F

:*::man-lifting-weights::
Send,🏋‍♂️
return

;1F3CB FE0F 200D 2642

:*::man-lifting-weights::
Send,🏋️‍♂
return

;1F3CB 200D 2642

:*::man-lifting-weights::
Send,🏋‍♂
return

;1F3CB 1F3FB 200D 2642 FE0F

:*::man-lifting-weights:-light-skin-tone::
Send,🏋🏻‍♂️
return

;1F3CB 1F3FB 200D 2642

:*::man-lifting-weights:-light-skin-tone::
Send,🏋🏻‍♂
return

;1F3CB 1F3FC 200D 2642 FE0F

:*::man-lifting-weights:-medium-light-skin-::
Send,🏋🏼‍♂️
return

;1F3CB 1F3FC 200D 2642

:*::man-lifting-weights:-medium-light-skin-::
Send,🏋🏼‍♂
return

;1F3CB 1F3FD 200D 2642 FE0F

:*::man-lifting-weights:-medium-skin-tone::
Send,🏋🏽‍♂️
return

;1F3CB 1F3FD 200D 2642

:*::man-lifting-weights:-medium-skin-tone::
Send,🏋🏽‍♂
return

;1F3CB 1F3FE 200D 2642 FE0F

:*::man-lifting-weights:-medium-dark-skin-t::
Send,🏋🏾‍♂️
return

;1F3CB 1F3FE 200D 2642

:*::man-lifting-weights:-medium-dark-skin-t::
Send,🏋🏾‍♂
return

;1F3CB 1F3FF 200D 2642 FE0F

:*::man-lifting-weights:-dark-skin-tone::
Send,🏋🏿‍♂️
return

;1F3CB 1F3FF 200D 2642

:*::man-lifting-weights:-dark-skin-tone::
Send,🏋🏿‍♂
return

;1F3CB FE0F 200D 2640 FE0F

:*::woman-lifting-weights::
Send,🏋️‍♀️
return

;1F3CB 200D 2640 FE0F

:*::woman-lifting-weights::
Send,🏋‍♀️
return

;1F3CB FE0F 200D 2640

:*::woman-lifting-weights::
Send,🏋️‍♀
return

;1F3CB 200D 2640

:*::woman-lifting-weights::
Send,🏋‍♀
return

;1F3CB 1F3FB 200D 2640 FE0F

:*::woman-lifting-weights:-light-skin-tone::
Send,🏋🏻‍♀️
return

;1F3CB 1F3FB 200D 2640

:*::woman-lifting-weights:-light-skin-tone::
Send,🏋🏻‍♀
return

;1F3CB 1F3FC 200D 2640 FE0F

:*::woman-lifting-weights:-medium-light-ski::
Send,🏋🏼‍♀️
return

;1F3CB 1F3FC 200D 2640

:*::woman-lifting-weights:-medium-light-ski::
Send,🏋🏼‍♀
return

;1F3CB 1F3FD 200D 2640 FE0F

:*::woman-lifting-weights:-medium-skin-tone::
Send,🏋🏽‍♀️
return

;1F3CB 1F3FD 200D 2640

:*::woman-lifting-weights:-medium-skin-tone::
Send,🏋🏽‍♀
return

;1F3CB 1F3FE 200D 2640 FE0F

:*::woman-lifting-weights:-medium-dark-skin::
Send,🏋🏾‍♀️
return

;1F3CB 1F3FE 200D 2640

:*::woman-lifting-weights:-medium-dark-skin::
Send,🏋🏾‍♀
return

;1F3CB 1F3FF 200D 2640 FE0F

:*::woman-lifting-weights:-dark-skin-tone::
Send,🏋🏿‍♀️
return

;1F3CB 1F3FF 200D 2640

:*::woman-lifting-weights:-dark-skin-tone::
Send,🏋🏿‍♀
return

;1F6B4

:*::person-biking::
Send,🚴
return

;1F6B4 1F3FB

:*::person-biking:-light-skin-tone::
Send,🚴🏻
return

;1F6B4 1F3FC

:*::person-biking:-medium-light-skin-tone::
Send,🚴🏼
return

;1F6B4 1F3FD

:*::person-biking:-medium-skin-tone::
Send,🚴🏽
return

;1F6B4 1F3FE

:*::person-biking:-medium-dark-skin-tone::
Send,🚴🏾
return

;1F6B4 1F3FF

:*::person-biking:-dark-skin-tone::
Send,🚴🏿
return

;1F6B4 200D 2642 FE0F
:*::male-cyclist::
:*::man-biking::
Send,🚴‍♂️
return

;1F6B4 200D 2642
:*::male-cyclist::
:*::man-biking::
Send,🚴‍♂
return

;1F6B4 1F3FB 200D 2642 FE0F

:*::man-biking:-light-skin-tone::
Send,🚴🏻‍♂️
return

;1F6B4 1F3FB 200D 2642

:*::man-biking:-light-skin-tone::
Send,🚴🏻‍♂
return

;1F6B4 1F3FC 200D 2642 FE0F

:*::man-biking:-medium-light-skin-tone::
Send,🚴🏼‍♂️
return

;1F6B4 1F3FC 200D 2642

:*::man-biking:-medium-light-skin-tone::
Send,🚴🏼‍♂
return

;1F6B4 1F3FD 200D 2642 FE0F

:*::man-biking:-medium-skin-tone::
Send,🚴🏽‍♂️
return

;1F6B4 1F3FD 200D 2642

:*::man-biking:-medium-skin-tone::
Send,🚴🏽‍♂
return

;1F6B4 1F3FE 200D 2642 FE0F

:*::man-biking:-medium-dark-skin-tone::
Send,🚴🏾‍♂️
return

;1F6B4 1F3FE 200D 2642

:*::man-biking:-medium-dark-skin-tone::
Send,🚴🏾‍♂
return

;1F6B4 1F3FF 200D 2642 FE0F

:*::man-biking:-dark-skin-tone::
Send,🚴🏿‍♂️
return

;1F6B4 1F3FF 200D 2642

:*::man-biking:-dark-skin-tone::
Send,🚴🏿‍♂
return

;1F6B4 200D 2640 FE0F
:*::female-cyclist::
:*::woman-biking::
Send,🚴‍♀️
return

;1F6B4 200D 2640
:*::female-cyclist::
:*::woman-biking::
Send,🚴‍♀
return

;1F6B4 1F3FB 200D 2640 FE0F

:*::woman-biking:-light-skin-tone::
Send,🚴🏻‍♀️
return

;1F6B4 1F3FB 200D 2640

:*::woman-biking:-light-skin-tone::
Send,🚴🏻‍♀
return

;1F6B4 1F3FC 200D 2640 FE0F

:*::woman-biking:-medium-light-skin-tone::
Send,🚴🏼‍♀️
return

;1F6B4 1F3FC 200D 2640

:*::woman-biking:-medium-light-skin-tone::
Send,🚴🏼‍♀
return

;1F6B4 1F3FD 200D 2640 FE0F

:*::woman-biking:-medium-skin-tone::
Send,🚴🏽‍♀️
return

;1F6B4 1F3FD 200D 2640

:*::woman-biking:-medium-skin-tone::
Send,🚴🏽‍♀
return

;1F6B4 1F3FE 200D 2640 FE0F

:*::woman-biking:-medium-dark-skin-tone::
Send,🚴🏾‍♀️
return

;1F6B4 1F3FE 200D 2640

:*::woman-biking:-medium-dark-skin-tone::
Send,🚴🏾‍♀
return

;1F6B4 1F3FF 200D 2640 FE0F

:*::woman-biking:-dark-skin-tone::
Send,🚴🏿‍♀️
return

;1F6B4 1F3FF 200D 2640

:*::woman-biking:-dark-skin-tone::
Send,🚴🏿‍♀
return

;1F6B5

:*::person-mountain-biking::
Send,🚵
return

;1F6B5 1F3FB

:*::person-mountain-biking:-light-skin-tone::
Send,🚵🏻
return

;1F6B5 1F3FC

:*::person-mountain-biking:-medium-light-sk::
Send,🚵🏼
return

;1F6B5 1F3FD

:*::person-mountain-biking:-medium-skin-ton::
Send,🚵🏽
return

;1F6B5 1F3FE

:*::person-mountain-biking:-medium-dark-ski::
Send,🚵🏾
return

;1F6B5 1F3FF

:*::person-mountain-biking:-dark-skin-tone::
Send,🚵🏿
return

;1F6B5 200D 2642 FE0F

:*::man-mountain-biking::
Send,🚵‍♂️
return

;1F6B5 200D 2642

:*::man-mountain-biking::
Send,🚵‍♂
return

;1F6B5 1F3FB 200D 2642 FE0F

:*::man-mountain-biking:-light-skin-tone::
Send,🚵🏻‍♂️
return

;1F6B5 1F3FB 200D 2642

:*::man-mountain-biking:-light-skin-tone::
Send,🚵🏻‍♂
return

;1F6B5 1F3FC 200D 2642 FE0F

:*::man-mountain-biking:-medium-light-skin-::
Send,🚵🏼‍♂️
return

;1F6B5 1F3FC 200D 2642

:*::man-mountain-biking:-medium-light-skin-::
Send,🚵🏼‍♂
return

;1F6B5 1F3FD 200D 2642 FE0F

:*::man-mountain-biking:-medium-skin-tone::
Send,🚵🏽‍♂️
return

;1F6B5 1F3FD 200D 2642

:*::man-mountain-biking:-medium-skin-tone::
Send,🚵🏽‍♂
return

;1F6B5 1F3FE 200D 2642 FE0F

:*::man-mountain-biking:-medium-dark-skin-t::
Send,🚵🏾‍♂️
return

;1F6B5 1F3FE 200D 2642

:*::man-mountain-biking:-medium-dark-skin-t::
Send,🚵🏾‍♂
return

;1F6B5 1F3FF 200D 2642 FE0F

:*::man-mountain-biking:-dark-skin-tone::
Send,🚵🏿‍♂️
return

;1F6B5 1F3FF 200D 2642

:*::man-mountain-biking:-dark-skin-tone::
Send,🚵🏿‍♂
return

;1F6B5 200D 2640 FE0F

:*::woman-mountain-biking::
Send,🚵‍♀️
return

;1F6B5 200D 2640

:*::woman-mountain-biking::
Send,🚵‍♀
return

;1F6B5 1F3FB 200D 2640 FE0F

:*::woman-mountain-biking:-light-skin-tone::
Send,🚵🏻‍♀️
return

;1F6B5 1F3FB 200D 2640

:*::woman-mountain-biking:-light-skin-tone::
Send,🚵🏻‍♀
return

;1F6B5 1F3FC 200D 2640 FE0F

:*::woman-mountain-biking:-medium-light-ski::
Send,🚵🏼‍♀️
return

;1F6B5 1F3FC 200D 2640

:*::woman-mountain-biking:-medium-light-ski::
Send,🚵🏼‍♀
return

;1F6B5 1F3FD 200D 2640 FE0F

:*::woman-mountain-biking:-medium-skin-tone::
Send,🚵🏽‍♀️
return

;1F6B5 1F3FD 200D 2640

:*::woman-mountain-biking:-medium-skin-tone::
Send,🚵🏽‍♀
return

;1F6B5 1F3FE 200D 2640 FE0F

:*::woman-mountain-biking:-medium-dark-skin::
Send,🚵🏾‍♀️
return

;1F6B5 1F3FE 200D 2640

:*::woman-mountain-biking:-medium-dark-skin::
Send,🚵🏾‍♀
return

;1F6B5 1F3FF 200D 2640 FE0F

:*::woman-mountain-biking:-dark-skin-tone::
Send,🚵🏿‍♀️
return

;1F6B5 1F3FF 200D 2640

:*::woman-mountain-biking:-dark-skin-tone::
Send,🚵🏿‍♀
return

;1F938

:*::person-cartwheeling::
Send,🤸
return

;1F938 1F3FB

:*::person-cartwheeling:-light-skin-tone::
Send,🤸🏻
return

;1F938 1F3FC

:*::person-cartwheeling:-medium-light-skin-::
Send,🤸🏼
return

;1F938 1F3FD

:*::person-cartwheeling:-medium-skin-tone::
Send,🤸🏽
return

;1F938 1F3FE

:*::person-cartwheeling:-medium-dark-skin-t::
Send,🤸🏾
return

;1F938 1F3FF

:*::person-cartwheeling:-dark-skin-tone::
Send,🤸🏿
return

;1F938 200D 2642 FE0F

:*::man-cartwheeling::
Send,🤸‍♂️
return

;1F938 200D 2642

:*::man-cartwheeling::
Send,🤸‍♂
return

;1F938 1F3FB 200D 2642 FE0F

:*::man-cartwheeling:-light-skin-tone::
Send,🤸🏻‍♂️
return

;1F938 1F3FB 200D 2642

:*::man-cartwheeling:-light-skin-tone::
Send,🤸🏻‍♂
return

;1F938 1F3FC 200D 2642 FE0F

:*::man-cartwheeling:-medium-light-skin-ton::
Send,🤸🏼‍♂️
return

;1F938 1F3FC 200D 2642

:*::man-cartwheeling:-medium-light-skin-ton::
Send,🤸🏼‍♂
return

;1F938 1F3FD 200D 2642 FE0F

:*::man-cartwheeling:-medium-skin-tone::
Send,🤸🏽‍♂️
return

;1F938 1F3FD 200D 2642

:*::man-cartwheeling:-medium-skin-tone::
Send,🤸🏽‍♂
return

;1F938 1F3FE 200D 2642 FE0F

:*::man-cartwheeling:-medium-dark-skin-tone::
Send,🤸🏾‍♂️
return

;1F938 1F3FE 200D 2642

:*::man-cartwheeling:-medium-dark-skin-tone::
Send,🤸🏾‍♂
return

;1F938 1F3FF 200D 2642 FE0F

:*::man-cartwheeling:-dark-skin-tone::
Send,🤸🏿‍♂️
return

;1F938 1F3FF 200D 2642

:*::man-cartwheeling:-dark-skin-tone::
Send,🤸🏿‍♂
return

;1F938 200D 2640 FE0F

:*::woman-cartwheeling::
Send,🤸‍♀️
return

;1F938 200D 2640

:*::woman-cartwheeling::
Send,🤸‍♀
return

;1F938 1F3FB 200D 2640 FE0F

:*::woman-cartwheeling:-light-skin-tone::
Send,🤸🏻‍♀️
return

;1F938 1F3FB 200D 2640

:*::woman-cartwheeling:-light-skin-tone::
Send,🤸🏻‍♀
return

;1F938 1F3FC 200D 2640 FE0F

:*::woman-cartwheeling:-medium-light-skin-t::
Send,🤸🏼‍♀️
return

;1F938 1F3FC 200D 2640

:*::woman-cartwheeling:-medium-light-skin-t::
Send,🤸🏼‍♀
return

;1F938 1F3FD 200D 2640 FE0F

:*::woman-cartwheeling:-medium-skin-tone::
Send,🤸🏽‍♀️
return

;1F938 1F3FD 200D 2640

:*::woman-cartwheeling:-medium-skin-tone::
Send,🤸🏽‍♀
return

;1F938 1F3FE 200D 2640 FE0F

:*::woman-cartwheeling:-medium-dark-skin-to::
Send,🤸🏾‍♀️
return

;1F938 1F3FE 200D 2640

:*::woman-cartwheeling:-medium-dark-skin-to::
Send,🤸🏾‍♀
return

;1F938 1F3FF 200D 2640 FE0F

:*::woman-cartwheeling:-dark-skin-tone::
Send,🤸🏿‍♀️
return

;1F938 1F3FF 200D 2640

:*::woman-cartwheeling:-dark-skin-tone::
Send,🤸🏿‍♀
return

;1F93C

:*::people-wrestling::
Send,🤼
return

;1F93C 200D 2642 FE0F

:*::men-wrestling::
Send,🤼‍♂️
return

;1F93C 200D 2642

:*::men-wrestling::
Send,🤼‍♂
return

;1F93C 200D 2640 FE0F

:*::women-wrestling::
Send,🤼‍♀️
return

;1F93C 200D 2640

:*::women-wrestling::
Send,🤼‍♀
return

;1F93D

:*::person-playing-water-polo::
Send,🤽
return

;1F93D 1F3FB

:*::person-playing-water-polo:-light-skin-t::
Send,🤽🏻
return

;1F93D 1F3FC

:*::person-playing-water-polo:-medium-light::
Send,🤽🏼
return

;1F93D 1F3FD

:*::person-playing-water-polo:-medium-skin-::
Send,🤽🏽
return

;1F93D 1F3FE

:*::person-playing-water-polo:-medium-dark-::
Send,🤽🏾
return

;1F93D 1F3FF

:*::person-playing-water-polo:-dark-skin-to::
Send,🤽🏿
return

;1F93D 200D 2642 FE0F

:*::man-playing-water-polo::
Send,🤽‍♂️
return

;1F93D 200D 2642

:*::man-playing-water-polo::
Send,🤽‍♂
return

;1F93D 1F3FB 200D 2642 FE0F

:*::man-playing-water-polo:-light-skin-tone::
Send,🤽🏻‍♂️
return

;1F93D 1F3FB 200D 2642

:*::man-playing-water-polo:-light-skin-tone::
Send,🤽🏻‍♂
return

;1F93D 1F3FC 200D 2642 FE0F

:*::man-playing-water-polo:-medium-light-sk::
Send,🤽🏼‍♂️
return

;1F93D 1F3FC 200D 2642

:*::man-playing-water-polo:-medium-light-sk::
Send,🤽🏼‍♂
return

;1F93D 1F3FD 200D 2642 FE0F

:*::man-playing-water-polo:-medium-skin-ton::
Send,🤽🏽‍♂️
return

;1F93D 1F3FD 200D 2642

:*::man-playing-water-polo:-medium-skin-ton::
Send,🤽🏽‍♂
return

;1F93D 1F3FE 200D 2642 FE0F

:*::man-playing-water-polo:-medium-dark-ski::
Send,🤽🏾‍♂️
return

;1F93D 1F3FE 200D 2642

:*::man-playing-water-polo:-medium-dark-ski::
Send,🤽🏾‍♂
return

;1F93D 1F3FF 200D 2642 FE0F

:*::man-playing-water-polo:-dark-skin-tone::
Send,🤽🏿‍♂️
return

;1F93D 1F3FF 200D 2642

:*::man-playing-water-polo:-dark-skin-tone::
Send,🤽🏿‍♂
return

;1F93D 200D 2640 FE0F

:*::woman-playing-water-polo::
Send,🤽‍♀️
return

;1F93D 200D 2640

:*::woman-playing-water-polo::
Send,🤽‍♀
return

;1F93D 1F3FB 200D 2640 FE0F

:*::woman-playing-water-polo:-light-skin-to::
Send,🤽🏻‍♀️
return

;1F93D 1F3FB 200D 2640

:*::woman-playing-water-polo:-light-skin-to::
Send,🤽🏻‍♀
return

;1F93D 1F3FC 200D 2640 FE0F

:*::woman-playing-water-polo:-medium-light-::
Send,🤽🏼‍♀️
return

;1F93D 1F3FC 200D 2640

:*::woman-playing-water-polo:-medium-light-::
Send,🤽🏼‍♀
return

;1F93D 1F3FD 200D 2640 FE0F

:*::woman-playing-water-polo:-medium-skin-t::
Send,🤽🏽‍♀️
return

;1F93D 1F3FD 200D 2640

:*::woman-playing-water-polo:-medium-skin-t::
Send,🤽🏽‍♀
return

;1F93D 1F3FE 200D 2640 FE0F

:*::woman-playing-water-polo:-medium-dark-s::
Send,🤽🏾‍♀️
return

;1F93D 1F3FE 200D 2640

:*::woman-playing-water-polo:-medium-dark-s::
Send,🤽🏾‍♀
return

;1F93D 1F3FF 200D 2640 FE0F

:*::woman-playing-water-polo:-dark-skin-ton::
Send,🤽🏿‍♀️
return

;1F93D 1F3FF 200D 2640

:*::woman-playing-water-polo:-dark-skin-ton::
Send,🤽🏿‍♀
return

;1F93E

:*::person-playing-handball::
Send,🤾
return

;1F93E 1F3FB

:*::person-playing-handball:-light-skin-ton::
Send,🤾🏻
return

;1F93E 1F3FC

:*::person-playing-handball:-medium-light-s::
Send,🤾🏼
return

;1F93E 1F3FD

:*::person-playing-handball:-medium-skin-to::
Send,🤾🏽
return

;1F93E 1F3FE

:*::person-playing-handball:-medium-dark-sk::
Send,🤾🏾
return

;1F93E 1F3FF

:*::person-playing-handball:-dark-skin-tone::
Send,🤾🏿
return

;1F93E 200D 2642 FE0F

:*::man-playing-handball::
Send,🤾‍♂️
return

;1F93E 200D 2642

:*::man-playing-handball::
Send,🤾‍♂
return

;1F93E 1F3FB 200D 2642 FE0F

:*::man-playing-handball:-light-skin-tone::
Send,🤾🏻‍♂️
return

;1F93E 1F3FB 200D 2642

:*::man-playing-handball:-light-skin-tone::
Send,🤾🏻‍♂
return

;1F93E 1F3FC 200D 2642 FE0F

:*::man-playing-handball:-medium-light-skin::
Send,🤾🏼‍♂️
return

;1F93E 1F3FC 200D 2642

:*::man-playing-handball:-medium-light-skin::
Send,🤾🏼‍♂
return

;1F93E 1F3FD 200D 2642 FE0F

:*::man-playing-handball:-medium-skin-tone::
Send,🤾🏽‍♂️
return

;1F93E 1F3FD 200D 2642

:*::man-playing-handball:-medium-skin-tone::
Send,🤾🏽‍♂
return

;1F93E 1F3FE 200D 2642 FE0F

:*::man-playing-handball:-medium-dark-skin-::
Send,🤾🏾‍♂️
return

;1F93E 1F3FE 200D 2642

:*::man-playing-handball:-medium-dark-skin-::
Send,🤾🏾‍♂
return

;1F93E 1F3FF 200D 2642 FE0F

:*::man-playing-handball:-dark-skin-tone::
Send,🤾🏿‍♂️
return

;1F93E 1F3FF 200D 2642

:*::man-playing-handball:-dark-skin-tone::
Send,🤾🏿‍♂
return

;1F93E 200D 2640 FE0F

:*::woman-playing-handball::
Send,🤾‍♀️
return

;1F93E 200D 2640

:*::woman-playing-handball::
Send,🤾‍♀
return

;1F93E 1F3FB 200D 2640 FE0F

:*::woman-playing-handball:-light-skin-tone::
Send,🤾🏻‍♀️
return

;1F93E 1F3FB 200D 2640

:*::woman-playing-handball:-light-skin-tone::
Send,🤾🏻‍♀
return

;1F93E 1F3FC 200D 2640 FE0F

:*::woman-playing-handball:-medium-light-sk::
Send,🤾🏼‍♀️
return

;1F93E 1F3FC 200D 2640

:*::woman-playing-handball:-medium-light-sk::
Send,🤾🏼‍♀
return

;1F93E 1F3FD 200D 2640 FE0F

:*::woman-playing-handball:-medium-skin-ton::
Send,🤾🏽‍♀️
return

;1F93E 1F3FD 200D 2640

:*::woman-playing-handball:-medium-skin-ton::
Send,🤾🏽‍♀
return

;1F93E 1F3FE 200D 2640 FE0F

:*::woman-playing-handball:-medium-dark-ski::
Send,🤾🏾‍♀️
return

;1F93E 1F3FE 200D 2640

:*::woman-playing-handball:-medium-dark-ski::
Send,🤾🏾‍♀
return

;1F93E 1F3FF 200D 2640 FE0F

:*::woman-playing-handball:-dark-skin-tone::
Send,🤾🏿‍♀️
return

;1F93E 1F3FF 200D 2640

:*::woman-playing-handball:-dark-skin-tone::
Send,🤾🏿‍♀
return

;1F939

:*::person-juggling::
Send,🤹
return

;1F939 1F3FB

:*::person-juggling:-light-skin-tone::
Send,🤹🏻
return

;1F939 1F3FC

:*::person-juggling:-medium-light-skin-tone::
Send,🤹🏼
return

;1F939 1F3FD

:*::person-juggling:-medium-skin-tone::
Send,🤹🏽
return

;1F939 1F3FE

:*::person-juggling:-medium-dark-skin-tone::
Send,🤹🏾
return

;1F939 1F3FF

:*::person-juggling:-dark-skin-tone::
Send,🤹🏿
return

;1F939 200D 2642 FE0F
:*::male-juggler::
:*::man-juggling::
Send,🤹‍♂️
return

;1F939 200D 2642
:*::male-juggler::
:*::man-juggling::
Send,🤹‍♂
return

;1F939 1F3FB 200D 2642 FE0F

:*::man-juggling:-light-skin-tone::
Send,🤹🏻‍♂️
return

;1F939 1F3FB 200D 2642

:*::man-juggling:-light-skin-tone::
Send,🤹🏻‍♂
return

;1F939 1F3FC 200D 2642 FE0F

:*::man-juggling:-medium-light-skin-tone::
Send,🤹🏼‍♂️
return

;1F939 1F3FC 200D 2642

:*::man-juggling:-medium-light-skin-tone::
Send,🤹🏼‍♂
return

;1F939 1F3FD 200D 2642 FE0F

:*::man-juggling:-medium-skin-tone::
Send,🤹🏽‍♂️
return

;1F939 1F3FD 200D 2642

:*::man-juggling:-medium-skin-tone::
Send,🤹🏽‍♂
return

;1F939 1F3FE 200D 2642 FE0F

:*::man-juggling:-medium-dark-skin-tone::
Send,🤹🏾‍♂️
return

;1F939 1F3FE 200D 2642

:*::man-juggling:-medium-dark-skin-tone::
Send,🤹🏾‍♂
return

;1F939 1F3FF 200D 2642 FE0F

:*::man-juggling:-dark-skin-tone::
Send,🤹🏿‍♂️
return

;1F939 1F3FF 200D 2642

:*::man-juggling:-dark-skin-tone::
Send,🤹🏿‍♂
return

;1F939 200D 2640 FE0F
:*::female-juggler::
:*::woman-juggling::
Send,🤹‍♀️
return

;1F939 200D 2640
:*::female-juggler::
:*::woman-juggling::
Send,🤹‍♀
return

;1F939 1F3FB 200D 2640 FE0F

:*::woman-juggling:-light-skin-tone::
Send,🤹🏻‍♀️
return

;1F939 1F3FB 200D 2640

:*::woman-juggling:-light-skin-tone::
Send,🤹🏻‍♀
return

;1F939 1F3FC 200D 2640 FE0F

:*::woman-juggling:-medium-light-skin-tone::
Send,🤹🏼‍♀️
return

;1F939 1F3FC 200D 2640

:*::woman-juggling:-medium-light-skin-tone::
Send,🤹🏼‍♀
return

;1F939 1F3FD 200D 2640 FE0F

:*::woman-juggling:-medium-skin-tone::
Send,🤹🏽‍♀️
return

;1F939 1F3FD 200D 2640

:*::woman-juggling:-medium-skin-tone::
Send,🤹🏽‍♀
return

;1F939 1F3FE 200D 2640 FE0F

:*::woman-juggling:-medium-dark-skin-tone::
Send,🤹🏾‍♀️
return

;1F939 1F3FE 200D 2640

:*::woman-juggling:-medium-dark-skin-tone::
Send,🤹🏾‍♀
return

;1F939 1F3FF 200D 2640 FE0F

:*::woman-juggling:-dark-skin-tone::
Send,🤹🏿‍♀️
return

;1F939 1F3FF 200D 2640

:*::woman-juggling:-dark-skin-tone::
Send,🤹🏿‍♀
return

;1F9D8
:*::meditation::
:*::yoga::
:*::person-in-lotus-position::
Send,🧘
return

;1F9D8 1F3FB

:*::person-in-lotus-position:-light-skin-to::
Send,🧘🏻
return

;1F9D8 1F3FC

:*::person-in-lotus-position:-medium-light-::
Send,🧘🏼
return

;1F9D8 1F3FD

:*::person-in-lotus-position:-medium-skin-t::
Send,🧘🏽
return

;1F9D8 1F3FE

:*::person-in-lotus-position:-medium-dark-s::
Send,🧘🏾
return

;1F9D8 1F3FF

:*::person-in-lotus-position:-dark-skin-ton::
Send,🧘🏿
return

;1F9D8 200D 2642 FE0F

:*::man-in-lotus-position::
Send,🧘‍♂️
return

;1F9D8 200D 2642

:*::man-in-lotus-position::
Send,🧘‍♂
return

;1F9D8 1F3FB 200D 2642 FE0F

:*::man-in-lotus-position:-light-skin-tone::
Send,🧘🏻‍♂️
return

;1F9D8 1F3FB 200D 2642

:*::man-in-lotus-position:-light-skin-tone::
Send,🧘🏻‍♂
return

;1F9D8 1F3FC 200D 2642 FE0F

:*::man-in-lotus-position:-medium-light-ski::
Send,🧘🏼‍♂️
return

;1F9D8 1F3FC 200D 2642

:*::man-in-lotus-position:-medium-light-ski::
Send,🧘🏼‍♂
return

;1F9D8 1F3FD 200D 2642 FE0F

:*::man-in-lotus-position:-medium-skin-tone::
Send,🧘🏽‍♂️
return

;1F9D8 1F3FD 200D 2642

:*::man-in-lotus-position:-medium-skin-tone::
Send,🧘🏽‍♂
return

;1F9D8 1F3FE 200D 2642 FE0F

:*::man-in-lotus-position:-medium-dark-skin::
Send,🧘🏾‍♂️
return

;1F9D8 1F3FE 200D 2642

:*::man-in-lotus-position:-medium-dark-skin::
Send,🧘🏾‍♂
return

;1F9D8 1F3FF 200D 2642 FE0F

:*::man-in-lotus-position:-dark-skin-tone::
Send,🧘🏿‍♂️
return

;1F9D8 1F3FF 200D 2642

:*::man-in-lotus-position:-dark-skin-tone::
Send,🧘🏿‍♂
return

;1F9D8 200D 2640 FE0F

:*::woman-in-lotus-position::
Send,🧘‍♀️
return

;1F9D8 200D 2640

:*::woman-in-lotus-position::
Send,🧘‍♀
return

;1F9D8 1F3FB 200D 2640 FE0F

:*::woman-in-lotus-position:-light-skin-ton::
Send,🧘🏻‍♀️
return

;1F9D8 1F3FB 200D 2640

:*::woman-in-lotus-position:-light-skin-ton::
Send,🧘🏻‍♀
return

;1F9D8 1F3FC 200D 2640 FE0F

:*::woman-in-lotus-position:-medium-light-s::
Send,🧘🏼‍♀️
return

;1F9D8 1F3FC 200D 2640

:*::woman-in-lotus-position:-medium-light-s::
Send,🧘🏼‍♀
return

;1F9D8 1F3FD 200D 2640 FE0F

:*::woman-in-lotus-position:-medium-skin-to::
Send,🧘🏽‍♀️
return

;1F9D8 1F3FD 200D 2640

:*::woman-in-lotus-position:-medium-skin-to::
Send,🧘🏽‍♀
return

;1F9D8 1F3FE 200D 2640 FE0F

:*::woman-in-lotus-position:-medium-dark-sk::
Send,🧘🏾‍♀️
return

;1F9D8 1F3FE 200D 2640

:*::woman-in-lotus-position:-medium-dark-sk::
Send,🧘🏾‍♀
return

;1F9D8 1F3FF 200D 2640 FE0F

:*::woman-in-lotus-position:-dark-skin-tone::
Send,🧘🏿‍♀️
return

;1F9D8 1F3FF 200D 2640

:*::woman-in-lotus-position:-dark-skin-tone::
Send,🧘🏿‍♀
return

;1F6C0

:*::person-taking-bath::
Send,🛀
return

;1F6C0 1F3FB

:*::person-taking-bath:-light-skin-tone::
Send,🛀🏻
return

;1F6C0 1F3FC

:*::person-taking-bath:-medium-light-skin-t::
Send,🛀🏼
return

;1F6C0 1F3FD

:*::person-taking-bath:-medium-skin-tone::
Send,🛀🏽
return

;1F6C0 1F3FE

:*::person-taking-bath:-medium-dark-skin-to::
Send,🛀🏾
return

;1F6C0 1F3FF

:*::person-taking-bath:-dark-skin-tone::
Send,🛀🏿
return

;1F6CC

:*::person-in-bed::
Send,🛌
return

;1F6CC 1F3FB

:*::person-in-bed:-light-skin-tone::
Send,🛌🏻
return

;1F6CC 1F3FC

:*::person-in-bed:-medium-light-skin-tone::
Send,🛌🏼
return

;1F6CC 1F3FD

:*::person-in-bed:-medium-skin-tone::
Send,🛌🏽
return

;1F6CC 1F3FE

:*::person-in-bed:-medium-dark-skin-tone::
Send,🛌🏾
return

;1F6CC 1F3FF

:*::person-in-bed:-dark-skin-tone::
Send,🛌🏿
return

;1F9D1 200D 1F91D 200D 1F9D1
:*::gender-inclusive-couple::
:*::gender-neutral-couple::
:*::gender-nonconforming-couple::
:*::people-holding-hands::
Send,🧑‍🤝‍🧑
return

;1F9D1 1F3FB 200D 1F91D 200D 1F9D1 1F3FB

:*::people-holding-hands:-light-skin-tone::
Send,🧑🏻‍🤝‍🧑🏻
return

;1F9D1 1F3FC 200D 1F91D 200D 1F9D1 1F3FB

:*::people-holding-hands:-medium-light-skin::
Send,🧑🏼‍🤝‍🧑🏻
return

;1F9D1 1F3FC 200D 1F91D 200D 1F9D1 1F3FC

:*::people-holding-hands:-medium-light-skin::
Send,🧑🏼‍🤝‍🧑🏼
return

;1F9D1 1F3FD 200D 1F91D 200D 1F9D1 1F3FB

:*::people-holding-hands:-medium-skin-tone,::
Send,🧑🏽‍🤝‍🧑🏻
return

;1F9D1 1F3FD 200D 1F91D 200D 1F9D1 1F3FC

:*::people-holding-hands:-medium-skin-tone,::
Send,🧑🏽‍🤝‍🧑🏼
return

;1F9D1 1F3FD 200D 1F91D 200D 1F9D1 1F3FD

:*::people-holding-hands:-medium-skin-tone::
Send,🧑🏽‍🤝‍🧑🏽
return

;1F9D1 1F3FE 200D 1F91D 200D 1F9D1 1F3FB

:*::people-holding-hands:-medium-dark-skin-::
Send,🧑🏾‍🤝‍🧑🏻
return

;1F9D1 1F3FE 200D 1F91D 200D 1F9D1 1F3FC

:*::people-holding-hands:-medium-dark-skin-::
Send,🧑🏾‍🤝‍🧑🏼
return

;1F9D1 1F3FE 200D 1F91D 200D 1F9D1 1F3FD

:*::people-holding-hands:-medium-dark-skin-::
Send,🧑🏾‍🤝‍🧑🏽
return

;1F9D1 1F3FE 200D 1F91D 200D 1F9D1 1F3FE

:*::people-holding-hands:-medium-dark-skin-::
Send,🧑🏾‍🤝‍🧑🏾
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FB

:*::people-holding-hands:-dark-skin-tone,-l::
Send,🧑🏿‍🤝‍🧑🏻
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FC

:*::people-holding-hands:-dark-skin-tone,-m::
Send,🧑🏿‍🤝‍🧑🏼
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FD

:*::people-holding-hands:-dark-skin-tone,-m::
Send,🧑🏿‍🤝‍🧑🏽
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FE

:*::people-holding-hands:-dark-skin-tone,-m::
Send,🧑🏿‍🤝‍🧑🏾
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FF

:*::people-holding-hands:-dark-skin-tone::
Send,🧑🏿‍🤝‍🧑🏿
return

;1F46D

:*::women-holding-hands::
Send,👭
return

;1F46D 1F3FB

:*::women-holding-hands:-light-skin-tone::
Send,👭🏻
return

;1F469 1F3FC 200D 1F91D 200D 1F469 1F3FB

:*::women-holding-hands:-medium-light-skin-::
Send,👩🏼‍🤝‍👩🏻
return

;1F46D 1F3FC

:*::women-holding-hands:-medium-light-skin-::
Send,👭🏼
return

;1F469 1F3FD 200D 1F91D 200D 1F469 1F3FB

:*::women-holding-hands:-medium-skin-tone,-::
Send,👩🏽‍🤝‍👩🏻
return

;1F469 1F3FD 200D 1F91D 200D 1F469 1F3FC

:*::women-holding-hands:-medium-skin-tone,-::
Send,👩🏽‍🤝‍👩🏼
return

;1F46D 1F3FD

:*::women-holding-hands:-medium-skin-tone::
Send,👭🏽
return

;1F469 1F3FE 200D 1F91D 200D 1F469 1F3FB

:*::women-holding-hands:-medium-dark-skin-t::
Send,👩🏾‍🤝‍👩🏻
return

;1F469 1F3FE 200D 1F91D 200D 1F469 1F3FC

:*::women-holding-hands:-medium-dark-skin-t::
Send,👩🏾‍🤝‍👩🏼
return

;1F469 1F3FE 200D 1F91D 200D 1F469 1F3FD

:*::women-holding-hands:-medium-dark-skin-t::
Send,👩🏾‍🤝‍👩🏽
return

;1F46D 1F3FE

:*::women-holding-hands:-medium-dark-skin-t::
Send,👭🏾
return

;1F469 1F3FF 200D 1F91D 200D 1F469 1F3FB

:*::women-holding-hands:-dark-skin-tone,-li::
Send,👩🏿‍🤝‍👩🏻
return

;1F469 1F3FF 200D 1F91D 200D 1F469 1F3FC

:*::women-holding-hands:-dark-skin-tone,-me::
Send,👩🏿‍🤝‍👩🏼
return

;1F469 1F3FF 200D 1F91D 200D 1F469 1F3FD

:*::women-holding-hands:-dark-skin-tone,-me::
Send,👩🏿‍🤝‍👩🏽
return

;1F469 1F3FF 200D 1F91D 200D 1F469 1F3FE

:*::women-holding-hands:-dark-skin-tone,-me::
Send,👩🏿‍🤝‍👩🏾
return

;1F46D 1F3FF

:*::women-holding-hands:-dark-skin-tone::
Send,👭🏿
return

;1F46B

:*::woman-and-man-holding-hands::
Send,👫
return

;1F46B 1F3FB

:*::woman-and-man-holding-hands:-light-skin::
Send,👫🏻
return

;1F469 1F3FB 200D 1F91D 200D 1F468 1F3FC

:*::woman-and-man-holding-hands:-light-skin::
Send,👩🏻‍🤝‍👨🏼
return

;1F469 1F3FB 200D 1F91D 200D 1F468 1F3FD

:*::woman-and-man-holding-hands:-light-skin::
Send,👩🏻‍🤝‍👨🏽
return

;1F469 1F3FB 200D 1F91D 200D 1F468 1F3FE

:*::woman-and-man-holding-hands:-light-skin::
Send,👩🏻‍🤝‍👨🏾
return

;1F469 1F3FB 200D 1F91D 200D 1F468 1F3FF

:*::woman-and-man-holding-hands:-light-skin::
Send,👩🏻‍🤝‍👨🏿
return

;1F469 1F3FC 200D 1F91D 200D 1F468 1F3FB

:*::woman-and-man-holding-hands:-medium-lig::
Send,👩🏼‍🤝‍👨🏻
return

;1F46B 1F3FC

:*::woman-and-man-holding-hands:-medium-lig::
Send,👫🏼
return

;1F469 1F3FC 200D 1F91D 200D 1F468 1F3FD

:*::woman-and-man-holding-hands:-medium-lig::
Send,👩🏼‍🤝‍👨🏽
return

;1F469 1F3FC 200D 1F91D 200D 1F468 1F3FE

:*::woman-and-man-holding-hands:-medium-lig::
Send,👩🏼‍🤝‍👨🏾
return

;1F469 1F3FC 200D 1F91D 200D 1F468 1F3FF

:*::woman-and-man-holding-hands:-medium-lig::
Send,👩🏼‍🤝‍👨🏿
return

;1F469 1F3FD 200D 1F91D 200D 1F468 1F3FB

:*::woman-and-man-holding-hands:-medium-ski::
Send,👩🏽‍🤝‍👨🏻
return

;1F469 1F3FD 200D 1F91D 200D 1F468 1F3FC

:*::woman-and-man-holding-hands:-medium-ski::
Send,👩🏽‍🤝‍👨🏼
return

;1F46B 1F3FD

:*::woman-and-man-holding-hands:-medium-ski::
Send,👫🏽
return

;1F469 1F3FD 200D 1F91D 200D 1F468 1F3FE

:*::woman-and-man-holding-hands:-medium-ski::
Send,👩🏽‍🤝‍👨🏾
return

;1F469 1F3FD 200D 1F91D 200D 1F468 1F3FF

:*::woman-and-man-holding-hands:-medium-ski::
Send,👩🏽‍🤝‍👨🏿
return

;1F469 1F3FE 200D 1F91D 200D 1F468 1F3FB

:*::woman-and-man-holding-hands:-medium-dar::
Send,👩🏾‍🤝‍👨🏻
return

;1F469 1F3FE 200D 1F91D 200D 1F468 1F3FC

:*::woman-and-man-holding-hands:-medium-dar::
Send,👩🏾‍🤝‍👨🏼
return

;1F469 1F3FE 200D 1F91D 200D 1F468 1F3FD

:*::woman-and-man-holding-hands:-medium-dar::
Send,👩🏾‍🤝‍👨🏽
return

;1F46B 1F3FE

:*::woman-and-man-holding-hands:-medium-dar::
Send,👫🏾
return

;1F469 1F3FE 200D 1F91D 200D 1F468 1F3FF

:*::woman-and-man-holding-hands:-medium-dar::
Send,👩🏾‍🤝‍👨🏿
return

;1F469 1F3FF 200D 1F91D 200D 1F468 1F3FB

:*::woman-and-man-holding-hands:-dark-skin-::
Send,👩🏿‍🤝‍👨🏻
return

;1F469 1F3FF 200D 1F91D 200D 1F468 1F3FC

:*::woman-and-man-holding-hands:-dark-skin-::
Send,👩🏿‍🤝‍👨🏼
return

;1F469 1F3FF 200D 1F91D 200D 1F468 1F3FD

:*::woman-and-man-holding-hands:-dark-skin-::
Send,👩🏿‍🤝‍👨🏽
return

;1F469 1F3FF 200D 1F91D 200D 1F468 1F3FE

:*::woman-and-man-holding-hands:-dark-skin-::
Send,👩🏿‍🤝‍👨🏾
return

;1F46B 1F3FF

:*::woman-and-man-holding-hands:-dark-skin-::
Send,👫🏿
return

;1F46C

:*::men-holding-hands::
Send,👬
return

;1F46C 1F3FB

:*::men-holding-hands:-light-skin-tone::
Send,👬🏻
return

;1F468 1F3FC 200D 1F91D 200D 1F468 1F3FB

:*::men-holding-hands:-medium-light-skin-to::
Send,👨🏼‍🤝‍👨🏻
return

;1F46C 1F3FC

:*::men-holding-hands:-medium-light-skin-to::
Send,👬🏼
return

;1F468 1F3FD 200D 1F91D 200D 1F468 1F3FB

:*::men-holding-hands:-medium-skin-tone,-li::
Send,👨🏽‍🤝‍👨🏻
return

;1F468 1F3FD 200D 1F91D 200D 1F468 1F3FC

:*::men-holding-hands:-medium-skin-tone,-me::
Send,👨🏽‍🤝‍👨🏼
return

;1F46C 1F3FD

:*::men-holding-hands:-medium-skin-tone::
Send,👬🏽
return

;1F468 1F3FE 200D 1F91D 200D 1F468 1F3FB

:*::men-holding-hands:-medium-dark-skin-ton::
Send,👨🏾‍🤝‍👨🏻
return

;1F468 1F3FE 200D 1F91D 200D 1F468 1F3FC

:*::men-holding-hands:-medium-dark-skin-ton::
Send,👨🏾‍🤝‍👨🏼
return

;1F468 1F3FE 200D 1F91D 200D 1F468 1F3FD

:*::men-holding-hands:-medium-dark-skin-ton::
Send,👨🏾‍🤝‍👨🏽
return

;1F46C 1F3FE

:*::men-holding-hands:-medium-dark-skin-ton::
Send,👬🏾
return

;1F468 1F3FF 200D 1F91D 200D 1F468 1F3FB

:*::men-holding-hands:-dark-skin-tone,-ligh::
Send,👨🏿‍🤝‍👨🏻
return

;1F468 1F3FF 200D 1F91D 200D 1F468 1F3FC

:*::men-holding-hands:-dark-skin-tone,-medi::
Send,👨🏿‍🤝‍👨🏼
return

;1F468 1F3FF 200D 1F91D 200D 1F468 1F3FD

:*::men-holding-hands:-dark-skin-tone,-medi::
Send,👨🏿‍🤝‍👨🏽
return

;1F468 1F3FF 200D 1F91D 200D 1F468 1F3FE

:*::men-holding-hands:-dark-skin-tone,-medi::
Send,👨🏿‍🤝‍👨🏾
return

;1F46C 1F3FF

:*::men-holding-hands:-dark-skin-tone::
Send,👬🏿
return

;1F48F
:*::boy-and-girl-kiss::
:*::couple-kissing::
:*::man-and-woman-kiss::
:*::kiss::
Send,💏
return

;1F469 200D 2764 FE0F 200D 1F48B 200D 1F468

:*::kiss:-woman,-man::
Send,👩‍❤️‍💋‍👨
return

;1F469 200D 2764 200D 1F48B 200D 1F468

:*::kiss:-woman,-man::
Send,👩‍❤‍💋‍👨
return

;1F468 200D 2764 FE0F 200D 1F48B 200D 1F468

:*::kiss:-man,-man::
Send,👨‍❤️‍💋‍👨
return

;1F468 200D 2764 200D 1F48B 200D 1F468

:*::kiss:-man,-man::
Send,👨‍❤‍💋‍👨
return

;1F469 200D 2764 FE0F 200D 1F48B 200D 1F469

:*::kiss:-woman,-woman::
Send,👩‍❤️‍💋‍👩
return

;1F469 200D 2764 200D 1F48B 200D 1F469

:*::kiss:-woman,-woman::
Send,👩‍❤‍💋‍👩
return

;1F491
:*::couple-in-love::
:*::loving-couple::
:*::couple-with-heart::
Send,💑
return

;1F469 200D 2764 FE0F 200D 1F468

:*::couple-with-heart:-woman,-man::
Send,👩‍❤️‍👨
return

;1F469 200D 2764 200D 1F468

:*::couple-with-heart:-woman,-man::
Send,👩‍❤‍👨
return

;1F468 200D 2764 FE0F 200D 1F468

:*::couple-with-heart:-man,-man::
Send,👨‍❤️‍👨
return

;1F468 200D 2764 200D 1F468

:*::couple-with-heart:-man,-man::
Send,👨‍❤‍👨
return

;1F469 200D 2764 FE0F 200D 1F469

:*::couple-with-heart:-woman,-woman::
Send,👩‍❤️‍👩
return

;1F469 200D 2764 200D 1F469

:*::couple-with-heart:-woman,-woman::
Send,👩‍❤‍👩
return

;1F46A
:*::parents-with-child::
:*::family::
Send,👪
return

;1F468 200D 1F469 200D 1F466

:*::family:-man,-woman,-boy::
Send,👨‍👩‍👦
return

;1F468 200D 1F469 200D 1F467

:*::family:-man,-woman,-girl::
Send,👨‍👩‍👧
return

;1F468 200D 1F469 200D 1F467 200D 1F466

:*::family:-man,-woman,-girl,-boy::
Send,👨‍👩‍👧‍👦
return

;1F468 200D 1F469 200D 1F466 200D 1F466

:*::family:-man,-woman,-boy,-boy::
Send,👨‍👩‍👦‍👦
return

;1F468 200D 1F469 200D 1F467 200D 1F467

:*::family:-man,-woman,-girl,-girl::
Send,👨‍👩‍👧‍👧
return

;1F468 200D 1F468 200D 1F466

:*::family:-man,-man,-boy::
Send,👨‍👨‍👦
return

;1F468 200D 1F468 200D 1F467

:*::family:-man,-man,-girl::
Send,👨‍👨‍👧
return

;1F468 200D 1F468 200D 1F467 200D 1F466

:*::family:-man,-man,-girl,-boy::
Send,👨‍👨‍👧‍👦
return

;1F468 200D 1F468 200D 1F466 200D 1F466

:*::family:-man,-man,-boy,-boy::
Send,👨‍👨‍👦‍👦
return

;1F468 200D 1F468 200D 1F467 200D 1F467

:*::family:-man,-man,-girl,-girl::
Send,👨‍👨‍👧‍👧
return

;1F469 200D 1F469 200D 1F466

:*::family:-woman,-woman,-boy::
Send,👩‍👩‍👦
return

;1F469 200D 1F469 200D 1F467

:*::family:-woman,-woman,-girl::
Send,👩‍👩‍👧
return

;1F469 200D 1F469 200D 1F467 200D 1F466

:*::family:-woman,-woman,-girl,-boy::
Send,👩‍👩‍👧‍👦
return

;1F469 200D 1F469 200D 1F466 200D 1F466

:*::family:-woman,-woman,-boy,-boy::
Send,👩‍👩‍👦‍👦
return

;1F469 200D 1F469 200D 1F467 200D 1F467

:*::family:-woman,-woman,-girl,-girl::
Send,👩‍👩‍👧‍👧
return

;1F468 200D 1F466

:*::family:-man,-boy::
Send,👨‍👦
return

;1F468 200D 1F466 200D 1F466

:*::family:-man,-boy,-boy::
Send,👨‍👦‍👦
return

;1F468 200D 1F467

:*::family:-man,-girl::
Send,👨‍👧
return

;1F468 200D 1F467 200D 1F466

:*::family:-man,-girl,-boy::
Send,👨‍👧‍👦
return

;1F468 200D 1F467 200D 1F467

:*::family:-man,-girl,-girl::
Send,👨‍👧‍👧
return

;1F469 200D 1F466

:*::family:-woman,-boy::
Send,👩‍👦
return

;1F469 200D 1F466 200D 1F466

:*::family:-woman,-boy,-boy::
Send,👩‍👦‍👦
return

;1F469 200D 1F467

:*::family:-woman,-girl::
Send,👩‍👧
return

;1F469 200D 1F467 200D 1F466

:*::family:-woman,-girl,-boy::
Send,👩‍👧‍👦
return

;1F469 200D 1F467 200D 1F467

:*::family:-woman,-girl,-girl::
Send,👩‍👧‍👧
return

;1F5E3 FE0F

:*::speaking-head::
Send,🗣️
return

;1F5E3

:*::speaking-head::
Send,🗣
return

;1F464
:*::shadow::
:*::silhouette::
:*::user::
:*::bust-in-silhouette::
Send,👤
return

;1F465
:*::shadows::
:*::silhouettes::
:*::users::
:*::busts-in-silhouette::
Send,👥
return

;1F463
:*::feet::
:*::footsteps::
:*::footprints::
Send,👣
return

;1F3FB

:*::light-skin-tone::
Send,🏻
return

;1F3FC

:*::medium-light-skin-tone::
Send,🏼
return

;1F3FD

:*::medium-skin-tone::
Send,🏽
return

;1F3FE

:*::medium-dark-skin-tone::
Send,🏾
return

;1F3FF

:*::dark-skin-tone::
Send,🏿
return

;1F9B0

:*::red-hair::
Send,🦰
return

;1F9B1

:*::curly-hair::
Send,🦱
return

;1F9B3

:*::white-hair::
Send,🦳
return

;1F9B2

:*::bald::
Send,🦲
return

;1F435
:*::monkey-head::
:*::monkey-face::
Send,🐵
return

;1F412
:*::cheeky-monkey::
:*::monkey::
Send,🐒
return

;1F98D

:*::gorilla::
Send,🦍
return

;1F9A7

:*::orangutan::
Send,🦧
return

;1F436
:*::dog::
:*::puppy::
:*::dog-face::
Send,🐶
return

;1F415
:*::doggo::
:*::dog::
Send,🐕
return

;1F9AE
:*::seeing-eye-dog::
:*::guide-dog::
Send,🦮
return

;1F415 200D 1F9BA

:*::service-dog::
Send,🐕‍🦺
return

;1F429
:*::miniature-poodle::
:*::standard-poodle::
:*::toy-poodle::
:*::poodle::
Send,🐩
return

;1F43A

:*::wolf::
Send,🐺
return

;1F98A

:*::fox::
Send,🦊
return

;1F99D

:*::raccoon::
Send,🦝
return

;1F431
:*:::3::
:*::kitten::
:*::kitty::
:*::cat-face::
Send,🐱
return

;1F408
:*::domestic-cat::
:*::feline::
:*::housecat::
:*::cat::
Send,🐈
return

;1F981

:*::lion::
Send,🦁
return

;1F42F
:*::cute-tiger::
:*::tiger-face::
Send,🐯
return

;1F405
:*::bengal-tiger::
:*::tiger::
Send,🐅
return

;1F406
:*::african-leopard::
:*::jaguar::
:*::leopard::
Send,🐆
return

;1F434
:*::horse-head::
:*::horse-face::
Send,🐴
return

;1F40E
:*::galloping-horse::
:*::racehorse::
:*::horse::
Send,🐎
return

;1F984

:*::unicorn::
Send,🦄
return

;1F993

:*::zebra::
Send,🦓
return

;1F98C
:*::buck::
:*::reindeer::
:*::stag::
:*::deer::
Send,🦌
return

;1F42E
:*::cow::
:*::happy-cow::
:*::cow-face::
Send,🐮
return

;1F402
:*::bull::
:*::bullock::
:*::oxen::
:*::steer::
:*::ox::
Send,🐂
return

;1F403
:*::buffalo::
:*::domestic-water-buffalo::
:*::water-buffalo::
Send,🐃
return

;1F404
:*::dairy-cow::
:*::cow::
Send,🐄
return

;1F437
:*::pig-head::
:*::pig-face::
Send,🐷
return

;1F416
:*::hog::
:*::sow::
:*::pig::
Send,🐖
return

;1F417
:*::warthog::
:*::wild-boar::
:*::wild-pig::
:*::boar::
Send,🐗
return

;1F43D
:*::pig-snout::
:*::pig-nose::
Send,🐽
return

;1F40F
:*::sheep::
:*::ram::
Send,🐏
return

;1F411

:*::ewe::
Send,🐑
return

;1F410

:*::goat::
Send,🐐
return

;1F42A

:*::camel::
Send,🐪
return

;1F42B

:*::two-hump-camel::
Send,🐫
return

;1F999
:*::alpaca::
:*::llama::
Send,🦙
return

;1F992

:*::giraffe::
Send,🦒
return

;1F418

:*::elephant::
Send,🐘
return

;1F98F
:*::rhino::
:*::rhinoceros::
Send,🦏
return

;1F99B
:*::hippo::
:*::hippopotamus::
Send,🦛
return

;1F42D
:*::mouse::
:*::mouse-face::
Send,🐭
return

;1F401
:*::dormouse::
:*::mice::
:*::rodent::
:*::mouse::
Send,🐁
return

;1F400
:*::rodent::
:*::rat::
Send,🐀
return

;1F439

:*::hamster::
Send,🐹
return

;1F430
:*::easter-bunny::
:*::rabbit-face::
Send,🐰
return

;1F407
:*::bunny::
:*::bunny-rabbit::
:*::rabbit::
Send,🐇
return

;1F43F FE0F
:*::squirrel::
:*::chipmunk::
Send,🐿️
return

;1F43F
:*::squirrel::
:*::chipmunk::
Send,🐿
return

;1F994

:*::hedgehog::
Send,🦔
return

;1F987
:*::batman::
:*::bat::
Send,🦇
return

;1F43B

:*::bear::
Send,🐻
return

;1F428
:*::koala-bear::
:*::koala::
Send,🐨
return

;1F43C

:*::panda::
Send,🐼
return

;1F9A5

:*::sloth::
Send,🦥
return

;1F9A6

:*::otter::
Send,🦦
return

;1F9A8

:*::skunk::
Send,🦨
return

;1F998
:*::roo::
:*::kangaroo::
Send,🦘
return

;1F9A1

:*::badger::
Send,🦡
return

;1F43E
:*::cat-paw-prints::
:*::dog-paw-prints::
:*::kitten-paw-prints::
:*::puppy-paw-prints::
:*::paw-prints::
Send,🐾
return

;1F983
:*::thanksgiving-turkey::
:*::wild-turkey::
:*::turkey::
Send,🦃
return

;1F414
:*::hen::
:*::chicken::
Send,🐔
return

;1F413
:*::cock::
:*::cockerel::
:*::rooster::
Send,🐓
return

;1F423
:*::baby-chicken::
:*::chick-hatching::
:*::hatching-chick::
Send,🐣
return

;1F424
:*::yellow-bird::
:*::baby-chick::
Send,🐤
return

;1F425
:*::baby-chick::
:*::front-facing-baby-chick::
Send,🐥
return

;1F426

:*::bird::
Send,🐦
return

;1F427

:*::penguin::
Send,🐧
return

;1F54A FE0F

:*::dove::
Send,🕊️
return

;1F54A

:*::dove::
Send,🕊
return

;1F985
:*::bald-eagle::
:*::eagle::
Send,🦅
return

;1F986

:*::duck::
Send,🦆
return

;1F9A2

:*::swan::
Send,🦢
return

;1F989

:*::owl::
Send,🦉
return

;1F9A9

:*::flamingo::
Send,🦩
return

;1F99A

:*::peacock::
Send,🦚
return

;1F99C

:*::parrot::
Send,🦜
return

;1F438

:*::frog::
Send,🐸
return

;1F40A
:*::alligator::
:*::croc::
:*::crocodile::
Send,🐊
return

;1F422
:*::tortoise::
:*::turtle::
Send,🐢
return

;1F98E
:*::gecko::
:*::lizard::
Send,🦎
return

;1F40D
:*::serpent::
:*::snake::
Send,🐍
return

;1F432
:*::dragon-head::
:*::dragon-face::
Send,🐲
return

;1F409

:*::dragon::
Send,🐉
return

;1F995
:*::brachiosaurus::
:*::brontosaurus::
:*::dinosaur::
:*::sauropod::
Send,🦕
return

;1F996
:*::tyrannosaurus-rex::
:*::t-rex::
Send,🦖
return

;1F433
:*::cute-whale::
:*::spouting-whale::
Send,🐳
return

;1F40B

:*::whale::
Send,🐋
return

;1F42C

:*::dolphin::
Send,🐬
return

;1F41F
:*::freshwater-fish::
:*::fish::
Send,🐟
return

;1F420
:*::fish::
:*::yellow-blue-fish::
:*::tropical-fish::
Send,🐠
return

;1F421
:*::fugu::
:*::pufferfish::
:*::blowfish::
Send,🐡
return

;1F988
:*::great-white-shark::
:*::shark::
Send,🦈
return

;1F419

:*::octopus::
Send,🐙
return

;1F41A
:*::beach::
:*::seashell::
:*::shell::
:*::spiral-shell::
Send,🐚
return

;1F40C
:*::garden-snail::
:*::slug::
:*::snail::
Send,🐌
return

;1F98B

:*::butterfly::
Send,🦋
return

;1F41B
:*::caterpillar::
:*::insect::
:*::bug::
Send,🐛
return

;1F41C
:*::bug::
:*::insect::
:*::ant::
Send,🐜
return

;1F41D
:*::bee::
:*::bumblebee::
:*::honeybee::
Send,🐝
return

;1F41E
:*::ladybird::
:*::ladybug::
:*::lady-bug::
:*::lady-beetle::
Send,🐞
return

;1F997
:*::grasshopper::
:*::cricket::
Send,🦗
return

;1F577 FE0F

:*::spider::
Send,🕷️
return

;1F577

:*::spider::
Send,🕷
return

;1F578 FE0F
:*::cobweb::
:*::web::
:*::spider-web::
Send,🕸️
return

;1F578
:*::cobweb::
:*::web::
:*::spider-web::
Send,🕸
return

;1F982

:*::scorpion::
Send,🦂
return

;1F99F

:*::mosquito::
Send,🦟
return

;1F9A0
:*::cell::
:*::germ::
:*::microorganism::
:*::microbe::
Send,🦠
return

;1F490
:*::bouquet-of-flowers::
:*::bouquet::
Send,💐
return

;1F338
:*::pink-flower::
:*::sakura::
:*::cherry-blossom::
Send,🌸
return

;1F4AE
:*::cherry-blossom::
:*::paper-doily::
:*::well-done-stamp::
:*::white-flower::
Send,💮
return

;1F3F5 FE0F

:*::rosette::
Send,🏵️
return

;1F3F5

:*::rosette::
Send,🏵
return

;1F339
:*::red-flower::
:*::red-rose::
:*::rose::
Send,🌹
return

;1F940
:*::dead-flower::
:*::drooping-flower::
:*::wilted-flower::
Send,🥀
return

;1F33A

:*::hibiscus::
Send,🌺
return

;1F33B
:*::yellow-flower::
:*::sunflower::
Send,🌻
return

;1F33C
:*::blossoming flower::
:*::daisy::
:*::yellow-flower::
:*::blossom::
Send,🌼
return

;1F337

:*::tulip::
Send,🌷
return

;1F331
:*::spring::
:*::sprout::
:*::sprouting::
:*::seedling::
Send,🌱
return

;1F332
:*::fir-tree::
:*::pine-tree::
:*::tree::
:*::evergreen-tree::
Send,🌲
return

;1F333
:*::rounded-tree::
:*::deciduous-tree::
Send,🌳
return

;1F334
:*::coconut-tree::
:*::palm-tree::
Send,🌴
return

;1F335
:*::desert::
:*::cactus::
Send,🌵
return

;1F33E

:*::sheaf-of-rice::
Send,🌾
return

;1F33F
:*::crop::
:*::plant::
:*::herb::
Send,🌿
return

;2618 FE0F
:*::clover::
:*::trefoil::
:*::shamrock::
Send,☘️
return

;2618
:*::clover::
:*::trefoil::
:*::shamrock::
Send,☘
return

;1F340
:*::clover::
:*::ireland::
:*::lucky::
:*::four-leaf-clover::
Send,🍀
return

;1F341
:*::canada::
:*::canadian::
:*::maple::
:*::maple-leaf::
Send,🍁
return

;1F342
:*::autumn-leaves::
:*::brown-leaves::
:*::fall-leaves::
:*::fallen-leaf::
Send,🍂
return

;1F343
:*::green-leaves::
:*::spring::
:*::leaf-fluttering-in-wind::
Send,🍃
return

;1F347
:*::grape::
:*::grapes::
Send,🍇
return

;1F348
:*::cantaloupe::
:*::honeydew::
:*::muskmelon::
:*::melon::
Send,🍈
return

;1F349

:*::watermelon::
Send,🍉
return

;1F34A
:*::mandarin::
:*::orange::
:*::tangerine::
Send,🍊
return

;1F34B
:*::lemonade::
:*::lemon::
Send,🍋
return

;1F34C
:*::plantain::
:*::banana::
Send,🍌
return

;1F34D

:*::pineapple::
Send,🍍
return

;1F96D

:*::mango::
Send,🥭
return

;1F34E
:*::red-delicious-apple::
:*::red-apple::
Send,🍎
return

;1F34F
:*::golden-delicious-apple::
:*::granny-smith-apple::
:*::green-apple::
Send,🍏
return

;1F350

:*::pear::
Send,🍐
return

;1F351
:*::bottom::
:*::butt::
:*::peach::
Send,🍑
return

;1F352
:*::cherry::
:*::wild-cherry::
:*::cherries::
Send,🍒
return

;1F353

:*::strawberry::
Send,🍓
return

;1F95D

:*::kiwi-fruit::
Send,🥝
return

;1F345

:*::tomato::
Send,🍅
return

;1F965
:*::cocoanut::
:*::coconut::
Send,🥥
return

;1F951

:*::avocado::
Send,🥑
return

;1F346

:*::eggplant::
Send,🍆
return

;1F954
:*::baked-potato::
:*::idaho-potato::
:*::potato::
Send,🥔
return

;1F955

:*::carrot::
Send,🥕
return

;1F33D

:*::ear-of-corn::
Send,🌽
return

;1F336 FE0F
:*::chili-pepper::
:*::spicy::
:*::hot-pepper::
Send,🌶️
return

;1F336
:*::chili-pepper::
:*::spicy::
:*::hot-pepper::
Send,🌶
return

;1F952
:*::gherkin::
:*::pickle::
:*::cucumber::
Send,🥒
return

;1F96C
:*::bok-choy::
:*::chinese-cabbage::
:*::cos-lettuce::
:*::romaine-lettuce::
:*::leafy-green::
Send,🥬
return

;1F966

:*::broccoli::
Send,🥦
return

;1F9C4

:*::garlic::
Send,🧄
return

;1F9C5

:*::onion::
Send,🧅
return

;1F344
:*::shroom::
:*::toadstool::
:*::mushroom::
Send,🍄
return

;1F95C
:*::nuts::
:*::peanuts::
Send,🥜
return

;1F330
:*::acorn::
:*::nut::
:*::chestnut::
Send,🌰
return

;1F35E
:*::loaf-of-bread::
:*::bread::
Send,🍞
return

;1F950

:*::croissant::
Send,🥐
return

;1F956
:*::french-bread::
:*::baguette-bread::
Send,🥖
return

;1F968

:*::pretzel::
Send,🥨
return

;1F96F

:*::bagel::
Send,🥯
return

;1F95E
:*::crêpes::
:*::hotcakes::
:*::pancakes::
Send,🥞
return

;1F9C7

:*::waffle::
Send,🧇
return

;1F9C0
:*::cheese::
:*::cheese-wedge::
Send,🧀
return

;1F356
:*::barbecue::
:*::bbq::
:*::manga-meat::
:*::meat-on-bone::
Send,🍖
return

;1F357
:*::drumstick::
:*::turkey-leg::
:*::poultry-leg::
Send,🍗
return

;1F969
:*::meat::
:*::steak::
:*::cut-of-meat::
Send,🥩
return

;1F953
:*::rashers::
:*::bacon::
Send,🥓
return

;1F354
:*::burger::
:*::cheeseburger::
:*::hamburger::
Send,🍔
return

;1F35F
:*::chips::
:*::fries::
:*::mcdonald's-fries::
:*::french-fries::
Send,🍟
return

;1F355

:*::pizza::
Send,🍕
return

;1F32D
:*::hotdog::
:*::sausage::
:*::hot-dog::
Send,🌭
return

;1F96A

:*::sandwich::
Send,🥪
return

;1F32E

:*::taco::
Send,🌮
return

;1F32F
:*::wrap::
:*::burrito::
Send,🌯
return

;1F959
:*::doner-kebab::
:*::gyro::
:*::shawarma::
:*::stuffed-flatbread::
Send,🥙
return

;1F9C6

:*::falafel::
Send,🧆
return

;1F95A

:*::egg::
Send,🥚
return

;1F373
:*::breakfast::
:*::fried-egg::
:*::frying-pan::
:*::cooking::
Send,🍳
return

;1F958
:*::paella::
:*::shallow-pan-of-food::
Send,🥘
return

;1F372
:*::bowl-of-food::
:*::soup::
:*::stew::
:*::pot-of-food::
Send,🍲
return

;1F963
:*::cereal-bowl::
:*::bowl-with-spoon::
Send,🥣
return

;1F957
:*::salad::
:*::green-salad::
Send,🥗
return

;1F37F
:*::popping-corn::
:*::popcorn::
Send,🍿
return

;1F9C8

:*::butter::
Send,🧈
return

;1F9C2

:*::salt::
Send,🧂
return

;1F96B
:*::can-of-food::
:*::tin-can::
:*::tinned-food::
:*::canned-food::
Send,🥫
return

;1F371
:*::lunch-box::
:*::bento-box::
Send,🍱
return

;1F358
:*::cracker::
:*::rice-cracker::
Send,🍘
return

;1F359
:*::triangle-rice::
:*::rice-ball::
Send,🍙
return

;1F35A
:*::boiled-rice::
:*::bowl-of-rice::
:*::rice::
:*::steamed-rice::
:*::cooked-rice::
Send,🍚
return

;1F35B

:*::curry-rice::
Send,🍛
return

;1F35C
:*::noodles::
:*::noodles-with-chopsticks::
:*::ramen::
:*::steaming-bowl::
Send,🍜
return

;1F35D
:*::pasta::
:*::spaghetti::
Send,🍝
return

;1F360
:*::sweet-potato::
:*::yam::
:*::roasted-sweet-potato::
Send,🍠
return

;1F362
:*::kebab::
:*::skewer::
:*::oden::
Send,🍢
return

;1F363
:*::sashimi::
:*::seafood::
:*::sushi::
Send,🍣
return

;1F364
:*::fried-prawn::
:*::shrimp-tempura::
:*::fried-shrimp::
Send,🍤
return

;1F365

:*::fish-cake-with-swirl::
Send,🍥
return

;1F96E

:*::moon-cake::
Send,🥮
return

;1F361
:*::dessert-stick::
:*::pink-white-green-balls::
:*::dango::
Send,🍡
return

;1F95F
:*::empanada::
:*::pierogi::
:*::dumpling::
Send,🥟
return

;1F960

:*::fortune-cookie::
Send,🥠
return

;1F961
:*::chinese-food-box::
:*::oyster-pail::
:*::takeout-box::
Send,🥡
return

;1F980

:*::crab::
Send,🦀
return

;1F99E

:*::lobster::
Send,🦞
return

;1F990
:*::prawn::
:*::shrimp::
Send,🦐
return

;1F991

:*::squid::
Send,🦑
return

;1F9AA

:*::oyster::
Send,🦪
return

;1F366
:*::mr.-whippy::
:*::soft-serve::
:*::soft-ice-cream::
Send,🍦
return

;1F367
:*::snow-cone::
:*::shaved-ice::
Send,🍧
return

;1F368
:*::bowl-of-ice-cream::
:*::dessert::
:*::ice-cream::
Send,🍨
return

;1F369
:*::donut::
:*::doughnut::
Send,🍩
return

;1F36A
:*::biscuit::
:*::chocolate-chip-cookie::
:*::cookie::
Send,🍪
return

;1F382
:*::birthday::
:*::cake::
:*::cake-with-candles::
:*::birthday-cake::
Send,🎂
return

;1F370
:*::cake::
:*::piece-of-cake::
:*::strawberry-shortcake::
:*::shortcake::
Send,🍰
return

;1F9C1
:*::fairy-cake::
:*::cupcake::
Send,🧁
return

;1F967

:*::pie::
Send,🥧
return

;1F36B
:*::candy-bar::
:*::chocolate::
:*::chocolate-bar::
Send,🍫
return

;1F36C
:*::lolly::
:*::sweet::
:*::candy::
Send,🍬
return

;1F36D
:*::lollypop::
:*::sucker::
:*::lollipop::
Send,🍭
return

;1F36E
:*::creme-caramel::
:*::dessert::
:*::flan::
:*::pudding::
:*::custard::
Send,🍮
return

;1F36F
:*::honey::
:*::pot::
:*::honey-pot::
Send,🍯
return

;1F37C
:*::bottle-feeding::
:*::baby-bottle::
Send,🍼
return

;1F95B
:*::milk::
:*::glass-of-milk::
Send,🥛
return

;2615
:*::coffee::
:*::espresso::
:*::hot-chocolate::
:*::tea::
:*::hot-beverage::
Send,☕
return

;1F375
:*::green-tea::
:*::matcha::
:*::matcha-green-tea::
:*::teacup-without-handle::
Send,🍵
return

;1F376

:*::sake::
Send,🍶
return

;1F37E
:*::celebration::
:*::champagne::
:*::sparkling-wine::
:*::bottle-with-popping-cork::
Send,🍾
return

;1F377
:*::alcohol::
:*::red-wine::
:*::wine::
:*::wine-glass::
Send,🍷
return

;1F378
:*::cocktail::
:*::martini::
:*::cocktail-glass::
Send,🍸
return

;1F379
:*::fruit-punch::
:*::tiki-drink::
:*::tropical-drink::
Send,🍹
return

;1F37A
:*::beer::
:*::beer-stein::
:*::beer-mug::
Send,🍺
return

;1F37B
:*::beers::
:*::cheers::
:*::clinking-beer-mugs::
Send,🍻
return

;1F942
:*::celebration::
:*::champagne-glasses::
:*::cheers::
:*::clinking-glasses::
Send,🥂
return

;1F943
:*::bourbon::
:*::liquor::
:*::rum::
:*::whiskey::
:*::whisky::
:*::tumbler-glass::
Send,🥃
return

;1F964
:*::milkshake::
:*::smoothie::
:*::soda-pop::
:*::soft-drink::
:*::cup-with-straw::
Send,🥤
return

;1F9C3
:*::juice-box::
:*::beverage-box::
Send,🧃
return

;1F9C9

:*::mate::
Send,🧉
return

;1F9CA

:*::ice-cube::
Send,🧊
return

;1F962

:*::chopsticks::
Send,🥢
return

;1F37D FE0F
:*::dinner::
:*::fork-and-knife-with-plate::
Send,🍽️
return

;1F37D
:*::dinner::
:*::fork-and-knife-with-plate::
Send,🍽
return

;1F374
:*::cutlery::
:*::knife-and-fork::
:*::silverware::
:*::fork-and-knife::
Send,🍴
return

;1F944

:*::spoon::
Send,🥄
return

;1F52A

:*::kitchen-knife::
Send,🔪
return

;1F3FA
:*::jar::
:*::vase::
:*::amphora::
Send,🏺
return

;1F30D

:*::globe-showing-europe-africa::
Send,🌍
return

;1F30E

:*::globe-showing-americas::
Send,🌎
return

;1F30F

:*::globe-showing-asia-australia::
Send,🌏
return

;1F310
:*::internet::
:*::world-wide-web::
:*::www::
:*::globe-with-meridians::
Send,🌐
return

;1F5FA FE0F

:*::world-map::
Send,🗺️
return

;1F5FA

:*::world-map::
Send,🗺
return

;1F5FE

:*::map-of-japan::
Send,🗾
return

;1F9ED

:*::compass::
Send,🧭
return

;1F3D4 FE0F

:*::snow-capped-mountain::
Send,🏔️
return

;1F3D4

:*::snow-capped-mountain::
Send,🏔
return

;26F0 FE0F

:*::mountain::
Send,⛰️
return

;26F0

:*::mountain::
Send,⛰
return

;1F30B

:*::volcano::
Send,🌋
return

;1F5FB
:*::fuji-san::
:*::snow-capped-mountain::
:*::mount-fuji::
Send,🗻
return

;1F3D5 FE0F
:*::campsite::
:*::camping::
Send,🏕️
return

;1F3D5
:*::campsite::
:*::camping::
Send,🏕
return

;1F3D6 FE0F

:*::beach-with-umbrella::
Send,🏖️
return

;1F3D6

:*::beach-with-umbrella::
Send,🏖
return

;1F3DC FE0F

:*::desert::
Send,🏜️
return

;1F3DC

:*::desert::
Send,🏜
return

;1F3DD FE0F

:*::desert-island::
Send,🏝️
return

;1F3DD

:*::desert-island::
Send,🏝
return

;1F3DE FE0F

:*::national-park::
Send,🏞️
return

;1F3DE

:*::national-park::
Send,🏞
return

;1F3DF FE0F
:*::grandstand::
:*::sport-stadium::
:*::stadium::
Send,🏟️
return

;1F3DF
:*::grandstand::
:*::sport-stadium::
:*::stadium::
Send,🏟
return

;1F3DB FE0F

:*::classical-building::
Send,🏛️
return

;1F3DB

:*::classical-building::
Send,🏛
return

;1F3D7 FE0F
:*::crane::
:*::building-construction::
Send,🏗️
return

;1F3D7
:*::crane::
:*::building-construction::
Send,🏗
return

;1F9F1

:*::brick::
Send,🧱
return

;1F3D8 FE0F

:*::houses::
Send,🏘️
return

;1F3D8

:*::houses::
Send,🏘
return

;1F3DA FE0F

:*::derelict-house::
Send,🏚️
return

;1F3DA

:*::derelict-house::
Send,🏚
return

;1F3E0

:*::house::
Send,🏠
return

;1F3E1
:*::house-and-tree::
:*::house-with-garden::
Send,🏡
return

;1F3E2
:*::city-building::
:*::high-rise-building::
:*::office-building::
Send,🏢
return

;1F3E3
:*::japanese-postal-mark::
:*::japan-post::
:*::japanese-post-office::
Send,🏣
return

;1F3E4

:*::post-office::
Send,🏤
return

;1F3E5
:*::emergency-room::
:*::medical::
:*::red-cross::
:*::hospital::
Send,🏥
return

;1F3E6
:*::bakkureru::
:*::bank-branch::
:*::bk::
:*::bank::
Send,🏦
return

;1F3E8
:*::accommodation::
:*::h-building::
:*::hotel::
Send,🏨
return

;1F3E9
:*::heart-hospital::
:*::love-heart-hotel::
:*::love-hotel::
Send,🏩
return

;1F3EA
:*::24-hour-store::
:*::7-eleven®::
:*::corner-shop::
:*::kwik-e-mart::
:*::convenience-store::
Send,🏪
return

;1F3EB
:*::clock-tower::
:*::elementary-school::
:*::high-school::
:*::middle-school::
:*::school::
Send,🏫
return

;1F3EC
:*::shopping-center::
:*::shops::
:*::department-store::
Send,🏬
return

;1F3ED
:*::industrial::
:*::industry::
:*::pollution::
:*::smog::
:*::factory::
Send,🏭
return

;1F3EF
:*::fortress::
:*::japanese-castle::
Send,🏯
return

;1F3F0

:*::castle::
Send,🏰
return

;1F492
:*::church-heart::
:*::church-wedding::
:*::marriage::
:*::wedding::
Send,💒
return

;1F5FC
:*::eiffel-tower::
:*::red-tower::
:*::tokyo-tower::
Send,🗼
return

;1F5FD
:*::new-york::
:*::statue-of-liberty::
Send,🗽
return

;26EA
:*::church-building::
:*::cross::
:*::church::
Send,⛪
return

;1F54C
:*::domed-roof::
:*::minaret::
:*::mosque::
Send,🕌
return

;1F6D5

:*::hindu-temple::
Send,🛕
return

;1F54D
:*::jewish::
:*::synagog::
:*::temple::
:*::synagogue::
Send,🕍
return

;26E9 FE0F
:*::kami-no-michi::
:*::shinto-shrine::
Send,⛩️
return

;26E9
:*::kami-no-michi::
:*::shinto-shrine::
Send,⛩
return

;1F54B
:*::mecca::
:*::kaaba::
Send,🕋
return

;26F2
:*::park::
:*::water-feature::
:*::water-fountain::
:*::fountain::
Send,⛲
return

;26FA
:*::camping-tent::
:*::tent::
Send,⛺
return

;1F301
:*::fog::
:*::foggy-city::
:*::fog bridge::
:*::foggy::
Send,🌁
return

;1F303
:*::city-at-night::
:*::starry-night::
:*::night-with-stars::
Send,🌃
return

;1F3D9 FE0F

:*::cityscape::
Send,🏙️
return

;1F3D9

:*::cityscape::
Send,🏙
return

;1F304
:*::morning::
:*::sunrise::
:*::sunrise-over-mountains::
Send,🌄
return

;1F305
:*::sunset::
:*::sunrise::
Send,🌅
return

;1F306
:*::dusk-city::
:*::orange-sky-city::
:*::cityscape-at-dusk::
Send,🌆
return

;1F307

:*::sunset::
Send,🌇
return

;1F309
:*::bridge::

:*::golden-gate-bridge::
:*::bridge-at-night::
Send,🌉
return

;2668 FE0F
:*::onsen::
:*::steam::
:*::hot-springs::
Send,♨️
return

;2668
:*::onsen::
:*::steam::
:*::hot-springs::
Send,♨
return

;1F3A0
:*::carnival::
:*::fairground::
:*::merry-go-round::
:*::carousel-horse::
Send,🎠
return

;1F3A1
:*::big-wheel::
:*::fairground::
:*::observation-wheel::
:*::ferris-wheel::
Send,🎡
return

;1F3A2
:*::rollercoaster::
:*::theme-park::
:*::roller-coaster::
Send,🎢
return

;1F488
:*::barber-shop::
:*::barber's-stripes::
:*::hairdresser::
:*::barber-pole::
Send,💈
return

;1F3AA
:*::big-top::
:*::circus::
:*::circus-tent::
Send,🎪
return

;1F682

:*::locomotive::
Send,🚂
return

;1F683
:*::railcar::
:*::railroad-car::
:*::railway-carriage::
:*::railway-wagon::
:*::railway-car::
Send,🚃
return

;1F684

:*::high-speed-train::
Send,🚄
return

;1F685

:*::bullet-train::
Send,🚅
return

;1F686
:*::diesel-train::
:*::electric-train::
:*::passenger-train::
:*::regular-train::
:*::train::
Send,🚆
return

;1F687
:*::subway::
:*::tube::
:*::underground::
:*::metro::
Send,🚇
return

;1F688

:*::light-rail::
Send,🚈
return

;1F689
:*::train-platform::
:*::train-station::
:*::station::
Send,🚉
return

;1F68A

:*::tram::
Send,🚊
return

;1F69D

:*::monorail::
Send,🚝
return

;1F69E
:*::funicular::
:*::train-and-mountain::
:*::mountain-railway::
Send,🚞
return

;1F68B

:*::tram-car::
Send,🚋
return

;1F68C
:*::school-bus::
:*::bus::
Send,🚌
return

;1F68D
:*::front-of-bus::
:*::oncoming-bus::
Send,🚍
return

;1F68E
:*::electric bus::
:*::trolley-bus::
:*::trolleybus::
Send,🚎
return

;1F690
:*::minivan::
:*::people-mover::
:*::minibus::
Send,🚐
return

;1F691

:*::ambulance::
Send,🚑
return

;1F692
:*::fire-department::
:*::fire-truck::
:*::fire-engine::
Send,🚒
return

;1F693
:*::cop-car::
:*::side-of-police-car::
:*::police-car::
Send,🚓
return

;1F694
:*::🚓-front of-police-car::
:*::🚓 cop-car::
:*::oncoming-police-car::
Send,🚔
return

;1F695
:*::new-york-taxi::
:*::side-of-taxi::
:*::taxicab::
:*::taxi::
Send,🚕
return

;1F696
:*::front-of-taxi::
:*::taxicab::
:*::oncoming-taxi::
Send,🚖
return

;1F697
:*::car::
:*::red-car::
:*::side-of-car::
:*::automobile::
Send,🚗
return

;1F698
:*::front-of-car::
:*::oncoming-automobile::
Send,🚘
return

;1F699

:*::sport-utility-vehicle::
Send,🚙
return

;1F69A

:*::delivery-truck::
Send,🚚
return

;1F69B
:*::green-truck::
:*::truck::
:*::articulated-lorry::
Send,🚛
return

;1F69C
:*::farm::

:*::farming::
:*::tractor::
Send,🚜
return

;1F3CE FE0F
:*::f1::
:*::formula-one::
:*::racing-car::
Send,🏎️
return

;1F3CE
:*::f1::
:*::formula-one::
:*::racing-car::
Send,🏎
return

;1F3CD FE0F

:*::motorcycle::
Send,🏍️
return

;1F3CD

:*::motorcycle::
Send,🏍
return

;1F6F5
:*::motor-bike::
:*::motor-cycle::
:*::vespa::
:*::motor-scooter::
Send,🛵
return

;1F9BD

:*::manual-wheelchair::
Send,🦽
return

;1F9BC

:*::motorized-wheelchair::
Send,🦼
return

;1F6FA
:*::tuk-tuk::
:*::auto-rickshaw::
Send,🛺
return

;1F6B2
:*::bike::

:*::push-bike::
:*::bicycle::
Send,🚲
return

;1F6F4

:*::kick-scooter::
Send,🛴
return

;1F6F9

:*::skateboard::
Send,🛹
return

;1F68F

:*::bus-stop::
Send,🚏
return

;1F6E3 FE0F
:*::highway::
:*::interstate::
:*::road::
:*::motorway::
Send,🛣️
return

;1F6E3
:*::highway::
:*::interstate::
:*::road::
:*::motorway::
Send,🛣
return

;1F6E4 FE0F

:*::railway-track::
Send,🛤️
return

;1F6E4

:*::railway-track::
Send,🛤
return

;1F6E2 FE0F

:*::oil-drum::
Send,🛢️
return

;1F6E2

:*::oil-drum::
Send,🛢
return

;26FD
:*::gas-pump::
:*::petrol-pump::
:*::fuel-pump::
Send,⛽
return

;1F6A8

:*::police-car-light::
Send,🚨
return

;1F6A5
:*::traffic-light::
:*::horizontal-traffic-light::
Send,🚥
return

;1F6A6
:*::traffic-light::
:*::vertical-traffic-light::
Send,🚦
return

;1F6D1

:*::stop-sign::
Send,🛑
return

;1F6A7

:*::construction::
Send,🚧
return

;2693
:*::admiralty-pattern-anchor::
:*::fisherman::
:*::anchor::
Send,⚓
return

;26F5
:*::dinghy::
:*::yacht::
:*::sailboat::
Send,⛵
return

;1F6F6

:*::canoe::
Send,🛶
return

;1F6A4
:*::motorboat::
:*::powerboat::
:*::speedboat::
Send,🚤
return

;1F6F3 FE0F

:*::passenger-ship::
Send,🛳️
return

;1F6F3

:*::passenger-ship::
Send,🛳
return

;26F4 FE0F

:*::ferry::
Send,⛴️
return

;26F4

:*::ferry::
Send,⛴
return

;1F6E5 FE0F

:*::motor-boat::
Send,🛥️
return

;1F6E5

:*::motor-boat::
Send,🛥
return

;1F6A2
:*::cruise::
:*::cruise-ship::
:*::ship::
Send,🚢
return

;2708 FE0F
:*::aeroplane::
:*::plane::
:*::airplane::
Send,✈️
return

;2708
:*::aeroplane::
:*::plane::
:*::airplane::
Send,✈
return

;1F6E9 FE0F
:*::small-aeroplane::
:*::small-plane::
:*::small-airplane::
Send,🛩️
return

;1F6E9
:*::small-aeroplane::
:*::small-plane::
:*::small-airplane::
Send,🛩
return

;1F6EB
:*::aeroplane-taking-off::
:*::plane-taking-off::
:*::airplane-departure::
Send,🛫
return

;1F6EC

:*::airplane-arrival::
Send,🛬
return

;1FA82

:*::parachute::
Send,🪂
return

;1F4BA
:*::aeroplane-seat::
:*::airplane-seat::
:*::bus-seat::
:*::train-seat::
:*::seat::
Send,💺
return

;1F681

:*::helicopter::
Send,🚁
return

;1F69F

:*::suspension-railway::
Send,🚟
return

;1F6A0

:*::mountain-cableway::
Send,🚠
return

;1F6A1
:*::cable-car::
:*::gondola::
:*::ropeway::
:*::aerial-tramway::
Send,🚡
return

;1F6F0 FE0F

:*::satellite::
Send,🛰️
return

;1F6F0

:*::satellite::
Send,🛰
return

;1F680
:*::rocket-ship::
:*::space-shuttle::
:*::rocket::
Send,🚀
return

;1F6F8
:*::ufo::
:*::flying-saucer::
Send,🛸
return

;1F6CE FE0F

:*::bellhop-bell::
Send,🛎️
return

;1F6CE

:*::bellhop-bell::
Send,🛎
return

;1F9F3
:*::suitcase::
:*::luggage::
Send,🧳
return

;231B

:*::hourglass-done::
Send,⌛
return

;23F3

:*::hourglass-not-done::
Send,⏳
return

;231A
:*::apple-watch::
:*::timepiece::
:*::wrist-watch::
:*::watch::
Send,⌚
return

;23F0
:*::alarm::
:*::clock::
:*::alarm-clock::
Send,⏰
return

;23F1 FE0F

:*::stopwatch::
Send,⏱️
return

;23F1

:*::stopwatch::
Send,⏱
return

;23F2 FE0F

:*::timer-clock::
Send,⏲️
return

;23F2

:*::timer-clock::
Send,⏲
return

;1F570 FE0F

:*::mantelpiece-clock::
Send,🕰️
return

;1F570

:*::mantelpiece-clock::
Send,🕰
return

;1F55B

:*::twelve-o’clock::
Send,🕛
return

;1F567

:*::twelve-thirty::
Send,🕧
return

;1F550

:*::one-o’clock::
Send,🕐
return

;1F55C

:*::one-thirty::
Send,🕜
return

;1F551

:*::two-o’clock::
Send,🕑
return

;1F55D

:*::two-thirty::
Send,🕝
return

;1F552

:*::three-o’clock::
Send,🕒
return

;1F55E

:*::three-thirty::
Send,🕞
return

;1F553

:*::four-o’clock::
Send,🕓
return

;1F55F

:*::four-thirty::
Send,🕟
return

;1F554

:*::five-o’clock::
Send,🕔
return

;1F560

:*::five-thirty::
Send,🕠
return

;1F555

:*::six-o’clock::
Send,🕕
return

;1F561

:*::six-thirty::
Send,🕡
return

;1F556

:*::seven-o’clock::
Send,🕖
return

;1F562

:*::seven-thirty::
Send,🕢
return

;1F557

:*::eight-o’clock::
Send,🕗
return

;1F563

:*::eight-thirty::
Send,🕣
return

;1F558

:*::nine-o’clock::
Send,🕘
return

;1F564

:*::nine-thirty::
Send,🕤
return

;1F559

:*::ten-o’clock::
Send,🕙
return

;1F565

:*::ten-thirty::
Send,🕥
return

;1F55A

:*::eleven-o’clock::
Send,🕚
return

;1F566

:*::eleven-thirty::
Send,🕦
return

;1F311

:*::new-moon::
Send,🌑
return

;1F312

:*::waxing-crescent-moon::
Send,🌒
return

;1F313

:*::first-quarter-moon::
Send,🌓
return

;1F314

:*::waxing-gibbous-moon::
Send,🌔
return

;1F315

:*::full-moon::
Send,🌕
return

;1F316

:*::waning-gibbous-moon::
Send,🌖
return

;1F317

:*::last-quarter-moon::
Send,🌗
return

;1F318

:*::waning-crescent-moon::
Send,🌘
return

;1F319

:*::crescent-moon::
Send,🌙
return

;1F31A

:*::new-moon-face::
Send,🌚
return

;1F31B

:*::first-quarter-moon-face::
Send,🌛
return

;1F31C

:*::last-quarter-moon-face::
Send,🌜
return

;1F321 FE0F
:*::hot-weather::
:*::temperature::
:*::thermometer::
Send,🌡️
return

;1F321
:*::hot-weather::
:*::temperature::
:*::thermometer::
Send,🌡
return

;2600 FE0F

:*::sun::
Send,☀️
return

;2600

:*::sun::
Send,☀
return

;1F31D

:*::full-moon-face::
Send,🌝
return

;1F31E
:*::smiley-sun::
:*::smiling-sun::
:*::sunface::
:*::sun-with-face::
Send,🌞
return

;1FA90
:*::saturn::
:*::ringed-planet::
Send,🪐
return

;2B50

:*::star::
Send,⭐
return

;1F31F
:*::shining-star::
:*::glowing-star::
Send,🌟
return

;1F320
:*::meteoroid::
:*::when-you-wish-upon-a-star::
:*::shooting-star::
Send,🌠
return

;1F30C
:*::galaxy::
:*::night-sky::
:*::space::
:*::stars::
:*::universe::
:*::milky-way::
Send,🌌
return

;2601 FE0F
:*::cloudy::
:*::overcast::
:*::cloud::
Send,☁️
return

;2601
:*::cloudy::
:*::overcast::
:*::cloud::
Send,☁
return

;26C5

:*::sun-behind-cloud::
Send,⛅
return

;26C8 FE0F

:*::cloud-with-lightning-and-rain::
Send,⛈️
return

;26C8

:*::cloud-with-lightning-and-rain::
Send,⛈
return

;1F324 FE0F

:*::sun-behind-small-cloud::
Send,🌤️
return

;1F324

:*::sun-behind-small-cloud::
Send,🌤
return

;1F325 FE0F

:*::sun-behind-large-cloud::
Send,🌥️
return

;1F325

:*::sun-behind-large-cloud::
Send,🌥
return

;1F326 FE0F

:*::sun-behind-rain-cloud::
Send,🌦️
return

;1F326

:*::sun-behind-rain-cloud::
Send,🌦
return

;1F327 FE0F

:*::cloud-with-rain::
Send,🌧️
return

;1F327

:*::cloud-with-rain::
Send,🌧
return

;1F328 FE0F

:*::cloud-with-snow::
Send,🌨️
return

;1F328

:*::cloud-with-snow::
Send,🌨
return

;1F329 FE0F

:*::cloud-with-lightning::
Send,🌩️
return

;1F329

:*::cloud-with-lightning::
Send,🌩
return

;1F32A FE0F

:*::tornado::
Send,🌪️
return

;1F32A

:*::tornado::
Send,🌪
return

;1F32B FE0F

:*::fog::
Send,🌫️
return

;1F32B

:*::fog::
Send,🌫
return

;1F32C FE0F

:*::wind-face::
Send,🌬️
return

;1F32C

:*::wind-face::
Send,🌬
return

;1F300
:*::hurricane::
:*::spiral::
:*::swirl::
:*::tornado::
:*::cyclone::
Send,🌀
return

;1F308
:*::gay-pride::
:*::primary-rainbow::
:*::rainbow::
Send,🌈
return

;1F302
:*::collapsed umbrella::
:*::pink-umbrella::
:*::closed-umbrella::
Send,🌂
return

;2602 FE0F
:*::umbrella::
:*::umbrella::
Send,☂️
return

;2602
:*::umbrella::
:*::umbrella::
Send,☂
return

;2614
:*::raining::
:*::rainy::
:*::umbrella-with-rain-drops::
Send,☔
return

;26F1 FE0F

:*::umbrella-on-ground::
Send,⛱️
return

;26F1

:*::umbrella-on-ground::
Send,⛱
return

;26A1

:*::high-voltage::
Send,⚡
return

;2744 FE0F
:*::snow::
:*::snowing::
:*::snowflake::
Send,❄️
return

;2744
:*::snow::
:*::snowing::
:*::snowflake::
Send,❄
return

;2603 FE0F
:*::snowing-snowman::
:*::snowman::
Send,☃️
return

;2603
:*::snowing-snowman::
:*::snowman::
Send,☃
return

;26C4
:*::frosty-the-snowman::
:*::olaf::
:*::snowman::
:*::snowman-without-snow::
Send,⛄
return

;2604 FE0F

:*::comet::
Send,☄️
return

;2604

:*::comet::
Send,☄
return

;1F525
:*::flame::
:*::hot::
:*::lit::
:*::snapstreak::
:*::fire::
Send,🔥
return

;1F4A7
:*::water::
:*::water-drop::
:*::droplet::
Send,💧
return

;1F30A
:*::beach::
:*::ocean-wave::
:*::sea::
:*::waves::
:*::water-wave::
Send,🌊
return

;1F383
:*::halloween::
:*::pumpkin::
:*::jack-o-lantern::
Send,🎃
return

;1F384
:*::christmas::
:*::xmas tree::
:*::christmas-tree::
Send,🎄
return

;1F386
:*::explosion::
:*::fireworks::
Send,🎆
return

;1F387

:*::sparkler::
Send,🎇
return

;1F9E8
:*::dynamite::
:*::firecracker::
Send,🧨
return

;2728
:*::glitter::
:*::shiny::
:*::sparkles::
Send,✨
return

;1F388
:*::party::
:*::red-balloon::
:*::balloon::
Send,🎈
return

;1F389
:*::celebration::
:*::party-hat::
:*::party-popper::
Send,🎉
return

;1F38A
:*::confetti::
:*::confetti-ball::
Send,🎊
return

;1F38B
:*::tanabata::
:*::wish-tree::
:*::tanabata-tree::
Send,🎋
return

;1F38D
:*::bamboo::
:*::kadomatsu::
:*::new-year-decoration::
:*::pine-decoration::
Send,🎍
return

;1F38E
:*::dolls::
:*::hinamatsuri::
:*::imperial-dolls::
:*::japanese-dolls::
Send,🎎
return

;1F38F
:*::fish-flag::
:*::koinobori::
:*::wind-socks::
:*::carp-streamer::
Send,🎏
return

;1F390
:*::furin::
:*::jellyfish::
:*::wind-bell::
:*::wind-chime::
Send,🎐
return

;1F391
:*::grass,-dumplings-and-moon::
:*::harvest-moon::
:*::mid-autumn-festival::
:*::tsukimi::
:*::moon-viewing-ceremony::
Send,🎑
return

;1F9E7
:*::ang-pao::
:*::hóngbāo::
:*::lai-see::
:*::red-packet::
:*::red-envelope::
Send,🧧
return

;1F380
:*::bow::
:*::pink-bow::
:*::ribbon::
Send,🎀
return

;1F381

:*::wrapped-gift::
Send,🎁
return

;1F397 FE0F

:*::reminder-ribbon::
Send,🎗️
return

;1F397

:*::reminder-ribbon::
Send,🎗
return

;1F39F FE0F

:*::admission-tickets::
Send,🎟️
return

;1F39F

:*::admission-tickets::
Send,🎟
return

;1F3AB
:*::ticket-stub::
:*::world-tour-ticket::
:*::ticket::
Send,🎫
return

;1F396 FE0F
:*::medal::
:*::medallion::
:*::military-decoration::
:*::military-medal::
Send,🎖️
return

;1F396
:*::medal::
:*::medallion::
:*::military-decoration::
:*::military-medal::
Send,🎖
return

;1F3C6
:*::championship-trophy::
:*::winners-trophy::
:*::trophy::
Send,🏆
return

;1F3C5

:*::sports-medal::
Send,🏅
return

;1F947

:*::1st-place-medal::
Send,🥇
return

;1F948

:*::2nd-place-medal::
Send,🥈
return

;1F949

:*::3rd-place-medal::
Send,🥉
return

;26BD
:*::football::
:*::soccer::
:*::soccer-ball::
Send,⚽
return

;26BE
:*::softball::
:*::baseball::
Send,⚾
return

;1F94E

:*::softball::
Send,🥎
return

;1F3C0

:*::basketball::
Send,🏀
return

;1F3D0

:*::volleyball::
Send,🏐
return

;1F3C8
:*::football::
:*::gridiron::
:*::superbowl::
:*::american-football::
Send,🏈
return

;1F3C9
:*::football::
:*::league::
:*::rugby::
:*::union::
:*::rugby-football::
Send,🏉
return

;1F3BE

:*::tennis::
Send,🎾
return

;1F94F

:*::flying-disc::
Send,🥏
return

;1F3B3
:*::bowling-ball::
:*::pins::
:*::skittles::
:*::ten-pin-bowling::
:*::bowling::
Send,🎳
return

;1F3CF

:*::cricket-game::
Send,🏏
return

;1F3D1

:*::field-hockey::
Send,🏑
return

;1F3D2

:*::ice-hockey::
Send,🏒
return

;1F94D

:*::lacrosse::
Send,🥍
return

;1F3D3

:*::ping-pong::
Send,🏓
return

;1F3F8

:*::badminton::
Send,🏸
return

;1F94A

:*::boxing-glove::
Send,🥊
return

;1F94B
:*::judo::
:*::martial-arts-uniform::
Send,🥋
return

;1F945

:*::goal-net::
Send,🥅
return

;26F3
:*::golf::
:*::golf-flag::
:*::flag-in-hole::
Send,⛳
return

;26F8 FE0F
:*::ice-skating::
:*::ice-skate::
Send,⛸️
return

;26F8
:*::ice-skating::
:*::ice-skate::
Send,⛸
return

;1F3A3

:*::fishing-pole::
Send,🎣
return

;1F93F

:*::diving-mask::
Send,🤿
return

;1F3BD

:*::running-shirt::
Send,🎽
return

;1F3BF

:*::skis::
Send,🎿
return

;1F6F7

:*::sled::
Send,🛷
return

;1F94C

:*::curling-stone::
Send,🥌
return

;1F3AF
:*::archery::
:*::bullseye::
:*::darts::
:*::direct-hit::
Send,🎯
return

;1FA80

:*::yo-yo::
Send,🪀
return

;1FA81

:*::kite::
Send,🪁
return

;1F3B1

:*::pool-8-ball::
Send,🎱
return

;1F52E
:*::clairvoyant::
:*::fortune-teller::
:*::psychic::
:*::purple-crystal::
:*::crystal-ball::
Send,🔮
return

;1F9FF
:*::evil-eye-talisman::
:*::nazar-boncuğu::
:*::nazar-amulet::
Send,🧿
return

;1F3AE
:*::gamepad::
:*::playstation::
:*::wii-u::
:*::xbox::
:*::video-game::
Send,🎮
return

;1F579 FE0F

:*::joystick::
Send,🕹️
return

;1F579

:*::joystick::
Send,🕹
return

;1F3B0
:*::casino::
:*::fruit-machine::
:*::gambling::
:*::poker-machine::
:*::slot-machine::
Send,🎰
return

;1F3B2
:*::dice::
:*::game-die::
Send,🎲
return

;1F9E9

:*::puzzle-piece::
Send,🧩
return

;1F9F8
:*::toy::
:*::teddy-bear::
Send,🧸
return

;2660 FE0F

:*::spade-suit::
Send,♠️
return

;2660

:*::spade-suit::
Send,♠
return

;2665 FE0F

:*::heart-suit::
Send,♥️
return

;2665

:*::heart-suit::
Send,♥
return

;2666 FE0F

:*::diamond-suit::
Send,♦️
return

;2666

:*::diamond-suit::
Send,♦
return

;2663 FE0F

:*::club-suit::
Send,♣️
return

;2663

:*::club-suit::
Send,♣
return

;265F FE0F

:*::chess-pawn::
Send,♟️
return

;265F

:*::chess-pawn::
Send,♟
return

;1F0CF

:*::joker::
Send,🃏
return

;1F004

:*::mahjong-red-dragon::
Send,🀄
return

;1F3B4
:*::deck-of cards::
:*::hanafuda::
:*::hwatu::
:*::playing-cards::
:*::flower-playing-cards::
Send,🎴
return

;1F3AD
:*::drama-masks::
:*::greek-theatre masks::
:*::theatre-logo::
:*::tragedy-and-comedy-masks::
:*::performing-arts::
Send,🎭
return

;1F5BC FE0F

:*::framed-picture::
Send,🖼️
return

;1F5BC

:*::framed-picture::
Send,🖼
return

;1F3A8
:*::art::
:*::painting::
:*::artist-palette::
Send,🎨
return

;1F9F5

:*::thread::
Send,🧵
return

;1F9F6

:*::yarn::
Send,🧶
return

;1F453

:*::glasses::
Send,👓
return

;1F576 FE0F

:*::sunglasses::
Send,🕶️
return

;1F576

:*::sunglasses::
Send,🕶
return

;1F97D

:*::goggles::
Send,🥽
return

;1F97C

:*::lab-coat::
Send,🥼
return

;1F9BA

:*::safety-vest::
Send,🦺
return

;1F454
:*::business-shirt::
:*::shirt-and-tie::
:*::tie::
:*::necktie::
Send,👔
return

;1F455
:*::polo-shirt::
:*::tee-shirt::
:*::t-shirt::
Send,👕
return

;1F456
:*::denim::
:*::pants::
:*::trousers::
:*::jeans::
Send,👖
return

;1F9E3

:*::scarf::
Send,🧣
return

;1F9E4

:*::gloves::
Send,🧤
return

;1F9E5

:*::coat::
Send,🧥
return

;1F9E6

:*::socks::
Send,🧦
return

;1F457
:*::gown::
:*::skirt::
:*::dress::
Send,👗
return

;1F458
:*::dressing-gown::
:*::-japanese-dress::
:*::kimono::
Send,👘
return

;1F97B
:*::saree::
:*::shari::
:*::sari::
Send,🥻
return

;1FA71

:*::one-piece-swimsuit::
Send,🩱
return

;1FA72

:*::swim-brief::
Send,🩲
return

;1FA73

:*::shorts::
Send,🩳
return

;1F459
:*::bathers::
:*::swimsuit::
:*::bikini::
Send,👙
return

;1F45A

:*::woman’s-clothes::
Send,👚
return

;1F45B
:*::wallet::
:*::purse::
Send,👛
return

;1F45C
:*::women’s-bag::
:*::handbag::
Send,👜
return

;1F45D

:*::clutch-bag::
Send,👝
return

;1F6CD FE0F

:*::shopping-bags::
Send,🛍️
return

;1F6CD

:*::shopping-bags::
Send,🛍
return

;1F392

:*::backpack::
Send,🎒
return

;1F45E

:*::man’s-shoe::
Send,👞
return

;1F45F

:*::running-shoe::
Send,👟
return

;1F97E

:*::hiking-boot::
Send,🥾
return

;1F97F

:*::flat-shoe::
Send,🥿
return

;1F460
:*::high-heels::
:*::stiletto::
:*::high-heeled-shoe::
Send,👠
return

;1F461

:*::woman’s-sandal::
Send,👡
return

;1FA70
:*::pointe-shoe::
:*::ballet-shoes::
Send,🩰
return

;1F462

:*::woman’s-boot::
Send,👢
return

;1F451
:*::king::
:*::queen::
:*::royal::
:*::crown::
Send,👑
return

;1F452

:*::woman’s-hat::
Send,👒
return

;1F3A9
:*::formal-wear::
:*::groom::
:*::top-hat::
Send,🎩
return

;1F393
:*::college::
:*::graduate::
:*::mortar-board::
:*::square-academic-cap::
:*::university::
:*::graduation-cap::
Send,🎓
return

;1F9E2
:*::baseball-cap::
:*::billed-cap::
Send,🧢
return

;26D1 FE0F

:*::rescue-worker’s-helmet::
Send,⛑️
return

;26D1

:*::rescue-worker’s-helmet::
Send,⛑
return

;1F4FF
:*::dhikr-beads::
:*::rosary-beads::
:*::prayer-beads::
Send,📿
return

;1F484
:*::lip-gloss::
:*::makeup::
:*::lipstick::
Send,💄
return

;1F48D
:*::diamond-ring::
:*::engagement-ring::
:*::ring::
Send,💍
return

;1F48E
:*::diamond::
:*::gem::
:*::jewel::
:*::gem-stone::
Send,💎
return

;1F507

:*::muted-speaker::
Send,🔇
return

;1F508

:*::speaker-low-volume::
Send,🔈
return

;1F509

:*::speaker-medium-volume::
Send,🔉
return

;1F50A

:*::speaker-high-volume::
Send,🔊
return

;1F4E2

:*::loudspeaker::
Send,📢
return

;1F4E3

:*::megaphone::
Send,📣
return

;1F4EF
:*::bugle::
:*::french-horn::
:*::postal-horn::
Send,📯
return

;1F514
:*::liberty-bell::
:*::ringer::
:*::wedding-bell::
:*::bell::
Send,🔔
return

;1F515

:*::bell-with-slash::
Send,🔕
return

;1F3BC
:*::sheet-music::
:*::treble-clef::
:*::musical-score::
Send,🎼
return

;1F3B5
:*::beamed-pair-of-eighth-notes::
:*::beamed-pair-of-quavers::
:*::music-note::
:*::musical-note::
Send,🎵
return

;1F3B6

:*::musical-notes::
Send,🎶
return

;1F399 FE0F

:*::studio-microphone::
Send,🎙️
return

;1F399

:*::studio-microphone::
Send,🎙
return

;1F39A FE0F

:*::level-slider::
Send,🎚️
return

;1F39A

:*::level-slider::
Send,🎚
return

;1F39B FE0F

:*::control-knobs::
Send,🎛️
return

;1F39B

:*::control-knobs::
Send,🎛
return

;1F3A4
:*::karaoke::
:*::singing::
:*::microphone::
Send,🎤
return

;1F3A7
:*::earphone::
:*::headphones::
:*::ipod::
:*::headphone::
Send,🎧
return

;1F4FB
:*::digital-radio::
:*::wireless::
:*::radio::
Send,📻
return

;1F3B7
:*::jazz::
:*::sax::
:*::saxophone::
Send,🎷
return

;1F3B8
:*::acoustic guitar::
:*::bass-guitar::
:*::electric-guitar::
:*::guitar::
Send,🎸
return

;1F3B9
:*::piano::
:*::musical-keyboard::
Send,🎹
return

;1F3BA
:*::horn::
:*::jazz::
:*::trumpet::
Send,🎺
return

;1F3BB
:*::string-quartet::
:*::world’s-smallest-violin::
:*::violin::
Send,🎻
return

;1FA95

:*::banjo::
Send,🪕
return

;1F941

:*::drum::
Send,🥁
return

;1F4F1
:*::cell-phone::
:*::iphone::
:*::smartphone::
:*::mobile-phone::
Send,📱
return

;1F4F2

:*::mobile-phone-with-arrow::
Send,📲
return

;260E FE0F

:*::telephone::
Send,☎️
return

;260E

:*::telephone::
Send,☎
return

;1F4DE
:*::handset::
:*::phone::
:*::telephone-receiver::
Send,📞
return

;1F4DF
:*::beeper::
:*::bleeper::
:*::pager::
Send,📟
return

;1F4E0
:*::facsimile::
:*::fax::
:*::fax-machine::
Send,📠
return

;1F50B
:*::aa-battery::
:*::phone-battery::
:*::battery::
Send,🔋
return

;1F50C
:*::ac-adaptor::
:*::power-cable::
:*::power-plug::
:*::electric-plug::
Send,🔌
return

;1F4BB

:*::laptop-computer::
Send,💻
return

;1F5A5 FE0F
:*::imac::
:*::desktop-computer::
Send,🖥️
return

;1F5A5
:*::imac::
:*::desktop-computer::
Send,🖥
return

;1F5A8 FE0F

:*::printer::
Send,🖨️
return

;1F5A8

:*::printer::
Send,🖨
return

;2328 FE0F

:*::keyboard::
Send,⌨️
return

;2328

:*::keyboard::
Send,⌨
return

;1F5B1 FE0F

:*::computer-mouse::
Send,🖱️
return

;1F5B1

:*::computer-mouse::
Send,🖱
return

;1F5B2 FE0F

:*::trackball::
Send,🖲️
return

;1F5B2

:*::trackball::
Send,🖲
return

;1F4BD

:*::computer-disk::
Send,💽
return

;1F4BE
:*::3.5″-disk::
:*::disk::
:*::floppy-disk::
Send,💾
return

;1F4BF

:*::optical-disk::
Send,💿
return

;1F4C0
:*::dvd-rom::
:*::dvd-video::
:*::dvd::
Send,📀
return

;1F9EE

:*::abacus::
Send,🧮
return

;1F3A5
:*::film-camera::
:*::hollywood::
:*::movie::
:*::movie-camera::
Send,🎥
return

;1F39E FE0F

:*::film-frames::
Send,🎞️
return

;1F39E

:*::film-frames::
Send,🎞
return

;1F4FD FE0F

:*::film-projector::
Send,📽️
return

;1F4FD

:*::film-projector::
Send,📽
return

;1F3AC
:*::clapboard::
:*::director::
:*::film-slate::
:*::clapper-board::
Send,🎬
return

;1F4FA
:*::tv::
:*::television::
Send,📺
return

;1F4F7
:*::digital-camera::
:*::camera::
Send,📷
return

;1F4F8

:*::camera-with-flash::
Send,📸
return

;1F4F9
:*::camcorder::
:*::video-camera::
Send,📹
return

;1F4FC
:*::vcr::
:*::vhs::
:*::video-tape::
:*::videocassette::
Send,📼
return

;1F50D

:*::magnifying-glass-tilted-left::
Send,🔍
return

;1F50E

:*::magnifying-glass-tilted-right::
Send,🔎
return

;1F56F FE0F

:*::candle::
Send,🕯️
return

;1F56F

:*::candle::
Send,🕯
return

;1F4A1

:*::light-bulb::
Send,💡
return

;1F526

:*::flashlight::
Send,🔦
return

;1F3EE

:*::red-paper-lantern::
Send,🏮
return

;1FA94

:*::diya-lamp::
Send,🪔
return

;1F4D4

:*::notebook-with-decorative-cover::
Send,📔
return

;1F4D5
:*::red-book::
:*::closed-book::
Send,📕
return

;1F4D6
:*::book::
:*::novel::
:*::open-book::
Send,📖
return

;1F4D7

:*::green-book::
Send,📗
return

;1F4D8

:*::blue-book::
Send,📘
return

;1F4D9

:*::orange-book::
Send,📙
return

;1F4DA
:*::pile-of-books::
:*::stack-of-books::
:*::books::
Send,📚
return

;1F4D3
:*::black-and-white-book::
:*::notebook::
Send,📓
return

;1F4D2
:*::binder::
:*::spiral-bound-book::
:*::yellow-book::
:*::ledger::
Send,📒
return

;1F4C3
:*::curled-page::
:*::curly page::
:*::page-with-curl::
Send,📃
return

;1F4DC
:*::degree::
:*::parchment::
:*::scroll::
Send,📜
return

;1F4C4
:*::printed-page::
:*::page-facing-up::
Send,📄
return

;1F4F0

:*::newspaper::
Send,📰
return

;1F5DE FE0F
:*::newspaper-delivery::
:*::rolled-up-newspaper::
Send,🗞️
return

;1F5DE
:*::newspaper-delivery::
:*::rolled-up-newspaper::
Send,🗞
return

;1F4D1

:*::bookmark-tabs::
Send,📑
return

;1F516
:*::price-tag::
:*::tag::
:*::bookmark::
Send,🔖
return

;1F3F7 FE0F

:*::label::
Send,🏷️
return

;1F3F7

:*::label::
Send,🏷
return

;1F4B0
:*::moneybags::
:*::rich::
:*::money-bag::
Send,💰
return

;1F4B4

:*::yen-banknote::
Send,💴
return

;1F4B5

:*::dollar-banknote::
Send,💵
return

;1F4B6

:*::euro-banknote::
Send,💶
return

;1F4B7

:*::pound-banknote::
Send,💷
return

;1F4B8
:*::flying-money::
:*::losing-money::
:*::money-with-wings::
Send,💸
return

;1F4B3
:*::amex::
:*::diners-club::
:*::mastercard::
:*::visa-card::
:*::credit-card::
Send,💳
return

;1F9FE

:*::receipt::
Send,🧾
return

;1F4B9

:*::chart-increasing-with-yen::
Send,💹
return

;1F4B1

:*::currency-exchange::
Send,💱
return

;1F4B2
:*::dollar::
:*::dollar-sign::
:*::heavy-dollar-sign::
Send,💲
return

;2709 FE0F
:*::✉ letter::
:*::envelope::
Send,✉️
return

;2709
:*::✉ letter::
:*::envelope::
Send,✉
return

;1F4E7

:*::e-mail::
Send,📧
return

;1F4E8
:*::envelope-with-lines::
:*::fast-envelope::
:*::incoming-envelope::
Send,📨
return

;1F4E9

:*::envelope-with-arrow::
Send,📩
return

;1F4E4
:*::outbox::
:*::outbox-tray::
Send,📤
return

;1F4E5
:*::inbox::
:*::inbox-tray::
Send,📥
return

;1F4E6
:*::box::
:*::parcel::
:*::package::
Send,📦
return

;1F4EB

:*::closed-mailbox-with-raised-flag::
Send,📫
return

;1F4EA

:*::closed-mailbox-with-lowered-flag::
Send,📪
return

;1F4EC
:*::open-mailbox::
:*::open-mailbox-with-raised-flag::
Send,📬
return

;1F4ED

:*::open-mailbox-with-lowered-flag::
Send,📭
return

;1F4EE

:*::postbox::
Send,📮
return

;1F5F3 FE0F
:*::vote-box::
:*::voting::
:*::ballot-box-with-ballot::
Send,🗳️
return

;1F5F3
:*::vote-box::
:*::voting::
:*::ballot-box-with-ballot::
Send,🗳
return

;270F FE0F
:*::✏-lead-pencil::
:*::pencil::
Send,✏️
return

;270F
:*::✏-lead-pencil::
:*::pencil::
Send,✏
return

;2712 FE0F
:*::✒-pen-nib::
:*::✒ fountain-pen::
:*::black-nib::
Send,✒️
return

;2712
:*::✒-pen-nib::
:*::✒ fountain-pen::
:*::black-nib::
Send,✒
return

;1F58B FE0F

:*::fountain-pen::
Send,🖋️
return

;1F58B

:*::fountain-pen::
Send,🖋
return

;1F58A FE0F

:*::pen::
Send,🖊️
return

;1F58A

:*::pen::
Send,🖊
return

;1F58C FE0F

:*::paintbrush::
Send,🖌️
return

;1F58C

:*::paintbrush::
Send,🖌
return

;1F58D FE0F

:*::crayon::
Send,🖍️
return

;1F58D

:*::crayon::
Send,🖍
return

;1F4DD
:*::memorandum::
:*::note::
:*::pencil-and-paper::
:*::memo::
Send,📝
return

;1F4BC
:*::suitcase::
:*::briefcase::
Send,💼
return

;1F4C1
:*::folder::
:*::manilla-folder::
:*::file-folder::
Send,📁
return

;1F4C2
:*::open-folder::
:*::open-file-folder::
Send,📂
return

;1F5C2 FE0F

:*::card-index-dividers::
Send,🗂️
return

;1F5C2

:*::card-index-dividers::
Send,🗂
return

;1F4C5
:*::july-17::
:*::world-emoji-day::
:*::calendar::
Send,📅
return

;1F4C6
:*::day-calendar::
:*::desk-calendar::
:*::tear-off-calendar::
Send,📆
return

;1F5D2 FE0F

:*::spiral-notepad::
Send,🗒️
return

;1F5D2

:*::spiral-notepad::
Send,🗒
return

;1F5D3 FE0F

:*::spiral-calendar::
Send,🗓️
return

;1F5D3

:*::spiral-calendar::
Send,🗓
return

;1F4C7
:*::index-card::
:*::rolodex::
:*::system-card::
:*::card-index::
Send,📇
return

;1F4C8

:*::chart-increasing::
Send,📈
return

;1F4C9

:*::chart-decreasing::
Send,📉
return

;1F4CA
:*::bar-graph::
:*::bar-chart::
Send,📊
return

;1F4CB

:*::clipboard::
Send,📋
return

;1F4CC
:*::thumb-tack::
:*::pushpin::
Send,📌
return

;1F4CD
:*::dropped-pin::
:*::map-pin::
:*::pin::
:*::red-pin::
:*::round-pushpin::
Send,📍
return

;1F4CE
:*::clippy::
:*::paperclip::
Send,📎
return

;1F587 FE0F

:*::linked-paperclips::
Send,🖇️
return

;1F587

:*::linked-paperclips::
Send,🖇
return

;1F4CF

:*::straight-ruler::
Send,📏
return

;1F4D0
:*::triangle-ruler::
:*::triangular-ruler::
Send,📐
return

;2702 FE0F

:*::scissors::
Send,✂️
return

;2702

:*::scissors::
Send,✂
return

;1F5C3 FE0F

:*::card-file-box::
Send,🗃️
return

;1F5C3

:*::card-file-box::
Send,🗃
return

;1F5C4 FE0F
:*::filing-cabinet::
:*::file-cabinet::
Send,🗄️
return

;1F5C4
:*::filing-cabinet::
:*::file-cabinet::
Send,🗄
return

;1F5D1 FE0F
:*::garbage-can::
:*::rubbish-bin::
:*::trash-can::
:*::wastepaper-basket::
:*::wastebasket::
Send,🗑️
return

;1F5D1
:*::garbage-can::
:*::rubbish-bin::
:*::trash-can::
:*::wastepaper-basket::
:*::wastebasket::
Send,🗑
return

;1F512

:*::locked::
Send,🔒
return

;1F513

:*::unlocked::
Send,🔓
return

;1F50F

:*::locked-with-pen::
Send,🔏
return

;1F510

:*::locked-with-key::
Send,🔐
return

;1F511
:*::gold-key::
:*::key::
Send,🔑
return

;1F5DD FE0F

:*::old-key::
Send,🗝️
return

;1F5DD

:*::old-key::
Send,🗝
return

;1F528
:*::claw-hammer::
:*::handyman::
:*::tool::
:*::hammer::
Send,🔨
return

;1FA93

:*::axe::
Send,🪓
return

;26CF FE0F
:*::pickaxe::
:*::pick::
Send,⛏️
return

;26CF
:*::pickaxe::
:*::pick::
Send,⛏
return

;2692 FE0F

:*::hammer-and-pick::
Send,⚒️
return

;2692

:*::hammer-and-pick::
Send,⚒
return

;1F6E0 FE0F

:*::hammer-and-wrench::
Send,🛠️
return

;1F6E0

:*::hammer-and-wrench::
Send,🛠
return

;1F5E1 FE0F

:*::dagger::
Send,🗡️
return

;1F5E1

:*::dagger::
Send,🗡
return

;2694 FE0F

:*::crossed-swords::
Send,⚔️
return

;2694

:*::crossed-swords::
Send,⚔
return

;1F52B
:*::gun::
:*::revolver::
:*::squirt-gun::
:*::water-gun::
:*::water-pistol::
:*::pistol::
Send,🔫
return

;1F3F9
:*::archery::
:*::bow-and-arrow::
Send,🏹
return

;1F6E1 FE0F

:*::shield::
Send,🛡️
return

;1F6E1

:*::shield::
Send,🛡
return

;1F527
:*::spanner::
:*::wrench::
Send,🔧
return

;1F529
:*::bolt::
:*::screw::
:*::nut-and-bolt::
Send,🔩
return

;2699 FE0F

:*::gear::
Send,⚙️
return

;2699

:*::gear::
Send,⚙
return

;1F5DC FE0F

:*::clamp::
Send,🗜️
return

;1F5DC

:*::clamp::
Send,🗜
return

;2696 FE0F

:*::balance-scale::
Send,⚖️
return

;2696

:*::balance-scale::
Send,⚖
return

;1F9AF

:*::probing-cane::
Send,🦯
return

;1F517

:*::link::
Send,🔗
return

;26D3 FE0F

:*::chains::
Send,⛓️
return

;26D3

:*::chains::
Send,⛓
return

;1F9F0

:*::toolbox::
Send,🧰
return

;1F9F2

:*::magnet::
Send,🧲
return

;2697 FE0F

:*::alembic::
Send,⚗️
return

;2697

:*::alembic::
Send,⚗
return

;1F9EA

:*::test-tube::
Send,🧪
return

;1F9EB

:*::petri-dish::
Send,🧫
return

;1F9EC

:*::dna::
Send,🧬
return

;1F52C
:*::magnify::
:*::science::
:*::microscope::
Send,🔬
return

;1F52D
:*::stargazing::
:*::telescope::
Send,🔭
return

;1F4E1

:*::satellite-antenna::
Send,📡
return

;1F489
:*::blood-donation::
:*::blood-test::
:*::needle::
:*::vaccination::
:*::syringe::
Send,💉
return

;1FA78

:*::drop-of-blood::
Send,🩸
return

;1F48A
:*::capsule::
:*::drugs::
:*::tablet::
:*::pill::
Send,💊
return

;1FA79

:*::adhesive-bandage::
Send,🩹
return

;1FA7A

:*::stethoscope::
Send,🩺
return

;1F6AA
:*::doorway::
:*::front-door::
:*::door::
Send,🚪
return

;1F6CF FE0F
:*::bedroom::
:*::bed::
Send,🛏️
return

;1F6CF
:*::bedroom::
:*::bed::
Send,🛏
return

;1F6CB FE0F
:*::lounge::
:*::settee::
:*::sofa::
:*::couch-and-lamp::
Send,🛋️
return

;1F6CB
:*::lounge::
:*::settee::
:*::sofa::
:*::couch-and-lamp::
Send,🛋
return

;1FA91

:*::chair::
Send,🪑
return

;1F6BD
:*::bathroom::
:*::loo::
:*::restroom::
:*::toilet::
Send,🚽
return

;1F6BF
:*::shower-head::
:*::shower::
Send,🚿
return

;1F6C1
:*::bubble-bath::
:*::bathtub::
Send,🛁
return

;1FA92

:*::razor::
Send,🪒
return

;1F9F4

:*::lotion-bottle::
Send,🧴
return

;1F9F7

:*::safety-pin::
Send,🧷
return

;1F9F9
:*::brush::
:*::sweep::
:*::broom::
Send,🧹
return

;1F9FA

:*::basket::
Send,🧺
return

;1F9FB
:*::toilet-paper::
:*::roll-of-paper::
Send,🧻
return

;1F9FC

:*::soap::
Send,🧼
return

;1F9FD

:*::sponge::
Send,🧽
return

;1F9EF

:*::fire-extinguisher::
Send,🧯
return

;1F6D2

:*::shopping-cart::
Send,🛒
return

;1F6AC

:*::cigarette::
Send,🚬
return

;26B0 FE0F
:*::casket::
:*::funeral::
:*::coffin::
Send,⚰️
return

;26B0
:*::casket::
:*::funeral::
:*::coffin::
Send,⚰
return

;26B1 FE0F
:*::vase::
:*::funeral-urn::
Send,⚱️
return

;26B1
:*::vase::
:*::funeral-urn::
Send,⚱
return

;1F5FF

:*::moai::
Send,🗿
return

;1F3E7

:*::atm-sign::
Send,🏧
return

;1F6AE

:*::litter-in-bin-sign::
Send,🚮
return

;1F6B0

:*::potable-water::
Send,🚰
return

;267F
:*::accessible-bathroom::
:*::wheelchair-symbol::
Send,♿
return

;1F6B9

:*::men’s-room::
Send,🚹
return

;1F6BA

:*::women’s-room::
Send,🚺
return

;1F6BB
:*::bathroom-sign::
:*::toilet-sign::
:*::restroom::
Send,🚻
return

;1F6BC
:*::baby-change-station::
:*::baby-change-symbol::
:*::nursery::
:*::baby-symbol::
Send,🚼
return

;1F6BE
:*::toilet-wc::
:*::wc::
:*::water-closet::
Send,🚾
return

;1F6C2
:*::border-control::
:*::passport-control::
Send,🛂
return

;1F6C3

:*::customs::
Send,🛃
return

;1F6C4

:*::baggage-claim::
Send,🛄
return

;1F6C5
:*::bag with-key::
:*::locked-suitcase::
:*::left-luggage::
Send,🛅
return

;26A0 FE0F

:*::warning::
Send,⚠️
return

;26A0

:*::warning::
Send,⚠
return

;1F6B8
:*::kids-crossing::
:*::school-crossing::
:*::children-crossing::
Send,🚸
return

;26D4

:*::no-entry::
Send,⛔
return

;1F6AB

:*::prohibited::
Send,🚫
return

;1F6B3
:*::no-bikes-sign::
:*::no-bicycles::
Send,🚳
return

;1F6AD

:*::no-smoking::
Send,🚭
return

;1F6AF

:*::no-littering::
Send,🚯
return

;1F6B1

:*::non-potable-water::
Send,🚱
return

;1F6B7
:*::no-people::
:*::no-walking::
:*::no-pedestrians::
Send,🚷
return

;1F4F5
:*::no-cell-phones::
:*::no-phones::
:*::no-smartphones::
:*::no-mobile-phones::
Send,📵
return

;1F51E

:*::no-one-under-eighteen::
Send,🔞
return

;2622 FE0F

:*::radioactive::
Send,☢️
return

;2622

:*::radioactive::
Send,☢
return

;2623 FE0F

:*::biohazard::
Send,☣️
return

;2623

:*::biohazard::
Send,☣
return

;2B06 FE0F

:*::up-arrow::
Send,⬆️
return

;2B06

:*::up-arrow::
Send,⬆
return

;2197 FE0F

:*::up-right-arrow::
Send,↗️
return

;2197

:*::up-right-arrow::
Send,↗
return

;27A1 FE0F

:*::right-arrow::
Send,➡️
return

;27A1

:*::right-arrow::
Send,➡
return

;2198 FE0F

:*::down-right-arrow::
Send,↘️
return

;2198

:*::down-right-arrow::
Send,↘
return

;2B07 FE0F

:*::down-arrow::
Send,⬇️
return

;2B07

:*::down-arrow::
Send,⬇
return

;2199 FE0F

:*::down-left-arrow::
Send,↙️
return

;2199

:*::down-left-arrow::
Send,↙
return

;2B05 FE0F

:*::left-arrow::
Send,⬅️
return

;2B05

:*::left-arrow::
Send,⬅
return

;2196 FE0F

:*::up-left-arrow::
Send,↖️
return

;2196

:*::up-left-arrow::
Send,↖
return

;2195 FE0F
:*::vertical-arrows::
:*::up-down-arrow::
Send,↕️
return

;2195
:*::vertical-arrows::
:*::up-down-arrow::
Send,↕
return

;2194 FE0F
:*::horizontal arrows::
:*::sideways-arrows::
:*::left-right-arrow::
Send,↔️
return

;2194
:*::horizontal arrows::
:*::sideways-arrows::
:*::left-right-arrow::
Send,↔
return

;21A9 FE0F

:*::right-arrow-curving-left::
Send,↩️
return

;21A9

:*::right-arrow-curving-left::
Send,↩
return

;21AA FE0F

:*::left-arrow-curving-right::
Send,↪️
return

;21AA

:*::left-arrow-curving-right::
Send,↪
return

;2934 FE0F

:*::right-arrow-curving-up::
Send,⤴️
return

;2934

:*::right-arrow-curving-up::
Send,⤴
return

;2935 FE0F

:*::right-arrow-curving-down::
Send,⤵️
return

;2935

:*::right-arrow-curving-down::
Send,⤵
return

;1F503

:*::clockwise-vertical-arrows::
Send,🔃
return

;1F504

:*::counterclockwise-arrows-button::
Send,🔄
return

;1F519

:*::back-arrow::
Send,🔙
return

;1F51A

:*::end-arrow::
Send,🔚
return

;1F51B

:*::on!-arrow::
Send,🔛
return

;1F51C

:*::soon-arrow::
Send,🔜
return

;1F51D

:*::top-arrow::
Send,🔝
return

;1F6D0
:*::religious-building::
:*::place-of-worship::
Send,🛐
return

;269B FE0F

:*::atom-symbol::
Send,⚛️
return

;269B

:*::atom-symbol::
Send,⚛
return

;1F549 FE0F

:*::om::
Send,🕉️
return

;1F549

:*::om::
Send,🕉
return

;2721 FE0F
:*::magen-david::
:*::star-of-david::
Send,✡️
return

;2721
:*::magen-david::
:*::star-of-david::
Send,✡
return

;2638 FE0F
:*::helm::
:*::wheel-of-dharma::
Send,☸️
return

;2638
:*::helm::
:*::wheel-of-dharma::
Send,☸
return

;262F FE0F

:*::yin-yang::
Send,☯️
return

;262F

:*::yin-yang::
Send,☯
return

;271D FE0F
:*::christian-cross::
:*::latin-cross::
Send,✝️
return

;271D
:*::christian-cross::
:*::latin-cross::
Send,✝
return

;2626 FE0F

:*::orthodox-cross::
Send,☦️
return

;2626

:*::orthodox-cross::
Send,☦
return

;262A FE0F

:*::star-and-crescent::
Send,☪️
return

;262A

:*::star-and-crescent::
Send,☪
return

;262E FE0F
:*::peace-sign::
:*::peace-symbol::
Send,☮️
return

;262E
:*::peace-sign::
:*::peace-symbol::
Send,☮
return

;1F54E

:*::menorah::
Send,🕎
return

;1F52F

:*::dotted-six-pointed-star::
Send,🔯
return

;2648

:*::aries::
Send,♈
return

;2649

:*::taurus::
Send,♉
return

;264A

:*::gemini::
Send,♊
return

;264B

:*::cancer::
Send,♋
return

;264C

:*::leo::
Send,♌
return

;264D

:*::virgo::
Send,♍
return

;264E

:*::libra::
Send,♎
return

;264F

:*::scorpio::
Send,♏
return

;2650

:*::sagittarius::
Send,♐
return

;2651

:*::capricorn::
Send,♑
return

;2652

:*::aquarius::
Send,♒
return

;2653

:*::pisces::
Send,♓
return

;26CE

:*::ophiuchus::
Send,⛎
return

;1F500

:*::shuffle-tracks-button::
Send,🔀
return

;1F501

:*::repeat-button::
Send,🔁
return

;1F502

:*::repeat-single-button::
Send,🔂
return

;25B6 FE0F

:*::play-button::
Send,▶️
return

;25B6

:*::play-button::
Send,▶
return

;23E9

:*::fast-forward-button::
Send,⏩
return

;23ED FE0F

:*::next-track-button::
Send,⏭️
return

;23ED

:*::next-track-button::
Send,⏭
return

;23EF FE0F

:*::play-or-pause-button::
Send,⏯️
return

;23EF

:*::play-or-pause-button::
Send,⏯
return

;25C0 FE0F

:*::reverse-button::
Send,◀️
return

;25C0

:*::reverse-button::
Send,◀
return

;23EA

:*::fast-reverse-button::
Send,⏪
return

;23EE FE0F

:*::last-track-button::
Send,⏮️
return

;23EE

:*::last-track-button::
Send,⏮
return

;1F53C

:*::upwards-button::
Send,🔼
return

;23EB

:*::fast-up-button::
Send,⏫
return

;1F53D

:*::downwards-button::
Send,🔽
return

;23EC

:*::fast-down-button::
Send,⏬
return

;23F8 FE0F

:*::pause-button::
Send,⏸️
return

;23F8

:*::pause-button::
Send,⏸
return

;23F9 FE0F

:*::stop-button::
Send,⏹️
return

;23F9

:*::stop-button::
Send,⏹
return

;23FA FE0F

:*::record-button::
Send,⏺️
return

;23FA

:*::record-button::
Send,⏺
return

;23CF FE0F

:*::eject-button::
Send,⏏️
return

;23CF

:*::eject-button::
Send,⏏
return

;1F3A6
:*::cinema-screen::
:*::movies::
:*::cinema::
Send,🎦
return

;1F505

:*::dim-button::
Send,🔅
return

;1F506

:*::bright-button::
Send,🔆
return

;1F4F6

:*::antenna-bars::
Send,📶
return

;1F4F3
:*::phone-heart::
:*::silent-mode::
:*::vibration-mode::
Send,📳
return

;1F4F4

:*::mobile-phone-off::
Send,📴
return

;2640 FE0F
:*::venus-symbol::
:*::woman-symbol::
:*::female-sign::
Send,♀️
return

;2640
:*::venus-symbol::
:*::woman-symbol::
:*::female-sign::
Send,♀
return

;2642 FE0F
:*::man-symbol::
:*::mars-symbol::
:*::male-sign::
Send,♂️
return

;2642
:*::man-symbol::
:*::mars-symbol::
:*::male-sign::
Send,♂
return

;2695 FE0F

:*::medical-symbol::
Send,⚕️
return

;2695

:*::medical-symbol::
Send,⚕
return

;267E FE0F
:*::infinity::
:*::infinity::
Send,♾️
return

;267E
:*::infinity::
:*::infinity::
Send,♾
return

;267B FE0F

:*::recycling-symbol::
Send,♻️
return

;267B

:*::recycling-symbol::
Send,♻
return

;269C FE0F
:*::new-orleans-saints::
:*::scouts::
:*::fleur-de-lis::
Send,⚜️
return

;269C
:*::new-orleans-saints::
:*::scouts::
:*::fleur-de-lis::
Send,⚜
return

;1F531
:*::pitchfork::
:*::trident::
:*::trident-emblem::
Send,🔱
return

;1F4DB
:*::fire-tag::
:*::name-tag::
:*::tofu-on-fire::
:*::name-badge::
Send,📛
return

;1F530
:*::shoshinsha-mark::
:*::yellow-green-shield::
:*::japanese-symbol-for-beginner::
Send,🔰
return

;2B55

:*::hollow-red-circle::
Send,⭕
return

;2705

:*::check-mark-button::
Send,✅
return

;2611 FE0F

:*::check-box-with-check::
Send,☑️
return

;2611

:*::check-box-with-check::
Send,☑
return

;2714 FE0F

:*::check-mark::
Send,✔️
return

;2714

:*::check-mark::
Send,✔
return

;2716 FE0F

:*::multiplication-sign::
Send,✖️
return

;2716

:*::multiplication-sign::
Send,✖
return

;274C
:*::cross::
:*::x::
:*::cross-mark::
Send,❌
return

;274E

:*::cross-mark-button::
Send,❎
return

;2795

:*::plus-sign::
Send,➕
return

;2796

:*::minus-sign::
Send,➖
return

;2797

:*::division-sign::
Send,➗
return

;27B0
:*::curling-loop::
:*::loop::
:*::curly-loop::
Send,➰
return

;27BF
:*::double-curling-loop::
:*::voicemail::
:*::double-curly-loop::
Send,➿
return

;303D FE0F
:*::m::
:*::mcdonald’s::
:*::part-alternation-mark::
Send,〽️
return

;303D
:*::m::
:*::mcdonald’s::
:*::part-alternation-mark::
Send,〽
return

;2733 FE0F

:*::eight-spoked-asterisk::
Send,✳️
return

;2733

:*::eight-spoked-asterisk::
Send,✳
return

;2734 FE0F

:*::eight-pointed-star::
Send,✴️
return

;2734

:*::eight-pointed-star::
Send,✴
return

;2747 FE0F

:*::sparkle::
Send,❇️
return

;2747

:*::sparkle::
Send,❇
return

;203C FE0F

:*::double-exclamation-mark::
Send,‼️
return

;203C

:*::double-exclamation-mark::
Send,‼
return

;2049 FE0F

:*::exclamation-question-mark::
Send,⁉️
return

;2049

:*::exclamation-question-mark::
Send,⁉
return

;2753

:*::question-mark::
Send,❓
return

;2754

:*::white-question-mark::
Send,❔
return

;2755

:*::white-exclamation-mark::
Send,❕
return

;2757

:*::exclamation-mark::
Send,❗
return

;3030 FE0F
:*::〰-wave::
:*::wavy-dash::
Send,〰️
return

;3030
:*::〰-wave::
:*::wavy-dash::
Send,〰
return

;00A9 FE0F

:*::copyright::
Send,©️
return

;00A9

:*::copyright::
Send,©
return

;00AE FE0F

:*::registered::
Send,®️
return

;00AE

:*::registered::
Send,®
return

;2122 FE0F

:*::trade-mark::
Send,™️
return

;2122

:*::trade-mark::
Send,™
return

;0023 FE0F 20E3

:*::keycap:-#::
Send,#️⃣
return

;0023 20E3

:*::keycap:-#::
Send,#⃣
return

;002A FE0F 20E3

:*::keycap:-*::
Send,*️⃣
return

;002A 20E3

:*::keycap:-*::
Send,*⃣
return

;0030 FE0F 20E3

:*::keycap:-0::
Send,0️⃣
return

;0030 20E3

:*::keycap:-0::
Send,0⃣
return

;0031 FE0F 20E3

:*::keycap:-1::
Send,1️⃣
return

;0031 20E3

:*::keycap:-1::
Send,1⃣
return

;0032 FE0F 20E3

:*::keycap:-2::
Send,2️⃣
return

;0032 20E3

:*::keycap:-2::
Send,2⃣
return

;0033 FE0F 20E3

:*::keycap:-3::
Send,3️⃣
return

;0033 20E3

:*::keycap:-3::
Send,3⃣
return

;0034 FE0F 20E3

:*::keycap:-4::
Send,4️⃣
return

;0034 20E3

:*::keycap:-4::
Send,4⃣
return

;0035 FE0F 20E3

:*::keycap:-5::
Send,5️⃣
return

;0035 20E3

:*::keycap:-5::
Send,5⃣
return

;0036 FE0F 20E3

:*::keycap:-6::
Send,6️⃣
return

;0036 20E3

:*::keycap:-6::
Send,6⃣
return

;0037 FE0F 20E3

:*::keycap:-7::
Send,7️⃣
return

;0037 20E3

:*::keycap:-7::
Send,7⃣
return

;0038 FE0F 20E3

:*::keycap:-8::
Send,8️⃣
return

;0038 20E3

:*::keycap:-8::
Send,8⃣
return

;0039 FE0F 20E3

:*::keycap:-9::
Send,9️⃣
return

;0039 20E3

:*::keycap:-9::
Send,9⃣
return

;1F51F

:*::keycap:-10::
Send,🔟
return

;1F520

:*::input-latin-uppercase::
Send,🔠
return

;1F521

:*::input-latin-lowercase::
Send,🔡
return

;1F522

:*::input-numbers::
Send,🔢
return

;1F523

:*::input-symbols::
Send,🔣
return

;1F524

:*::input-latin-letters::
Send,🔤
return

;1F170 FE0F

:*::a-button-(blood-type)::
Send,🅰️
return

;1F170

:*::a-button-(blood-type)::
Send,🅰
return

;1F18E

:*::ab-button-(blood-type)::
Send,🆎
return

;1F171 FE0F

:*::b-button-(blood-type)::
Send,🅱️
return

;1F171

:*::b-button-(blood-type)::
Send,🅱
return

;1F191

:*::cl-button::
Send,🆑
return

;1F192

:*::cool-button::
Send,🆒
return

;1F193

:*::free-button::
Send,🆓
return

;2139 FE0F

:*::information::
Send,ℹ️
return

;2139

:*::information::
Send,ℹ
return

;1F194

:*::id-button::
Send,🆔
return

;24C2 FE0F

:*::circled-m::
Send,Ⓜ️
return

;24C2

:*::circled-m::
Send,Ⓜ
return

;1F195

:*::new-button::
Send,🆕
return

;1F196

:*::ng-button::
Send,🆖
return

;1F17E FE0F

:*::o-button-(blood-type)::
Send,🅾️
return

;1F17E

:*::o-button-(blood-type)::
Send,🅾
return

;1F197

:*::ok-button::
Send,🆗
return

;1F17F FE0F

:*::p-button::
Send,🅿️
return

;1F17F

:*::p-button::
Send,🅿
return

;1F198

:*::sos-button::
Send,🆘
return

;1F199

:*::up!-button::
Send,🆙
return

;1F19A

:*::vs-button::
Send,🆚
return

;1F201

:*::japanese-“here”-button::
Send,🈁
return

;1F202 FE0F

:*::japanese-“service-charge”-button::
Send,🈂️
return

;1F202

:*::japanese-“service-charge”-button::
Send,🈂
return

;1F237 FE0F

:*::japanese-“monthly-amount”-button::
Send,🈷️
return

;1F237

:*::japanese-“monthly-amount”-button::
Send,🈷
return

;1F236

:*::japanese-“not-free-of-charge”-button::
Send,🈶
return

;1F22F

:*::japanese-“reserved”-button::
Send,🈯
return

;1F250

:*::japanese-“bargain”-button::
Send,🉐
return

;1F239

:*::japanese-“discount”-button::
Send,🈹
return

;1F21A

:*::japanese-“free-of-charge”-button::
Send,🈚
return

;1F232

:*::japanese-“prohibited”-button::
Send,🈲
return

;1F251

:*::japanese-“acceptable”-button::
Send,🉑
return

;1F238

:*::japanese-“application”-button::
Send,🈸
return

;1F234

:*::japanese-“passing-grade”-button::
Send,🈴
return

;1F233

:*::japanese-“vacancy”-button::
Send,🈳
return

;3297 FE0F

:*::japanese-“congratulations”-button::
Send,㊗️
return

;3297

:*::japanese-“congratulations”-button::
Send,㊗
return

;3299 FE0F

:*::japanese-“secret”-button::
Send,㊙️
return

;3299

:*::japanese-“secret”-button::
Send,㊙
return

;1F23A

:*::japanese-“open-for-business”-button::
Send,🈺
return

;1F235

:*::japanese-“no-vacancy”-button::
Send,🈵
return

;1F534

:*::red-circle::
Send,🔴
return

;1F7E0

:*::orange-circle::
Send,🟠
return

;1F7E1

:*::yellow-circle::
Send,🟡
return

;1F7E2

:*::green-circle::
Send,🟢
return

;1F535

:*::blue-circle::
Send,🔵
return

;1F7E3

:*::purple-circle::
Send,🟣
return

;1F7E4

:*::brown-circle::
Send,🟤
return

;26AB

:*::black-circle::
Send,⚫
return

;26AA

:*::white-circle::
Send,⚪
return

;1F7E5

:*::red-square::
Send,🟥
return

;1F7E7

:*::orange-square::
Send,🟧
return

;1F7E8

:*::yellow-square::
Send,🟨
return

;1F7E9

:*::green-square::
Send,🟩
return

;1F7E6

:*::blue-square::
Send,🟦
return

;1F7EA

:*::purple-square::
Send,🟪
return

;1F7EB

:*::brown-square::
Send,🟫
return

;2B1B

:*::black-large-square::
Send,⬛
return

;2B1C

:*::white-large-square::
Send,⬜
return

;25FC FE0F

:*::black-medium-square::
Send,◼️
return

;25FC

:*::black-medium-square::
Send,◼
return

;25FB FE0F

:*::white-medium-square::
Send,◻️
return

;25FB

:*::white-medium-square::
Send,◻
return

;25FE

:*::black-medium-small-square::
Send,◾
return

;25FD

:*::white-medium-small-square::
Send,◽
return

;25AA FE0F

:*::black-small-square::
Send,▪️
return

;25AA

:*::black-small-square::
Send,▪
return

;25AB FE0F

:*::white-small-square::
Send,▫️
return

;25AB

:*::white-small-square::
Send,▫
return

;1F536

:*::large-orange-diamond::
Send,🔶
return

;1F537

:*::large-blue-diamond::
Send,🔷
return

;1F538

:*::small-orange-diamond::
Send,🔸
return

;1F539

:*::small-blue-diamond::
Send,🔹
return

;1F53A

:*::red-triangle-pointed-up::
Send,🔺
return

;1F53B

:*::red-triangle-pointed-down::
Send,🔻
return

;1F4A0

:*::diamond-with-a-dot::
Send,💠
return

;1F518

:*::radio-button::
Send,🔘
return

;1F533

:*::white-square-button::
Send,🔳
return

;1F532

:*::black-square-button::
Send,🔲
return

;1F3C1
:*::checkered-flag::
:*::grid-girl::
:*::racing-flag::
:*::chequered-flag::
Send,🏁
return

;1F6A9

:*::triangular-flag::
Send,🚩
return

;1F38C
:*::two-flags::
:*::crossed-flags::
Send,🎌
return

;1F3F4

:*::black-flag::
Send,🏴
return

;1F3F3 FE0F

:*::white-flag::
Send,🏳️
return

;1F3F3

:*::white-flag::
Send,🏳
return

;1F3F3 FE0F 200D 1F308
:*::pride-flag::
:*::rainbow-flag::
Send,🏳️‍🌈
return

;1F3F3 200D 1F308
:*::pride-flag::
:*::rainbow-flag::
Send,🏳‍🌈
return

;1F3F4 200D 2620 FE0F
:*::jolly-roger::
:*::pirate-flag::
Send,🏴‍☠️
return

;1F3F4 200D 2620
:*::jolly-roger::
:*::pirate-flag::
Send,🏴‍☠
return

;1F1E6 1F1E8

:*::flag:-ascension-island::
Send,🇦🇨
return

;1F1E6 1F1E9

:*::flag:-andorra::
Send,🇦🇩
return

;1F1E6 1F1EA

:*::flag:-united-arab-emirates::
Send,🇦🇪
return

;1F1E6 1F1EB

:*::flag:-afghanistan::
Send,🇦🇫
return

;1F1E6 1F1EC

:*::flag:-antigua-&-barbuda::
Send,🇦🇬
return

;1F1E6 1F1EE

:*::flag:-anguilla::
Send,🇦🇮
return

;1F1E6 1F1F1

:*::flag:-albania::
Send,🇦🇱
return

;1F1E6 1F1F2

:*::flag:-armenia::
Send,🇦🇲
return

;1F1E6 1F1F4

:*::flag:-angola::
Send,🇦🇴
return

;1F1E6 1F1F6

:*::flag:-antarctica::
Send,🇦🇶
return

;1F1E6 1F1F7

:*::flag:-argentina::
Send,🇦🇷
return

;1F1E6 1F1F8

:*::flag:-american-samoa::
Send,🇦🇸
return

;1F1E6 1F1F9

:*::flag:-austria::
Send,🇦🇹
return

;1F1E6 1F1FA

:*::flag:-australia::
Send,🇦🇺
return

;1F1E6 1F1FC

:*::flag:-aruba::
Send,🇦🇼
return

;1F1E6 1F1FD

:*::flag:-åland-islands::
Send,🇦🇽
return

;1F1E6 1F1FF

:*::flag:-azerbaijan::
Send,🇦🇿
return

;1F1E7 1F1E6

:*::flag:-bosnia-&-herzegovina::
Send,🇧🇦
return

;1F1E7 1F1E7

:*::flag:-barbados::
Send,🇧🇧
return

;1F1E7 1F1E9

:*::flag:-bangladesh::
Send,🇧🇩
return

;1F1E7 1F1EA

:*::flag:-belgium::
Send,🇧🇪
return

;1F1E7 1F1EB

:*::flag:-burkina-faso::
Send,🇧🇫
return

;1F1E7 1F1EC

:*::flag:-bulgaria::
Send,🇧🇬
return

;1F1E7 1F1ED

:*::flag:-bahrain::
Send,🇧🇭
return

;1F1E7 1F1EE

:*::flag:-burundi::
Send,🇧🇮
return

;1F1E7 1F1EF

:*::flag:-benin::
Send,🇧🇯
return

;1F1E7 1F1F1

:*::flag:-st.-barthélemy::
Send,🇧🇱
return

;1F1E7 1F1F2

:*::flag:-bermuda::
Send,🇧🇲
return

;1F1E7 1F1F3

:*::flag:-brunei::
Send,🇧🇳
return

;1F1E7 1F1F4

:*::flag:-bolivia::
Send,🇧🇴
return

;1F1E7 1F1F6

:*::flag:-caribbean-netherlands::
Send,🇧🇶
return

;1F1E7 1F1F7

:*::flag:-brazil::
Send,🇧🇷
return

;1F1E7 1F1F8

:*::flag:-bahamas::
Send,🇧🇸
return

;1F1E7 1F1F9

:*::flag:-bhutan::
Send,🇧🇹
return

;1F1E7 1F1FB

:*::flag:-bouvet-island::
Send,🇧🇻
return

;1F1E7 1F1FC

:*::flag:-botswana::
Send,🇧🇼
return

;1F1E7 1F1FE

:*::flag:-belarus::
Send,🇧🇾
return

;1F1E7 1F1FF

:*::flag:-belize::
Send,🇧🇿
return

;1F1E8 1F1E6

:*::flag:-canada::
Send,🇨🇦
return

;1F1E8 1F1E8

:*::flag:-cocos-(keeling)-islands::
Send,🇨🇨
return

;1F1E8 1F1E9

:*::flag:-congo---kinshasa::
Send,🇨🇩
return

;1F1E8 1F1EB

:*::flag:-central-african-republic::
Send,🇨🇫
return

;1F1E8 1F1EC

:*::flag:-congo---brazzaville::
Send,🇨🇬
return

;1F1E8 1F1ED

:*::flag:-switzerland::
Send,🇨🇭
return

;1F1E8 1F1EE

:*::flag:-côte-d’ivoire::
Send,🇨🇮
return

;1F1E8 1F1F0

:*::flag:-cook-islands::
Send,🇨🇰
return

;1F1E8 1F1F1

:*::flag:-chile::
Send,🇨🇱
return

;1F1E8 1F1F2

:*::flag:-cameroon::
Send,🇨🇲
return

;1F1E8 1F1F3

:*::flag:-china::
Send,🇨🇳
return

;1F1E8 1F1F4

:*::flag:-colombia::
Send,🇨🇴
return

;1F1E8 1F1F5

:*::flag:-clipperton-island::
Send,🇨🇵
return

;1F1E8 1F1F7

:*::flag:-costa-rica::
Send,🇨🇷
return

;1F1E8 1F1FA

:*::flag:-cuba::
Send,🇨🇺
return

;1F1E8 1F1FB

:*::flag:-cape-verde::
Send,🇨🇻
return

;1F1E8 1F1FC

:*::flag:-curaçao::
Send,🇨🇼
return

;1F1E8 1F1FD

:*::flag:-christmas-island::
Send,🇨🇽
return

;1F1E8 1F1FE

:*::flag:-cyprus::
Send,🇨🇾
return

;1F1E8 1F1FF

:*::flag:-czechia::
Send,🇨🇿
return

;1F1E9 1F1EA

:*::flag:-germany::
Send,🇩🇪
return

;1F1E9 1F1EC

:*::flag:-diego-garcia::
Send,🇩🇬
return

;1F1E9 1F1EF

:*::flag:-djibouti::
Send,🇩🇯
return

;1F1E9 1F1F0

:*::flag:-denmark::
Send,🇩🇰
return

;1F1E9 1F1F2

:*::flag:-dominica::
Send,🇩🇲
return

;1F1E9 1F1F4

:*::flag:-dominican-republic::
Send,🇩🇴
return

;1F1E9 1F1FF

:*::flag:-algeria::
Send,🇩🇿
return

;1F1EA 1F1E6

:*::flag:-ceuta-&-melilla::
Send,🇪🇦
return

;1F1EA 1F1E8

:*::flag:-ecuador::
Send,🇪🇨
return

;1F1EA 1F1EA

:*::flag:-estonia::
Send,🇪🇪
return

;1F1EA 1F1EC

:*::flag:-egypt::
Send,🇪🇬
return

;1F1EA 1F1ED

:*::flag:-western-sahara::
Send,🇪🇭
return

;1F1EA 1F1F7

:*::flag:-eritrea::
Send,🇪🇷
return

;1F1EA 1F1F8

:*::flag:-spain::
Send,🇪🇸
return

;1F1EA 1F1F9

:*::flag:-ethiopia::
Send,🇪🇹
return

;1F1EA 1F1FA

:*::flag:-european-union::
Send,🇪🇺
return

;1F1EB 1F1EE

:*::flag:-finland::
Send,🇫🇮
return

;1F1EB 1F1EF

:*::flag:-fiji::
Send,🇫🇯
return

;1F1EB 1F1F0

:*::flag:-falkland-islands::
Send,🇫🇰
return

;1F1EB 1F1F2

:*::flag:-micronesia::
Send,🇫🇲
return

;1F1EB 1F1F4

:*::flag:-faroe-islands::
Send,🇫🇴
return

;1F1EB 1F1F7

:*::flag:-france::
Send,🇫🇷
return

;1F1EC 1F1E6

:*::flag:-gabon::
Send,🇬🇦
return

;1F1EC 1F1E7

:*::flag:-united-kingdom::
Send,🇬🇧
return

;1F1EC 1F1E9

:*::flag:-grenada::
Send,🇬🇩
return

;1F1EC 1F1EA

:*::flag:-georgia::
Send,🇬🇪
return

;1F1EC 1F1EB

:*::flag:-french-guiana::
Send,🇬🇫
return

;1F1EC 1F1EC

:*::flag:-guernsey::
Send,🇬🇬
return

;1F1EC 1F1ED

:*::flag:-ghana::
Send,🇬🇭
return

;1F1EC 1F1EE

:*::flag:-gibraltar::
Send,🇬🇮
return

;1F1EC 1F1F1

:*::flag:-greenland::
Send,🇬🇱
return

;1F1EC 1F1F2

:*::flag:-gambia::
Send,🇬🇲
return

;1F1EC 1F1F3

:*::flag:-guinea::
Send,🇬🇳
return

;1F1EC 1F1F5

:*::flag:-guadeloupe::
Send,🇬🇵
return

;1F1EC 1F1F6

:*::flag:-equatorial-guinea::
Send,🇬🇶
return

;1F1EC 1F1F7

:*::flag:-greece::
Send,🇬🇷
return

;1F1EC 1F1F8

:*::flag:-south-georgia-&-south-sandwich-is::
Send,🇬🇸
return

;1F1EC 1F1F9

:*::flag:-guatemala::
Send,🇬🇹
return

;1F1EC 1F1FA

:*::flag:-guam::
Send,🇬🇺
return

;1F1EC 1F1FC

:*::flag:-guinea-bissau::
Send,🇬🇼
return

;1F1EC 1F1FE

:*::flag:-guyana::
Send,🇬🇾
return

;1F1ED 1F1F0

:*::flag:-hong-kong-sar-china::
Send,🇭🇰
return

;1F1ED 1F1F2

:*::flag:-heard-&-mcdonald-islands::
Send,🇭🇲
return

;1F1ED 1F1F3

:*::flag:-honduras::
Send,🇭🇳
return

;1F1ED 1F1F7

:*::flag:-croatia::
Send,🇭🇷
return

;1F1ED 1F1F9

:*::flag:-haiti::
Send,🇭🇹
return

;1F1ED 1F1FA

:*::flag:-hungary::
Send,🇭🇺
return

;1F1EE 1F1E8

:*::flag:-canary-islands::
Send,🇮🇨
return

;1F1EE 1F1E9

:*::flag:-indonesia::
Send,🇮🇩
return

;1F1EE 1F1EA

:*::flag:-ireland::
Send,🇮🇪
return

;1F1EE 1F1F1

:*::flag:-israel::
Send,🇮🇱
return

;1F1EE 1F1F2

:*::flag:-isle-of-man::
Send,🇮🇲
return

;1F1EE 1F1F3

:*::flag:-india::
Send,🇮🇳
return

;1F1EE 1F1F4

:*::flag:-british-indian-ocean-territory::
Send,🇮🇴
return

;1F1EE 1F1F6

:*::flag:-iraq::
Send,🇮🇶
return

;1F1EE 1F1F7

:*::flag:-iran::
Send,🇮🇷
return

;1F1EE 1F1F8

:*::flag:-iceland::
Send,🇮🇸
return

;1F1EE 1F1F9

:*::flag:-italy::
Send,🇮🇹
return

;1F1EF 1F1EA

:*::flag:-jersey::
Send,🇯🇪
return

;1F1EF 1F1F2

:*::flag:-jamaica::
Send,🇯🇲
return

;1F1EF 1F1F4

:*::flag:-jordan::
Send,🇯🇴
return

;1F1EF 1F1F5

:*::flag:-japan::
Send,🇯🇵
return

;1F1F0 1F1EA

:*::flag:-kenya::
Send,🇰🇪
return

;1F1F0 1F1EC

:*::flag:-kyrgyzstan::
Send,🇰🇬
return

;1F1F0 1F1ED

:*::flag:-cambodia::
Send,🇰🇭
return

;1F1F0 1F1EE

:*::flag:-kiribati::
Send,🇰🇮
return

;1F1F0 1F1F2

:*::flag:-comoros::
Send,🇰🇲
return

;1F1F0 1F1F3

:*::flag:-st.-kitts-&-nevis::
Send,🇰🇳
return

;1F1F0 1F1F5

:*::flag:-north-korea::
Send,🇰🇵
return

;1F1F0 1F1F7

:*::flag:-south-korea::
Send,🇰🇷
return

;1F1F0 1F1FC

:*::flag:-kuwait::
Send,🇰🇼
return

;1F1F0 1F1FE

:*::flag:-cayman-islands::
Send,🇰🇾
return

;1F1F0 1F1FF

:*::flag:-kazakhstan::
Send,🇰🇿
return

;1F1F1 1F1E6

:*::flag:-laos::
Send,🇱🇦
return

;1F1F1 1F1E7

:*::flag:-lebanon::
Send,🇱🇧
return

;1F1F1 1F1E8

:*::flag:-st.-lucia::
Send,🇱🇨
return

;1F1F1 1F1EE

:*::flag:-liechtenstein::
Send,🇱🇮
return

;1F1F1 1F1F0

:*::flag:-sri-lanka::
Send,🇱🇰
return

;1F1F1 1F1F7

:*::flag:-liberia::
Send,🇱🇷
return

;1F1F1 1F1F8

:*::flag:-lesotho::
Send,🇱🇸
return

;1F1F1 1F1F9

:*::flag:-lithuania::
Send,🇱🇹
return

;1F1F1 1F1FA

:*::flag:-luxembourg::
Send,🇱🇺
return

;1F1F1 1F1FB

:*::flag:-latvia::
Send,🇱🇻
return

;1F1F1 1F1FE

:*::flag:-libya::
Send,🇱🇾
return

;1F1F2 1F1E6

:*::flag:-morocco::
Send,🇲🇦
return

;1F1F2 1F1E8

:*::flag:-monaco::
Send,🇲🇨
return

;1F1F2 1F1E9

:*::flag:-moldova::
Send,🇲🇩
return

;1F1F2 1F1EA

:*::flag:-montenegro::
Send,🇲🇪
return

;1F1F2 1F1EB

:*::flag:-st.-martin::
Send,🇲🇫
return

;1F1F2 1F1EC

:*::flag:-madagascar::
Send,🇲🇬
return

;1F1F2 1F1ED

:*::flag:-marshall-islands::
Send,🇲🇭
return

;1F1F2 1F1F0

:*::flag:-macedonia::
Send,🇲🇰
return

;1F1F2 1F1F1

:*::flag:-mali::
Send,🇲🇱
return

;1F1F2 1F1F2

:*::flag:-myanmar-(burma)::
Send,🇲🇲
return

;1F1F2 1F1F3

:*::flag:-mongolia::
Send,🇲🇳
return

;1F1F2 1F1F4

:*::flag:-macao-sar-china::
Send,🇲🇴
return

;1F1F2 1F1F5

:*::flag:-northern-mariana-islands::
Send,🇲🇵
return

;1F1F2 1F1F6

:*::flag:-martinique::
Send,🇲🇶
return

;1F1F2 1F1F7

:*::flag:-mauritania::
Send,🇲🇷
return

;1F1F2 1F1F8

:*::flag:-montserrat::
Send,🇲🇸
return

;1F1F2 1F1F9

:*::flag:-malta::
Send,🇲🇹
return

;1F1F2 1F1FA

:*::flag:-mauritius::
Send,🇲🇺
return

;1F1F2 1F1FB

:*::flag:-maldives::
Send,🇲🇻
return

;1F1F2 1F1FC

:*::flag:-malawi::
Send,🇲🇼
return

;1F1F2 1F1FD

:*::flag:-mexico::
Send,🇲🇽
return

;1F1F2 1F1FE

:*::flag:-malaysia::
Send,🇲🇾
return

;1F1F2 1F1FF

:*::flag:-mozambique::
Send,🇲🇿
return

;1F1F3 1F1E6

:*::flag:-namibia::
Send,🇳🇦
return

;1F1F3 1F1E8

:*::flag:-new-caledonia::
Send,🇳🇨
return

;1F1F3 1F1EA

:*::flag:-niger::
Send,🇳🇪
return

;1F1F3 1F1EB

:*::flag:-norfolk-island::
Send,🇳🇫
return

;1F1F3 1F1EC

:*::flag:-nigeria::
Send,🇳🇬
return

;1F1F3 1F1EE

:*::flag:-nicaragua::
Send,🇳🇮
return

;1F1F3 1F1F1

:*::flag:-netherlands::
Send,🇳🇱
return

;1F1F3 1F1F4

:*::flag:-norway::
Send,🇳🇴
return

;1F1F3 1F1F5

:*::flag:-nepal::
Send,🇳🇵
return

;1F1F3 1F1F7

:*::flag:-nauru::
Send,🇳🇷
return

;1F1F3 1F1FA

:*::flag:-niue::
Send,🇳🇺
return

;1F1F3 1F1FF

:*::flag:-new-zealand::
Send,🇳🇿
return

;1F1F4 1F1F2

:*::flag:-oman::
Send,🇴🇲
return

;1F1F5 1F1E6

:*::flag:-panama::
Send,🇵🇦
return

;1F1F5 1F1EA

:*::flag:-peru::
Send,🇵🇪
return

;1F1F5 1F1EB

:*::flag:-french-polynesia::
Send,🇵🇫
return

;1F1F5 1F1EC

:*::flag:-papua-new-guinea::
Send,🇵🇬
return

;1F1F5 1F1ED

:*::flag:-philippines::
Send,🇵🇭
return

;1F1F5 1F1F0

:*::flag:-pakistan::
Send,🇵🇰
return

;1F1F5 1F1F1

:*::flag:-poland::
Send,🇵🇱
return

;1F1F5 1F1F2

:*::flag:-st.-pierre-&-miquelon::
Send,🇵🇲
return

;1F1F5 1F1F3

:*::flag:-pitcairn-islands::
Send,🇵🇳
return

;1F1F5 1F1F7

:*::flag:-puerto-rico::
Send,🇵🇷
return

;1F1F5 1F1F8

:*::flag:-palestinian-territories::
Send,🇵🇸
return

;1F1F5 1F1F9

:*::flag:-portugal::
Send,🇵🇹
return

;1F1F5 1F1FC

:*::flag:-palau::
Send,🇵🇼
return

;1F1F5 1F1FE

:*::flag:-paraguay::
Send,🇵🇾
return

;1F1F6 1F1E6

:*::flag:-qatar::
Send,🇶🇦
return

;1F1F7 1F1EA

:*::flag:-réunion::
Send,🇷🇪
return

;1F1F7 1F1F4

:*::flag:-romania::
Send,🇷🇴
return

;1F1F7 1F1F8

:*::flag:-serbia::
Send,🇷🇸
return

;1F1F7 1F1FA

:*::flag:-russia::
Send,🇷🇺
return

;1F1F7 1F1FC

:*::flag:-rwanda::
Send,🇷🇼
return

;1F1F8 1F1E6

:*::flag:-saudi-arabia::
Send,🇸🇦
return

;1F1F8 1F1E7

:*::flag:-solomon-islands::
Send,🇸🇧
return

;1F1F8 1F1E8

:*::flag:-seychelles::
Send,🇸🇨
return

;1F1F8 1F1E9

:*::flag:-sudan::
Send,🇸🇩
return

;1F1F8 1F1EA

:*::flag:-sweden::
Send,🇸🇪
return

;1F1F8 1F1EC

:*::flag:-singapore::
Send,🇸🇬
return

;1F1F8 1F1ED

:*::flag:-st.-helena::
Send,🇸🇭
return

;1F1F8 1F1EE

:*::flag:-slovenia::
Send,🇸🇮
return

;1F1F8 1F1EF

:*::flag:-svalbard-&-jan-mayen::
Send,🇸🇯
return

;1F1F8 1F1F0

:*::flag:-slovakia::
Send,🇸🇰
return

;1F1F8 1F1F1

:*::flag:-sierra-leone::
Send,🇸🇱
return

;1F1F8 1F1F2

:*::flag:-san-marino::
Send,🇸🇲
return

;1F1F8 1F1F3

:*::flag:-senegal::
Send,🇸🇳
return

;1F1F8 1F1F4

:*::flag:-somalia::
Send,🇸🇴
return

;1F1F8 1F1F7

:*::flag:-suriname::
Send,🇸🇷
return

;1F1F8 1F1F8

:*::flag:-south-sudan::
Send,🇸🇸
return

;1F1F8 1F1F9

:*::flag:-são-tomé-&-príncipe::
Send,🇸🇹
return

;1F1F8 1F1FB

:*::flag:-el-salvador::
Send,🇸🇻
return

;1F1F8 1F1FD

:*::flag:-sint-maarten::
Send,🇸🇽
return

;1F1F8 1F1FE

:*::flag:-syria::
Send,🇸🇾
return

;1F1F8 1F1FF

:*::flag:-eswatini::
Send,🇸🇿
return

;1F1F9 1F1E6

:*::flag:-tristan-da-cunha::
Send,🇹🇦
return

;1F1F9 1F1E8

:*::flag:-turks-&-caicos-islands::
Send,🇹🇨
return

;1F1F9 1F1E9

:*::flag:-chad::
Send,🇹🇩
return

;1F1F9 1F1EB

:*::flag:-french-southern-territories::
Send,🇹🇫
return

;1F1F9 1F1EC

:*::flag:-togo::
Send,🇹🇬
return

;1F1F9 1F1ED

:*::flag:-thailand::
Send,🇹🇭
return

;1F1F9 1F1EF

:*::flag:-tajikistan::
Send,🇹🇯
return

;1F1F9 1F1F0

:*::flag:-tokelau::
Send,🇹🇰
return

;1F1F9 1F1F1

:*::flag:-timor-leste::
Send,🇹🇱
return

;1F1F9 1F1F2

:*::flag:-turkmenistan::
Send,🇹🇲
return

;1F1F9 1F1F3

:*::flag:-tunisia::
Send,🇹🇳
return

;1F1F9 1F1F4

:*::flag:-tonga::
Send,🇹🇴
return

;1F1F9 1F1F7

:*::flag:-turkey::
Send,🇹🇷
return

;1F1F9 1F1F9

:*::flag:-trinidad-&-tobago::
Send,🇹🇹
return

;1F1F9 1F1FB

:*::flag:-tuvalu::
Send,🇹🇻
return

;1F1F9 1F1FC

:*::flag:-taiwan::
Send,🇹🇼
return

;1F1F9 1F1FF

:*::flag:-tanzania::
Send,🇹🇿
return

;1F1FA 1F1E6

:*::flag:-ukraine::
Send,🇺🇦
return

;1F1FA 1F1EC

:*::flag:-uganda::
Send,🇺🇬
return

;1F1FA 1F1F2

:*::flag:-u.s.-outlying-islands::
Send,🇺🇲
return

;1F1FA 1F1F3

:*::flag:-united-nations::
Send,🇺🇳
return

;1F1FA 1F1F8

:*::flag:-united-states::
Send,🇺🇸
return

;1F1FA 1F1FE

:*::flag:-uruguay::
Send,🇺🇾
return

;1F1FA 1F1FF

:*::flag:-uzbekistan::
Send,🇺🇿
return

;1F1FB 1F1E6

:*::flag:-vatican-city::
Send,🇻🇦
return

;1F1FB 1F1E8

:*::flag:-st.-vincent-&-grenadines::
Send,🇻🇨
return

;1F1FB 1F1EA

:*::flag:-venezuela::
Send,🇻🇪
return

;1F1FB 1F1EC

:*::flag:-british-virgin-islands::
Send,🇻🇬
return

;1F1FB 1F1EE

:*::flag:-u.s.-virgin-islands::
Send,🇻🇮
return

;1F1FB 1F1F3

:*::flag:-vietnam::
Send,🇻🇳
return

;1F1FB 1F1FA

:*::flag:-vanuatu::
Send,🇻🇺
return

;1F1FC 1F1EB

:*::flag:-wallis-&-futuna::
Send,🇼🇫
return

;1F1FC 1F1F8

:*::flag:-samoa::
Send,🇼🇸
return

;1F1FD 1F1F0

:*::flag:-kosovo::
Send,🇽🇰
return

;1F1FE 1F1EA

:*::flag:-yemen::
Send,🇾🇪
return

;1F1FE 1F1F9

:*::flag:-mayotte::
Send,🇾🇹
return

;1F1FF 1F1E6

:*::flag:-south-africa::
Send,🇿🇦
return

;1F1FF 1F1F2

:*::flag:-zambia::
Send,🇿🇲
return

;1F1FF 1F1FC

:*::flag:-zimbabwe::
Send,🇿🇼
return

;1F3F4 E0067 E0062 E0065 E006E E0067 E007F

:*::flag:-england::
Send,🏴󠁧󠁢󠁥󠁮󠁧󠁿
return

;1F3F4 E0067 E0062 E0073 E0063 E0074 E007F

:*::flag:-scotland::
Send,🏴󠁧󠁢󠁳󠁣󠁴󠁿
return

;1F3F4 E0067 E0062 E0077 E006C E0073 E007F

:*::flag:-wales::
Send,🏴󠁧󠁢󠁷󠁬󠁳󠁿
return

;1F601
:*::big-smile::
:*::cheesy-smile::
:*::beaming-face-with-smiling-eyes::
Send,😁
return

;1F923
:*::rofl::
:*::side-cry-laugh::
:*::rolling-on-the-floor-laughing::
Send,🤣
return
