﻿;1F600
:*::smile::
:*::grinning-face::
Send,😀
return

;1F603
:*::grinning-face-with-big-eyes::
Send,😃
return

;1F604
:*::grinning-face-with-smiling-eyes::
Send,😄
return

;1F601
:*::beaming-face-with-smiling-eyes::
Send,😁
return

;1F606
:*::grinning-squinting-face::
Send,😆
return

;1F605
:*::grinning-face-with-sweat::
Send,😅
return

;1F923
:*::rolling-on-the-floor-laughing::
Send,🤣
return

;1F602
:*::face-with-tears-of-joy::
Send,😂
return

;1F642
:*::slightly-smiling-face::
Send,🙂
return

;1F643
:*::upsidedown::
:*::upside-down-face::
Send,🙃
return

;1F609
:*::wink::
:*::winking-face::
Send,😉
return

;1F60A
:*::happy::
:*::smiling-face-with-smiling-eyes::
Send,😊
return

;1F607
:*::halo::
:*::smiling-face-with-halo::
Send,😇
return

;1F970
:*::smile-heart::
:*::smiling-face-with-hearts::
Send,🥰
return

;1F60D
:*::heart-eyes::
:*::smiling-face-with-heart-eyes::
Send,😍
return

;1F929
:*::star-struck::
Send,🤩
return

;1F618
:*::blowkiss::
:*::face-blowing-a-kiss::
Send,😘
return

;1F617
:*::kissing-face::
Send,😗
return

;263A FE0F
:*::smiling-face::
Send,☺️
return

;263A
:*::smiling-face::
Send,☺
return

;1F61A
:*::kissing-face-with-closed-eyes::
Send,😚
return

;1F619
:*::kissing-face-with-smiling-eyes::
Send,😙
return

;1F60B
:*::toung-face::
:*::face-savoring-food::
Send,😋
return

;1F61B
:*::toung-out::
:*::face-with-tongue::
Send,😛
return

;1F61C
:*::winking-face-with-tongue::
Send,😜
return

;1F92A
:*::zany-face::
Send,🤪
return

;1F61D
:*::squinting-face-with-tongue::
Send,😝
return

;1F911
:*::money-mouth::
:*::money-mouth-face::
Send,🤑
return

;1F917
:*::hugging-face::
Send,🤗
return

;1F92D
:*::face-with-hand-over-mouth::
Send,🤭
return

;1F92B
:*::shushing-face::
Send,🤫
return

;1F914
:*::thinking::
:*::thinking-face::
Send,🤔
return

;1F910
:*::zipper-mouth-face::
Send,🤐
return

;1F928
:*::face-with-raised-eyebrow::
Send,🤨
return

;1F610
:*::neutral-face::
Send,😐
return

;1F611
:*::expressionless-face::
Send,😑
return

;1F636
:*::face-without-mouth::
Send,😶
return

;1F60F
:*::smirking-face::
Send,😏
return

;1F612
:*::unamused-face::
Send,😒
return

;1F644
:*::face-with-rolling-eyes::
Send,🙄
return

;1F62C
:*::grimacing-face::
Send,😬
return

;1F925
:*::lying-face::
Send,🤥
return

;1F60C
:*::relieved-face::
Send,😌
return

;1F614
:*::pensive-face::
Send,😔
return

;1F62A
:*::sleepy-face::
Send,😪
return

;1F924
:*::drooling-face::
Send,🤤
return

;1F634
:*::sleeping-face::
Send,😴
return

;1F637
:*::face-with-medical-mask::
Send,😷
return

;1F912
:*::face-with-thermometer::
Send,🤒
return

;1F915
:*::face-with-head-bandage::
Send,🤕
return

;1F922
:*::nauseated-face::
Send,🤢
return

;1F92E
:*::face-vomiting::
Send,🤮
return

;1F927
:*::sneezing-face::
Send,🤧
return

;1F975
:*::hot-face::
Send,🥵
return

;1F976
:*::cold-face::
Send,🥶
return

;1F974
:*::woozy-face::
Send,🥴
return

;1F635
:*::dizzy-face::
Send,😵
return

;1F92F
:*::exploding-head::
Send,🤯
return

;1F920
:*::cowboy-hat-face::
Send,🤠
return

;1F973
:*::partying-face::
Send,🥳
return

;1F60E
:*::smiling-face-with-sunglasses::
Send,😎
return

;1F913
:*::nerd-face::
Send,🤓
return

;1F9D0
:*::face-with-monocle::
Send,🧐
return

;1F615
:*::confused-face::
Send,😕
return

;1F61F
:*::worried-face::
Send,😟
return

;1F641
:*::slightly-frowning-face::
Send,🙁
return

;2639 FE0F
:*::frowning-face::
Send,☹️
return

;2639
:*::frowning-face::
Send,☹
return

;1F62E
:*::face-with-open-mouth::
Send,😮
return

;1F62F
:*::hushed-face::
Send,😯
return

;1F632
:*::astonished-face::
Send,😲
return

;1F633
:*::flushed-face::
Send,😳
return

;1F97A
:*::pleading-face::
Send,🥺
return

;1F626
:*::frowning-face-with-open-mouth::
Send,😦
return

;1F627
:*::anguished-face::
Send,😧
return

;1F628
:*::fearful-face::
Send,😨
return

;1F630
:*::anxious-face-with-sweat::
Send,😰
return

;1F625
:*::sad-but-relieved-face::
Send,😥
return

;1F622
:*::crying-face::
Send,😢
return

;1F62D
:*::loudly-crying-face::
Send,😭
return

;1F631
:*::face-screaming-in-fear::
Send,😱
return

;1F616
:*::confounded-face::
Send,😖
return

;1F623
:*::persevering-face::
Send,😣
return

;1F61E
:*::disappointed-face::
Send,😞
return

;1F613
:*::downcast-face-with-sweat::
Send,😓
return

;1F629
:*::weary-face::
Send,😩
return

;1F62B
:*::tired-face::
Send,😫
return

;1F971
:*::yawning-face::
Send,🥱
return

;1F624
:*::face-with-steam-from-nose::
Send,😤
return

;1F621
:*::pouting-face::
Send,😡
return

;1F620
:*::angry-face::
Send,😠
return

;1F92C
:*::face-with-symbols-on-mouth::
Send,🤬
return

;1F608
:*::smiling-face-with-horns::
Send,😈
return

;1F47F
:*::angry-face-with-horns::
Send,👿
return

;1F480
:*::skull::
Send,💀
return

;2620 FE0F
:*::skull-and-crossbones::
Send,☠️
return

;2620
:*::skull-and-crossbones::
Send,☠
return

;1F4A9
:*::pile-of-poo::
Send,💩
return

;1F921
:*::clown-face::
Send,🤡
return

;1F479
:*::ogre::
Send,👹
return

;1F47A
:*::goblin::
Send,👺
return

;1F47B
:*::ghost::
Send,👻
return

;1F47D
:*::alien::
Send,👽
return

;1F47E
:*::alien-monster::
Send,👾
return

;1F916
:*::robot::
Send,🤖
return

;1F63A
:*::grinning-cat::
Send,😺
return

;1F638
:*::grinning-cat-with-smiling-eyes::
Send,😸
return

;1F639
:*::cat-with-tears-of-joy::
Send,😹
return

;1F63B
:*::smiling-cat-with-heart-eyes::
Send,😻
return

;1F63C
:*::cat-with-wry-smile::
Send,😼
return

;1F63D
:*::kissing-cat::
Send,😽
return

;1F640
:*::weary-cat::
Send,🙀
return

;1F63F
:*::crying-cat::
Send,😿
return

;1F63E
:*::pouting-cat::
Send,😾
return

;1F648
:*::see-no-evil-monkey::
Send,🙈
return

;1F649
:*::hear-no-evil-monkey::
Send,🙉
return

;1F64A
:*::speak-no-evil-monkey::
Send,🙊
return

;1F48B
:*::kiss-mark::
Send,💋
return

;1F48C
:*::love-letter::
Send,💌
return

;1F498
:*::heart-with-arrow::
Send,💘
return

;1F49D
:*::heart-with-ribbon::
Send,💝
return

;1F496
:*::sparkling-heart::
Send,💖
return

;1F497
:*::growing-heart::
Send,💗
return

;1F493
:*::beating-heart::
Send,💓
return

;1F49E
:*::revolving-hearts::
Send,💞
return

;1F495
:*::two-hearts::
Send,💕
return

;1F49F
:*::heart-decoration::
Send,💟
return

;2763 FE0F
:*::heart-exclamation::
Send,❣️
return

;2763
:*::heart-exclamation::
Send,❣
return

;1F494
:*::broken-heart::
Send,💔
return

;2764 FE0F
:*::red-heart::
Send,❤️
return

;2764
:*::red-heart::
Send,❤
return

;1F9E1
:*::orange-heart::
Send,🧡
return

;1F49B
:*::yellow-heart::
Send,💛
return

;1F49A
:*::green-heart::
Send,💚
return

;1F499
:*::blue-heart::
Send,💙
return

;1F49C
:*::purple-heart::
Send,💜
return

;1F90E
:*::brown-heart::
Send,🤎
return

;1F5A4
:*::black-heart::
Send,🖤
return

;1F90D
:*::white-heart::
Send,🤍
return

;1F4AF
:*::hundred-points::
Send,💯
return

;1F4A2
:*::anger-symbol::
Send,💢
return

;1F4A5
:*::collision::
Send,💥
return

;1F4AB
:*::dizzy::
Send,💫
return

;1F4A6
:*::sweat-droplets::
Send,💦
return

;1F4A8
:*::dashing-away::
Send,💨
return

;1F573 FE0F
:*::hole::
Send,🕳️
return

;1F573
:*::hole::
Send,🕳
return

;1F4A3
:*::bomb::
Send,💣
return

;1F4AC
:*::speech-balloon::
Send,💬
return

;1F441 FE0F 200D 1F5E8 FE0F
:*::eye-in-speech-bubble::
Send,👁️‍🗨️
return

;1F441 200D 1F5E8 FE0F
:*::eye-in-speech-bubble::
Send,👁‍🗨️
return

;1F441 FE0F 200D 1F5E8
:*::eye-in-speech-bubble::
Send,👁️‍🗨
return

;1F441 200D 1F5E8
:*::eye-in-speech-bubble::
Send,👁‍🗨
return

;1F5E8 FE0F
:*::left-speech-bubble::
Send,🗨️
return

;1F5E8
:*::left-speech-bubble::
Send,🗨
return

;1F5EF FE0F
:*::right-anger-bubble::
Send,🗯️
return

;1F5EF
:*::right-anger-bubble::
Send,🗯
return

;1F4AD
:*::thought-balloon::
Send,💭
return

;1F4A4
:*::zzz::
Send,💤
return

;1F44B
:*::waving-hand::
Send,👋
return

;1F44B 1F3FB
:*::waving-hand:-light-skin-tone::
Send,👋🏻
return

;1F44B 1F3FC
:*::waving-hand:-medium-light-skin-tone::
Send,👋🏼
return

;1F44B 1F3FD
:*::waving-hand:-medium-skin-tone::
Send,👋🏽
return

;1F44B 1F3FE
:*::waving-hand:-medium-dark-skin-tone::
Send,👋🏾
return

;1F44B 1F3FF
:*::waving-hand:-dark-skin-tone::
Send,👋🏿
return

;1F91A
:*::raised-back-of-hand::
Send,🤚
return

;1F91A 1F3FB
:*::raised-back-of-hand:-light-skin-tone::
Send,🤚🏻
return

;1F91A 1F3FC
:*::raised-back-of-hand:-medium-light-skin::
Send,🤚🏼
return

;1F91A 1F3FD
:*::raised-back-of-hand:-medium-skin-tone::
Send,🤚🏽
return

;1F91A 1F3FE
:*::raised-back-of-hand:-medium-dark-skin-t::
Send,🤚🏾
return

;1F91A 1F3FF
:*::raised-back-of-hand:-dark-skin-tone::
Send,🤚🏿
return

;1F590 FE0F
:*::hand-with-fingers-splayed::
Send,🖐️
return

;1F590
:*::hand-with-fingers-splayed::
Send,🖐
return

;1F590 1F3FB
:*::hand-with-fingers-splayed:-light-skin-t::
Send,🖐🏻
return

;1F590 1F3FC
:*::hand-with-fingers-splayed:-medium-light::
Send,🖐🏼
return

;1F590 1F3FD
:*::hand-with-fingers-splayed:-medium-skin::
Send,🖐🏽
return

;1F590 1F3FE
:*::hand-with-fingers-splayed:-medium-dark::
Send,🖐🏾
return

;1F590 1F3FF
:*::hand-with-fingers-splayed:-dark-skin-to::
Send,🖐🏿
return

;270B
:*::raised-hand::
Send,✋
return

;270B 1F3FB
:*::raised-hand:-light-skin-tone::
Send,✋🏻
return

;270B 1F3FC
:*::raised-hand:-medium-light-skin-tone::
Send,✋🏼
return

;270B 1F3FD
:*::raised-hand:-medium-skin-tone::
Send,✋🏽
return

;270B 1F3FE
:*::raised-hand:-medium-dark-skin-tone::
Send,✋🏾
return

;270B 1F3FF
:*::raised-hand:-dark-skin-tone::
Send,✋🏿
return

;1F596
:*::vulcan-salute::
Send,🖖
return

;1F596 1F3FB
:*::vulcan-salute:-light-skin-tone::
Send,🖖🏻
return

;1F596 1F3FC
:*::vulcan-salute:-medium-light-skin-tone::
Send,🖖🏼
return

;1F596 1F3FD
:*::vulcan-salute:-medium-skin-tone::
Send,🖖🏽
return

;1F596 1F3FE
:*::vulcan-salute:-medium-dark-skin-tone::
Send,🖖🏾
return

;1F596 1F3FF
:*::vulcan-salute:-dark-skin-tone::
Send,🖖🏿
return

;1F44C
:*::OK-hand::
Send,👌
return

;1F44C 1F3FB
:*::OK-hand:-light-skin-tone::
Send,👌🏻
return

;1F44C 1F3FC
:*::OK-hand:-medium-light-skin-tone::
Send,👌🏼
return

;1F44C 1F3FD
:*::OK-hand:-medium-skin-tone::
Send,👌🏽
return

;1F44C 1F3FE
:*::OK-hand:-medium-dark-skin-tone::
Send,👌🏾
return

;1F44C 1F3FF
:*::OK-hand:-dark-skin-tone::
Send,👌🏿
return

;1F90F
:*::pinching-hand::
Send,🤏
return

;1F90F 1F3FB
:*::pinching-hand:-light-skin-tone::
Send,🤏🏻
return

;1F90F 1F3FC
:*::pinching-hand:-medium-light-skin-tone::
Send,🤏🏼
return

;1F90F 1F3FD
:*::pinching-hand:-medium-skin-tone::
Send,🤏🏽
return

;1F90F 1F3FE
:*::pinching-hand:-medium-dark-skin-tone::
Send,🤏🏾
return

;1F90F 1F3FF
:*::pinching-hand:-dark-skin-tone::
Send,🤏🏿
return

;270C FE0F
:*::victory-hand::
Send,✌️
return

;270C
:*::victory-hand::
Send,✌
return

;270C 1F3FB
:*::victory-hand:-light-skin-tone::
Send,✌🏻
return

;270C 1F3FC
:*::victory-hand:-medium-light-skin-tone::
Send,✌🏼
return

;270C 1F3FD
:*::victory-hand:-medium-skin-tone::
Send,✌🏽
return

;270C 1F3FE
:*::victory-hand:-medium-dark-skin-tone::
Send,✌🏾
return

;270C 1F3FF
:*::victory-hand:-dark-skin-tone::
Send,✌🏿
return

;1F91E
:*::crossed-fingers::
Send,🤞
return

;1F91E 1F3FB
:*::crossed-fingers:-light-skin-tone::
Send,🤞🏻
return

;1F91E 1F3FC
:*::crossed-fingers:-medium-light-skin-tone::
Send,🤞🏼
return

;1F91E 1F3FD
:*::crossed-fingers:-medium-skin-tone::
Send,🤞🏽
return

;1F91E 1F3FE
:*::crossed-fingers:-medium-dark-skin-tone::
Send,🤞🏾
return

;1F91E 1F3FF
:*::crossed-fingers:-dark-skin-tone::
Send,🤞🏿
return

;1F91F
:*::love-you-gesture::
Send,🤟
return

;1F91F 1F3FB
:*::love-you-gesture:-light-skin-tone::
Send,🤟🏻
return

;1F91F 1F3FC
:*::love-you-gesture:-medium-light-skin-ton::
Send,🤟🏼
return

;1F91F 1F3FD
:*::love-you-gesture:-medium-skin-tone::
Send,🤟🏽
return

;1F91F 1F3FE
:*::love-you-gesture:-medium-dark-skin-tone::
Send,🤟🏾
return

;1F91F 1F3FF
:*::love-you-gesture:-dark-skin-tone::
Send,🤟🏿
return

;1F918
:*::sign-of-the-horns::
Send,🤘
return

;1F918 1F3FB
:*::sign-of-the-horns:-light-skin-tone::
Send,🤘🏻
return

;1F918 1F3FC
:*::sign-of-the-horns:-medium-light-skin-to::
Send,🤘🏼
return

;1F918 1F3FD
:*::sign-of-the-horns:-medium-skin-tone::
Send,🤘🏽
return

;1F918 1F3FE
:*::sign-of-the-horns:-medium-dark-skin-ton::
Send,🤘🏾
return

;1F918 1F3FF
:*::sign-of-the-horns:-dark-skin-tone::
Send,🤘🏿
return

;1F919
:*::call-me-hand::
Send,🤙
return

;1F919 1F3FB
:*::call-me-hand:-light-skin-tone::
Send,🤙🏻
return

;1F919 1F3FC
:*::call-me-hand:-medium-light-skin-tone::
Send,🤙🏼
return

;1F919 1F3FD
:*::call-me-hand:-medium-skin-tone::
Send,🤙🏽
return

;1F919 1F3FE
:*::call-me-hand:-medium-dark-skin-tone::
Send,🤙🏾
return

;1F919 1F3FF
:*::call-me-hand:-dark-skin-tone::
Send,🤙🏿
return

;1F448
:*::backhand-index-pointing-left::
Send,👈
return

;1F448 1F3FB
:*::backhand-index-pointing-left:-light-ski::
Send,👈🏻
return

;1F448 1F3FC
:*::backhand-index-pointing-left:-medium-li::
Send,👈🏼
return

;1F448 1F3FD
:*::backhand-index-pointing-left:-medium-sk::
Send,👈🏽
return

;1F448 1F3FE
:*::backhand-index-pointing-left:-medium-da::
Send,👈🏾
return

;1F448 1F3FF
:*::backhand-index-pointing-left:-dark-skin::
Send,👈🏿
return

;1F449
:*::backhand-index-pointing-right::
Send,👉
return

;1F449 1F3FB
:*::backhand-index-pointing-right:-light-sk::
Send,👉🏻
return

;1F449 1F3FC
:*::backhand-index-pointing-right:-medium-l::
Send,👉🏼
return

;1F449 1F3FD
:*::backhand-index-pointing-right:-medium-s::
Send,👉🏽
return

;1F449 1F3FE
:*::backhand-index-pointing-right:-medium-d::
Send,👉🏾
return

;1F449 1F3FF
:*::backhand-index-pointing-right:-dark-ski::
Send,👉🏿
return

;1F446
:*::backhand-index-pointing-up::
Send,👆
return

;1F446 1F3FB
:*::backhand-index-pointing-up:-light-skin::
Send,👆🏻
return

;1F446 1F3FC
:*::backhand-index-pointing-up:-medium-ligh::
Send,👆🏼
return

;1F446 1F3FD
:*::backhand-index-pointing-up:-medium-skin::
Send,👆🏽
return

;1F446 1F3FE
:*::backhand-index-pointing-up:-medium-dark::
Send,👆🏾
return

;1F446 1F3FF
:*::backhand-index-pointing-up:-dark-skin-t::
Send,👆🏿
return

;1F595
:*::middle-finger::
Send,🖕
return

;1F595 1F3FB
:*::middle-finger:-light-skin-tone::
Send,🖕🏻
return

;1F595 1F3FC
:*::middle-finger:-medium-light-skin-tone::
Send,🖕🏼
return

;1F595 1F3FD
:*::middle-finger:-medium-skin-tone::
Send,🖕🏽
return

;1F595 1F3FE
:*::middle-finger:-medium-dark-skin-tone::
Send,🖕🏾
return

;1F595 1F3FF
:*::middle-finger:-dark-skin-tone::
Send,🖕🏿
return

;1F447
:*::backhand-index-pointing-down::
Send,👇
return

;1F447 1F3FB
:*::backhand-index-pointing-down:-light-ski::
Send,👇🏻
return

;1F447 1F3FC
:*::backhand-index-pointing-down:-medium-li::
Send,👇🏼
return

;1F447 1F3FD
:*::backhand-index-pointing-down:-medium-sk::
Send,👇🏽
return

;1F447 1F3FE
:*::backhand-index-pointing-down:-medium-da::
Send,👇🏾
return

;1F447 1F3FF
:*::backhand-index-pointing-down:-dark-skin::
Send,👇🏿
return

;261D FE0F
:*::index-pointing-up::
Send,☝️
return

;261D
:*::index-pointing-up::
Send,☝
return

;261D 1F3FB
:*::index-pointing-up:-light-skin-tone::
Send,☝🏻
return

;261D 1F3FC
:*::index-pointing-up:-medium-light-skin-to::
Send,☝🏼
return

;261D 1F3FD
:*::index-pointing-up:-medium-skin-tone::
Send,☝🏽
return

;261D 1F3FE
:*::index-pointing-up:-medium-dark-skin-ton::
Send,☝🏾
return

;261D 1F3FF
:*::index-pointing-up:-dark-skin-tone::
Send,☝🏿
return

;1F44D
:*::thumbs-up::
Send,👍
return

;1F44D 1F3FB
:*::thumbs-up:-light-skin-tone::
Send,👍🏻
return

;1F44D 1F3FC
:*::thumbs-up:-medium-light-skin-tone::
Send,👍🏼
return

;1F44D 1F3FD
:*::thumbs-up:-medium-skin-tone::
Send,👍🏽
return

;1F44D 1F3FE
:*::thumbs-up:-medium-dark-skin-tone::
Send,👍🏾
return

;1F44D 1F3FF
:*::thumbs-up:-dark-skin-tone::
Send,👍🏿
return

;1F44E
:*::thumbs-down::
Send,👎
return

;1F44E 1F3FB
:*::thumbs-down:-light-skin-tone::
Send,👎🏻
return

;1F44E 1F3FC
:*::thumbs-down:-medium-light-skin-tone::
Send,👎🏼
return

;1F44E 1F3FD
:*::thumbs-down:-medium-skin-tone::
Send,👎🏽
return

;1F44E 1F3FE
:*::thumbs-down:-medium-dark-skin-tone::
Send,👎🏾
return

;1F44E 1F3FF
:*::thumbs-down:-dark-skin-tone::
Send,👎🏿
return

;270A
:*::raised-fist::
Send,✊
return

;270A 1F3FB
:*::raised-fist:-light-skin-tone::
Send,✊🏻
return

;270A 1F3FC
:*::raised-fist:-medium-light-skin-tone::
Send,✊🏼
return

;270A 1F3FD
:*::raised-fist:-medium-skin-tone::
Send,✊🏽
return

;270A 1F3FE
:*::raised-fist:-medium-dark-skin-tone::
Send,✊🏾
return

;270A 1F3FF
:*::raised-fist:-dark-skin-tone::
Send,✊🏿
return

;1F44A
:*::oncoming-fist::
Send,👊
return

;1F44A 1F3FB
:*::oncoming-fist:-light-skin-tone::
Send,👊🏻
return

;1F44A 1F3FC
:*::oncoming-fist:-medium-light-skin-tone::
Send,👊🏼
return

;1F44A 1F3FD
:*::oncoming-fist:-medium-skin-tone::
Send,👊🏽
return

;1F44A 1F3FE
:*::oncoming-fist:-medium-dark-skin-tone::
Send,👊🏾
return

;1F44A 1F3FF
:*::oncoming-fist:-dark-skin-tone::
Send,👊🏿
return

;1F91B
:*::left-facing-fist::
Send,🤛
return

;1F91B 1F3FB
:*::left-facing-fist:-light-skin-tone::
Send,🤛🏻
return

;1F91B 1F3FC
:*::left-facing-fist:-medium-light-skin-ton::
Send,🤛🏼
return

;1F91B 1F3FD
:*::left-facing-fist:-medium-skin-tone::
Send,🤛🏽
return

;1F91B 1F3FE
:*::left-facing-fist:-medium-dark-skin-tone::
Send,🤛🏾
return

;1F91B 1F3FF
:*::left-facing-fist:-dark-skin-tone::
Send,🤛🏿
return

;1F91C
:*::right-facing-fist::
Send,🤜
return

;1F91C 1F3FB
:*::right-facing-fist:-light-skin-tone::
Send,🤜🏻
return

;1F91C 1F3FC
:*::right-facing-fist:-medium-light-skin-to::
Send,🤜🏼
return

;1F91C 1F3FD
:*::right-facing-fist:-medium-skin-tone::
Send,🤜🏽
return

;1F91C 1F3FE
:*::right-facing-fist:-medium-dark-skin-ton::
Send,🤜🏾
return

;1F91C 1F3FF
:*::right-facing-fist:-dark-skin-tone::
Send,🤜🏿
return

;1F44F
:*::clapping-hands::
Send,👏
return

;1F44F 1F3FB
:*::clapping-hands:-light-skin-tone::
Send,👏🏻
return

;1F44F 1F3FC
:*::clapping-hands:-medium-light-skin-tone::
Send,👏🏼
return

;1F44F 1F3FD
:*::clapping-hands:-medium-skin-tone::
Send,👏🏽
return

;1F44F 1F3FE
:*::clapping-hands:-medium-dark-skin-tone::
Send,👏🏾
return

;1F44F 1F3FF
:*::clapping-hands:-dark-skin-tone::
Send,👏🏿
return

;1F64C
:*::raising-hands::
Send,🙌
return

;1F64C 1F3FB
:*::raising-hands:-light-skin-tone::
Send,🙌🏻
return

;1F64C 1F3FC
:*::raising-hands:-medium-light-skin-tone::
Send,🙌🏼
return

;1F64C 1F3FD
:*::raising-hands:-medium-skin-tone::
Send,🙌🏽
return

;1F64C 1F3FE
:*::raising-hands:-medium-dark-skin-tone::
Send,🙌🏾
return

;1F64C 1F3FF
:*::raising-hands:-dark-skin-tone::
Send,🙌🏿
return

;1F450
:*::open-hands::
Send,👐
return

;1F450 1F3FB
:*::open-hands:-light-skin-tone::
Send,👐🏻
return

;1F450 1F3FC
:*::open-hands:-medium-light-skin-tone::
Send,👐🏼
return

;1F450 1F3FD
:*::open-hands:-medium-skin-tone::
Send,👐🏽
return

;1F450 1F3FE
:*::open-hands:-medium-dark-skin-tone::
Send,👐🏾
return

;1F450 1F3FF
:*::open-hands:-dark-skin-tone::
Send,👐🏿
return

;1F932
:*::palms-up-together::
Send,🤲
return

;1F932 1F3FB
:*::palms-up-together:-light-skin-tone::
Send,🤲🏻
return

;1F932 1F3FC
:*::palms-up-together:-medium-light-skin-to::
Send,🤲🏼
return

;1F932 1F3FD
:*::palms-up-together:-medium-skin-tone::
Send,🤲🏽
return

;1F932 1F3FE
:*::palms-up-together:-medium-dark-skin-ton::
Send,🤲🏾
return

;1F932 1F3FF
:*::palms-up-together:-dark-skin-tone::
Send,🤲🏿
return

;1F91D
:*::handshake::
Send,🤝
return

;1F64F
:*::folded-hands::
Send,🙏
return

;1F64F 1F3FB
:*::folded-hands:-light-skin-tone::
Send,🙏🏻
return

;1F64F 1F3FC
:*::folded-hands:-medium-light-skin-tone::
Send,🙏🏼
return

;1F64F 1F3FD
:*::folded-hands:-medium-skin-tone::
Send,🙏🏽
return

;1F64F 1F3FE
:*::folded-hands:-medium-dark-skin-tone::
Send,🙏🏾
return

;1F64F 1F3FF
:*::folded-hands:-dark-skin-tone::
Send,🙏🏿
return

;270D FE0F
:*::writing-hand::
Send,✍️
return

;270D
:*::writing-hand::
Send,✍
return

;270D 1F3FB
:*::writing-hand:-light-skin-tone::
Send,✍🏻
return

;270D 1F3FC
:*::writing-hand:-medium-light-skin-tone::
Send,✍🏼
return

;270D 1F3FD
:*::writing-hand:-medium-skin-tone::
Send,✍🏽
return

;270D 1F3FE
:*::writing-hand:-medium-dark-skin-tone::
Send,✍🏾
return

;270D 1F3FF
:*::writing-hand:-dark-skin-tone::
Send,✍🏿
return

;1F485
:*::nail-polish::
Send,💅
return

;1F485 1F3FB
:*::nail-polish:-light-skin-tone::
Send,💅🏻
return

;1F485 1F3FC
:*::nail-polish:-medium-light-skin-tone::
Send,💅🏼
return

;1F485 1F3FD
:*::nail-polish:-medium-skin-tone::
Send,💅🏽
return

;1F485 1F3FE
:*::nail-polish:-medium-dark-skin-tone::
Send,💅🏾
return

;1F485 1F3FF
:*::nail-polish:-dark-skin-tone::
Send,💅🏿
return

;1F933
:*::selfie::
Send,🤳
return

;1F933 1F3FB
:*::selfie:-light-skin-tone::
Send,🤳🏻
return

;1F933 1F3FC
:*::selfie:-medium-light-skin-tone::
Send,🤳🏼
return

;1F933 1F3FD
:*::selfie:-medium-skin-tone::
Send,🤳🏽
return

;1F933 1F3FE
:*::selfie:-medium-dark-skin-tone::
Send,🤳🏾
return

;1F933 1F3FF
:*::selfie:-dark-skin-tone::
Send,🤳🏿
return

;1F4AA
:*::flexed-biceps::
Send,💪
return

;1F4AA 1F3FB
:*::flexed-biceps:-light-skin-tone::
Send,💪🏻
return

;1F4AA 1F3FC
:*::flexed-biceps:-medium-light-skin-tone::
Send,💪🏼
return

;1F4AA 1F3FD
:*::flexed-biceps:-medium-skin-tone::
Send,💪🏽
return

;1F4AA 1F3FE
:*::flexed-biceps:-medium-dark-skin-tone::
Send,💪🏾
return

;1F4AA 1F3FF
:*::flexed-biceps:-dark-skin-tone::
Send,💪🏿
return

;1F9BE
:*::mechanical-arm::
Send,🦾
return

;1F9BF
:*::mechanical-leg::
Send,🦿
return

;1F9B5
:*::leg::
Send,🦵
return

;1F9B5 1F3FB
:*::leg:-light-skin-tone::
Send,🦵🏻
return

;1F9B5 1F3FC
:*::leg:-medium-light-skin-tone::
Send,🦵🏼
return

;1F9B5 1F3FD
:*::leg:-medium-skin-tone::
Send,🦵🏽
return

;1F9B5 1F3FE
:*::leg:-medium-dark-skin-tone::
Send,🦵🏾
return

;1F9B5 1F3FF
:*::leg:-dark-skin-tone::
Send,🦵🏿
return

;1F9B6
:*::foot::
Send,🦶
return

;1F9B6 1F3FB
:*::foot:-light-skin-tone::
Send,🦶🏻
return

;1F9B6 1F3FC
:*::foot:-medium-light-skin-tone::
Send,🦶🏼
return

;1F9B6 1F3FD
:*::foot:-medium-skin-tone::
Send,🦶🏽
return

;1F9B6 1F3FE
:*::foot:-medium-dark-skin-tone::
Send,🦶🏾
return

;1F9B6 1F3FF
:*::foot:-dark-skin-tone::
Send,🦶🏿
return

;1F442
:*::ear::
Send,👂
return

;1F442 1F3FB
:*::ear:-light-skin-tone::
Send,👂🏻
return

;1F442 1F3FC
:*::ear:-medium-light-skin-tone::
Send,👂🏼
return

;1F442 1F3FD
:*::ear:-medium-skin-tone::
Send,👂🏽
return

;1F442 1F3FE
:*::ear:-medium-dark-skin-tone::
Send,👂🏾
return

;1F442 1F3FF
:*::ear:-dark-skin-tone::
Send,👂🏿
return

;1F9BB
:*::ear-with-hearing-aid::
Send,🦻
return

;1F9BB 1F3FB
:*::ear-with-hearing-aid:-light-skin-tone::
Send,🦻🏻
return

;1F9BB 1F3FC
:*::ear-with-hearing-aid:-medium-light-skin::
Send,🦻🏼
return

;1F9BB 1F3FD
:*::ear-with-hearing-aid:-medium-skin-tone::
Send,🦻🏽
return

;1F9BB 1F3FE
:*::ear-with-hearing-aid:-medium-dark-skin::
Send,🦻🏾
return

;1F9BB 1F3FF
:*::ear-with-hearing-aid:-dark-skin-tone::
Send,🦻🏿
return

;1F443
:*::nose::
Send,👃
return

;1F443 1F3FB
:*::nose:-light-skin-tone::
Send,👃🏻
return

;1F443 1F3FC
:*::nose:-medium-light-skin-tone::
Send,👃🏼
return

;1F443 1F3FD
:*::nose:-medium-skin-tone::
Send,👃🏽
return

;1F443 1F3FE
:*::nose:-medium-dark-skin-tone::
Send,👃🏾
return

;1F443 1F3FF
:*::nose:-dark-skin-tone::
Send,👃🏿
return

;1F9E0
:*::brain::
Send,🧠
return

;1F9B7
:*::tooth::
Send,🦷
return

;1F9B4
:*::bone::
Send,🦴
return

;1F440
:*::eyes::
Send,👀
return

;1F441 FE0F
:*::eye::
Send,👁️
return

;1F441
:*::eye::
Send,👁
return

;1F445
:*::tongue::
Send,👅
return

;1F444
:*::mouth::
Send,👄
return

;1F476
:*::baby::
Send,👶
return

;1F476 1F3FB
:*::baby:-light-skin-tone::
Send,👶🏻
return

;1F476 1F3FC
:*::baby:-medium-light-skin-tone::
Send,👶🏼
return

;1F476 1F3FD
:*::baby:-medium-skin-tone::
Send,👶🏽
return

;1F476 1F3FE
:*::baby:-medium-dark-skin-tone::
Send,👶🏾
return

;1F476 1F3FF
:*::baby:-dark-skin-tone::
Send,👶🏿
return

;1F9D2
:*::child::
Send,🧒
return

;1F9D2 1F3FB
:*::child:-light-skin-tone::
Send,🧒🏻
return

;1F9D2 1F3FC
:*::child:-medium-light-skin-tone::
Send,🧒🏼
return

;1F9D2 1F3FD
:*::child:-medium-skin-tone::
Send,🧒🏽
return

;1F9D2 1F3FE
:*::child:-medium-dark-skin-tone::
Send,🧒🏾
return

;1F9D2 1F3FF
:*::child:-dark-skin-tone::
Send,🧒🏿
return

;1F466
:*::boy::
Send,👦
return

;1F466 1F3FB
:*::boy:-light-skin-tone::
Send,👦🏻
return

;1F466 1F3FC
:*::boy:-medium-light-skin-tone::
Send,👦🏼
return

;1F466 1F3FD
:*::boy:-medium-skin-tone::
Send,👦🏽
return

;1F466 1F3FE
:*::boy:-medium-dark-skin-tone::
Send,👦🏾
return

;1F466 1F3FF
:*::boy:-dark-skin-tone::
Send,👦🏿
return

;1F467
:*::girl::
Send,👧
return

;1F467 1F3FB
:*::girl:-light-skin-tone::
Send,👧🏻
return

;1F467 1F3FC
:*::girl:-medium-light-skin-tone::
Send,👧🏼
return

;1F467 1F3FD
:*::girl:-medium-skin-tone::
Send,👧🏽
return

;1F467 1F3FE
:*::girl:-medium-dark-skin-tone::
Send,👧🏾
return

;1F467 1F3FF
:*::girl:-dark-skin-tone::
Send,👧🏿
return

;1F9D1
:*::person::
Send,🧑
return

;1F9D1 1F3FB
:*::person:-light-skin-tone::
Send,🧑🏻
return

;1F9D1 1F3FC
:*::person:-medium-light-skin-tone::
Send,🧑🏼
return

;1F9D1 1F3FD
:*::person:-medium-skin-tone::
Send,🧑🏽
return

;1F9D1 1F3FE
:*::person:-medium-dark-skin-tone::
Send,🧑🏾
return

;1F9D1 1F3FF
:*::person:-dark-skin-tone::
Send,🧑🏿
return

;1F471
:*::person:-blond-hair::
Send,👱
return

;1F471 1F3FB
:*::person:-light-skin-tone,-blond-hair::
Send,👱🏻
return

;1F471 1F3FC
:*::person:-medium-light-skin-tone,-blond-h::
Send,👱🏼
return

;1F471 1F3FD
:*::person:-medium-skin-tone,-blond-hair::
Send,👱🏽
return

;1F471 1F3FE
:*::person:-medium-dark-skin-tone,-blond-ha::
Send,👱🏾
return

;1F471 1F3FF
:*::person:-dark-skin-tone,-blond-hair::
Send,👱🏿
return

;1F468
:*::man::
Send,👨
return

;1F468 1F3FB
:*::man:-light-skin-tone::
Send,👨🏻
return

;1F468 1F3FC
:*::man:-medium-light-skin-tone::
Send,👨🏼
return

;1F468 1F3FD
:*::man:-medium-skin-tone::
Send,👨🏽
return

;1F468 1F3FE
:*::man:-medium-dark-skin-tone::
Send,👨🏾
return

;1F468 1F3FF
:*::man:-dark-skin-tone::
Send,👨🏿
return

;1F9D4
:*::man:-beard::
Send,🧔
return

;1F9D4 1F3FB
:*::man:-light-skin-tone,-beard::
Send,🧔🏻
return

;1F9D4 1F3FC
:*::man:-medium-light-skin-tone,-beard::
Send,🧔🏼
return

;1F9D4 1F3FD
:*::man:-medium-skin-tone,-beard::
Send,🧔🏽
return

;1F9D4 1F3FE
:*::man:-medium-dark-skin-tone,-beard::
Send,🧔🏾
return

;1F9D4 1F3FF
:*::man:-dark-skin-tone,-beard::
Send,🧔🏿
return

;1F471 200D 2642 FE0F
:*::man:-blond-hair::
Send,👱‍♂️
return

;1F471 200D 2642
:*::man:-blond-hair::
Send,👱‍♂
return

;1F471 1F3FB 200D 2642 FE0F
:*::man:-light-skin-tone,-blond-hair::
Send,👱🏻‍♂️
return

;1F471 1F3FB 200D 2642
:*::man:-light-skin-tone,-blond-hair::
Send,👱🏻‍♂
return

;1F471 1F3FC 200D 2642 FE0F
:*::man:-medium-light-skin-tone,-blond-hair::
Send,👱🏼‍♂️
return

;1F471 1F3FC 200D 2642
:*::man:-medium-light-skin-tone,-blond-hair::
Send,👱🏼‍♂
return

;1F471 1F3FD 200D 2642 FE0F
:*::man:-medium-skin-tone,-blond-hair::
Send,👱🏽‍♂️
return

;1F471 1F3FD 200D 2642
:*::man:-medium-skin-tone,-blond-hair::
Send,👱🏽‍♂
return

;1F471 1F3FE 200D 2642 FE0F
:*::man:-medium-dark-skin-tone,-blond-hair::
Send,👱🏾‍♂️
return

;1F471 1F3FE 200D 2642
:*::man:-medium-dark-skin-tone,-blond-hair::
Send,👱🏾‍♂
return

;1F471 1F3FF 200D 2642 FE0F
:*::man:-dark-skin-tone,-blond-hair::
Send,👱🏿‍♂️
return

;1F471 1F3FF 200D 2642
:*::man:-dark-skin-tone,-blond-hair::
Send,👱🏿‍♂
return

;1F468 200D 1F9B0
:*::man:-red-hair::
Send,👨‍🦰
return

;1F468 1F3FB 200D 1F9B0
:*::man:-light-skin-tone,-red-hair::
Send,👨🏻‍🦰
return

;1F468 1F3FC 200D 1F9B0
:*::man:-medium-light-skin-tone,-red-hair::
Send,👨🏼‍🦰
return

;1F468 1F3FD 200D 1F9B0
:*::man:-medium-skin-tone,-red-hair::
Send,👨🏽‍🦰
return

;1F468 1F3FE 200D 1F9B0
:*::man:-medium-dark-skin-tone,-red-hair::
Send,👨🏾‍🦰
return

;1F468 1F3FF 200D 1F9B0
:*::man:-dark-skin-tone,-red-hair::
Send,👨🏿‍🦰
return

;1F468 200D 1F9B1
:*::man:-curly-hair::
Send,👨‍🦱
return

;1F468 1F3FB 200D 1F9B1
:*::man:-light-skin-tone,-curly-hair::
Send,👨🏻‍🦱
return

;1F468 1F3FC 200D 1F9B1
:*::man:-medium-light-skin-tone,-curly-hair::
Send,👨🏼‍🦱
return

;1F468 1F3FD 200D 1F9B1
:*::man:-medium-skin-tone,-curly-hair::
Send,👨🏽‍🦱
return

;1F468 1F3FE 200D 1F9B1
:*::man:-medium-dark-skin-tone,-curly-hair::
Send,👨🏾‍🦱
return

;1F468 1F3FF 200D 1F9B1
:*::man:-dark-skin-tone,-curly-hair::
Send,👨🏿‍🦱
return

;1F468 200D 1F9B3
:*::man:-white-hair::
Send,👨‍🦳
return

;1F468 1F3FB 200D 1F9B3
:*::man:-light-skin-tone,-white-hair::
Send,👨🏻‍🦳
return

;1F468 1F3FC 200D 1F9B3
:*::man:-medium-light-skin-tone,-white-hair::
Send,👨🏼‍🦳
return

;1F468 1F3FD 200D 1F9B3
:*::man:-medium-skin-tone,-white-hair::
Send,👨🏽‍🦳
return

;1F468 1F3FE 200D 1F9B3
:*::man:-medium-dark-skin-tone,-white-hair::
Send,👨🏾‍🦳
return

;1F468 1F3FF 200D 1F9B3
:*::man:-dark-skin-tone,-white-hair::
Send,👨🏿‍🦳
return

;1F468 200D 1F9B2
:*::man:-bald::
Send,👨‍🦲
return

;1F468 1F3FB 200D 1F9B2
:*::man:-light-skin-tone,-bald::
Send,👨🏻‍🦲
return

;1F468 1F3FC 200D 1F9B2
:*::man:-medium-light-skin-tone,-bald::
Send,👨🏼‍🦲
return

;1F468 1F3FD 200D 1F9B2
:*::man:-medium-skin-tone,-bald::
Send,👨🏽‍🦲
return

;1F468 1F3FE 200D 1F9B2
:*::man:-medium-dark-skin-tone,-bald::
Send,👨🏾‍🦲
return

;1F468 1F3FF 200D 1F9B2
:*::man:-dark-skin-tone,-bald::
Send,👨🏿‍🦲
return

;1F469
:*::woman::
Send,👩
return

;1F469 1F3FB
:*::woman:-light-skin-tone::
Send,👩🏻
return

;1F469 1F3FC
:*::woman:-medium-light-skin-tone::
Send,👩🏼
return

;1F469 1F3FD
:*::woman:-medium-skin-tone::
Send,👩🏽
return

;1F469 1F3FE
:*::woman:-medium-dark-skin-tone::
Send,👩🏾
return

;1F469 1F3FF
:*::woman:-dark-skin-tone::
Send,👩🏿
return

;1F471 200D 2640 FE0F
:*::woman:-blond-hair::
Send,👱‍♀️
return

;1F471 200D 2640
:*::woman:-blond-hair::
Send,👱‍♀
return

;1F471 1F3FB 200D 2640 FE0F
:*::woman:-light-skin-tone,-blond-hair::
Send,👱🏻‍♀️
return

;1F471 1F3FB 200D 2640
:*::woman:-light-skin-tone,-blond-hair::
Send,👱🏻‍♀
return

;1F471 1F3FC 200D 2640 FE0F
:*::woman:-medium-light-skin-tone,-blond-ha::
Send,👱🏼‍♀️
return

;1F471 1F3FC 200D 2640
:*::woman:-medium-light-skin-tone,-blond-ha::
Send,👱🏼‍♀
return

;1F471 1F3FD 200D 2640 FE0F
:*::woman:-medium-skin-tone,-blond-hair::
Send,👱🏽‍♀️
return

;1F471 1F3FD 200D 2640
:*::woman:-medium-skin-tone,-blond-hair::
Send,👱🏽‍♀
return

;1F471 1F3FE 200D 2640 FE0F
:*::woman:-medium-dark-skin-tone,-blond-hai::
Send,👱🏾‍♀️
return

;1F471 1F3FE 200D 2640
:*::woman:-medium-dark-skin-tone,-blond-hai::
Send,👱🏾‍♀
return

;1F471 1F3FF 200D 2640 FE0F
:*::woman:-dark-skin-tone,-blond-hair::
Send,👱🏿‍♀️
return

;1F471 1F3FF 200D 2640
:*::woman:-dark-skin-tone,-blond-hair::
Send,👱🏿‍♀
return

;1F469 200D 1F9B0
:*::woman:-red-hair::
Send,👩‍🦰
return

;1F469 1F3FB 200D 1F9B0
:*::woman:-light-skin-tone,-red-hair::
Send,👩🏻‍🦰
return

;1F469 1F3FC 200D 1F9B0
:*::woman:-medium-light-skin-tone,-red-hair::
Send,👩🏼‍🦰
return

;1F469 1F3FD 200D 1F9B0
:*::woman:-medium-skin-tone,-red-hair::
Send,👩🏽‍🦰
return

;1F469 1F3FE 200D 1F9B0
:*::woman:-medium-dark-skin-tone,-red-hair::
Send,👩🏾‍🦰
return

;1F469 1F3FF 200D 1F9B0
:*::woman:-dark-skin-tone,-red-hair::
Send,👩🏿‍🦰
return

;1F469 200D 1F9B1
:*::woman:-curly-hair::
Send,👩‍🦱
return

;1F469 1F3FB 200D 1F9B1
:*::woman:-light-skin-tone,-curly-hair::
Send,👩🏻‍🦱
return

;1F469 1F3FC 200D 1F9B1
:*::woman:-medium-light-skin-tone,-curly-ha::
Send,👩🏼‍🦱
return

;1F469 1F3FD 200D 1F9B1
:*::woman:-medium-skin-tone,-curly-hair::
Send,👩🏽‍🦱
return

;1F469 1F3FE 200D 1F9B1
:*::woman:-medium-dark-skin-tone,-curly-hai::
Send,👩🏾‍🦱
return

;1F469 1F3FF 200D 1F9B1
:*::woman:-dark-skin-tone,-curly-hair::
Send,👩🏿‍🦱
return

;1F469 200D 1F9B3
:*::woman:-white-hair::
Send,👩‍🦳
return

;1F469 1F3FB 200D 1F9B3
:*::woman:-light-skin-tone,-white-hair::
Send,👩🏻‍🦳
return

;1F469 1F3FC 200D 1F9B3
:*::woman:-medium-light-skin-tone,-white-ha::
Send,👩🏼‍🦳
return

;1F469 1F3FD 200D 1F9B3
:*::woman:-medium-skin-tone,-white-hair::
Send,👩🏽‍🦳
return

;1F469 1F3FE 200D 1F9B3
:*::woman:-medium-dark-skin-tone,-white-hai::
Send,👩🏾‍🦳
return

;1F469 1F3FF 200D 1F9B3
:*::woman:-dark-skin-tone,-white-hair::
Send,👩🏿‍🦳
return

;1F469 200D 1F9B2
:*::woman:-bald::
Send,👩‍🦲
return

;1F469 1F3FB 200D 1F9B2
:*::woman:-light-skin-tone,-bald::
Send,👩🏻‍🦲
return

;1F469 1F3FC 200D 1F9B2
:*::woman:-medium-light-skin-tone,-bald::
Send,👩🏼‍🦲
return

;1F469 1F3FD 200D 1F9B2
:*::woman:-medium-skin-tone,-bald::
Send,👩🏽‍🦲
return

;1F469 1F3FE 200D 1F9B2
:*::woman:-medium-dark-skin-tone,-bald::
Send,👩🏾‍🦲
return

;1F469 1F3FF 200D 1F9B2
:*::woman:-dark-skin-tone,-bald::
Send,👩🏿‍🦲
return

;1F9D3
:*::older-person::
Send,🧓
return

;1F9D3 1F3FB
:*::older-person:-light-skin-tone::
Send,🧓🏻
return

;1F9D3 1F3FC
:*::older-person:-medium-light-skin-tone::
Send,🧓🏼
return

;1F9D3 1F3FD
:*::older-person:-medium-skin-tone::
Send,🧓🏽
return

;1F9D3 1F3FE
:*::older-person:-medium-dark-skin-tone::
Send,🧓🏾
return

;1F9D3 1F3FF
:*::older-person:-dark-skin-tone::
Send,🧓🏿
return

;1F474
:*::old-man::
Send,👴
return

;1F474 1F3FB
:*::old-man:-light-skin-tone::
Send,👴🏻
return

;1F474 1F3FC
:*::old-man:-medium-light-skin-tone::
Send,👴🏼
return

;1F474 1F3FD
:*::old-man:-medium-skin-tone::
Send,👴🏽
return

;1F474 1F3FE
:*::old-man:-medium-dark-skin-tone::
Send,👴🏾
return

;1F474 1F3FF
:*::old-man:-dark-skin-tone::
Send,👴🏿
return

;1F475
:*::old-woman::
Send,👵
return

;1F475 1F3FB
:*::old-woman:-light-skin-tone::
Send,👵🏻
return

;1F475 1F3FC
:*::old-woman:-medium-light-skin-tone::
Send,👵🏼
return

;1F475 1F3FD
:*::old-woman:-medium-skin-tone::
Send,👵🏽
return

;1F475 1F3FE
:*::old-woman:-medium-dark-skin-tone::
Send,👵🏾
return

;1F475 1F3FF
:*::old-woman:-dark-skin-tone::
Send,👵🏿
return

;1F64D
:*::person-frowning::
Send,🙍
return

;1F64D 1F3FB
:*::person-frowning:-light-skin-tone::
Send,🙍🏻
return

;1F64D 1F3FC
:*::person-frowning:-medium-light-skin-tone::
Send,🙍🏼
return

;1F64D 1F3FD
:*::person-frowning:-medium-skin-tone::
Send,🙍🏽
return

;1F64D 1F3FE
:*::person-frowning:-medium-dark-skin-tone::
Send,🙍🏾
return

;1F64D 1F3FF
:*::person-frowning:-dark-skin-tone::
Send,🙍🏿
return

;1F64D 200D 2642 FE0F
:*::man-frowning::
Send,🙍‍♂️
return

;1F64D 200D 2642
:*::man-frowning::
Send,🙍‍♂
return

;1F64D 1F3FB 200D 2642 FE0F
:*::man-frowning:-light-skin-tone::
Send,🙍🏻‍♂️
return

;1F64D 1F3FB 200D 2642
:*::man-frowning:-light-skin-tone::
Send,🙍🏻‍♂
return

;1F64D 1F3FC 200D 2642 FE0F
:*::man-frowning:-medium-light-skin-tone::
Send,🙍🏼‍♂️
return

;1F64D 1F3FC 200D 2642
:*::man-frowning:-medium-light-skin-tone::
Send,🙍🏼‍♂
return

;1F64D 1F3FD 200D 2642 FE0F
:*::man-frowning:-medium-skin-tone::
Send,🙍🏽‍♂️
return

;1F64D 1F3FD 200D 2642
:*::man-frowning:-medium-skin-tone::
Send,🙍🏽‍♂
return

;1F64D 1F3FE 200D 2642 FE0F
:*::man-frowning:-medium-dark-skin-tone::
Send,🙍🏾‍♂️
return

;1F64D 1F3FE 200D 2642
:*::man-frowning:-medium-dark-skin-tone::
Send,🙍🏾‍♂
return

;1F64D 1F3FF 200D 2642 FE0F
:*::man-frowning:-dark-skin-tone::
Send,🙍🏿‍♂️
return

;1F64D 1F3FF 200D 2642
:*::man-frowning:-dark-skin-tone::
Send,🙍🏿‍♂
return

;1F64D 200D 2640 FE0F
:*::woman-frowning::
Send,🙍‍♀️
return

;1F64D 200D 2640
:*::woman-frowning::
Send,🙍‍♀
return

;1F64D 1F3FB 200D 2640 FE0F
:*::woman-frowning:-light-skin-tone::
Send,🙍🏻‍♀️
return

;1F64D 1F3FB 200D 2640
:*::woman-frowning:-light-skin-tone::
Send,🙍🏻‍♀
return

;1F64D 1F3FC 200D 2640 FE0F
:*::woman-frowning:-medium-light-skin-tone::
Send,🙍🏼‍♀️
return

;1F64D 1F3FC 200D 2640
:*::woman-frowning:-medium-light-skin-tone::
Send,🙍🏼‍♀
return

;1F64D 1F3FD 200D 2640 FE0F
:*::woman-frowning:-medium-skin-tone::
Send,🙍🏽‍♀️
return

;1F64D 1F3FD 200D 2640
:*::woman-frowning:-medium-skin-tone::
Send,🙍🏽‍♀
return

;1F64D 1F3FE 200D 2640 FE0F
:*::woman-frowning:-medium-dark-skin-tone::
Send,🙍🏾‍♀️
return

;1F64D 1F3FE 200D 2640
:*::woman-frowning:-medium-dark-skin-tone::
Send,🙍🏾‍♀
return

;1F64D 1F3FF 200D 2640 FE0F
:*::woman-frowning:-dark-skin-tone::
Send,🙍🏿‍♀️
return

;1F64D 1F3FF 200D 2640
:*::woman-frowning:-dark-skin-tone::
Send,🙍🏿‍♀
return

;1F64E
:*::person-pouting::
Send,🙎
return

;1F64E 1F3FB
:*::person-pouting:-light-skin-tone::
Send,🙎🏻
return

;1F64E 1F3FC
:*::person-pouting:-medium-light-skin-tone::
Send,🙎🏼
return

;1F64E 1F3FD
:*::person-pouting:-medium-skin-tone::
Send,🙎🏽
return

;1F64E 1F3FE
:*::person-pouting:-medium-dark-skin-tone::
Send,🙎🏾
return

;1F64E 1F3FF
:*::person-pouting:-dark-skin-tone::
Send,🙎🏿
return

;1F64E 200D 2642 FE0F
:*::man-pouting::
Send,🙎‍♂️
return

;1F64E 200D 2642
:*::man-pouting::
Send,🙎‍♂
return

;1F64E 1F3FB 200D 2642 FE0F
:*::man-pouting:-light-skin-tone::
Send,🙎🏻‍♂️
return

;1F64E 1F3FB 200D 2642
:*::man-pouting:-light-skin-tone::
Send,🙎🏻‍♂
return

;1F64E 1F3FC 200D 2642 FE0F
:*::man-pouting:-medium-light-skin-tone::
Send,🙎🏼‍♂️
return

;1F64E 1F3FC 200D 2642
:*::man-pouting:-medium-light-skin-tone::
Send,🙎🏼‍♂
return

;1F64E 1F3FD 200D 2642 FE0F
:*::man-pouting:-medium-skin-tone::
Send,🙎🏽‍♂️
return

;1F64E 1F3FD 200D 2642
:*::man-pouting:-medium-skin-tone::
Send,🙎🏽‍♂
return

;1F64E 1F3FE 200D 2642 FE0F
:*::man-pouting:-medium-dark-skin-tone::
Send,🙎🏾‍♂️
return

;1F64E 1F3FE 200D 2642
:*::man-pouting:-medium-dark-skin-tone::
Send,🙎🏾‍♂
return

;1F64E 1F3FF 200D 2642 FE0F
:*::man-pouting:-dark-skin-tone::
Send,🙎🏿‍♂️
return

;1F64E 1F3FF 200D 2642
:*::man-pouting:-dark-skin-tone::
Send,🙎🏿‍♂
return

;1F64E 200D 2640 FE0F
:*::woman-pouting::
Send,🙎‍♀️
return

;1F64E 200D 2640
:*::woman-pouting::
Send,🙎‍♀
return

;1F64E 1F3FB 200D 2640 FE0F
:*::woman-pouting:-light-skin-tone::
Send,🙎🏻‍♀️
return

;1F64E 1F3FB 200D 2640
:*::woman-pouting:-light-skin-tone::
Send,🙎🏻‍♀
return

;1F64E 1F3FC 200D 2640 FE0F
:*::woman-pouting:-medium-light-skin-tone::
Send,🙎🏼‍♀️
return

;1F64E 1F3FC 200D 2640
:*::woman-pouting:-medium-light-skin-tone::
Send,🙎🏼‍♀
return

;1F64E 1F3FD 200D 2640 FE0F
:*::woman-pouting:-medium-skin-tone::
Send,🙎🏽‍♀️
return

;1F64E 1F3FD 200D 2640
:*::woman-pouting:-medium-skin-tone::
Send,🙎🏽‍♀
return

;1F64E 1F3FE 200D 2640 FE0F
:*::woman-pouting:-medium-dark-skin-tone::
Send,🙎🏾‍♀️
return

;1F64E 1F3FE 200D 2640
:*::woman-pouting:-medium-dark-skin-tone::
Send,🙎🏾‍♀
return

;1F64E 1F3FF 200D 2640 FE0F
:*::woman-pouting:-dark-skin-tone::
Send,🙎🏿‍♀️
return

;1F64E 1F3FF 200D 2640
:*::woman-pouting:-dark-skin-tone::
Send,🙎🏿‍♀
return

;1F645
:*::person-gesturing-NO::
Send,🙅
return

;1F645 1F3FB
:*::person-gesturing-NO:-light-skin-tone::
Send,🙅🏻
return

;1F645 1F3FC
:*::person-gesturing-NO:-medium-light-skin::
Send,🙅🏼
return

;1F645 1F3FD
:*::person-gesturing-NO:-medium-skin-tone::
Send,🙅🏽
return

;1F645 1F3FE
:*::person-gesturing-NO:-medium-dark-skin-t::
Send,🙅🏾
return

;1F645 1F3FF
:*::person-gesturing-NO:-dark-skin-tone::
Send,🙅🏿
return

;1F645 200D 2642 FE0F
:*::man-gesturing-NO::
Send,🙅‍♂️
return

;1F645 200D 2642
:*::man-gesturing-NO::
Send,🙅‍♂
return

;1F645 1F3FB 200D 2642 FE0F
:*::man-gesturing-NO:-light-skin-tone::
Send,🙅🏻‍♂️
return

;1F645 1F3FB 200D 2642
:*::man-gesturing-NO:-light-skin-tone::
Send,🙅🏻‍♂
return

;1F645 1F3FC 200D 2642 FE0F
:*::man-gesturing-NO:-medium-light-skin-ton::
Send,🙅🏼‍♂️
return

;1F645 1F3FC 200D 2642
:*::man-gesturing-NO:-medium-light-skin-ton::
Send,🙅🏼‍♂
return

;1F645 1F3FD 200D 2642 FE0F
:*::man-gesturing-NO:-medium-skin-tone::
Send,🙅🏽‍♂️
return

;1F645 1F3FD 200D 2642
:*::man-gesturing-NO:-medium-skin-tone::
Send,🙅🏽‍♂
return

;1F645 1F3FE 200D 2642 FE0F
:*::man-gesturing-NO:-medium-dark-skin-tone::
Send,🙅🏾‍♂️
return

;1F645 1F3FE 200D 2642
:*::man-gesturing-NO:-medium-dark-skin-tone::
Send,🙅🏾‍♂
return

;1F645 1F3FF 200D 2642 FE0F
:*::man-gesturing-NO:-dark-skin-tone::
Send,🙅🏿‍♂️
return

;1F645 1F3FF 200D 2642
:*::man-gesturing-NO:-dark-skin-tone::
Send,🙅🏿‍♂
return

;1F645 200D 2640 FE0F
:*::woman-gesturing-NO::
Send,🙅‍♀️
return

;1F645 200D 2640
:*::woman-gesturing-NO::
Send,🙅‍♀
return

;1F645 1F3FB 200D 2640 FE0F
:*::woman-gesturing-NO:-light-skin-tone::
Send,🙅🏻‍♀️
return

;1F645 1F3FB 200D 2640
:*::woman-gesturing-NO:-light-skin-tone::
Send,🙅🏻‍♀
return

;1F645 1F3FC 200D 2640 FE0F
:*::woman-gesturing-NO:-medium-light-skin-t::
Send,🙅🏼‍♀️
return

;1F645 1F3FC 200D 2640
:*::woman-gesturing-NO:-medium-light-skin-t::
Send,🙅🏼‍♀
return

;1F645 1F3FD 200D 2640 FE0F
:*::woman-gesturing-NO:-medium-skin-tone::
Send,🙅🏽‍♀️
return

;1F645 1F3FD 200D 2640
:*::woman-gesturing-NO:-medium-skin-tone::
Send,🙅🏽‍♀
return

;1F645 1F3FE 200D 2640 FE0F
:*::woman-gesturing-NO:-medium-dark-skin-to::
Send,🙅🏾‍♀️
return

;1F645 1F3FE 200D 2640
:*::woman-gesturing-NO:-medium-dark-skin-to::
Send,🙅🏾‍♀
return

;1F645 1F3FF 200D 2640 FE0F
:*::woman-gesturing-NO:-dark-skin-tone::
Send,🙅🏿‍♀️
return

;1F645 1F3FF 200D 2640
:*::woman-gesturing-NO:-dark-skin-tone::
Send,🙅🏿‍♀
return

;1F646
:*::person-gesturing-OK::
Send,🙆
return

;1F646 1F3FB
:*::person-gesturing-OK:-light-skin-tone::
Send,🙆🏻
return

;1F646 1F3FC
:*::person-gesturing-OK:-medium-light-skin::
Send,🙆🏼
return

;1F646 1F3FD
:*::person-gesturing-OK:-medium-skin-tone::
Send,🙆🏽
return

;1F646 1F3FE
:*::person-gesturing-OK:-medium-dark-skin-t::
Send,🙆🏾
return

;1F646 1F3FF
:*::person-gesturing-OK:-dark-skin-tone::
Send,🙆🏿
return

;1F646 200D 2642 FE0F
:*::man-gesturing-OK::
Send,🙆‍♂️
return

;1F646 200D 2642
:*::man-gesturing-OK::
Send,🙆‍♂
return

;1F646 1F3FB 200D 2642 FE0F
:*::man-gesturing-OK:-light-skin-tone::
Send,🙆🏻‍♂️
return

;1F646 1F3FB 200D 2642
:*::man-gesturing-OK:-light-skin-tone::
Send,🙆🏻‍♂
return

;1F646 1F3FC 200D 2642 FE0F
:*::man-gesturing-OK:-medium-light-skin-ton::
Send,🙆🏼‍♂️
return

;1F646 1F3FC 200D 2642
:*::man-gesturing-OK:-medium-light-skin-ton::
Send,🙆🏼‍♂
return

;1F646 1F3FD 200D 2642 FE0F
:*::man-gesturing-OK:-medium-skin-tone::
Send,🙆🏽‍♂️
return

;1F646 1F3FD 200D 2642
:*::man-gesturing-OK:-medium-skin-tone::
Send,🙆🏽‍♂
return

;1F646 1F3FE 200D 2642 FE0F
:*::man-gesturing-OK:-medium-dark-skin-tone::
Send,🙆🏾‍♂️
return

;1F646 1F3FE 200D 2642
:*::man-gesturing-OK:-medium-dark-skin-tone::
Send,🙆🏾‍♂
return

;1F646 1F3FF 200D 2642 FE0F
:*::man-gesturing-OK:-dark-skin-tone::
Send,🙆🏿‍♂️
return

;1F646 1F3FF 200D 2642
:*::man-gesturing-OK:-dark-skin-tone::
Send,🙆🏿‍♂
return

;1F646 200D 2640 FE0F
:*::woman-gesturing-OK::
Send,🙆‍♀️
return

;1F646 200D 2640
:*::woman-gesturing-OK::
Send,🙆‍♀
return

;1F646 1F3FB 200D 2640 FE0F
:*::woman-gesturing-OK:-light-skin-tone::
Send,🙆🏻‍♀️
return

;1F646 1F3FB 200D 2640
:*::woman-gesturing-OK:-light-skin-tone::
Send,🙆🏻‍♀
return

;1F646 1F3FC 200D 2640 FE0F
:*::woman-gesturing-OK:-medium-light-skin-t::
Send,🙆🏼‍♀️
return

;1F646 1F3FC 200D 2640
:*::woman-gesturing-OK:-medium-light-skin-t::
Send,🙆🏼‍♀
return

;1F646 1F3FD 200D 2640 FE0F
:*::woman-gesturing-OK:-medium-skin-tone::
Send,🙆🏽‍♀️
return

;1F646 1F3FD 200D 2640
:*::woman-gesturing-OK:-medium-skin-tone::
Send,🙆🏽‍♀
return

;1F646 1F3FE 200D 2640 FE0F
:*::woman-gesturing-OK:-medium-dark-skin-to::
Send,🙆🏾‍♀️
return

;1F646 1F3FE 200D 2640
:*::woman-gesturing-OK:-medium-dark-skin-to::
Send,🙆🏾‍♀
return

;1F646 1F3FF 200D 2640 FE0F
:*::woman-gesturing-OK:-dark-skin-tone::
Send,🙆🏿‍♀️
return

;1F646 1F3FF 200D 2640
:*::woman-gesturing-OK:-dark-skin-tone::
Send,🙆🏿‍♀
return

;1F481
:*::person-tipping-hand::
Send,💁
return

;1F481 1F3FB
:*::person-tipping-hand:-light-skin-tone::
Send,💁🏻
return

;1F481 1F3FC
:*::person-tipping-hand:-medium-light-skin::
Send,💁🏼
return

;1F481 1F3FD
:*::person-tipping-hand:-medium-skin-tone::
Send,💁🏽
return

;1F481 1F3FE
:*::person-tipping-hand:-medium-dark-skin-t::
Send,💁🏾
return

;1F481 1F3FF
:*::person-tipping-hand:-dark-skin-tone::
Send,💁🏿
return

;1F481 200D 2642 FE0F
:*::man-tipping-hand::
Send,💁‍♂️
return

;1F481 200D 2642
:*::man-tipping-hand::
Send,💁‍♂
return

;1F481 1F3FB 200D 2642 FE0F
:*::man-tipping-hand:-light-skin-tone::
Send,💁🏻‍♂️
return

;1F481 1F3FB 200D 2642
:*::man-tipping-hand:-light-skin-tone::
Send,💁🏻‍♂
return

;1F481 1F3FC 200D 2642 FE0F
:*::man-tipping-hand:-medium-light-skin-ton::
Send,💁🏼‍♂️
return

;1F481 1F3FC 200D 2642
:*::man-tipping-hand:-medium-light-skin-ton::
Send,💁🏼‍♂
return

;1F481 1F3FD 200D 2642 FE0F
:*::man-tipping-hand:-medium-skin-tone::
Send,💁🏽‍♂️
return

;1F481 1F3FD 200D 2642
:*::man-tipping-hand:-medium-skin-tone::
Send,💁🏽‍♂
return

;1F481 1F3FE 200D 2642 FE0F
:*::man-tipping-hand:-medium-dark-skin-tone::
Send,💁🏾‍♂️
return

;1F481 1F3FE 200D 2642
:*::man-tipping-hand:-medium-dark-skin-tone::
Send,💁🏾‍♂
return

;1F481 1F3FF 200D 2642 FE0F
:*::man-tipping-hand:-dark-skin-tone::
Send,💁🏿‍♂️
return

;1F481 1F3FF 200D 2642
:*::man-tipping-hand:-dark-skin-tone::
Send,💁🏿‍♂
return

;1F481 200D 2640 FE0F
:*::woman-tipping-hand::
Send,💁‍♀️
return

;1F481 200D 2640
:*::woman-tipping-hand::
Send,💁‍♀
return

;1F481 1F3FB 200D 2640 FE0F
:*::woman-tipping-hand:-light-skin-tone::
Send,💁🏻‍♀️
return

;1F481 1F3FB 200D 2640
:*::woman-tipping-hand:-light-skin-tone::
Send,💁🏻‍♀
return

;1F481 1F3FC 200D 2640 FE0F
:*::woman-tipping-hand:-medium-light-skin-t::
Send,💁🏼‍♀️
return

;1F481 1F3FC 200D 2640
:*::woman-tipping-hand:-medium-light-skin-t::
Send,💁🏼‍♀
return

;1F481 1F3FD 200D 2640 FE0F
:*::woman-tipping-hand:-medium-skin-tone::
Send,💁🏽‍♀️
return

;1F481 1F3FD 200D 2640
:*::woman-tipping-hand:-medium-skin-tone::
Send,💁🏽‍♀
return

;1F481 1F3FE 200D 2640 FE0F
:*::woman-tipping-hand:-medium-dark-skin-to::
Send,💁🏾‍♀️
return

;1F481 1F3FE 200D 2640
:*::woman-tipping-hand:-medium-dark-skin-to::
Send,💁🏾‍♀
return

;1F481 1F3FF 200D 2640 FE0F
:*::woman-tipping-hand:-dark-skin-tone::
Send,💁🏿‍♀️
return

;1F481 1F3FF 200D 2640
:*::woman-tipping-hand:-dark-skin-tone::
Send,💁🏿‍♀
return

;1F64B
:*::person-raising-hand::
Send,🙋
return

;1F64B 1F3FB
:*::person-raising-hand:-light-skin-tone::
Send,🙋🏻
return

;1F64B 1F3FC
:*::person-raising-hand:-medium-light-skin::
Send,🙋🏼
return

;1F64B 1F3FD
:*::person-raising-hand:-medium-skin-tone::
Send,🙋🏽
return

;1F64B 1F3FE
:*::person-raising-hand:-medium-dark-skin-t::
Send,🙋🏾
return

;1F64B 1F3FF
:*::person-raising-hand:-dark-skin-tone::
Send,🙋🏿
return

;1F64B 200D 2642 FE0F
:*::man-raising-hand::
Send,🙋‍♂️
return

;1F64B 200D 2642
:*::man-raising-hand::
Send,🙋‍♂
return

;1F64B 1F3FB 200D 2642 FE0F
:*::man-raising-hand:-light-skin-tone::
Send,🙋🏻‍♂️
return

;1F64B 1F3FB 200D 2642
:*::man-raising-hand:-light-skin-tone::
Send,🙋🏻‍♂
return

;1F64B 1F3FC 200D 2642 FE0F
:*::man-raising-hand:-medium-light-skin-ton::
Send,🙋🏼‍♂️
return

;1F64B 1F3FC 200D 2642
:*::man-raising-hand:-medium-light-skin-ton::
Send,🙋🏼‍♂
return

;1F64B 1F3FD 200D 2642 FE0F
:*::man-raising-hand:-medium-skin-tone::
Send,🙋🏽‍♂️
return

;1F64B 1F3FD 200D 2642
:*::man-raising-hand:-medium-skin-tone::
Send,🙋🏽‍♂
return

;1F64B 1F3FE 200D 2642 FE0F
:*::man-raising-hand:-medium-dark-skin-tone::
Send,🙋🏾‍♂️
return

;1F64B 1F3FE 200D 2642
:*::man-raising-hand:-medium-dark-skin-tone::
Send,🙋🏾‍♂
return

;1F64B 1F3FF 200D 2642 FE0F
:*::man-raising-hand:-dark-skin-tone::
Send,🙋🏿‍♂️
return

;1F64B 1F3FF 200D 2642
:*::man-raising-hand:-dark-skin-tone::
Send,🙋🏿‍♂
return

;1F64B 200D 2640 FE0F
:*::woman-raising-hand::
Send,🙋‍♀️
return

;1F64B 200D 2640
:*::woman-raising-hand::
Send,🙋‍♀
return

;1F64B 1F3FB 200D 2640 FE0F
:*::woman-raising-hand:-light-skin-tone::
Send,🙋🏻‍♀️
return

;1F64B 1F3FB 200D 2640
:*::woman-raising-hand:-light-skin-tone::
Send,🙋🏻‍♀
return

;1F64B 1F3FC 200D 2640 FE0F
:*::woman-raising-hand:-medium-light-skin-t::
Send,🙋🏼‍♀️
return

;1F64B 1F3FC 200D 2640
:*::woman-raising-hand:-medium-light-skin-t::
Send,🙋🏼‍♀
return

;1F64B 1F3FD 200D 2640 FE0F
:*::woman-raising-hand:-medium-skin-tone::
Send,🙋🏽‍♀️
return

;1F64B 1F3FD 200D 2640
:*::woman-raising-hand:-medium-skin-tone::
Send,🙋🏽‍♀
return

;1F64B 1F3FE 200D 2640 FE0F
:*::woman-raising-hand:-medium-dark-skin-to::
Send,🙋🏾‍♀️
return

;1F64B 1F3FE 200D 2640
:*::woman-raising-hand:-medium-dark-skin-to::
Send,🙋🏾‍♀
return

;1F64B 1F3FF 200D 2640 FE0F
:*::woman-raising-hand:-dark-skin-tone::
Send,🙋🏿‍♀️
return

;1F64B 1F3FF 200D 2640
:*::woman-raising-hand:-dark-skin-tone::
Send,🙋🏿‍♀
return

;1F9CF
:*::deaf-person::
Send,🧏
return

;1F9CF 1F3FB
:*::deaf-person:-light-skin-tone::
Send,🧏🏻
return

;1F9CF 1F3FC
:*::deaf-person:-medium-light-skin-tone::
Send,🧏🏼
return

;1F9CF 1F3FD
:*::deaf-person:-medium-skin-tone::
Send,🧏🏽
return

;1F9CF 1F3FE
:*::deaf-person:-medium-dark-skin-tone::
Send,🧏🏾
return

;1F9CF 1F3FF
:*::deaf-person:-dark-skin-tone::
Send,🧏🏿
return

;1F9CF 200D 2642 FE0F
:*::deaf-man::
Send,🧏‍♂️
return

;1F9CF 200D 2642
:*::deaf-man::
Send,🧏‍♂
return

;1F9CF 1F3FB 200D 2642 FE0F
:*::deaf-man:-light-skin-tone::
Send,🧏🏻‍♂️
return

;1F9CF 1F3FB 200D 2642
:*::deaf-man:-light-skin-tone::
Send,🧏🏻‍♂
return

;1F9CF 1F3FC 200D 2642 FE0F
:*::deaf-man:-medium-light-skin-tone::
Send,🧏🏼‍♂️
return

;1F9CF 1F3FC 200D 2642
:*::deaf-man:-medium-light-skin-tone::
Send,🧏🏼‍♂
return

;1F9CF 1F3FD 200D 2642 FE0F
:*::deaf-man:-medium-skin-tone::
Send,🧏🏽‍♂️
return

;1F9CF 1F3FD 200D 2642
:*::deaf-man:-medium-skin-tone::
Send,🧏🏽‍♂
return

;1F9CF 1F3FE 200D 2642 FE0F
:*::deaf-man:-medium-dark-skin-tone::
Send,🧏🏾‍♂️
return

;1F9CF 1F3FE 200D 2642
:*::deaf-man:-medium-dark-skin-tone::
Send,🧏🏾‍♂
return

;1F9CF 1F3FF 200D 2642 FE0F
:*::deaf-man:-dark-skin-tone::
Send,🧏🏿‍♂️
return

;1F9CF 1F3FF 200D 2642
:*::deaf-man:-dark-skin-tone::
Send,🧏🏿‍♂
return

;1F9CF 200D 2640 FE0F
:*::deaf-woman::
Send,🧏‍♀️
return

;1F9CF 200D 2640
:*::deaf-woman::
Send,🧏‍♀
return

;1F9CF 1F3FB 200D 2640 FE0F
:*::deaf-woman:-light-skin-tone::
Send,🧏🏻‍♀️
return

;1F9CF 1F3FB 200D 2640
:*::deaf-woman:-light-skin-tone::
Send,🧏🏻‍♀
return

;1F9CF 1F3FC 200D 2640 FE0F
:*::deaf-woman:-medium-light-skin-tone::
Send,🧏🏼‍♀️
return

;1F9CF 1F3FC 200D 2640
:*::deaf-woman:-medium-light-skin-tone::
Send,🧏🏼‍♀
return

;1F9CF 1F3FD 200D 2640 FE0F
:*::deaf-woman:-medium-skin-tone::
Send,🧏🏽‍♀️
return

;1F9CF 1F3FD 200D 2640
:*::deaf-woman:-medium-skin-tone::
Send,🧏🏽‍♀
return

;1F9CF 1F3FE 200D 2640 FE0F
:*::deaf-woman:-medium-dark-skin-tone::
Send,🧏🏾‍♀️
return

;1F9CF 1F3FE 200D 2640
:*::deaf-woman:-medium-dark-skin-tone::
Send,🧏🏾‍♀
return

;1F9CF 1F3FF 200D 2640 FE0F
:*::deaf-woman:-dark-skin-tone::
Send,🧏🏿‍♀️
return

;1F9CF 1F3FF 200D 2640
:*::deaf-woman:-dark-skin-tone::
Send,🧏🏿‍♀
return

;1F647
:*::person-bowing::
Send,🙇
return

;1F647 1F3FB
:*::person-bowing:-light-skin-tone::
Send,🙇🏻
return

;1F647 1F3FC
:*::person-bowing:-medium-light-skin-tone::
Send,🙇🏼
return

;1F647 1F3FD
:*::person-bowing:-medium-skin-tone::
Send,🙇🏽
return

;1F647 1F3FE
:*::person-bowing:-medium-dark-skin-tone::
Send,🙇🏾
return

;1F647 1F3FF
:*::person-bowing:-dark-skin-tone::
Send,🙇🏿
return

;1F647 200D 2642 FE0F
:*::man-bowing::
Send,🙇‍♂️
return

;1F647 200D 2642
:*::man-bowing::
Send,🙇‍♂
return

;1F647 1F3FB 200D 2642 FE0F
:*::man-bowing:-light-skin-tone::
Send,🙇🏻‍♂️
return

;1F647 1F3FB 200D 2642
:*::man-bowing:-light-skin-tone::
Send,🙇🏻‍♂
return

;1F647 1F3FC 200D 2642 FE0F
:*::man-bowing:-medium-light-skin-tone::
Send,🙇🏼‍♂️
return

;1F647 1F3FC 200D 2642
:*::man-bowing:-medium-light-skin-tone::
Send,🙇🏼‍♂
return

;1F647 1F3FD 200D 2642 FE0F
:*::man-bowing:-medium-skin-tone::
Send,🙇🏽‍♂️
return

;1F647 1F3FD 200D 2642
:*::man-bowing:-medium-skin-tone::
Send,🙇🏽‍♂
return

;1F647 1F3FE 200D 2642 FE0F
:*::man-bowing:-medium-dark-skin-tone::
Send,🙇🏾‍♂️
return

;1F647 1F3FE 200D 2642
:*::man-bowing:-medium-dark-skin-tone::
Send,🙇🏾‍♂
return

;1F647 1F3FF 200D 2642 FE0F
:*::man-bowing:-dark-skin-tone::
Send,🙇🏿‍♂️
return

;1F647 1F3FF 200D 2642
:*::man-bowing:-dark-skin-tone::
Send,🙇🏿‍♂
return

;1F647 200D 2640 FE0F
:*::woman-bowing::
Send,🙇‍♀️
return

;1F647 200D 2640
:*::woman-bowing::
Send,🙇‍♀
return

;1F647 1F3FB 200D 2640 FE0F
:*::woman-bowing:-light-skin-tone::
Send,🙇🏻‍♀️
return

;1F647 1F3FB 200D 2640
:*::woman-bowing:-light-skin-tone::
Send,🙇🏻‍♀
return

;1F647 1F3FC 200D 2640 FE0F
:*::woman-bowing:-medium-light-skin-tone::
Send,🙇🏼‍♀️
return

;1F647 1F3FC 200D 2640
:*::woman-bowing:-medium-light-skin-tone::
Send,🙇🏼‍♀
return

;1F647 1F3FD 200D 2640 FE0F
:*::woman-bowing:-medium-skin-tone::
Send,🙇🏽‍♀️
return

;1F647 1F3FD 200D 2640
:*::woman-bowing:-medium-skin-tone::
Send,🙇🏽‍♀
return

;1F647 1F3FE 200D 2640 FE0F
:*::woman-bowing:-medium-dark-skin-tone::
Send,🙇🏾‍♀️
return

;1F647 1F3FE 200D 2640
:*::woman-bowing:-medium-dark-skin-tone::
Send,🙇🏾‍♀
return

;1F647 1F3FF 200D 2640 FE0F
:*::woman-bowing:-dark-skin-tone::
Send,🙇🏿‍♀️
return

;1F647 1F3FF 200D 2640
:*::woman-bowing:-dark-skin-tone::
Send,🙇🏿‍♀
return

;1F926
:*::person-facepalming::
Send,🤦
return

;1F926 1F3FB
:*::person-facepalming:-light-skin-tone::
Send,🤦🏻
return

;1F926 1F3FC
:*::person-facepalming:-medium-light-skin-t::
Send,🤦🏼
return

;1F926 1F3FD
:*::person-facepalming:-medium-skin-tone::
Send,🤦🏽
return

;1F926 1F3FE
:*::person-facepalming:-medium-dark-skin-to::
Send,🤦🏾
return

;1F926 1F3FF
:*::person-facepalming:-dark-skin-tone::
Send,🤦🏿
return

;1F926 200D 2642 FE0F
:*::man-facepalming::
Send,🤦‍♂️
return

;1F926 200D 2642
:*::man-facepalming::
Send,🤦‍♂
return

;1F926 1F3FB 200D 2642 FE0F
:*::man-facepalming:-light-skin-tone::
Send,🤦🏻‍♂️
return

;1F926 1F3FB 200D 2642
:*::man-facepalming:-light-skin-tone::
Send,🤦🏻‍♂
return

;1F926 1F3FC 200D 2642 FE0F
:*::man-facepalming:-medium-light-skin-tone::
Send,🤦🏼‍♂️
return

;1F926 1F3FC 200D 2642
:*::man-facepalming:-medium-light-skin-tone::
Send,🤦🏼‍♂
return

;1F926 1F3FD 200D 2642 FE0F
:*::man-facepalming:-medium-skin-tone::
Send,🤦🏽‍♂️
return

;1F926 1F3FD 200D 2642
:*::man-facepalming:-medium-skin-tone::
Send,🤦🏽‍♂
return

;1F926 1F3FE 200D 2642 FE0F
:*::man-facepalming:-medium-dark-skin-tone::
Send,🤦🏾‍♂️
return

;1F926 1F3FE 200D 2642
:*::man-facepalming:-medium-dark-skin-tone::
Send,🤦🏾‍♂
return

;1F926 1F3FF 200D 2642 FE0F
:*::man-facepalming:-dark-skin-tone::
Send,🤦🏿‍♂️
return

;1F926 1F3FF 200D 2642
:*::man-facepalming:-dark-skin-tone::
Send,🤦🏿‍♂
return

;1F926 200D 2640 FE0F
:*::woman-facepalming::
Send,🤦‍♀️
return

;1F926 200D 2640
:*::woman-facepalming::
Send,🤦‍♀
return

;1F926 1F3FB 200D 2640 FE0F
:*::woman-facepalming:-light-skin-tone::
Send,🤦🏻‍♀️
return

;1F926 1F3FB 200D 2640
:*::woman-facepalming:-light-skin-tone::
Send,🤦🏻‍♀
return

;1F926 1F3FC 200D 2640 FE0F
:*::woman-facepalming:-medium-light-skin-to::
Send,🤦🏼‍♀️
return

;1F926 1F3FC 200D 2640
:*::woman-facepalming:-medium-light-skin-to::
Send,🤦🏼‍♀
return

;1F926 1F3FD 200D 2640 FE0F
:*::woman-facepalming:-medium-skin-tone::
Send,🤦🏽‍♀️
return

;1F926 1F3FD 200D 2640
:*::woman-facepalming:-medium-skin-tone::
Send,🤦🏽‍♀
return

;1F926 1F3FE 200D 2640 FE0F
:*::woman-facepalming:-medium-dark-skin-ton::
Send,🤦🏾‍♀️
return

;1F926 1F3FE 200D 2640
:*::woman-facepalming:-medium-dark-skin-ton::
Send,🤦🏾‍♀
return

;1F926 1F3FF 200D 2640 FE0F
:*::woman-facepalming:-dark-skin-tone::
Send,🤦🏿‍♀️
return

;1F926 1F3FF 200D 2640
:*::woman-facepalming:-dark-skin-tone::
Send,🤦🏿‍♀
return

;1F937
:*::shrug::
:*::person-shrugging::
Send,🤷
return

;1F937 1F3FB
:*::person-shrugging:-light-skin-tone::
Send,🤷🏻
return

;1F937 1F3FC
:*::person-shrugging:-medium-light-skin-ton::
Send,🤷🏼
return

;1F937 1F3FD
:*::person-shrugging:-medium-skin-tone::
Send,🤷🏽
return

;1F937 1F3FE
:*::person-shrugging:-medium-dark-skin-tone::
Send,🤷🏾
return

;1F937 1F3FF
:*::person-shrugging:-dark-skin-tone::
Send,🤷🏿
return

;1F937 200D 2642 FE0F
:*::man-shrugging::
Send,🤷‍♂️
return

;1F937 200D 2642
:*::man-shrugging::
Send,🤷‍♂
return

;1F937 1F3FB 200D 2642 FE0F
:*::man-shrugging:-light-skin-tone::
Send,🤷🏻‍♂️
return

;1F937 1F3FB 200D 2642
:*::man-shrugging:-light-skin-tone::
Send,🤷🏻‍♂
return

;1F937 1F3FC 200D 2642 FE0F
:*::man-shrugging:-medium-light-skin-tone::
Send,🤷🏼‍♂️
return

;1F937 1F3FC 200D 2642
:*::man-shrugging:-medium-light-skin-tone::
Send,🤷🏼‍♂
return

;1F937 1F3FD 200D 2642 FE0F
:*::man-shrugging:-medium-skin-tone::
Send,🤷🏽‍♂️
return

;1F937 1F3FD 200D 2642
:*::man-shrugging:-medium-skin-tone::
Send,🤷🏽‍♂
return

;1F937 1F3FE 200D 2642 FE0F
:*::man-shrugging:-medium-dark-skin-tone::
Send,🤷🏾‍♂️
return

;1F937 1F3FE 200D 2642
:*::man-shrugging:-medium-dark-skin-tone::
Send,🤷🏾‍♂
return

;1F937 1F3FF 200D 2642 FE0F
:*::man-shrugging:-dark-skin-tone::
Send,🤷🏿‍♂️
return

;1F937 1F3FF 200D 2642
:*::man-shrugging:-dark-skin-tone::
Send,🤷🏿‍♂
return

;1F937 200D 2640 FE0F
:*::woman-shrugging::
Send,🤷‍♀️
return

;1F937 200D 2640
:*::woman-shrugging::
Send,🤷‍♀
return

;1F937 1F3FB 200D 2640 FE0F
:*::woman-shrugging:-light-skin-tone::
Send,🤷🏻‍♀️
return

;1F937 1F3FB 200D 2640
:*::woman-shrugging:-light-skin-tone::
Send,🤷🏻‍♀
return

;1F937 1F3FC 200D 2640 FE0F
:*::woman-shrugging:-medium-light-skin-tone::
Send,🤷🏼‍♀️
return

;1F937 1F3FC 200D 2640
:*::woman-shrugging:-medium-light-skin-tone::
Send,🤷🏼‍♀
return

;1F937 1F3FD 200D 2640 FE0F
:*::woman-shrugging:-medium-skin-tone::
Send,🤷🏽‍♀️
return

;1F937 1F3FD 200D 2640
:*::woman-shrugging:-medium-skin-tone::
Send,🤷🏽‍♀
return

;1F937 1F3FE 200D 2640 FE0F
:*::woman-shrugging:-medium-dark-skin-tone::
Send,🤷🏾‍♀️
return

;1F937 1F3FE 200D 2640
:*::woman-shrugging:-medium-dark-skin-tone::
Send,🤷🏾‍♀
return

;1F937 1F3FF 200D 2640 FE0F
:*::woman-shrugging:-dark-skin-tone::
Send,🤷🏿‍♀️
return

;1F937 1F3FF 200D 2640
:*::woman-shrugging:-dark-skin-tone::
Send,🤷🏿‍♀
return

;1F468 200D 2695 FE0F
:*::man-health-worker::
Send,👨‍⚕️
return

;1F468 200D 2695
:*::man-health-worker::
Send,👨‍⚕
return

;1F468 1F3FB 200D 2695 FE0F
:*::man-health-worker:-light-skin-tone::
Send,👨🏻‍⚕️
return

;1F468 1F3FB 200D 2695
:*::man-health-worker:-light-skin-tone::
Send,👨🏻‍⚕
return

;1F468 1F3FC 200D 2695 FE0F
:*::man-health-worker:-medium-light-skin-to::
Send,👨🏼‍⚕️
return

;1F468 1F3FC 200D 2695
:*::man-health-worker:-medium-light-skin-to::
Send,👨🏼‍⚕
return

;1F468 1F3FD 200D 2695 FE0F
:*::man-health-worker:-medium-skin-tone::
Send,👨🏽‍⚕️
return

;1F468 1F3FD 200D 2695
:*::man-health-worker:-medium-skin-tone::
Send,👨🏽‍⚕
return

;1F468 1F3FE 200D 2695 FE0F
:*::man-health-worker:-medium-dark-skin-ton::
Send,👨🏾‍⚕️
return

;1F468 1F3FE 200D 2695
:*::man-health-worker:-medium-dark-skin-ton::
Send,👨🏾‍⚕
return

;1F468 1F3FF 200D 2695 FE0F
:*::man-health-worker:-dark-skin-tone::
Send,👨🏿‍⚕️
return

;1F468 1F3FF 200D 2695
:*::man-health-worker:-dark-skin-tone::
Send,👨🏿‍⚕
return

;1F469 200D 2695 FE0F
:*::woman-health-worker::
Send,👩‍⚕️
return

;1F469 200D 2695
:*::woman-health-worker::
Send,👩‍⚕
return

;1F469 1F3FB 200D 2695 FE0F
:*::woman-health-worker:-light-skin-tone::
Send,👩🏻‍⚕️
return

;1F469 1F3FB 200D 2695
:*::woman-health-worker:-light-skin-tone::
Send,👩🏻‍⚕
return

;1F469 1F3FC 200D 2695 FE0F
:*::woman-health-worker:-medium-light-skin::
Send,👩🏼‍⚕️
return

;1F469 1F3FC 200D 2695
:*::woman-health-worker:-medium-light-skin::
Send,👩🏼‍⚕
return

;1F469 1F3FD 200D 2695 FE0F
:*::woman-health-worker:-medium-skin-tone::
Send,👩🏽‍⚕️
return

;1F469 1F3FD 200D 2695
:*::woman-health-worker:-medium-skin-tone::
Send,👩🏽‍⚕
return

;1F469 1F3FE 200D 2695 FE0F
:*::woman-health-worker:-medium-dark-skin-t::
Send,👩🏾‍⚕️
return

;1F469 1F3FE 200D 2695
:*::woman-health-worker:-medium-dark-skin-t::
Send,👩🏾‍⚕
return

;1F469 1F3FF 200D 2695 FE0F
:*::woman-health-worker:-dark-skin-tone::
Send,👩🏿‍⚕️
return

;1F469 1F3FF 200D 2695
:*::woman-health-worker:-dark-skin-tone::
Send,👩🏿‍⚕
return

;1F468 200D 1F393
:*::man-student::
Send,👨‍🎓
return

;1F468 1F3FB 200D 1F393
:*::man-student:-light-skin-tone::
Send,👨🏻‍🎓
return

;1F468 1F3FC 200D 1F393
:*::man-student:-medium-light-skin-tone::
Send,👨🏼‍🎓
return

;1F468 1F3FD 200D 1F393
:*::man-student:-medium-skin-tone::
Send,👨🏽‍🎓
return

;1F468 1F3FE 200D 1F393
:*::man-student:-medium-dark-skin-tone::
Send,👨🏾‍🎓
return

;1F468 1F3FF 200D 1F393
:*::man-student:-dark-skin-tone::
Send,👨🏿‍🎓
return

;1F469 200D 1F393
:*::woman-student::
Send,👩‍🎓
return

;1F469 1F3FB 200D 1F393
:*::woman-student:-light-skin-tone::
Send,👩🏻‍🎓
return

;1F469 1F3FC 200D 1F393
:*::woman-student:-medium-light-skin-tone::
Send,👩🏼‍🎓
return

;1F469 1F3FD 200D 1F393
:*::woman-student:-medium-skin-tone::
Send,👩🏽‍🎓
return

;1F469 1F3FE 200D 1F393
:*::woman-student:-medium-dark-skin-tone::
Send,👩🏾‍🎓
return

;1F469 1F3FF 200D 1F393
:*::woman-student:-dark-skin-tone::
Send,👩🏿‍🎓
return

;1F468 200D 1F3EB
:*::man-teacher::
Send,👨‍🏫
return

;1F468 1F3FB 200D 1F3EB
:*::man-teacher:-light-skin-tone::
Send,👨🏻‍🏫
return

;1F468 1F3FC 200D 1F3EB
:*::man-teacher:-medium-light-skin-tone::
Send,👨🏼‍🏫
return

;1F468 1F3FD 200D 1F3EB
:*::man-teacher:-medium-skin-tone::
Send,👨🏽‍🏫
return

;1F468 1F3FE 200D 1F3EB
:*::man-teacher:-medium-dark-skin-tone::
Send,👨🏾‍🏫
return

;1F468 1F3FF 200D 1F3EB
:*::man-teacher:-dark-skin-tone::
Send,👨🏿‍🏫
return

;1F469 200D 1F3EB
:*::woman-teacher::
Send,👩‍🏫
return

;1F469 1F3FB 200D 1F3EB
:*::woman-teacher:-light-skin-tone::
Send,👩🏻‍🏫
return

;1F469 1F3FC 200D 1F3EB
:*::woman-teacher:-medium-light-skin-tone::
Send,👩🏼‍🏫
return

;1F469 1F3FD 200D 1F3EB
:*::woman-teacher:-medium-skin-tone::
Send,👩🏽‍🏫
return

;1F469 1F3FE 200D 1F3EB
:*::woman-teacher:-medium-dark-skin-tone::
Send,👩🏾‍🏫
return

;1F469 1F3FF 200D 1F3EB
:*::woman-teacher:-dark-skin-tone::
Send,👩🏿‍🏫
return

;1F468 200D 2696 FE0F
:*::man-judge::
Send,👨‍⚖️
return

;1F468 200D 2696
:*::man-judge::
Send,👨‍⚖
return

;1F468 1F3FB 200D 2696 FE0F
:*::man-judge:-light-skin-tone::
Send,👨🏻‍⚖️
return

;1F468 1F3FB 200D 2696
:*::man-judge:-light-skin-tone::
Send,👨🏻‍⚖
return

;1F468 1F3FC 200D 2696 FE0F
:*::man-judge:-medium-light-skin-tone::
Send,👨🏼‍⚖️
return

;1F468 1F3FC 200D 2696
:*::man-judge:-medium-light-skin-tone::
Send,👨🏼‍⚖
return

;1F468 1F3FD 200D 2696 FE0F
:*::man-judge:-medium-skin-tone::
Send,👨🏽‍⚖️
return

;1F468 1F3FD 200D 2696
:*::man-judge:-medium-skin-tone::
Send,👨🏽‍⚖
return

;1F468 1F3FE 200D 2696 FE0F
:*::man-judge:-medium-dark-skin-tone::
Send,👨🏾‍⚖️
return

;1F468 1F3FE 200D 2696
:*::man-judge:-medium-dark-skin-tone::
Send,👨🏾‍⚖
return

;1F468 1F3FF 200D 2696 FE0F
:*::man-judge:-dark-skin-tone::
Send,👨🏿‍⚖️
return

;1F468 1F3FF 200D 2696
:*::man-judge:-dark-skin-tone::
Send,👨🏿‍⚖
return

;1F469 200D 2696 FE0F
:*::woman-judge::
Send,👩‍⚖️
return

;1F469 200D 2696
:*::woman-judge::
Send,👩‍⚖
return

;1F469 1F3FB 200D 2696 FE0F
:*::woman-judge:-light-skin-tone::
Send,👩🏻‍⚖️
return

;1F469 1F3FB 200D 2696
:*::woman-judge:-light-skin-tone::
Send,👩🏻‍⚖
return

;1F469 1F3FC 200D 2696 FE0F
:*::woman-judge:-medium-light-skin-tone::
Send,👩🏼‍⚖️
return

;1F469 1F3FC 200D 2696
:*::woman-judge:-medium-light-skin-tone::
Send,👩🏼‍⚖
return

;1F469 1F3FD 200D 2696 FE0F
:*::woman-judge:-medium-skin-tone::
Send,👩🏽‍⚖️
return

;1F469 1F3FD 200D 2696
:*::woman-judge:-medium-skin-tone::
Send,👩🏽‍⚖
return

;1F469 1F3FE 200D 2696 FE0F
:*::woman-judge:-medium-dark-skin-tone::
Send,👩🏾‍⚖️
return

;1F469 1F3FE 200D 2696
:*::woman-judge:-medium-dark-skin-tone::
Send,👩🏾‍⚖
return

;1F469 1F3FF 200D 2696 FE0F
:*::woman-judge:-dark-skin-tone::
Send,👩🏿‍⚖️
return

;1F469 1F3FF 200D 2696
:*::woman-judge:-dark-skin-tone::
Send,👩🏿‍⚖
return

;1F468 200D 1F33E
:*::man-farmer::
Send,👨‍🌾
return

;1F468 1F3FB 200D 1F33E
:*::man-farmer:-light-skin-tone::
Send,👨🏻‍🌾
return

;1F468 1F3FC 200D 1F33E
:*::man-farmer:-medium-light-skin-tone::
Send,👨🏼‍🌾
return

;1F468 1F3FD 200D 1F33E
:*::man-farmer:-medium-skin-tone::
Send,👨🏽‍🌾
return

;1F468 1F3FE 200D 1F33E
:*::man-farmer:-medium-dark-skin-tone::
Send,👨🏾‍🌾
return

;1F468 1F3FF 200D 1F33E
:*::man-farmer:-dark-skin-tone::
Send,👨🏿‍🌾
return

;1F469 200D 1F33E
:*::woman-farmer::
Send,👩‍🌾
return

;1F469 1F3FB 200D 1F33E
:*::woman-farmer:-light-skin-tone::
Send,👩🏻‍🌾
return

;1F469 1F3FC 200D 1F33E
:*::woman-farmer:-medium-light-skin-tone::
Send,👩🏼‍🌾
return

;1F469 1F3FD 200D 1F33E
:*::woman-farmer:-medium-skin-tone::
Send,👩🏽‍🌾
return

;1F469 1F3FE 200D 1F33E
:*::woman-farmer:-medium-dark-skin-tone::
Send,👩🏾‍🌾
return

;1F469 1F3FF 200D 1F33E
:*::woman-farmer:-dark-skin-tone::
Send,👩🏿‍🌾
return

;1F468 200D 1F373
:*::man-cook::
Send,👨‍🍳
return

;1F468 1F3FB 200D 1F373
:*::man-cook:-light-skin-tone::
Send,👨🏻‍🍳
return

;1F468 1F3FC 200D 1F373
:*::man-cook:-medium-light-skin-tone::
Send,👨🏼‍🍳
return

;1F468 1F3FD 200D 1F373
:*::man-cook:-medium-skin-tone::
Send,👨🏽‍🍳
return

;1F468 1F3FE 200D 1F373
:*::man-cook:-medium-dark-skin-tone::
Send,👨🏾‍🍳
return

;1F468 1F3FF 200D 1F373
:*::man-cook:-dark-skin-tone::
Send,👨🏿‍🍳
return

;1F469 200D 1F373
:*::woman-cook::
Send,👩‍🍳
return

;1F469 1F3FB 200D 1F373
:*::woman-cook:-light-skin-tone::
Send,👩🏻‍🍳
return

;1F469 1F3FC 200D 1F373
:*::woman-cook:-medium-light-skin-tone::
Send,👩🏼‍🍳
return

;1F469 1F3FD 200D 1F373
:*::woman-cook:-medium-skin-tone::
Send,👩🏽‍🍳
return

;1F469 1F3FE 200D 1F373
:*::woman-cook:-medium-dark-skin-tone::
Send,👩🏾‍🍳
return

;1F469 1F3FF 200D 1F373
:*::woman-cook:-dark-skin-tone::
Send,👩🏿‍🍳
return

;1F468 200D 1F527
:*::man-mechanic::
Send,👨‍🔧
return

;1F468 1F3FB 200D 1F527
:*::man-mechanic:-light-skin-tone::
Send,👨🏻‍🔧
return

;1F468 1F3FC 200D 1F527
:*::man-mechanic:-medium-light-skin-tone::
Send,👨🏼‍🔧
return

;1F468 1F3FD 200D 1F527
:*::man-mechanic:-medium-skin-tone::
Send,👨🏽‍🔧
return

;1F468 1F3FE 200D 1F527
:*::man-mechanic:-medium-dark-skin-tone::
Send,👨🏾‍🔧
return

;1F468 1F3FF 200D 1F527
:*::man-mechanic:-dark-skin-tone::
Send,👨🏿‍🔧
return

;1F469 200D 1F527
:*::woman-mechanic::
Send,👩‍🔧
return

;1F469 1F3FB 200D 1F527
:*::woman-mechanic:-light-skin-tone::
Send,👩🏻‍🔧
return

;1F469 1F3FC 200D 1F527
:*::woman-mechanic:-medium-light-skin-tone::
Send,👩🏼‍🔧
return

;1F469 1F3FD 200D 1F527
:*::woman-mechanic:-medium-skin-tone::
Send,👩🏽‍🔧
return

;1F469 1F3FE 200D 1F527
:*::woman-mechanic:-medium-dark-skin-tone::
Send,👩🏾‍🔧
return

;1F469 1F3FF 200D 1F527
:*::woman-mechanic:-dark-skin-tone::
Send,👩🏿‍🔧
return

;1F468 200D 1F3ED
:*::man-factory-worker::
Send,👨‍🏭
return

;1F468 1F3FB 200D 1F3ED
:*::man-factory-worker:-light-skin-tone::
Send,👨🏻‍🏭
return

;1F468 1F3FC 200D 1F3ED
:*::man-factory-worker:-medium-light-skin-t::
Send,👨🏼‍🏭
return

;1F468 1F3FD 200D 1F3ED
:*::man-factory-worker:-medium-skin-tone::
Send,👨🏽‍🏭
return

;1F468 1F3FE 200D 1F3ED
:*::man-factory-worker:-medium-dark-skin-to::
Send,👨🏾‍🏭
return

;1F468 1F3FF 200D 1F3ED
:*::man-factory-worker:-dark-skin-tone::
Send,👨🏿‍🏭
return

;1F469 200D 1F3ED
:*::woman-factory-worker::
Send,👩‍🏭
return

;1F469 1F3FB 200D 1F3ED
:*::woman-factory-worker:-light-skin-tone::
Send,👩🏻‍🏭
return

;1F469 1F3FC 200D 1F3ED
:*::woman-factory-worker:-medium-light-skin::
Send,👩🏼‍🏭
return

;1F469 1F3FD 200D 1F3ED
:*::woman-factory-worker:-medium-skin-tone::
Send,👩🏽‍🏭
return

;1F469 1F3FE 200D 1F3ED
:*::woman-factory-worker:-medium-dark-skin::
Send,👩🏾‍🏭
return

;1F469 1F3FF 200D 1F3ED
:*::woman-factory-worker:-dark-skin-tone::
Send,👩🏿‍🏭
return

;1F468 200D 1F4BC
:*::man-office-worker::
Send,👨‍💼
return

;1F468 1F3FB 200D 1F4BC
:*::man-office-worker:-light-skin-tone::
Send,👨🏻‍💼
return

;1F468 1F3FC 200D 1F4BC
:*::man-office-worker:-medium-light-skin-to::
Send,👨🏼‍💼
return

;1F468 1F3FD 200D 1F4BC
:*::man-office-worker:-medium-skin-tone::
Send,👨🏽‍💼
return

;1F468 1F3FE 200D 1F4BC
:*::man-office-worker:-medium-dark-skin-ton::
Send,👨🏾‍💼
return

;1F468 1F3FF 200D 1F4BC
:*::man-office-worker:-dark-skin-tone::
Send,👨🏿‍💼
return

;1F469 200D 1F4BC
:*::woman-office-worker::
Send,👩‍💼
return

;1F469 1F3FB 200D 1F4BC
:*::woman-office-worker:-light-skin-tone::
Send,👩🏻‍💼
return

;1F469 1F3FC 200D 1F4BC
:*::woman-office-worker:-medium-light-skin::
Send,👩🏼‍💼
return

;1F469 1F3FD 200D 1F4BC
:*::woman-office-worker:-medium-skin-tone::
Send,👩🏽‍💼
return

;1F469 1F3FE 200D 1F4BC
:*::woman-office-worker:-medium-dark-skin-t::
Send,👩🏾‍💼
return

;1F469 1F3FF 200D 1F4BC
:*::woman-office-worker:-dark-skin-tone::
Send,👩🏿‍💼
return

;1F468 200D 1F52C
:*::man-scientist::
Send,👨‍🔬
return

;1F468 1F3FB 200D 1F52C
:*::man-scientist:-light-skin-tone::
Send,👨🏻‍🔬
return

;1F468 1F3FC 200D 1F52C
:*::man-scientist:-medium-light-skin-tone::
Send,👨🏼‍🔬
return

;1F468 1F3FD 200D 1F52C
:*::man-scientist:-medium-skin-tone::
Send,👨🏽‍🔬
return

;1F468 1F3FE 200D 1F52C
:*::man-scientist:-medium-dark-skin-tone::
Send,👨🏾‍🔬
return

;1F468 1F3FF 200D 1F52C
:*::man-scientist:-dark-skin-tone::
Send,👨🏿‍🔬
return

;1F469 200D 1F52C
:*::woman-scientist::
Send,👩‍🔬
return

;1F469 1F3FB 200D 1F52C
:*::woman-scientist:-light-skin-tone::
Send,👩🏻‍🔬
return

;1F469 1F3FC 200D 1F52C
:*::woman-scientist:-medium-light-skin-tone::
Send,👩🏼‍🔬
return

;1F469 1F3FD 200D 1F52C
:*::woman-scientist:-medium-skin-tone::
Send,👩🏽‍🔬
return

;1F469 1F3FE 200D 1F52C
:*::woman-scientist:-medium-dark-skin-tone::
Send,👩🏾‍🔬
return

;1F469 1F3FF 200D 1F52C
:*::woman-scientist:-dark-skin-tone::
Send,👩🏿‍🔬
return

;1F468 200D 1F4BB
:*::man-technologist::
Send,👨‍💻
return

;1F468 1F3FB 200D 1F4BB
:*::man-technologist:-light-skin-tone::
Send,👨🏻‍💻
return

;1F468 1F3FC 200D 1F4BB
:*::man-technologist:-medium-light-skin-ton::
Send,👨🏼‍💻
return

;1F468 1F3FD 200D 1F4BB
:*::man-technologist:-medium-skin-tone::
Send,👨🏽‍💻
return

;1F468 1F3FE 200D 1F4BB
:*::man-technologist:-medium-dark-skin-tone::
Send,👨🏾‍💻
return

;1F468 1F3FF 200D 1F4BB
:*::man-technologist:-dark-skin-tone::
Send,👨🏿‍💻
return

;1F469 200D 1F4BB
:*::woman-technologist::
Send,👩‍💻
return

;1F469 1F3FB 200D 1F4BB
:*::woman-technologist:-light-skin-tone::
Send,👩🏻‍💻
return

;1F469 1F3FC 200D 1F4BB
:*::woman-technologist:-medium-light-skin-t::
Send,👩🏼‍💻
return

;1F469 1F3FD 200D 1F4BB
:*::woman-technologist:-medium-skin-tone::
Send,👩🏽‍💻
return

;1F469 1F3FE 200D 1F4BB
:*::woman-technologist:-medium-dark-skin-to::
Send,👩🏾‍💻
return

;1F469 1F3FF 200D 1F4BB
:*::woman-technologist:-dark-skin-tone::
Send,👩🏿‍💻
return

;1F468 200D 1F3A4
:*::man-singer::
Send,👨‍🎤
return

;1F468 1F3FB 200D 1F3A4
:*::man-singer:-light-skin-tone::
Send,👨🏻‍🎤
return

;1F468 1F3FC 200D 1F3A4
:*::man-singer:-medium-light-skin-tone::
Send,👨🏼‍🎤
return

;1F468 1F3FD 200D 1F3A4
:*::man-singer:-medium-skin-tone::
Send,👨🏽‍🎤
return

;1F468 1F3FE 200D 1F3A4
:*::man-singer:-medium-dark-skin-tone::
Send,👨🏾‍🎤
return

;1F468 1F3FF 200D 1F3A4
:*::man-singer:-dark-skin-tone::
Send,👨🏿‍🎤
return

;1F469 200D 1F3A4
:*::woman-singer::
Send,👩‍🎤
return

;1F469 1F3FB 200D 1F3A4
:*::woman-singer:-light-skin-tone::
Send,👩🏻‍🎤
return

;1F469 1F3FC 200D 1F3A4
:*::woman-singer:-medium-light-skin-tone::
Send,👩🏼‍🎤
return

;1F469 1F3FD 200D 1F3A4
:*::woman-singer:-medium-skin-tone::
Send,👩🏽‍🎤
return

;1F469 1F3FE 200D 1F3A4
:*::woman-singer:-medium-dark-skin-tone::
Send,👩🏾‍🎤
return

;1F469 1F3FF 200D 1F3A4
:*::woman-singer:-dark-skin-tone::
Send,👩🏿‍🎤
return

;1F468 200D 1F3A8
:*::man-artist::
Send,👨‍🎨
return

;1F468 1F3FB 200D 1F3A8
:*::man-artist:-light-skin-tone::
Send,👨🏻‍🎨
return

;1F468 1F3FC 200D 1F3A8
:*::man-artist:-medium-light-skin-tone::
Send,👨🏼‍🎨
return

;1F468 1F3FD 200D 1F3A8
:*::man-artist:-medium-skin-tone::
Send,👨🏽‍🎨
return

;1F468 1F3FE 200D 1F3A8
:*::man-artist:-medium-dark-skin-tone::
Send,👨🏾‍🎨
return

;1F468 1F3FF 200D 1F3A8
:*::man-artist:-dark-skin-tone::
Send,👨🏿‍🎨
return

;1F469 200D 1F3A8
:*::woman-artist::
Send,👩‍🎨
return

;1F469 1F3FB 200D 1F3A8
:*::woman-artist:-light-skin-tone::
Send,👩🏻‍🎨
return

;1F469 1F3FC 200D 1F3A8
:*::woman-artist:-medium-light-skin-tone::
Send,👩🏼‍🎨
return

;1F469 1F3FD 200D 1F3A8
:*::woman-artist:-medium-skin-tone::
Send,👩🏽‍🎨
return

;1F469 1F3FE 200D 1F3A8
:*::woman-artist:-medium-dark-skin-tone::
Send,👩🏾‍🎨
return

;1F469 1F3FF 200D 1F3A8
:*::woman-artist:-dark-skin-tone::
Send,👩🏿‍🎨
return

;1F468 200D 2708 FE0F
:*::man-pilot::
Send,👨‍✈️
return

;1F468 200D 2708
:*::man-pilot::
Send,👨‍✈
return

;1F468 1F3FB 200D 2708 FE0F
:*::man-pilot:-light-skin-tone::
Send,👨🏻‍✈️
return

;1F468 1F3FB 200D 2708
:*::man-pilot:-light-skin-tone::
Send,👨🏻‍✈
return

;1F468 1F3FC 200D 2708 FE0F
:*::man-pilot:-medium-light-skin-tone::
Send,👨🏼‍✈️
return

;1F468 1F3FC 200D 2708
:*::man-pilot:-medium-light-skin-tone::
Send,👨🏼‍✈
return

;1F468 1F3FD 200D 2708 FE0F
:*::man-pilot:-medium-skin-tone::
Send,👨🏽‍✈️
return

;1F468 1F3FD 200D 2708
:*::man-pilot:-medium-skin-tone::
Send,👨🏽‍✈
return

;1F468 1F3FE 200D 2708 FE0F
:*::man-pilot:-medium-dark-skin-tone::
Send,👨🏾‍✈️
return

;1F468 1F3FE 200D 2708
:*::man-pilot:-medium-dark-skin-tone::
Send,👨🏾‍✈
return

;1F468 1F3FF 200D 2708 FE0F
:*::man-pilot:-dark-skin-tone::
Send,👨🏿‍✈️
return

;1F468 1F3FF 200D 2708
:*::man-pilot:-dark-skin-tone::
Send,👨🏿‍✈
return

;1F469 200D 2708 FE0F
:*::woman-pilot::
Send,👩‍✈️
return

;1F469 200D 2708
:*::woman-pilot::
Send,👩‍✈
return

;1F469 1F3FB 200D 2708 FE0F
:*::woman-pilot:-light-skin-tone::
Send,👩🏻‍✈️
return

;1F469 1F3FB 200D 2708
:*::woman-pilot:-light-skin-tone::
Send,👩🏻‍✈
return

;1F469 1F3FC 200D 2708 FE0F
:*::woman-pilot:-medium-light-skin-tone::
Send,👩🏼‍✈️
return

;1F469 1F3FC 200D 2708
:*::woman-pilot:-medium-light-skin-tone::
Send,👩🏼‍✈
return

;1F469 1F3FD 200D 2708 FE0F
:*::woman-pilot:-medium-skin-tone::
Send,👩🏽‍✈️
return

;1F469 1F3FD 200D 2708
:*::woman-pilot:-medium-skin-tone::
Send,👩🏽‍✈
return

;1F469 1F3FE 200D 2708 FE0F
:*::woman-pilot:-medium-dark-skin-tone::
Send,👩🏾‍✈️
return

;1F469 1F3FE 200D 2708
:*::woman-pilot:-medium-dark-skin-tone::
Send,👩🏾‍✈
return

;1F469 1F3FF 200D 2708 FE0F
:*::woman-pilot:-dark-skin-tone::
Send,👩🏿‍✈️
return

;1F469 1F3FF 200D 2708
:*::woman-pilot:-dark-skin-tone::
Send,👩🏿‍✈
return

;1F468 200D 1F680
:*::man-astronaut::
Send,👨‍🚀
return

;1F468 1F3FB 200D 1F680
:*::man-astronaut:-light-skin-tone::
Send,👨🏻‍🚀
return

;1F468 1F3FC 200D 1F680
:*::man-astronaut:-medium-light-skin-tone::
Send,👨🏼‍🚀
return

;1F468 1F3FD 200D 1F680
:*::man-astronaut:-medium-skin-tone::
Send,👨🏽‍🚀
return

;1F468 1F3FE 200D 1F680
:*::man-astronaut:-medium-dark-skin-tone::
Send,👨🏾‍🚀
return

;1F468 1F3FF 200D 1F680
:*::man-astronaut:-dark-skin-tone::
Send,👨🏿‍🚀
return

;1F469 200D 1F680
:*::woman-astronaut::
Send,👩‍🚀
return

;1F469 1F3FB 200D 1F680
:*::woman-astronaut:-light-skin-tone::
Send,👩🏻‍🚀
return

;1F469 1F3FC 200D 1F680
:*::woman-astronaut:-medium-light-skin-tone::
Send,👩🏼‍🚀
return

;1F469 1F3FD 200D 1F680
:*::woman-astronaut:-medium-skin-tone::
Send,👩🏽‍🚀
return

;1F469 1F3FE 200D 1F680
:*::woman-astronaut:-medium-dark-skin-tone::
Send,👩🏾‍🚀
return

;1F469 1F3FF 200D 1F680
:*::woman-astronaut:-dark-skin-tone::
Send,👩🏿‍🚀
return

;1F468 200D 1F692
:*::man-firefighter::
Send,👨‍🚒
return

;1F468 1F3FB 200D 1F692
:*::man-firefighter:-light-skin-tone::
Send,👨🏻‍🚒
return

;1F468 1F3FC 200D 1F692
:*::man-firefighter:-medium-light-skin-tone::
Send,👨🏼‍🚒
return

;1F468 1F3FD 200D 1F692
:*::man-firefighter:-medium-skin-tone::
Send,👨🏽‍🚒
return

;1F468 1F3FE 200D 1F692
:*::man-firefighter:-medium-dark-skin-tone::
Send,👨🏾‍🚒
return

;1F468 1F3FF 200D 1F692
:*::man-firefighter:-dark-skin-tone::
Send,👨🏿‍🚒
return

;1F469 200D 1F692
:*::woman-firefighter::
Send,👩‍🚒
return

;1F469 1F3FB 200D 1F692
:*::woman-firefighter:-light-skin-tone::
Send,👩🏻‍🚒
return

;1F469 1F3FC 200D 1F692
:*::woman-firefighter:-medium-light-skin-to::
Send,👩🏼‍🚒
return

;1F469 1F3FD 200D 1F692
:*::woman-firefighter:-medium-skin-tone::
Send,👩🏽‍🚒
return

;1F469 1F3FE 200D 1F692
:*::woman-firefighter:-medium-dark-skin-ton::
Send,👩🏾‍🚒
return

;1F469 1F3FF 200D 1F692
:*::woman-firefighter:-dark-skin-tone::
Send,👩🏿‍🚒
return

;1F46E
:*::police-officer::
Send,👮
return

;1F46E 1F3FB
:*::police-officer:-light-skin-tone::
Send,👮🏻
return

;1F46E 1F3FC
:*::police-officer:-medium-light-skin-tone::
Send,👮🏼
return

;1F46E 1F3FD
:*::police-officer:-medium-skin-tone::
Send,👮🏽
return

;1F46E 1F3FE
:*::police-officer:-medium-dark-skin-tone::
Send,👮🏾
return

;1F46E 1F3FF
:*::police-officer:-dark-skin-tone::
Send,👮🏿
return

;1F46E 200D 2642 FE0F
:*::man-police-officer::
Send,👮‍♂️
return

;1F46E 200D 2642
:*::man-police-officer::
Send,👮‍♂
return

;1F46E 1F3FB 200D 2642 FE0F
:*::man-police-officer:-light-skin-tone::
Send,👮🏻‍♂️
return

;1F46E 1F3FB 200D 2642
:*::man-police-officer:-light-skin-tone::
Send,👮🏻‍♂
return

;1F46E 1F3FC 200D 2642 FE0F
:*::man-police-officer:-medium-light-skin-t::
Send,👮🏼‍♂️
return

;1F46E 1F3FC 200D 2642
:*::man-police-officer:-medium-light-skin-t::
Send,👮🏼‍♂
return

;1F46E 1F3FD 200D 2642 FE0F
:*::man-police-officer:-medium-skin-tone::
Send,👮🏽‍♂️
return

;1F46E 1F3FD 200D 2642
:*::man-police-officer:-medium-skin-tone::
Send,👮🏽‍♂
return

;1F46E 1F3FE 200D 2642 FE0F
:*::man-police-officer:-medium-dark-skin-to::
Send,👮🏾‍♂️
return

;1F46E 1F3FE 200D 2642
:*::man-police-officer:-medium-dark-skin-to::
Send,👮🏾‍♂
return

;1F46E 1F3FF 200D 2642 FE0F
:*::man-police-officer:-dark-skin-tone::
Send,👮🏿‍♂️
return

;1F46E 1F3FF 200D 2642
:*::man-police-officer:-dark-skin-tone::
Send,👮🏿‍♂
return

;1F46E 200D 2640 FE0F
:*::woman-police-officer::
Send,👮‍♀️
return

;1F46E 200D 2640
:*::woman-police-officer::
Send,👮‍♀
return

;1F46E 1F3FB 200D 2640 FE0F
:*::woman-police-officer:-light-skin-tone::
Send,👮🏻‍♀️
return

;1F46E 1F3FB 200D 2640
:*::woman-police-officer:-light-skin-tone::
Send,👮🏻‍♀
return

;1F46E 1F3FC 200D 2640 FE0F
:*::woman-police-officer:-medium-light-skin::
Send,👮🏼‍♀️
return

;1F46E 1F3FC 200D 2640
:*::woman-police-officer:-medium-light-skin::
Send,👮🏼‍♀
return

;1F46E 1F3FD 200D 2640 FE0F
:*::woman-police-officer:-medium-skin-tone::
Send,👮🏽‍♀️
return

;1F46E 1F3FD 200D 2640
:*::woman-police-officer:-medium-skin-tone::
Send,👮🏽‍♀
return

;1F46E 1F3FE 200D 2640 FE0F
:*::woman-police-officer:-medium-dark-skin::
Send,👮🏾‍♀️
return

;1F46E 1F3FE 200D 2640
:*::woman-police-officer:-medium-dark-skin::
Send,👮🏾‍♀
return

;1F46E 1F3FF 200D 2640 FE0F
:*::woman-police-officer:-dark-skin-tone::
Send,👮🏿‍♀️
return

;1F46E 1F3FF 200D 2640
:*::woman-police-officer:-dark-skin-tone::
Send,👮🏿‍♀
return

;1F575 FE0F
:*::detective::
Send,🕵️
return

;1F575
:*::detective::
Send,🕵
return

;1F575 1F3FB
:*::detective:-light-skin-tone::
Send,🕵🏻
return

;1F575 1F3FC
:*::detective:-medium-light-skin-tone::
Send,🕵🏼
return

;1F575 1F3FD
:*::detective:-medium-skin-tone::
Send,🕵🏽
return

;1F575 1F3FE
:*::detective:-medium-dark-skin-tone::
Send,🕵🏾
return

;1F575 1F3FF
:*::detective:-dark-skin-tone::
Send,🕵🏿
return

;1F575 FE0F 200D 2642 FE0F
:*::man-detective::
Send,🕵️‍♂️
return

;1F575 200D 2642 FE0F
:*::man-detective::
Send,🕵‍♂️
return

;1F575 FE0F 200D 2642
:*::man-detective::
Send,🕵️‍♂
return

;1F575 200D 2642
:*::man-detective::
Send,🕵‍♂
return

;1F575 1F3FB 200D 2642 FE0F
:*::man-detective:-light-skin-tone::
Send,🕵🏻‍♂️
return

;1F575 1F3FB 200D 2642
:*::man-detective:-light-skin-tone::
Send,🕵🏻‍♂
return

;1F575 1F3FC 200D 2642 FE0F
:*::man-detective:-medium-light-skin-tone::
Send,🕵🏼‍♂️
return

;1F575 1F3FC 200D 2642
:*::man-detective:-medium-light-skin-tone::
Send,🕵🏼‍♂
return

;1F575 1F3FD 200D 2642 FE0F
:*::man-detective:-medium-skin-tone::
Send,🕵🏽‍♂️
return

;1F575 1F3FD 200D 2642
:*::man-detective:-medium-skin-tone::
Send,🕵🏽‍♂
return

;1F575 1F3FE 200D 2642 FE0F
:*::man-detective:-medium-dark-skin-tone::
Send,🕵🏾‍♂️
return

;1F575 1F3FE 200D 2642
:*::man-detective:-medium-dark-skin-tone::
Send,🕵🏾‍♂
return

;1F575 1F3FF 200D 2642 FE0F
:*::man-detective:-dark-skin-tone::
Send,🕵🏿‍♂️
return

;1F575 1F3FF 200D 2642
:*::man-detective:-dark-skin-tone::
Send,🕵🏿‍♂
return

;1F575 FE0F 200D 2640 FE0F
:*::woman-detective::
Send,🕵️‍♀️
return

;1F575 200D 2640 FE0F
:*::woman-detective::
Send,🕵‍♀️
return

;1F575 FE0F 200D 2640
:*::woman-detective::
Send,🕵️‍♀
return

;1F575 200D 2640
:*::woman-detective::
Send,🕵‍♀
return

;1F575 1F3FB 200D 2640 FE0F
:*::woman-detective:-light-skin-tone::
Send,🕵🏻‍♀️
return

;1F575 1F3FB 200D 2640
:*::woman-detective:-light-skin-tone::
Send,🕵🏻‍♀
return

;1F575 1F3FC 200D 2640 FE0F
:*::woman-detective:-medium-light-skin-tone::
Send,🕵🏼‍♀️
return

;1F575 1F3FC 200D 2640
:*::woman-detective:-medium-light-skin-tone::
Send,🕵🏼‍♀
return

;1F575 1F3FD 200D 2640 FE0F
:*::woman-detective:-medium-skin-tone::
Send,🕵🏽‍♀️
return

;1F575 1F3FD 200D 2640
:*::woman-detective:-medium-skin-tone::
Send,🕵🏽‍♀
return

;1F575 1F3FE 200D 2640 FE0F
:*::woman-detective:-medium-dark-skin-tone::
Send,🕵🏾‍♀️
return

;1F575 1F3FE 200D 2640
:*::woman-detective:-medium-dark-skin-tone::
Send,🕵🏾‍♀
return

;1F575 1F3FF 200D 2640 FE0F
:*::woman-detective:-dark-skin-tone::
Send,🕵🏿‍♀️
return

;1F575 1F3FF 200D 2640
:*::woman-detective:-dark-skin-tone::
Send,🕵🏿‍♀
return

;1F482
:*::guard::
Send,💂
return

;1F482 1F3FB
:*::guard:-light-skin-tone::
Send,💂🏻
return

;1F482 1F3FC
:*::guard:-medium-light-skin-tone::
Send,💂🏼
return

;1F482 1F3FD
:*::guard:-medium-skin-tone::
Send,💂🏽
return

;1F482 1F3FE
:*::guard:-medium-dark-skin-tone::
Send,💂🏾
return

;1F482 1F3FF
:*::guard:-dark-skin-tone::
Send,💂🏿
return

;1F482 200D 2642 FE0F
:*::man-guard::
Send,💂‍♂️
return

;1F482 200D 2642
:*::man-guard::
Send,💂‍♂
return

;1F482 1F3FB 200D 2642 FE0F
:*::man-guard:-light-skin-tone::
Send,💂🏻‍♂️
return

;1F482 1F3FB 200D 2642
:*::man-guard:-light-skin-tone::
Send,💂🏻‍♂
return

;1F482 1F3FC 200D 2642 FE0F
:*::man-guard:-medium-light-skin-tone::
Send,💂🏼‍♂️
return

;1F482 1F3FC 200D 2642
:*::man-guard:-medium-light-skin-tone::
Send,💂🏼‍♂
return

;1F482 1F3FD 200D 2642 FE0F
:*::man-guard:-medium-skin-tone::
Send,💂🏽‍♂️
return

;1F482 1F3FD 200D 2642
:*::man-guard:-medium-skin-tone::
Send,💂🏽‍♂
return

;1F482 1F3FE 200D 2642 FE0F
:*::man-guard:-medium-dark-skin-tone::
Send,💂🏾‍♂️
return

;1F482 1F3FE 200D 2642
:*::man-guard:-medium-dark-skin-tone::
Send,💂🏾‍♂
return

;1F482 1F3FF 200D 2642 FE0F
:*::man-guard:-dark-skin-tone::
Send,💂🏿‍♂️
return

;1F482 1F3FF 200D 2642
:*::man-guard:-dark-skin-tone::
Send,💂🏿‍♂
return

;1F482 200D 2640 FE0F
:*::woman-guard::
Send,💂‍♀️
return

;1F482 200D 2640
:*::woman-guard::
Send,💂‍♀
return

;1F482 1F3FB 200D 2640 FE0F
:*::woman-guard:-light-skin-tone::
Send,💂🏻‍♀️
return

;1F482 1F3FB 200D 2640
:*::woman-guard:-light-skin-tone::
Send,💂🏻‍♀
return

;1F482 1F3FC 200D 2640 FE0F
:*::woman-guard:-medium-light-skin-tone::
Send,💂🏼‍♀️
return

;1F482 1F3FC 200D 2640
:*::woman-guard:-medium-light-skin-tone::
Send,💂🏼‍♀
return

;1F482 1F3FD 200D 2640 FE0F
:*::woman-guard:-medium-skin-tone::
Send,💂🏽‍♀️
return

;1F482 1F3FD 200D 2640
:*::woman-guard:-medium-skin-tone::
Send,💂🏽‍♀
return

;1F482 1F3FE 200D 2640 FE0F
:*::woman-guard:-medium-dark-skin-tone::
Send,💂🏾‍♀️
return

;1F482 1F3FE 200D 2640
:*::woman-guard:-medium-dark-skin-tone::
Send,💂🏾‍♀
return

;1F482 1F3FF 200D 2640 FE0F
:*::woman-guard:-dark-skin-tone::
Send,💂🏿‍♀️
return

;1F482 1F3FF 200D 2640
:*::woman-guard:-dark-skin-tone::
Send,💂🏿‍♀
return

;1F477
:*::construction-worker::
Send,👷
return

;1F477 1F3FB
:*::construction-worker:-light-skin-tone::
Send,👷🏻
return

;1F477 1F3FC
:*::construction-worker:-medium-light-skin::
Send,👷🏼
return

;1F477 1F3FD
:*::construction-worker:-medium-skin-tone::
Send,👷🏽
return

;1F477 1F3FE
:*::construction-worker:-medium-dark-skin-t::
Send,👷🏾
return

;1F477 1F3FF
:*::construction-worker:-dark-skin-tone::
Send,👷🏿
return

;1F477 200D 2642 FE0F
:*::man-construction-worker::
Send,👷‍♂️
return

;1F477 200D 2642
:*::man-construction-worker::
Send,👷‍♂
return

;1F477 1F3FB 200D 2642 FE0F
:*::man-construction-worker:-light-skin-ton::
Send,👷🏻‍♂️
return

;1F477 1F3FB 200D 2642
:*::man-construction-worker:-light-skin-ton::
Send,👷🏻‍♂
return

;1F477 1F3FC 200D 2642 FE0F
:*::man-construction-worker:-medium-light-s::
Send,👷🏼‍♂️
return

;1F477 1F3FC 200D 2642
:*::man-construction-worker:-medium-light-s::
Send,👷🏼‍♂
return

;1F477 1F3FD 200D 2642 FE0F
:*::man-construction-worker:-medium-skin-to::
Send,👷🏽‍♂️
return

;1F477 1F3FD 200D 2642
:*::man-construction-worker:-medium-skin-to::
Send,👷🏽‍♂
return

;1F477 1F3FE 200D 2642 FE0F
:*::man-construction-worker:-medium-dark-sk::
Send,👷🏾‍♂️
return

;1F477 1F3FE 200D 2642
:*::man-construction-worker:-medium-dark-sk::
Send,👷🏾‍♂
return

;1F477 1F3FF 200D 2642 FE0F
:*::man-construction-worker:-dark-skin-tone::
Send,👷🏿‍♂️
return

;1F477 1F3FF 200D 2642
:*::man-construction-worker:-dark-skin-tone::
Send,👷🏿‍♂
return

;1F477 200D 2640 FE0F
:*::woman-construction-worker::
Send,👷‍♀️
return

;1F477 200D 2640
:*::woman-construction-worker::
Send,👷‍♀
return

;1F477 1F3FB 200D 2640 FE0F
:*::woman-construction-worker:-light-skin-t::
Send,👷🏻‍♀️
return

;1F477 1F3FB 200D 2640
:*::woman-construction-worker:-light-skin-t::
Send,👷🏻‍♀
return

;1F477 1F3FC 200D 2640 FE0F
:*::woman-construction-worker:-medium-light::
Send,👷🏼‍♀️
return

;1F477 1F3FC 200D 2640
:*::woman-construction-worker:-medium-light::
Send,👷🏼‍♀
return

;1F477 1F3FD 200D 2640 FE0F
:*::woman-construction-worker:-medium-skin::
Send,👷🏽‍♀️
return

;1F477 1F3FD 200D 2640
:*::woman-construction-worker:-medium-skin::
Send,👷🏽‍♀
return

;1F477 1F3FE 200D 2640 FE0F
:*::woman-construction-worker:-medium-dark::
Send,👷🏾‍♀️
return

;1F477 1F3FE 200D 2640
:*::woman-construction-worker:-medium-dark::
Send,👷🏾‍♀
return

;1F477 1F3FF 200D 2640 FE0F
:*::woman-construction-worker:-dark-skin-to::
Send,👷🏿‍♀️
return

;1F477 1F3FF 200D 2640
:*::woman-construction-worker:-dark-skin-to::
Send,👷🏿‍♀
return

;1F934
:*::prince::
Send,🤴
return

;1F934 1F3FB
:*::prince:-light-skin-tone::
Send,🤴🏻
return

;1F934 1F3FC
:*::prince:-medium-light-skin-tone::
Send,🤴🏼
return

;1F934 1F3FD
:*::prince:-medium-skin-tone::
Send,🤴🏽
return

;1F934 1F3FE
:*::prince:-medium-dark-skin-tone::
Send,🤴🏾
return

;1F934 1F3FF
:*::prince:-dark-skin-tone::
Send,🤴🏿
return

;1F478
:*::princess::
Send,👸
return

;1F478 1F3FB
:*::princess:-light-skin-tone::
Send,👸🏻
return

;1F478 1F3FC
:*::princess:-medium-light-skin-tone::
Send,👸🏼
return

;1F478 1F3FD
:*::princess:-medium-skin-tone::
Send,👸🏽
return

;1F478 1F3FE
:*::princess:-medium-dark-skin-tone::
Send,👸🏾
return

;1F478 1F3FF
:*::princess:-dark-skin-tone::
Send,👸🏿
return

;1F473
:*::person-wearing-turban::
Send,👳
return

;1F473 1F3FB
:*::person-wearing-turban:-light-skin-tone::
Send,👳🏻
return

;1F473 1F3FC
:*::person-wearing-turban:-medium-light-ski::
Send,👳🏼
return

;1F473 1F3FD
:*::person-wearing-turban:-medium-skin-tone::
Send,👳🏽
return

;1F473 1F3FE
:*::person-wearing-turban:-medium-dark-skin::
Send,👳🏾
return

;1F473 1F3FF
:*::person-wearing-turban:-dark-skin-tone::
Send,👳🏿
return

;1F473 200D 2642 FE0F
:*::man-wearing-turban::
Send,👳‍♂️
return

;1F473 200D 2642
:*::man-wearing-turban::
Send,👳‍♂
return

;1F473 1F3FB 200D 2642 FE0F
:*::man-wearing-turban:-light-skin-tone::
Send,👳🏻‍♂️
return

;1F473 1F3FB 200D 2642
:*::man-wearing-turban:-light-skin-tone::
Send,👳🏻‍♂
return

;1F473 1F3FC 200D 2642 FE0F
:*::man-wearing-turban:-medium-light-skin-t::
Send,👳🏼‍♂️
return

;1F473 1F3FC 200D 2642
:*::man-wearing-turban:-medium-light-skin-t::
Send,👳🏼‍♂
return

;1F473 1F3FD 200D 2642 FE0F
:*::man-wearing-turban:-medium-skin-tone::
Send,👳🏽‍♂️
return

;1F473 1F3FD 200D 2642
:*::man-wearing-turban:-medium-skin-tone::
Send,👳🏽‍♂
return

;1F473 1F3FE 200D 2642 FE0F
:*::man-wearing-turban:-medium-dark-skin-to::
Send,👳🏾‍♂️
return

;1F473 1F3FE 200D 2642
:*::man-wearing-turban:-medium-dark-skin-to::
Send,👳🏾‍♂
return

;1F473 1F3FF 200D 2642 FE0F
:*::man-wearing-turban:-dark-skin-tone::
Send,👳🏿‍♂️
return

;1F473 1F3FF 200D 2642
:*::man-wearing-turban:-dark-skin-tone::
Send,👳🏿‍♂
return

;1F473 200D 2640 FE0F
:*::woman-wearing-turban::
Send,👳‍♀️
return

;1F473 200D 2640
:*::woman-wearing-turban::
Send,👳‍♀
return

;1F473 1F3FB 200D 2640 FE0F
:*::woman-wearing-turban:-light-skin-tone::
Send,👳🏻‍♀️
return

;1F473 1F3FB 200D 2640
:*::woman-wearing-turban:-light-skin-tone::
Send,👳🏻‍♀
return

;1F473 1F3FC 200D 2640 FE0F
:*::woman-wearing-turban:-medium-light-skin::
Send,👳🏼‍♀️
return

;1F473 1F3FC 200D 2640
:*::woman-wearing-turban:-medium-light-skin::
Send,👳🏼‍♀
return

;1F473 1F3FD 200D 2640 FE0F
:*::woman-wearing-turban:-medium-skin-tone::
Send,👳🏽‍♀️
return

;1F473 1F3FD 200D 2640
:*::woman-wearing-turban:-medium-skin-tone::
Send,👳🏽‍♀
return

;1F473 1F3FE 200D 2640 FE0F
:*::woman-wearing-turban:-medium-dark-skin::
Send,👳🏾‍♀️
return

;1F473 1F3FE 200D 2640
:*::woman-wearing-turban:-medium-dark-skin::
Send,👳🏾‍♀
return

;1F473 1F3FF 200D 2640 FE0F
:*::woman-wearing-turban:-dark-skin-tone::
Send,👳🏿‍♀️
return

;1F473 1F3FF 200D 2640
:*::woman-wearing-turban:-dark-skin-tone::
Send,👳🏿‍♀
return

;1F472
:*::man-with-Chinese-cap::
Send,👲
return

;1F472 1F3FB
:*::man-with-Chinese-cap:-light-skin-tone::
Send,👲🏻
return

;1F472 1F3FC
:*::man-with-Chinese-cap:-medium-light-skin::
Send,👲🏼
return

;1F472 1F3FD
:*::man-with-Chinese-cap:-medium-skin-tone::
Send,👲🏽
return

;1F472 1F3FE
:*::man-with-Chinese-cap:-medium-dark-skin::
Send,👲🏾
return

;1F472 1F3FF
:*::man-with-Chinese-cap:-dark-skin-tone::
Send,👲🏿
return

;1F9D5
:*::woman-with-headscarf::
Send,🧕
return

;1F9D5 1F3FB
:*::woman-with-headscarf:-light-skin-tone::
Send,🧕🏻
return

;1F9D5 1F3FC
:*::woman-with-headscarf:-medium-light-skin::
Send,🧕🏼
return

;1F9D5 1F3FD
:*::woman-with-headscarf:-medium-skin-tone::
Send,🧕🏽
return

;1F9D5 1F3FE
:*::woman-with-headscarf:-medium-dark-skin::
Send,🧕🏾
return

;1F9D5 1F3FF
:*::woman-with-headscarf:-dark-skin-tone::
Send,🧕🏿
return

;1F935
:*::man-in-tuxedo::
Send,🤵
return

;1F935 1F3FB
:*::man-in-tuxedo:-light-skin-tone::
Send,🤵🏻
return

;1F935 1F3FC
:*::man-in-tuxedo:-medium-light-skin-tone::
Send,🤵🏼
return

;1F935 1F3FD
:*::man-in-tuxedo:-medium-skin-tone::
Send,🤵🏽
return

;1F935 1F3FE
:*::man-in-tuxedo:-medium-dark-skin-tone::
Send,🤵🏾
return

;1F935 1F3FF
:*::man-in-tuxedo:-dark-skin-tone::
Send,🤵🏿
return

;1F470
:*::bride-with-veil::
Send,👰
return

;1F470 1F3FB
:*::bride-with-veil:-light-skin-tone::
Send,👰🏻
return

;1F470 1F3FC
:*::bride-with-veil:-medium-light-skin-tone::
Send,👰🏼
return

;1F470 1F3FD
:*::bride-with-veil:-medium-skin-tone::
Send,👰🏽
return

;1F470 1F3FE
:*::bride-with-veil:-medium-dark-skin-tone::
Send,👰🏾
return

;1F470 1F3FF
:*::bride-with-veil:-dark-skin-tone::
Send,👰🏿
return

;1F930
:*::pregnant-woman::
Send,🤰
return

;1F930 1F3FB
:*::pregnant-woman:-light-skin-tone::
Send,🤰🏻
return

;1F930 1F3FC
:*::pregnant-woman:-medium-light-skin-tone::
Send,🤰🏼
return

;1F930 1F3FD
:*::pregnant-woman:-medium-skin-tone::
Send,🤰🏽
return

;1F930 1F3FE
:*::pregnant-woman:-medium-dark-skin-tone::
Send,🤰🏾
return

;1F930 1F3FF
:*::pregnant-woman:-dark-skin-tone::
Send,🤰🏿
return

;1F931
:*::breast-feeding::
Send,🤱
return

;1F931 1F3FB
:*::breast-feeding:-light-skin-tone::
Send,🤱🏻
return

;1F931 1F3FC
:*::breast-feeding:-medium-light-skin-tone::
Send,🤱🏼
return

;1F931 1F3FD
:*::breast-feeding:-medium-skin-tone::
Send,🤱🏽
return

;1F931 1F3FE
:*::breast-feeding:-medium-dark-skin-tone::
Send,🤱🏾
return

;1F931 1F3FF
:*::breast-feeding:-dark-skin-tone::
Send,🤱🏿
return

;1F47C
:*::baby-angel::
Send,👼
return

;1F47C 1F3FB
:*::baby-angel:-light-skin-tone::
Send,👼🏻
return

;1F47C 1F3FC
:*::baby-angel:-medium-light-skin-tone::
Send,👼🏼
return

;1F47C 1F3FD
:*::baby-angel:-medium-skin-tone::
Send,👼🏽
return

;1F47C 1F3FE
:*::baby-angel:-medium-dark-skin-tone::
Send,👼🏾
return

;1F47C 1F3FF
:*::baby-angel:-dark-skin-tone::
Send,👼🏿
return

;1F385
:*::Santa-Claus::
Send,🎅
return

;1F385 1F3FB
:*::Santa-Claus:-light-skin-tone::
Send,🎅🏻
return

;1F385 1F3FC
:*::Santa-Claus:-medium-light-skin-tone::
Send,🎅🏼
return

;1F385 1F3FD
:*::Santa-Claus:-medium-skin-tone::
Send,🎅🏽
return

;1F385 1F3FE
:*::Santa-Claus:-medium-dark-skin-tone::
Send,🎅🏾
return

;1F385 1F3FF
:*::Santa-Claus:-dark-skin-tone::
Send,🎅🏿
return

;1F936
:*::Mrs.-Claus::
Send,🤶
return

;1F936 1F3FB
:*::Mrs.-Claus:-light-skin-tone::
Send,🤶🏻
return

;1F936 1F3FC
:*::Mrs.-Claus:-medium-light-skin-tone::
Send,🤶🏼
return

;1F936 1F3FD
:*::Mrs.-Claus:-medium-skin-tone::
Send,🤶🏽
return

;1F936 1F3FE
:*::Mrs.-Claus:-medium-dark-skin-tone::
Send,🤶🏾
return

;1F936 1F3FF
:*::Mrs.-Claus:-dark-skin-tone::
Send,🤶🏿
return

;1F9B8
:*::superhero::
Send,🦸
return

;1F9B8 1F3FB
:*::superhero:-light-skin-tone::
Send,🦸🏻
return

;1F9B8 1F3FC
:*::superhero:-medium-light-skin-tone::
Send,🦸🏼
return

;1F9B8 1F3FD
:*::superhero:-medium-skin-tone::
Send,🦸🏽
return

;1F9B8 1F3FE
:*::superhero:-medium-dark-skin-tone::
Send,🦸🏾
return

;1F9B8 1F3FF
:*::superhero:-dark-skin-tone::
Send,🦸🏿
return

;1F9B8 200D 2642 FE0F
:*::man-superhero::
Send,🦸‍♂️
return

;1F9B8 200D 2642
:*::man-superhero::
Send,🦸‍♂
return

;1F9B8 1F3FB 200D 2642 FE0F
:*::man-superhero:-light-skin-tone::
Send,🦸🏻‍♂️
return

;1F9B8 1F3FB 200D 2642
:*::man-superhero:-light-skin-tone::
Send,🦸🏻‍♂
return

;1F9B8 1F3FC 200D 2642 FE0F
:*::man-superhero:-medium-light-skin-tone::
Send,🦸🏼‍♂️
return

;1F9B8 1F3FC 200D 2642
:*::man-superhero:-medium-light-skin-tone::
Send,🦸🏼‍♂
return

;1F9B8 1F3FD 200D 2642 FE0F
:*::man-superhero:-medium-skin-tone::
Send,🦸🏽‍♂️
return

;1F9B8 1F3FD 200D 2642
:*::man-superhero:-medium-skin-tone::
Send,🦸🏽‍♂
return

;1F9B8 1F3FE 200D 2642 FE0F
:*::man-superhero:-medium-dark-skin-tone::
Send,🦸🏾‍♂️
return

;1F9B8 1F3FE 200D 2642
:*::man-superhero:-medium-dark-skin-tone::
Send,🦸🏾‍♂
return

;1F9B8 1F3FF 200D 2642 FE0F
:*::man-superhero:-dark-skin-tone::
Send,🦸🏿‍♂️
return

;1F9B8 1F3FF 200D 2642
:*::man-superhero:-dark-skin-tone::
Send,🦸🏿‍♂
return

;1F9B8 200D 2640 FE0F
:*::woman-superhero::
Send,🦸‍♀️
return

;1F9B8 200D 2640
:*::woman-superhero::
Send,🦸‍♀
return

;1F9B8 1F3FB 200D 2640 FE0F
:*::woman-superhero:-light-skin-tone::
Send,🦸🏻‍♀️
return

;1F9B8 1F3FB 200D 2640
:*::woman-superhero:-light-skin-tone::
Send,🦸🏻‍♀
return

;1F9B8 1F3FC 200D 2640 FE0F
:*::woman-superhero:-medium-light-skin-tone::
Send,🦸🏼‍♀️
return

;1F9B8 1F3FC 200D 2640
:*::woman-superhero:-medium-light-skin-tone::
Send,🦸🏼‍♀
return

;1F9B8 1F3FD 200D 2640 FE0F
:*::woman-superhero:-medium-skin-tone::
Send,🦸🏽‍♀️
return

;1F9B8 1F3FD 200D 2640
:*::woman-superhero:-medium-skin-tone::
Send,🦸🏽‍♀
return

;1F9B8 1F3FE 200D 2640 FE0F
:*::woman-superhero:-medium-dark-skin-tone::
Send,🦸🏾‍♀️
return

;1F9B8 1F3FE 200D 2640
:*::woman-superhero:-medium-dark-skin-tone::
Send,🦸🏾‍♀
return

;1F9B8 1F3FF 200D 2640 FE0F
:*::woman-superhero:-dark-skin-tone::
Send,🦸🏿‍♀️
return

;1F9B8 1F3FF 200D 2640
:*::woman-superhero:-dark-skin-tone::
Send,🦸🏿‍♀
return

;1F9B9
:*::supervillain::
Send,🦹
return

;1F9B9 1F3FB
:*::supervillain:-light-skin-tone::
Send,🦹🏻
return

;1F9B9 1F3FC
:*::supervillain:-medium-light-skin-tone::
Send,🦹🏼
return

;1F9B9 1F3FD
:*::supervillain:-medium-skin-tone::
Send,🦹🏽
return

;1F9B9 1F3FE
:*::supervillain:-medium-dark-skin-tone::
Send,🦹🏾
return

;1F9B9 1F3FF
:*::supervillain:-dark-skin-tone::
Send,🦹🏿
return

;1F9B9 200D 2642 FE0F
:*::man-supervillain::
Send,🦹‍♂️
return

;1F9B9 200D 2642
:*::man-supervillain::
Send,🦹‍♂
return

;1F9B9 1F3FB 200D 2642 FE0F
:*::man-supervillain:-light-skin-tone::
Send,🦹🏻‍♂️
return

;1F9B9 1F3FB 200D 2642
:*::man-supervillain:-light-skin-tone::
Send,🦹🏻‍♂
return

;1F9B9 1F3FC 200D 2642 FE0F
:*::man-supervillain:-medium-light-skin-ton::
Send,🦹🏼‍♂️
return

;1F9B9 1F3FC 200D 2642
:*::man-supervillain:-medium-light-skin-ton::
Send,🦹🏼‍♂
return

;1F9B9 1F3FD 200D 2642 FE0F
:*::man-supervillain:-medium-skin-tone::
Send,🦹🏽‍♂️
return

;1F9B9 1F3FD 200D 2642
:*::man-supervillain:-medium-skin-tone::
Send,🦹🏽‍♂
return

;1F9B9 1F3FE 200D 2642 FE0F
:*::man-supervillain:-medium-dark-skin-tone::
Send,🦹🏾‍♂️
return

;1F9B9 1F3FE 200D 2642
:*::man-supervillain:-medium-dark-skin-tone::
Send,🦹🏾‍♂
return

;1F9B9 1F3FF 200D 2642 FE0F
:*::man-supervillain:-dark-skin-tone::
Send,🦹🏿‍♂️
return

;1F9B9 1F3FF 200D 2642
:*::man-supervillain:-dark-skin-tone::
Send,🦹🏿‍♂
return

;1F9B9 200D 2640 FE0F
:*::woman-supervillain::
Send,🦹‍♀️
return

;1F9B9 200D 2640
:*::woman-supervillain::
Send,🦹‍♀
return

;1F9B9 1F3FB 200D 2640 FE0F
:*::woman-supervillain:-light-skin-tone::
Send,🦹🏻‍♀️
return

;1F9B9 1F3FB 200D 2640
:*::woman-supervillain:-light-skin-tone::
Send,🦹🏻‍♀
return

;1F9B9 1F3FC 200D 2640 FE0F
:*::woman-supervillain:-medium-light-skin-t::
Send,🦹🏼‍♀️
return

;1F9B9 1F3FC 200D 2640
:*::woman-supervillain:-medium-light-skin-t::
Send,🦹🏼‍♀
return

;1F9B9 1F3FD 200D 2640 FE0F
:*::woman-supervillain:-medium-skin-tone::
Send,🦹🏽‍♀️
return

;1F9B9 1F3FD 200D 2640
:*::woman-supervillain:-medium-skin-tone::
Send,🦹🏽‍♀
return

;1F9B9 1F3FE 200D 2640 FE0F
:*::woman-supervillain:-medium-dark-skin-to::
Send,🦹🏾‍♀️
return

;1F9B9 1F3FE 200D 2640
:*::woman-supervillain:-medium-dark-skin-to::
Send,🦹🏾‍♀
return

;1F9B9 1F3FF 200D 2640 FE0F
:*::woman-supervillain:-dark-skin-tone::
Send,🦹🏿‍♀️
return

;1F9B9 1F3FF 200D 2640
:*::woman-supervillain:-dark-skin-tone::
Send,🦹🏿‍♀
return

;1F9D9
:*::mage::
Send,🧙
return

;1F9D9 1F3FB
:*::mage:-light-skin-tone::
Send,🧙🏻
return

;1F9D9 1F3FC
:*::mage:-medium-light-skin-tone::
Send,🧙🏼
return

;1F9D9 1F3FD
:*::mage:-medium-skin-tone::
Send,🧙🏽
return

;1F9D9 1F3FE
:*::mage:-medium-dark-skin-tone::
Send,🧙🏾
return

;1F9D9 1F3FF
:*::mage:-dark-skin-tone::
Send,🧙🏿
return

;1F9D9 200D 2642 FE0F
:*::man-mage::
Send,🧙‍♂️
return

;1F9D9 200D 2642
:*::man-mage::
Send,🧙‍♂
return

;1F9D9 1F3FB 200D 2642 FE0F
:*::man-mage:-light-skin-tone::
Send,🧙🏻‍♂️
return

;1F9D9 1F3FB 200D 2642
:*::man-mage:-light-skin-tone::
Send,🧙🏻‍♂
return

;1F9D9 1F3FC 200D 2642 FE0F
:*::man-mage:-medium-light-skin-tone::
Send,🧙🏼‍♂️
return

;1F9D9 1F3FC 200D 2642
:*::man-mage:-medium-light-skin-tone::
Send,🧙🏼‍♂
return

;1F9D9 1F3FD 200D 2642 FE0F
:*::man-mage:-medium-skin-tone::
Send,🧙🏽‍♂️
return

;1F9D9 1F3FD 200D 2642
:*::man-mage:-medium-skin-tone::
Send,🧙🏽‍♂
return

;1F9D9 1F3FE 200D 2642 FE0F
:*::man-mage:-medium-dark-skin-tone::
Send,🧙🏾‍♂️
return

;1F9D9 1F3FE 200D 2642
:*::man-mage:-medium-dark-skin-tone::
Send,🧙🏾‍♂
return

;1F9D9 1F3FF 200D 2642 FE0F
:*::man-mage:-dark-skin-tone::
Send,🧙🏿‍♂️
return

;1F9D9 1F3FF 200D 2642
:*::man-mage:-dark-skin-tone::
Send,🧙🏿‍♂
return

;1F9D9 200D 2640 FE0F
:*::woman-mage::
Send,🧙‍♀️
return

;1F9D9 200D 2640
:*::woman-mage::
Send,🧙‍♀
return

;1F9D9 1F3FB 200D 2640 FE0F
:*::woman-mage:-light-skin-tone::
Send,🧙🏻‍♀️
return

;1F9D9 1F3FB 200D 2640
:*::woman-mage:-light-skin-tone::
Send,🧙🏻‍♀
return

;1F9D9 1F3FC 200D 2640 FE0F
:*::woman-mage:-medium-light-skin-tone::
Send,🧙🏼‍♀️
return

;1F9D9 1F3FC 200D 2640
:*::woman-mage:-medium-light-skin-tone::
Send,🧙🏼‍♀
return

;1F9D9 1F3FD 200D 2640 FE0F
:*::woman-mage:-medium-skin-tone::
Send,🧙🏽‍♀️
return

;1F9D9 1F3FD 200D 2640
:*::woman-mage:-medium-skin-tone::
Send,🧙🏽‍♀
return

;1F9D9 1F3FE 200D 2640 FE0F
:*::woman-mage:-medium-dark-skin-tone::
Send,🧙🏾‍♀️
return

;1F9D9 1F3FE 200D 2640
:*::woman-mage:-medium-dark-skin-tone::
Send,🧙🏾‍♀
return

;1F9D9 1F3FF 200D 2640 FE0F
:*::woman-mage:-dark-skin-tone::
Send,🧙🏿‍♀️
return

;1F9D9 1F3FF 200D 2640
:*::woman-mage:-dark-skin-tone::
Send,🧙🏿‍♀
return

;1F9DA
:*::fairy::
Send,🧚
return

;1F9DA 1F3FB
:*::fairy:-light-skin-tone::
Send,🧚🏻
return

;1F9DA 1F3FC
:*::fairy:-medium-light-skin-tone::
Send,🧚🏼
return

;1F9DA 1F3FD
:*::fairy:-medium-skin-tone::
Send,🧚🏽
return

;1F9DA 1F3FE
:*::fairy:-medium-dark-skin-tone::
Send,🧚🏾
return

;1F9DA 1F3FF
:*::fairy:-dark-skin-tone::
Send,🧚🏿
return

;1F9DA 200D 2642 FE0F
:*::man-fairy::
Send,🧚‍♂️
return

;1F9DA 200D 2642
:*::man-fairy::
Send,🧚‍♂
return

;1F9DA 1F3FB 200D 2642 FE0F
:*::man-fairy:-light-skin-tone::
Send,🧚🏻‍♂️
return

;1F9DA 1F3FB 200D 2642
:*::man-fairy:-light-skin-tone::
Send,🧚🏻‍♂
return

;1F9DA 1F3FC 200D 2642 FE0F
:*::man-fairy:-medium-light-skin-tone::
Send,🧚🏼‍♂️
return

;1F9DA 1F3FC 200D 2642
:*::man-fairy:-medium-light-skin-tone::
Send,🧚🏼‍♂
return

;1F9DA 1F3FD 200D 2642 FE0F
:*::man-fairy:-medium-skin-tone::
Send,🧚🏽‍♂️
return

;1F9DA 1F3FD 200D 2642
:*::man-fairy:-medium-skin-tone::
Send,🧚🏽‍♂
return

;1F9DA 1F3FE 200D 2642 FE0F
:*::man-fairy:-medium-dark-skin-tone::
Send,🧚🏾‍♂️
return

;1F9DA 1F3FE 200D 2642
:*::man-fairy:-medium-dark-skin-tone::
Send,🧚🏾‍♂
return

;1F9DA 1F3FF 200D 2642 FE0F
:*::man-fairy:-dark-skin-tone::
Send,🧚🏿‍♂️
return

;1F9DA 1F3FF 200D 2642
:*::man-fairy:-dark-skin-tone::
Send,🧚🏿‍♂
return

;1F9DA 200D 2640 FE0F
:*::woman-fairy::
Send,🧚‍♀️
return

;1F9DA 200D 2640
:*::woman-fairy::
Send,🧚‍♀
return

;1F9DA 1F3FB 200D 2640 FE0F
:*::woman-fairy:-light-skin-tone::
Send,🧚🏻‍♀️
return

;1F9DA 1F3FB 200D 2640
:*::woman-fairy:-light-skin-tone::
Send,🧚🏻‍♀
return

;1F9DA 1F3FC 200D 2640 FE0F
:*::woman-fairy:-medium-light-skin-tone::
Send,🧚🏼‍♀️
return

;1F9DA 1F3FC 200D 2640
:*::woman-fairy:-medium-light-skin-tone::
Send,🧚🏼‍♀
return

;1F9DA 1F3FD 200D 2640 FE0F
:*::woman-fairy:-medium-skin-tone::
Send,🧚🏽‍♀️
return

;1F9DA 1F3FD 200D 2640
:*::woman-fairy:-medium-skin-tone::
Send,🧚🏽‍♀
return

;1F9DA 1F3FE 200D 2640 FE0F
:*::woman-fairy:-medium-dark-skin-tone::
Send,🧚🏾‍♀️
return

;1F9DA 1F3FE 200D 2640
:*::woman-fairy:-medium-dark-skin-tone::
Send,🧚🏾‍♀
return

;1F9DA 1F3FF 200D 2640 FE0F
:*::woman-fairy:-dark-skin-tone::
Send,🧚🏿‍♀️
return

;1F9DA 1F3FF 200D 2640
:*::woman-fairy:-dark-skin-tone::
Send,🧚🏿‍♀
return

;1F9DB
:*::vampire::
Send,🧛
return

;1F9DB 1F3FB
:*::vampire:-light-skin-tone::
Send,🧛🏻
return

;1F9DB 1F3FC
:*::vampire:-medium-light-skin-tone::
Send,🧛🏼
return

;1F9DB 1F3FD
:*::vampire:-medium-skin-tone::
Send,🧛🏽
return

;1F9DB 1F3FE
:*::vampire:-medium-dark-skin-tone::
Send,🧛🏾
return

;1F9DB 1F3FF
:*::vampire:-dark-skin-tone::
Send,🧛🏿
return

;1F9DB 200D 2642 FE0F
:*::man-vampire::
Send,🧛‍♂️
return

;1F9DB 200D 2642
:*::man-vampire::
Send,🧛‍♂
return

;1F9DB 1F3FB 200D 2642 FE0F
:*::man-vampire:-light-skin-tone::
Send,🧛🏻‍♂️
return

;1F9DB 1F3FB 200D 2642
:*::man-vampire:-light-skin-tone::
Send,🧛🏻‍♂
return

;1F9DB 1F3FC 200D 2642 FE0F
:*::man-vampire:-medium-light-skin-tone::
Send,🧛🏼‍♂️
return

;1F9DB 1F3FC 200D 2642
:*::man-vampire:-medium-light-skin-tone::
Send,🧛🏼‍♂
return

;1F9DB 1F3FD 200D 2642 FE0F
:*::man-vampire:-medium-skin-tone::
Send,🧛🏽‍♂️
return

;1F9DB 1F3FD 200D 2642
:*::man-vampire:-medium-skin-tone::
Send,🧛🏽‍♂
return

;1F9DB 1F3FE 200D 2642 FE0F
:*::man-vampire:-medium-dark-skin-tone::
Send,🧛🏾‍♂️
return

;1F9DB 1F3FE 200D 2642
:*::man-vampire:-medium-dark-skin-tone::
Send,🧛🏾‍♂
return

;1F9DB 1F3FF 200D 2642 FE0F
:*::man-vampire:-dark-skin-tone::
Send,🧛🏿‍♂️
return

;1F9DB 1F3FF 200D 2642
:*::man-vampire:-dark-skin-tone::
Send,🧛🏿‍♂
return

;1F9DB 200D 2640 FE0F
:*::woman-vampire::
Send,🧛‍♀️
return

;1F9DB 200D 2640
:*::woman-vampire::
Send,🧛‍♀
return

;1F9DB 1F3FB 200D 2640 FE0F
:*::woman-vampire:-light-skin-tone::
Send,🧛🏻‍♀️
return

;1F9DB 1F3FB 200D 2640
:*::woman-vampire:-light-skin-tone::
Send,🧛🏻‍♀
return

;1F9DB 1F3FC 200D 2640 FE0F
:*::woman-vampire:-medium-light-skin-tone::
Send,🧛🏼‍♀️
return

;1F9DB 1F3FC 200D 2640
:*::woman-vampire:-medium-light-skin-tone::
Send,🧛🏼‍♀
return

;1F9DB 1F3FD 200D 2640 FE0F
:*::woman-vampire:-medium-skin-tone::
Send,🧛🏽‍♀️
return

;1F9DB 1F3FD 200D 2640
:*::woman-vampire:-medium-skin-tone::
Send,🧛🏽‍♀
return

;1F9DB 1F3FE 200D 2640 FE0F
:*::woman-vampire:-medium-dark-skin-tone::
Send,🧛🏾‍♀️
return

;1F9DB 1F3FE 200D 2640
:*::woman-vampire:-medium-dark-skin-tone::
Send,🧛🏾‍♀
return

;1F9DB 1F3FF 200D 2640 FE0F
:*::woman-vampire:-dark-skin-tone::
Send,🧛🏿‍♀️
return

;1F9DB 1F3FF 200D 2640
:*::woman-vampire:-dark-skin-tone::
Send,🧛🏿‍♀
return

;1F9DC
:*::merperson::
Send,🧜
return

;1F9DC 1F3FB
:*::merperson:-light-skin-tone::
Send,🧜🏻
return

;1F9DC 1F3FC
:*::merperson:-medium-light-skin-tone::
Send,🧜🏼
return

;1F9DC 1F3FD
:*::merperson:-medium-skin-tone::
Send,🧜🏽
return

;1F9DC 1F3FE
:*::merperson:-medium-dark-skin-tone::
Send,🧜🏾
return

;1F9DC 1F3FF
:*::merperson:-dark-skin-tone::
Send,🧜🏿
return

;1F9DC 200D 2642 FE0F
:*::merman::
Send,🧜‍♂️
return

;1F9DC 200D 2642
:*::merman::
Send,🧜‍♂
return

;1F9DC 1F3FB 200D 2642 FE0F
:*::merman:-light-skin-tone::
Send,🧜🏻‍♂️
return

;1F9DC 1F3FB 200D 2642
:*::merman:-light-skin-tone::
Send,🧜🏻‍♂
return

;1F9DC 1F3FC 200D 2642 FE0F
:*::merman:-medium-light-skin-tone::
Send,🧜🏼‍♂️
return

;1F9DC 1F3FC 200D 2642
:*::merman:-medium-light-skin-tone::
Send,🧜🏼‍♂
return

;1F9DC 1F3FD 200D 2642 FE0F
:*::merman:-medium-skin-tone::
Send,🧜🏽‍♂️
return

;1F9DC 1F3FD 200D 2642
:*::merman:-medium-skin-tone::
Send,🧜🏽‍♂
return

;1F9DC 1F3FE 200D 2642 FE0F
:*::merman:-medium-dark-skin-tone::
Send,🧜🏾‍♂️
return

;1F9DC 1F3FE 200D 2642
:*::merman:-medium-dark-skin-tone::
Send,🧜🏾‍♂
return

;1F9DC 1F3FF 200D 2642 FE0F
:*::merman:-dark-skin-tone::
Send,🧜🏿‍♂️
return

;1F9DC 1F3FF 200D 2642
:*::merman:-dark-skin-tone::
Send,🧜🏿‍♂
return

;1F9DC 200D 2640 FE0F
:*::mermaid::
Send,🧜‍♀️
return

;1F9DC 200D 2640
:*::mermaid::
Send,🧜‍♀
return

;1F9DC 1F3FB 200D 2640 FE0F
:*::mermaid:-light-skin-tone::
Send,🧜🏻‍♀️
return

;1F9DC 1F3FB 200D 2640
:*::mermaid:-light-skin-tone::
Send,🧜🏻‍♀
return

;1F9DC 1F3FC 200D 2640 FE0F
:*::mermaid:-medium-light-skin-tone::
Send,🧜🏼‍♀️
return

;1F9DC 1F3FC 200D 2640
:*::mermaid:-medium-light-skin-tone::
Send,🧜🏼‍♀
return

;1F9DC 1F3FD 200D 2640 FE0F
:*::mermaid:-medium-skin-tone::
Send,🧜🏽‍♀️
return

;1F9DC 1F3FD 200D 2640
:*::mermaid:-medium-skin-tone::
Send,🧜🏽‍♀
return

;1F9DC 1F3FE 200D 2640 FE0F
:*::mermaid:-medium-dark-skin-tone::
Send,🧜🏾‍♀️
return

;1F9DC 1F3FE 200D 2640
:*::mermaid:-medium-dark-skin-tone::
Send,🧜🏾‍♀
return

;1F9DC 1F3FF 200D 2640 FE0F
:*::mermaid:-dark-skin-tone::
Send,🧜🏿‍♀️
return

;1F9DC 1F3FF 200D 2640
:*::mermaid:-dark-skin-tone::
Send,🧜🏿‍♀
return

;1F9DD
:*::elf::
Send,🧝
return

;1F9DD 1F3FB
:*::elf:-light-skin-tone::
Send,🧝🏻
return

;1F9DD 1F3FC
:*::elf:-medium-light-skin-tone::
Send,🧝🏼
return

;1F9DD 1F3FD
:*::elf:-medium-skin-tone::
Send,🧝🏽
return

;1F9DD 1F3FE
:*::elf:-medium-dark-skin-tone::
Send,🧝🏾
return

;1F9DD 1F3FF
:*::elf:-dark-skin-tone::
Send,🧝🏿
return

;1F9DD 200D 2642 FE0F
:*::man-elf::
Send,🧝‍♂️
return

;1F9DD 200D 2642
:*::man-elf::
Send,🧝‍♂
return

;1F9DD 1F3FB 200D 2642 FE0F
:*::man-elf:-light-skin-tone::
Send,🧝🏻‍♂️
return

;1F9DD 1F3FB 200D 2642
:*::man-elf:-light-skin-tone::
Send,🧝🏻‍♂
return

;1F9DD 1F3FC 200D 2642 FE0F
:*::man-elf:-medium-light-skin-tone::
Send,🧝🏼‍♂️
return

;1F9DD 1F3FC 200D 2642
:*::man-elf:-medium-light-skin-tone::
Send,🧝🏼‍♂
return

;1F9DD 1F3FD 200D 2642 FE0F
:*::man-elf:-medium-skin-tone::
Send,🧝🏽‍♂️
return

;1F9DD 1F3FD 200D 2642
:*::man-elf:-medium-skin-tone::
Send,🧝🏽‍♂
return

;1F9DD 1F3FE 200D 2642 FE0F
:*::man-elf:-medium-dark-skin-tone::
Send,🧝🏾‍♂️
return

;1F9DD 1F3FE 200D 2642
:*::man-elf:-medium-dark-skin-tone::
Send,🧝🏾‍♂
return

;1F9DD 1F3FF 200D 2642 FE0F
:*::man-elf:-dark-skin-tone::
Send,🧝🏿‍♂️
return

;1F9DD 1F3FF 200D 2642
:*::man-elf:-dark-skin-tone::
Send,🧝🏿‍♂
return

;1F9DD 200D 2640 FE0F
:*::woman-elf::
Send,🧝‍♀️
return

;1F9DD 200D 2640
:*::woman-elf::
Send,🧝‍♀
return

;1F9DD 1F3FB 200D 2640 FE0F
:*::woman-elf:-light-skin-tone::
Send,🧝🏻‍♀️
return

;1F9DD 1F3FB 200D 2640
:*::woman-elf:-light-skin-tone::
Send,🧝🏻‍♀
return

;1F9DD 1F3FC 200D 2640 FE0F
:*::woman-elf:-medium-light-skin-tone::
Send,🧝🏼‍♀️
return

;1F9DD 1F3FC 200D 2640
:*::woman-elf:-medium-light-skin-tone::
Send,🧝🏼‍♀
return

;1F9DD 1F3FD 200D 2640 FE0F
:*::woman-elf:-medium-skin-tone::
Send,🧝🏽‍♀️
return

;1F9DD 1F3FD 200D 2640
:*::woman-elf:-medium-skin-tone::
Send,🧝🏽‍♀
return

;1F9DD 1F3FE 200D 2640 FE0F
:*::woman-elf:-medium-dark-skin-tone::
Send,🧝🏾‍♀️
return

;1F9DD 1F3FE 200D 2640
:*::woman-elf:-medium-dark-skin-tone::
Send,🧝🏾‍♀
return

;1F9DD 1F3FF 200D 2640 FE0F
:*::woman-elf:-dark-skin-tone::
Send,🧝🏿‍♀️
return

;1F9DD 1F3FF 200D 2640
:*::woman-elf:-dark-skin-tone::
Send,🧝🏿‍♀
return

;1F9DE
:*::genie::
Send,🧞
return

;1F9DE 200D 2642 FE0F
:*::man-genie::
Send,🧞‍♂️
return

;1F9DE 200D 2642
:*::man-genie::
Send,🧞‍♂
return

;1F9DE 200D 2640 FE0F
:*::woman-genie::
Send,🧞‍♀️
return

;1F9DE 200D 2640
:*::woman-genie::
Send,🧞‍♀
return

;1F9DF
:*::zombie::
Send,🧟
return

;1F9DF 200D 2642 FE0F
:*::man-zombie::
Send,🧟‍♂️
return

;1F9DF 200D 2642
:*::man-zombie::
Send,🧟‍♂
return

;1F9DF 200D 2640 FE0F
:*::woman-zombie::
Send,🧟‍♀️
return

;1F9DF 200D 2640
:*::woman-zombie::
Send,🧟‍♀
return

;1F486
:*::person-getting-massage::
Send,💆
return

;1F486 1F3FB
:*::person-getting-massage:-light-skin-tone::
Send,💆🏻
return

;1F486 1F3FC
:*::person-getting-massage:-medium-light-sk::
Send,💆🏼
return

;1F486 1F3FD
:*::person-getting-massage:-medium-skin-ton::
Send,💆🏽
return

;1F486 1F3FE
:*::person-getting-massage:-medium-dark-ski::
Send,💆🏾
return

;1F486 1F3FF
:*::person-getting-massage:-dark-skin-tone::
Send,💆🏿
return

;1F486 200D 2642 FE0F
:*::man-getting-massage::
Send,💆‍♂️
return

;1F486 200D 2642
:*::man-getting-massage::
Send,💆‍♂
return

;1F486 1F3FB 200D 2642 FE0F
:*::man-getting-massage:-light-skin-tone::
Send,💆🏻‍♂️
return

;1F486 1F3FB 200D 2642
:*::man-getting-massage:-light-skin-tone::
Send,💆🏻‍♂
return

;1F486 1F3FC 200D 2642 FE0F
:*::man-getting-massage:-medium-light-skin::
Send,💆🏼‍♂️
return

;1F486 1F3FC 200D 2642
:*::man-getting-massage:-medium-light-skin::
Send,💆🏼‍♂
return

;1F486 1F3FD 200D 2642 FE0F
:*::man-getting-massage:-medium-skin-tone::
Send,💆🏽‍♂️
return

;1F486 1F3FD 200D 2642
:*::man-getting-massage:-medium-skin-tone::
Send,💆🏽‍♂
return

;1F486 1F3FE 200D 2642 FE0F
:*::man-getting-massage:-medium-dark-skin-t::
Send,💆🏾‍♂️
return

;1F486 1F3FE 200D 2642
:*::man-getting-massage:-medium-dark-skin-t::
Send,💆🏾‍♂
return

;1F486 1F3FF 200D 2642 FE0F
:*::man-getting-massage:-dark-skin-tone::
Send,💆🏿‍♂️
return

;1F486 1F3FF 200D 2642
:*::man-getting-massage:-dark-skin-tone::
Send,💆🏿‍♂
return

;1F486 200D 2640 FE0F
:*::woman-getting-massage::
Send,💆‍♀️
return

;1F486 200D 2640
:*::woman-getting-massage::
Send,💆‍♀
return

;1F486 1F3FB 200D 2640 FE0F
:*::woman-getting-massage:-light-skin-tone::
Send,💆🏻‍♀️
return

;1F486 1F3FB 200D 2640
:*::woman-getting-massage:-light-skin-tone::
Send,💆🏻‍♀
return

;1F486 1F3FC 200D 2640 FE0F
:*::woman-getting-massage:-medium-light-ski::
Send,💆🏼‍♀️
return

;1F486 1F3FC 200D 2640
:*::woman-getting-massage:-medium-light-ski::
Send,💆🏼‍♀
return

;1F486 1F3FD 200D 2640 FE0F
:*::woman-getting-massage:-medium-skin-tone::
Send,💆🏽‍♀️
return

;1F486 1F3FD 200D 2640
:*::woman-getting-massage:-medium-skin-tone::
Send,💆🏽‍♀
return

;1F486 1F3FE 200D 2640 FE0F
:*::woman-getting-massage:-medium-dark-skin::
Send,💆🏾‍♀️
return

;1F486 1F3FE 200D 2640
:*::woman-getting-massage:-medium-dark-skin::
Send,💆🏾‍♀
return

;1F486 1F3FF 200D 2640 FE0F
:*::woman-getting-massage:-dark-skin-tone::
Send,💆🏿‍♀️
return

;1F486 1F3FF 200D 2640
:*::woman-getting-massage:-dark-skin-tone::
Send,💆🏿‍♀
return

;1F487
:*::person-getting-haircut::
Send,💇
return

;1F487 1F3FB
:*::person-getting-haircut:-light-skin-tone::
Send,💇🏻
return

;1F487 1F3FC
:*::person-getting-haircut:-medium-light-sk::
Send,💇🏼
return

;1F487 1F3FD
:*::person-getting-haircut:-medium-skin-ton::
Send,💇🏽
return

;1F487 1F3FE
:*::person-getting-haircut:-medium-dark-ski::
Send,💇🏾
return

;1F487 1F3FF
:*::person-getting-haircut:-dark-skin-tone::
Send,💇🏿
return

;1F487 200D 2642 FE0F
:*::man-getting-haircut::
Send,💇‍♂️
return

;1F487 200D 2642
:*::man-getting-haircut::
Send,💇‍♂
return

;1F487 1F3FB 200D 2642 FE0F
:*::man-getting-haircut:-light-skin-tone::
Send,💇🏻‍♂️
return

;1F487 1F3FB 200D 2642
:*::man-getting-haircut:-light-skin-tone::
Send,💇🏻‍♂
return

;1F487 1F3FC 200D 2642 FE0F
:*::man-getting-haircut:-medium-light-skin::
Send,💇🏼‍♂️
return

;1F487 1F3FC 200D 2642
:*::man-getting-haircut:-medium-light-skin::
Send,💇🏼‍♂
return

;1F487 1F3FD 200D 2642 FE0F
:*::man-getting-haircut:-medium-skin-tone::
Send,💇🏽‍♂️
return

;1F487 1F3FD 200D 2642
:*::man-getting-haircut:-medium-skin-tone::
Send,💇🏽‍♂
return

;1F487 1F3FE 200D 2642 FE0F
:*::man-getting-haircut:-medium-dark-skin-t::
Send,💇🏾‍♂️
return

;1F487 1F3FE 200D 2642
:*::man-getting-haircut:-medium-dark-skin-t::
Send,💇🏾‍♂
return

;1F487 1F3FF 200D 2642 FE0F
:*::man-getting-haircut:-dark-skin-tone::
Send,💇🏿‍♂️
return

;1F487 1F3FF 200D 2642
:*::man-getting-haircut:-dark-skin-tone::
Send,💇🏿‍♂
return

;1F487 200D 2640 FE0F
:*::woman-getting-haircut::
Send,💇‍♀️
return

;1F487 200D 2640
:*::woman-getting-haircut::
Send,💇‍♀
return

;1F487 1F3FB 200D 2640 FE0F
:*::woman-getting-haircut:-light-skin-tone::
Send,💇🏻‍♀️
return

;1F487 1F3FB 200D 2640
:*::woman-getting-haircut:-light-skin-tone::
Send,💇🏻‍♀
return

;1F487 1F3FC 200D 2640 FE0F
:*::woman-getting-haircut:-medium-light-ski::
Send,💇🏼‍♀️
return

;1F487 1F3FC 200D 2640
:*::woman-getting-haircut:-medium-light-ski::
Send,💇🏼‍♀
return

;1F487 1F3FD 200D 2640 FE0F
:*::woman-getting-haircut:-medium-skin-tone::
Send,💇🏽‍♀️
return

;1F487 1F3FD 200D 2640
:*::woman-getting-haircut:-medium-skin-tone::
Send,💇🏽‍♀
return

;1F487 1F3FE 200D 2640 FE0F
:*::woman-getting-haircut:-medium-dark-skin::
Send,💇🏾‍♀️
return

;1F487 1F3FE 200D 2640
:*::woman-getting-haircut:-medium-dark-skin::
Send,💇🏾‍♀
return

;1F487 1F3FF 200D 2640 FE0F
:*::woman-getting-haircut:-dark-skin-tone::
Send,💇🏿‍♀️
return

;1F487 1F3FF 200D 2640
:*::woman-getting-haircut:-dark-skin-tone::
Send,💇🏿‍♀
return

;1F6B6
:*::person-walking::
Send,🚶
return

;1F6B6 1F3FB
:*::person-walking:-light-skin-tone::
Send,🚶🏻
return

;1F6B6 1F3FC
:*::person-walking:-medium-light-skin-tone::
Send,🚶🏼
return

;1F6B6 1F3FD
:*::person-walking:-medium-skin-tone::
Send,🚶🏽
return

;1F6B6 1F3FE
:*::person-walking:-medium-dark-skin-tone::
Send,🚶🏾
return

;1F6B6 1F3FF
:*::person-walking:-dark-skin-tone::
Send,🚶🏿
return

;1F6B6 200D 2642 FE0F
:*::man-walking::
Send,🚶‍♂️
return

;1F6B6 200D 2642
:*::man-walking::
Send,🚶‍♂
return

;1F6B6 1F3FB 200D 2642 FE0F
:*::man-walking:-light-skin-tone::
Send,🚶🏻‍♂️
return

;1F6B6 1F3FB 200D 2642
:*::man-walking:-light-skin-tone::
Send,🚶🏻‍♂
return

;1F6B6 1F3FC 200D 2642 FE0F
:*::man-walking:-medium-light-skin-tone::
Send,🚶🏼‍♂️
return

;1F6B6 1F3FC 200D 2642
:*::man-walking:-medium-light-skin-tone::
Send,🚶🏼‍♂
return

;1F6B6 1F3FD 200D 2642 FE0F
:*::man-walking:-medium-skin-tone::
Send,🚶🏽‍♂️
return

;1F6B6 1F3FD 200D 2642
:*::man-walking:-medium-skin-tone::
Send,🚶🏽‍♂
return

;1F6B6 1F3FE 200D 2642 FE0F
:*::man-walking:-medium-dark-skin-tone::
Send,🚶🏾‍♂️
return

;1F6B6 1F3FE 200D 2642
:*::man-walking:-medium-dark-skin-tone::
Send,🚶🏾‍♂
return

;1F6B6 1F3FF 200D 2642 FE0F
:*::man-walking:-dark-skin-tone::
Send,🚶🏿‍♂️
return

;1F6B6 1F3FF 200D 2642
:*::man-walking:-dark-skin-tone::
Send,🚶🏿‍♂
return

;1F6B6 200D 2640 FE0F
:*::woman-walking::
Send,🚶‍♀️
return

;1F6B6 200D 2640
:*::woman-walking::
Send,🚶‍♀
return

;1F6B6 1F3FB 200D 2640 FE0F
:*::woman-walking:-light-skin-tone::
Send,🚶🏻‍♀️
return

;1F6B6 1F3FB 200D 2640
:*::woman-walking:-light-skin-tone::
Send,🚶🏻‍♀
return

;1F6B6 1F3FC 200D 2640 FE0F
:*::woman-walking:-medium-light-skin-tone::
Send,🚶🏼‍♀️
return

;1F6B6 1F3FC 200D 2640
:*::woman-walking:-medium-light-skin-tone::
Send,🚶🏼‍♀
return

;1F6B6 1F3FD 200D 2640 FE0F
:*::woman-walking:-medium-skin-tone::
Send,🚶🏽‍♀️
return

;1F6B6 1F3FD 200D 2640
:*::woman-walking:-medium-skin-tone::
Send,🚶🏽‍♀
return

;1F6B6 1F3FE 200D 2640 FE0F
:*::woman-walking:-medium-dark-skin-tone::
Send,🚶🏾‍♀️
return

;1F6B6 1F3FE 200D 2640
:*::woman-walking:-medium-dark-skin-tone::
Send,🚶🏾‍♀
return

;1F6B6 1F3FF 200D 2640 FE0F
:*::woman-walking:-dark-skin-tone::
Send,🚶🏿‍♀️
return

;1F6B6 1F3FF 200D 2640
:*::woman-walking:-dark-skin-tone::
Send,🚶🏿‍♀
return

;1F9CD
:*::person-standing::
Send,🧍
return

;1F9CD 1F3FB
:*::person-standing:-light-skin-tone::
Send,🧍🏻
return

;1F9CD 1F3FC
:*::person-standing:-medium-light-skin-tone::
Send,🧍🏼
return

;1F9CD 1F3FD
:*::person-standing:-medium-skin-tone::
Send,🧍🏽
return

;1F9CD 1F3FE
:*::person-standing:-medium-dark-skin-tone::
Send,🧍🏾
return

;1F9CD 1F3FF
:*::person-standing:-dark-skin-tone::
Send,🧍🏿
return

;1F9CD 200D 2642 FE0F
:*::man-standing::
Send,🧍‍♂️
return

;1F9CD 200D 2642
:*::man-standing::
Send,🧍‍♂
return

;1F9CD 1F3FB 200D 2642 FE0F
:*::man-standing:-light-skin-tone::
Send,🧍🏻‍♂️
return

;1F9CD 1F3FB 200D 2642
:*::man-standing:-light-skin-tone::
Send,🧍🏻‍♂
return

;1F9CD 1F3FC 200D 2642 FE0F
:*::man-standing:-medium-light-skin-tone::
Send,🧍🏼‍♂️
return

;1F9CD 1F3FC 200D 2642
:*::man-standing:-medium-light-skin-tone::
Send,🧍🏼‍♂
return

;1F9CD 1F3FD 200D 2642 FE0F
:*::man-standing:-medium-skin-tone::
Send,🧍🏽‍♂️
return

;1F9CD 1F3FD 200D 2642
:*::man-standing:-medium-skin-tone::
Send,🧍🏽‍♂
return

;1F9CD 1F3FE 200D 2642 FE0F
:*::man-standing:-medium-dark-skin-tone::
Send,🧍🏾‍♂️
return

;1F9CD 1F3FE 200D 2642
:*::man-standing:-medium-dark-skin-tone::
Send,🧍🏾‍♂
return

;1F9CD 1F3FF 200D 2642 FE0F
:*::man-standing:-dark-skin-tone::
Send,🧍🏿‍♂️
return

;1F9CD 1F3FF 200D 2642
:*::man-standing:-dark-skin-tone::
Send,🧍🏿‍♂
return

;1F9CD 200D 2640 FE0F
:*::woman-standing::
Send,🧍‍♀️
return

;1F9CD 200D 2640
:*::woman-standing::
Send,🧍‍♀
return

;1F9CD 1F3FB 200D 2640 FE0F
:*::woman-standing:-light-skin-tone::
Send,🧍🏻‍♀️
return

;1F9CD 1F3FB 200D 2640
:*::woman-standing:-light-skin-tone::
Send,🧍🏻‍♀
return

;1F9CD 1F3FC 200D 2640 FE0F
:*::woman-standing:-medium-light-skin-tone::
Send,🧍🏼‍♀️
return

;1F9CD 1F3FC 200D 2640
:*::woman-standing:-medium-light-skin-tone::
Send,🧍🏼‍♀
return

;1F9CD 1F3FD 200D 2640 FE0F
:*::woman-standing:-medium-skin-tone::
Send,🧍🏽‍♀️
return

;1F9CD 1F3FD 200D 2640
:*::woman-standing:-medium-skin-tone::
Send,🧍🏽‍♀
return

;1F9CD 1F3FE 200D 2640 FE0F
:*::woman-standing:-medium-dark-skin-tone::
Send,🧍🏾‍♀️
return

;1F9CD 1F3FE 200D 2640
:*::woman-standing:-medium-dark-skin-tone::
Send,🧍🏾‍♀
return

;1F9CD 1F3FF 200D 2640 FE0F
:*::woman-standing:-dark-skin-tone::
Send,🧍🏿‍♀️
return

;1F9CD 1F3FF 200D 2640
:*::woman-standing:-dark-skin-tone::
Send,🧍🏿‍♀
return

;1F9CE
:*::person-kneeling::
Send,🧎
return

;1F9CE 1F3FB
:*::person-kneeling:-light-skin-tone::
Send,🧎🏻
return

;1F9CE 1F3FC
:*::person-kneeling:-medium-light-skin-tone::
Send,🧎🏼
return

;1F9CE 1F3FD
:*::person-kneeling:-medium-skin-tone::
Send,🧎🏽
return

;1F9CE 1F3FE
:*::person-kneeling:-medium-dark-skin-tone::
Send,🧎🏾
return

;1F9CE 1F3FF
:*::person-kneeling:-dark-skin-tone::
Send,🧎🏿
return

;1F9CE 200D 2642 FE0F
:*::man-kneeling::
Send,🧎‍♂️
return

;1F9CE 200D 2642
:*::man-kneeling::
Send,🧎‍♂
return

;1F9CE 1F3FB 200D 2642 FE0F
:*::man-kneeling:-light-skin-tone::
Send,🧎🏻‍♂️
return

;1F9CE 1F3FB 200D 2642
:*::man-kneeling:-light-skin-tone::
Send,🧎🏻‍♂
return

;1F9CE 1F3FC 200D 2642 FE0F
:*::man-kneeling:-medium-light-skin-tone::
Send,🧎🏼‍♂️
return

;1F9CE 1F3FC 200D 2642
:*::man-kneeling:-medium-light-skin-tone::
Send,🧎🏼‍♂
return

;1F9CE 1F3FD 200D 2642 FE0F
:*::man-kneeling:-medium-skin-tone::
Send,🧎🏽‍♂️
return

;1F9CE 1F3FD 200D 2642
:*::man-kneeling:-medium-skin-tone::
Send,🧎🏽‍♂
return

;1F9CE 1F3FE 200D 2642 FE0F
:*::man-kneeling:-medium-dark-skin-tone::
Send,🧎🏾‍♂️
return

;1F9CE 1F3FE 200D 2642
:*::man-kneeling:-medium-dark-skin-tone::
Send,🧎🏾‍♂
return

;1F9CE 1F3FF 200D 2642 FE0F
:*::man-kneeling:-dark-skin-tone::
Send,🧎🏿‍♂️
return

;1F9CE 1F3FF 200D 2642
:*::man-kneeling:-dark-skin-tone::
Send,🧎🏿‍♂
return

;1F9CE 200D 2640 FE0F
:*::woman-kneeling::
Send,🧎‍♀️
return

;1F9CE 200D 2640
:*::woman-kneeling::
Send,🧎‍♀
return

;1F9CE 1F3FB 200D 2640 FE0F
:*::woman-kneeling:-light-skin-tone::
Send,🧎🏻‍♀️
return

;1F9CE 1F3FB 200D 2640
:*::woman-kneeling:-light-skin-tone::
Send,🧎🏻‍♀
return

;1F9CE 1F3FC 200D 2640 FE0F
:*::woman-kneeling:-medium-light-skin-tone::
Send,🧎🏼‍♀️
return

;1F9CE 1F3FC 200D 2640
:*::woman-kneeling:-medium-light-skin-tone::
Send,🧎🏼‍♀
return

;1F9CE 1F3FD 200D 2640 FE0F
:*::woman-kneeling:-medium-skin-tone::
Send,🧎🏽‍♀️
return

;1F9CE 1F3FD 200D 2640
:*::woman-kneeling:-medium-skin-tone::
Send,🧎🏽‍♀
return

;1F9CE 1F3FE 200D 2640 FE0F
:*::woman-kneeling:-medium-dark-skin-tone::
Send,🧎🏾‍♀️
return

;1F9CE 1F3FE 200D 2640
:*::woman-kneeling:-medium-dark-skin-tone::
Send,🧎🏾‍♀
return

;1F9CE 1F3FF 200D 2640 FE0F
:*::woman-kneeling:-dark-skin-tone::
Send,🧎🏿‍♀️
return

;1F9CE 1F3FF 200D 2640
:*::woman-kneeling:-dark-skin-tone::
Send,🧎🏿‍♀
return

;1F468 200D 1F9AF
:*::man-with-probing-cane::
Send,👨‍🦯
return

;1F468 1F3FB 200D 1F9AF
:*::man-with-probing-cane:-light-skin-tone::
Send,👨🏻‍🦯
return

;1F468 1F3FC 200D 1F9AF
:*::man-with-probing-cane:-medium-light-ski::
Send,👨🏼‍🦯
return

;1F468 1F3FD 200D 1F9AF
:*::man-with-probing-cane:-medium-skin-tone::
Send,👨🏽‍🦯
return

;1F468 1F3FE 200D 1F9AF
:*::man-with-probing-cane:-medium-dark-skin::
Send,👨🏾‍🦯
return

;1F468 1F3FF 200D 1F9AF
:*::man-with-probing-cane:-dark-skin-tone::
Send,👨🏿‍🦯
return

;1F469 200D 1F9AF
:*::woman-with-probing-cane::
Send,👩‍🦯
return

;1F469 1F3FB 200D 1F9AF
:*::woman-with-probing-cane:-light-skin-ton::
Send,👩🏻‍🦯
return

;1F469 1F3FC 200D 1F9AF
:*::woman-with-probing-cane:-medium-light-s::
Send,👩🏼‍🦯
return

;1F469 1F3FD 200D 1F9AF
:*::woman-with-probing-cane:-medium-skin-to::
Send,👩🏽‍🦯
return

;1F469 1F3FE 200D 1F9AF
:*::woman-with-probing-cane:-medium-dark-sk::
Send,👩🏾‍🦯
return

;1F469 1F3FF 200D 1F9AF
:*::woman-with-probing-cane:-dark-skin-tone::
Send,👩🏿‍🦯
return

;1F468 200D 1F9BC
:*::man-in-motorized-wheelchair::
Send,👨‍🦼
return

;1F468 1F3FB 200D 1F9BC
:*::man-in-motorized-wheelchair:-light-skin::
Send,👨🏻‍🦼
return

;1F468 1F3FC 200D 1F9BC
:*::man-in-motorized-wheelchair:-medium-lig::
Send,👨🏼‍🦼
return

;1F468 1F3FD 200D 1F9BC
:*::man-in-motorized-wheelchair:-medium-ski::
Send,👨🏽‍🦼
return

;1F468 1F3FE 200D 1F9BC
:*::man-in-motorized-wheelchair:-medium-dar::
Send,👨🏾‍🦼
return

;1F468 1F3FF 200D 1F9BC
:*::man-in-motorized-wheelchair:-dark-skin::
Send,👨🏿‍🦼
return

;1F469 200D 1F9BC
:*::woman-in-motorized-wheelchair::
Send,👩‍🦼
return

;1F469 1F3FB 200D 1F9BC
:*::woman-in-motorized-wheelchair:-light-sk::
Send,👩🏻‍🦼
return

;1F469 1F3FC 200D 1F9BC
:*::woman-in-motorized-wheelchair:-medium-l::
Send,👩🏼‍🦼
return

;1F469 1F3FD 200D 1F9BC
:*::woman-in-motorized-wheelchair:-medium-s::
Send,👩🏽‍🦼
return

;1F469 1F3FE 200D 1F9BC
:*::woman-in-motorized-wheelchair:-medium-d::
Send,👩🏾‍🦼
return

;1F469 1F3FF 200D 1F9BC
:*::woman-in-motorized-wheelchair:-dark-ski::
Send,👩🏿‍🦼
return

;1F468 200D 1F9BD
:*::man-in-manual-wheelchair::
Send,👨‍🦽
return

;1F468 1F3FB 200D 1F9BD
:*::man-in-manual-wheelchair:-light-skin-to::
Send,👨🏻‍🦽
return

;1F468 1F3FC 200D 1F9BD
:*::man-in-manual-wheelchair:-medium-light::
Send,👨🏼‍🦽
return

;1F468 1F3FD 200D 1F9BD
:*::man-in-manual-wheelchair:-medium-skin-t::
Send,👨🏽‍🦽
return

;1F468 1F3FE 200D 1F9BD
:*::man-in-manual-wheelchair:-medium-dark-s::
Send,👨🏾‍🦽
return

;1F468 1F3FF 200D 1F9BD
:*::man-in-manual-wheelchair:-dark-skin-ton::
Send,👨🏿‍🦽
return

;1F469 200D 1F9BD
:*::woman-in-manual-wheelchair::
Send,👩‍🦽
return

;1F469 1F3FB 200D 1F9BD
:*::woman-in-manual-wheelchair:-light-skin::
Send,👩🏻‍🦽
return

;1F469 1F3FC 200D 1F9BD
:*::woman-in-manual-wheelchair:-medium-ligh::
Send,👩🏼‍🦽
return

;1F469 1F3FD 200D 1F9BD
:*::woman-in-manual-wheelchair:-medium-skin::
Send,👩🏽‍🦽
return

;1F469 1F3FE 200D 1F9BD
:*::woman-in-manual-wheelchair:-medium-dark::
Send,👩🏾‍🦽
return

;1F469 1F3FF 200D 1F9BD
:*::woman-in-manual-wheelchair:-dark-skin-t::
Send,👩🏿‍🦽
return

;1F3C3
:*::person-running::
Send,🏃
return

;1F3C3 1F3FB
:*::person-running:-light-skin-tone::
Send,🏃🏻
return

;1F3C3 1F3FC
:*::person-running:-medium-light-skin-tone::
Send,🏃🏼
return

;1F3C3 1F3FD
:*::person-running:-medium-skin-tone::
Send,🏃🏽
return

;1F3C3 1F3FE
:*::person-running:-medium-dark-skin-tone::
Send,🏃🏾
return

;1F3C3 1F3FF
:*::person-running:-dark-skin-tone::
Send,🏃🏿
return

;1F3C3 200D 2642 FE0F
:*::man-running::
Send,🏃‍♂️
return

;1F3C3 200D 2642
:*::man-running::
Send,🏃‍♂
return

;1F3C3 1F3FB 200D 2642 FE0F
:*::man-running:-light-skin-tone::
Send,🏃🏻‍♂️
return

;1F3C3 1F3FB 200D 2642
:*::man-running:-light-skin-tone::
Send,🏃🏻‍♂
return

;1F3C3 1F3FC 200D 2642 FE0F
:*::man-running:-medium-light-skin-tone::
Send,🏃🏼‍♂️
return

;1F3C3 1F3FC 200D 2642
:*::man-running:-medium-light-skin-tone::
Send,🏃🏼‍♂
return

;1F3C3 1F3FD 200D 2642 FE0F
:*::man-running:-medium-skin-tone::
Send,🏃🏽‍♂️
return

;1F3C3 1F3FD 200D 2642
:*::man-running:-medium-skin-tone::
Send,🏃🏽‍♂
return

;1F3C3 1F3FE 200D 2642 FE0F
:*::man-running:-medium-dark-skin-tone::
Send,🏃🏾‍♂️
return

;1F3C3 1F3FE 200D 2642
:*::man-running:-medium-dark-skin-tone::
Send,🏃🏾‍♂
return

;1F3C3 1F3FF 200D 2642 FE0F
:*::man-running:-dark-skin-tone::
Send,🏃🏿‍♂️
return

;1F3C3 1F3FF 200D 2642
:*::man-running:-dark-skin-tone::
Send,🏃🏿‍♂
return

;1F3C3 200D 2640 FE0F
:*::woman-running::
Send,🏃‍♀️
return

;1F3C3 200D 2640
:*::woman-running::
Send,🏃‍♀
return

;1F3C3 1F3FB 200D 2640 FE0F
:*::woman-running:-light-skin-tone::
Send,🏃🏻‍♀️
return

;1F3C3 1F3FB 200D 2640
:*::woman-running:-light-skin-tone::
Send,🏃🏻‍♀
return

;1F3C3 1F3FC 200D 2640 FE0F
:*::woman-running:-medium-light-skin-tone::
Send,🏃🏼‍♀️
return

;1F3C3 1F3FC 200D 2640
:*::woman-running:-medium-light-skin-tone::
Send,🏃🏼‍♀
return

;1F3C3 1F3FD 200D 2640 FE0F
:*::woman-running:-medium-skin-tone::
Send,🏃🏽‍♀️
return

;1F3C3 1F3FD 200D 2640
:*::woman-running:-medium-skin-tone::
Send,🏃🏽‍♀
return

;1F3C3 1F3FE 200D 2640 FE0F
:*::woman-running:-medium-dark-skin-tone::
Send,🏃🏾‍♀️
return

;1F3C3 1F3FE 200D 2640
:*::woman-running:-medium-dark-skin-tone::
Send,🏃🏾‍♀
return

;1F3C3 1F3FF 200D 2640 FE0F
:*::woman-running:-dark-skin-tone::
Send,🏃🏿‍♀️
return

;1F3C3 1F3FF 200D 2640
:*::woman-running:-dark-skin-tone::
Send,🏃🏿‍♀
return

;1F483
:*::woman-dancing::
Send,💃
return

;1F483 1F3FB
:*::woman-dancing:-light-skin-tone::
Send,💃🏻
return

;1F483 1F3FC
:*::woman-dancing:-medium-light-skin-tone::
Send,💃🏼
return

;1F483 1F3FD
:*::woman-dancing:-medium-skin-tone::
Send,💃🏽
return

;1F483 1F3FE
:*::woman-dancing:-medium-dark-skin-tone::
Send,💃🏾
return

;1F483 1F3FF
:*::woman-dancing:-dark-skin-tone::
Send,💃🏿
return

;1F57A
:*::man-dancing::
Send,🕺
return

;1F57A 1F3FB
:*::man-dancing:-light-skin-tone::
Send,🕺🏻
return

;1F57A 1F3FC
:*::man-dancing:-medium-light-skin-tone::
Send,🕺🏼
return

;1F57A 1F3FD
:*::man-dancing:-medium-skin-tone::
Send,🕺🏽
return

;1F57A 1F3FE
:*::man-dancing:-medium-dark-skin-tone::
Send,🕺🏾
return

;1F57A 1F3FF
:*::man-dancing:-dark-skin-tone::
Send,🕺🏿
return

;1F574 FE0F
:*::man-in-suit-levitating::
Send,🕴️
return

;1F574
:*::man-in-suit-levitating::
Send,🕴
return

;1F574 1F3FB
:*::man-in-suit-levitating:-light-skin-tone::
Send,🕴🏻
return

;1F574 1F3FC
:*::man-in-suit-levitating:-medium-light-sk::
Send,🕴🏼
return

;1F574 1F3FD
:*::man-in-suit-levitating:-medium-skin-ton::
Send,🕴🏽
return

;1F574 1F3FE
:*::man-in-suit-levitating:-medium-dark-ski::
Send,🕴🏾
return

;1F574 1F3FF
:*::man-in-suit-levitating:-dark-skin-tone::
Send,🕴🏿
return

;1F46F
:*::people-with-bunny-ears::
Send,👯
return

;1F46F 200D 2642 FE0F
:*::men-with-bunny-ears::
Send,👯‍♂️
return

;1F46F 200D 2642
:*::men-with-bunny-ears::
Send,👯‍♂
return

;1F46F 200D 2640 FE0F
:*::women-with-bunny-ears::
Send,👯‍♀️
return

;1F46F 200D 2640
:*::women-with-bunny-ears::
Send,👯‍♀
return

;1F9D6
:*::person-in-steamy-room::
Send,🧖
return

;1F9D6 1F3FB
:*::person-in-steamy-room:-light-skin-tone::
Send,🧖🏻
return

;1F9D6 1F3FC
:*::person-in-steamy-room:-medium-light-ski::
Send,🧖🏼
return

;1F9D6 1F3FD
:*::person-in-steamy-room:-medium-skin-tone::
Send,🧖🏽
return

;1F9D6 1F3FE
:*::person-in-steamy-room:-medium-dark-skin::
Send,🧖🏾
return

;1F9D6 1F3FF
:*::person-in-steamy-room:-dark-skin-tone::
Send,🧖🏿
return

;1F9D6 200D 2642 FE0F
:*::man-in-steamy-room::
Send,🧖‍♂️
return

;1F9D6 200D 2642
:*::man-in-steamy-room::
Send,🧖‍♂
return

;1F9D6 1F3FB 200D 2642 FE0F
:*::man-in-steamy-room:-light-skin-tone::
Send,🧖🏻‍♂️
return

;1F9D6 1F3FB 200D 2642
:*::man-in-steamy-room:-light-skin-tone::
Send,🧖🏻‍♂
return

;1F9D6 1F3FC 200D 2642 FE0F
:*::man-in-steamy-room:-medium-light-skin-t::
Send,🧖🏼‍♂️
return

;1F9D6 1F3FC 200D 2642
:*::man-in-steamy-room:-medium-light-skin-t::
Send,🧖🏼‍♂
return

;1F9D6 1F3FD 200D 2642 FE0F
:*::man-in-steamy-room:-medium-skin-tone::
Send,🧖🏽‍♂️
return

;1F9D6 1F3FD 200D 2642
:*::man-in-steamy-room:-medium-skin-tone::
Send,🧖🏽‍♂
return

;1F9D6 1F3FE 200D 2642 FE0F
:*::man-in-steamy-room:-medium-dark-skin-to::
Send,🧖🏾‍♂️
return

;1F9D6 1F3FE 200D 2642
:*::man-in-steamy-room:-medium-dark-skin-to::
Send,🧖🏾‍♂
return

;1F9D6 1F3FF 200D 2642 FE0F
:*::man-in-steamy-room:-dark-skin-tone::
Send,🧖🏿‍♂️
return

;1F9D6 1F3FF 200D 2642
:*::man-in-steamy-room:-dark-skin-tone::
Send,🧖🏿‍♂
return

;1F9D6 200D 2640 FE0F
:*::woman-in-steamy-room::
Send,🧖‍♀️
return

;1F9D6 200D 2640
:*::woman-in-steamy-room::
Send,🧖‍♀
return

;1F9D6 1F3FB 200D 2640 FE0F
:*::woman-in-steamy-room:-light-skin-tone::
Send,🧖🏻‍♀️
return

;1F9D6 1F3FB 200D 2640
:*::woman-in-steamy-room:-light-skin-tone::
Send,🧖🏻‍♀
return

;1F9D6 1F3FC 200D 2640 FE0F
:*::woman-in-steamy-room:-medium-light-skin::
Send,🧖🏼‍♀️
return

;1F9D6 1F3FC 200D 2640
:*::woman-in-steamy-room:-medium-light-skin::
Send,🧖🏼‍♀
return

;1F9D6 1F3FD 200D 2640 FE0F
:*::woman-in-steamy-room:-medium-skin-tone::
Send,🧖🏽‍♀️
return

;1F9D6 1F3FD 200D 2640
:*::woman-in-steamy-room:-medium-skin-tone::
Send,🧖🏽‍♀
return

;1F9D6 1F3FE 200D 2640 FE0F
:*::woman-in-steamy-room:-medium-dark-skin::
Send,🧖🏾‍♀️
return

;1F9D6 1F3FE 200D 2640
:*::woman-in-steamy-room:-medium-dark-skin::
Send,🧖🏾‍♀
return

;1F9D6 1F3FF 200D 2640 FE0F
:*::woman-in-steamy-room:-dark-skin-tone::
Send,🧖🏿‍♀️
return

;1F9D6 1F3FF 200D 2640
:*::woman-in-steamy-room:-dark-skin-tone::
Send,🧖🏿‍♀
return

;1F9D7
:*::person-climbing::
Send,🧗
return

;1F9D7 1F3FB
:*::person-climbing:-light-skin-tone::
Send,🧗🏻
return

;1F9D7 1F3FC
:*::person-climbing:-medium-light-skin-tone::
Send,🧗🏼
return

;1F9D7 1F3FD
:*::person-climbing:-medium-skin-tone::
Send,🧗🏽
return

;1F9D7 1F3FE
:*::person-climbing:-medium-dark-skin-tone::
Send,🧗🏾
return

;1F9D7 1F3FF
:*::person-climbing:-dark-skin-tone::
Send,🧗🏿
return

;1F9D7 200D 2642 FE0F
:*::man-climbing::
Send,🧗‍♂️
return

;1F9D7 200D 2642
:*::man-climbing::
Send,🧗‍♂
return

;1F9D7 1F3FB 200D 2642 FE0F
:*::man-climbing:-light-skin-tone::
Send,🧗🏻‍♂️
return

;1F9D7 1F3FB 200D 2642
:*::man-climbing:-light-skin-tone::
Send,🧗🏻‍♂
return

;1F9D7 1F3FC 200D 2642 FE0F
:*::man-climbing:-medium-light-skin-tone::
Send,🧗🏼‍♂️
return

;1F9D7 1F3FC 200D 2642
:*::man-climbing:-medium-light-skin-tone::
Send,🧗🏼‍♂
return

;1F9D7 1F3FD 200D 2642 FE0F
:*::man-climbing:-medium-skin-tone::
Send,🧗🏽‍♂️
return

;1F9D7 1F3FD 200D 2642
:*::man-climbing:-medium-skin-tone::
Send,🧗🏽‍♂
return

;1F9D7 1F3FE 200D 2642 FE0F
:*::man-climbing:-medium-dark-skin-tone::
Send,🧗🏾‍♂️
return

;1F9D7 1F3FE 200D 2642
:*::man-climbing:-medium-dark-skin-tone::
Send,🧗🏾‍♂
return

;1F9D7 1F3FF 200D 2642 FE0F
:*::man-climbing:-dark-skin-tone::
Send,🧗🏿‍♂️
return

;1F9D7 1F3FF 200D 2642
:*::man-climbing:-dark-skin-tone::
Send,🧗🏿‍♂
return

;1F9D7 200D 2640 FE0F
:*::woman-climbing::
Send,🧗‍♀️
return

;1F9D7 200D 2640
:*::woman-climbing::
Send,🧗‍♀
return

;1F9D7 1F3FB 200D 2640 FE0F
:*::woman-climbing:-light-skin-tone::
Send,🧗🏻‍♀️
return

;1F9D7 1F3FB 200D 2640
:*::woman-climbing:-light-skin-tone::
Send,🧗🏻‍♀
return

;1F9D7 1F3FC 200D 2640 FE0F
:*::woman-climbing:-medium-light-skin-tone::
Send,🧗🏼‍♀️
return

;1F9D7 1F3FC 200D 2640
:*::woman-climbing:-medium-light-skin-tone::
Send,🧗🏼‍♀
return

;1F9D7 1F3FD 200D 2640 FE0F
:*::woman-climbing:-medium-skin-tone::
Send,🧗🏽‍♀️
return

;1F9D7 1F3FD 200D 2640
:*::woman-climbing:-medium-skin-tone::
Send,🧗🏽‍♀
return

;1F9D7 1F3FE 200D 2640 FE0F
:*::woman-climbing:-medium-dark-skin-tone::
Send,🧗🏾‍♀️
return

;1F9D7 1F3FE 200D 2640
:*::woman-climbing:-medium-dark-skin-tone::
Send,🧗🏾‍♀
return

;1F9D7 1F3FF 200D 2640 FE0F
:*::woman-climbing:-dark-skin-tone::
Send,🧗🏿‍♀️
return

;1F9D7 1F3FF 200D 2640
:*::woman-climbing:-dark-skin-tone::
Send,🧗🏿‍♀
return

;1F93A
:*::person-fencing::
Send,🤺
return

;1F3C7
:*::horse-racing::
Send,🏇
return

;1F3C7 1F3FB
:*::horse-racing:-light-skin-tone::
Send,🏇🏻
return

;1F3C7 1F3FC
:*::horse-racing:-medium-light-skin-tone::
Send,🏇🏼
return

;1F3C7 1F3FD
:*::horse-racing:-medium-skin-tone::
Send,🏇🏽
return

;1F3C7 1F3FE
:*::horse-racing:-medium-dark-skin-tone::
Send,🏇🏾
return

;1F3C7 1F3FF
:*::horse-racing:-dark-skin-tone::
Send,🏇🏿
return

;26F7 FE0F
:*::skier::
Send,⛷️
return

;26F7
:*::skier::
Send,⛷
return

;1F3C2
:*::snowboarder::
Send,🏂
return

;1F3C2 1F3FB
:*::snowboarder:-light-skin-tone::
Send,🏂🏻
return

;1F3C2 1F3FC
:*::snowboarder:-medium-light-skin-tone::
Send,🏂🏼
return

;1F3C2 1F3FD
:*::snowboarder:-medium-skin-tone::
Send,🏂🏽
return

;1F3C2 1F3FE
:*::snowboarder:-medium-dark-skin-tone::
Send,🏂🏾
return

;1F3C2 1F3FF
:*::snowboarder:-dark-skin-tone::
Send,🏂🏿
return

;1F3CC FE0F
:*::person-golfing::
Send,🏌️
return

;1F3CC
:*::person-golfing::
Send,🏌
return

;1F3CC 1F3FB
:*::person-golfing:-light-skin-tone::
Send,🏌🏻
return

;1F3CC 1F3FC
:*::person-golfing:-medium-light-skin-tone::
Send,🏌🏼
return

;1F3CC 1F3FD
:*::person-golfing:-medium-skin-tone::
Send,🏌🏽
return

;1F3CC 1F3FE
:*::person-golfing:-medium-dark-skin-tone::
Send,🏌🏾
return

;1F3CC 1F3FF
:*::person-golfing:-dark-skin-tone::
Send,🏌🏿
return

;1F3CC FE0F 200D 2642 FE0F
:*::man-golfing::
Send,🏌️‍♂️
return

;1F3CC 200D 2642 FE0F
:*::man-golfing::
Send,🏌‍♂️
return

;1F3CC FE0F 200D 2642
:*::man-golfing::
Send,🏌️‍♂
return

;1F3CC 200D 2642
:*::man-golfing::
Send,🏌‍♂
return

;1F3CC 1F3FB 200D 2642 FE0F
:*::man-golfing:-light-skin-tone::
Send,🏌🏻‍♂️
return

;1F3CC 1F3FB 200D 2642
:*::man-golfing:-light-skin-tone::
Send,🏌🏻‍♂
return

;1F3CC 1F3FC 200D 2642 FE0F
:*::man-golfing:-medium-light-skin-tone::
Send,🏌🏼‍♂️
return

;1F3CC 1F3FC 200D 2642
:*::man-golfing:-medium-light-skin-tone::
Send,🏌🏼‍♂
return

;1F3CC 1F3FD 200D 2642 FE0F
:*::man-golfing:-medium-skin-tone::
Send,🏌🏽‍♂️
return

;1F3CC 1F3FD 200D 2642
:*::man-golfing:-medium-skin-tone::
Send,🏌🏽‍♂
return

;1F3CC 1F3FE 200D 2642 FE0F
:*::man-golfing:-medium-dark-skin-tone::
Send,🏌🏾‍♂️
return

;1F3CC 1F3FE 200D 2642
:*::man-golfing:-medium-dark-skin-tone::
Send,🏌🏾‍♂
return

;1F3CC 1F3FF 200D 2642 FE0F
:*::man-golfing:-dark-skin-tone::
Send,🏌🏿‍♂️
return

;1F3CC 1F3FF 200D 2642
:*::man-golfing:-dark-skin-tone::
Send,🏌🏿‍♂
return

;1F3CC FE0F 200D 2640 FE0F
:*::woman-golfing::
Send,🏌️‍♀️
return

;1F3CC 200D 2640 FE0F
:*::woman-golfing::
Send,🏌‍♀️
return

;1F3CC FE0F 200D 2640
:*::woman-golfing::
Send,🏌️‍♀
return

;1F3CC 200D 2640
:*::woman-golfing::
Send,🏌‍♀
return

;1F3CC 1F3FB 200D 2640 FE0F
:*::woman-golfing:-light-skin-tone::
Send,🏌🏻‍♀️
return

;1F3CC 1F3FB 200D 2640
:*::woman-golfing:-light-skin-tone::
Send,🏌🏻‍♀
return

;1F3CC 1F3FC 200D 2640 FE0F
:*::woman-golfing:-medium-light-skin-tone::
Send,🏌🏼‍♀️
return

;1F3CC 1F3FC 200D 2640
:*::woman-golfing:-medium-light-skin-tone::
Send,🏌🏼‍♀
return

;1F3CC 1F3FD 200D 2640 FE0F
:*::woman-golfing:-medium-skin-tone::
Send,🏌🏽‍♀️
return

;1F3CC 1F3FD 200D 2640
:*::woman-golfing:-medium-skin-tone::
Send,🏌🏽‍♀
return

;1F3CC 1F3FE 200D 2640 FE0F
:*::woman-golfing:-medium-dark-skin-tone::
Send,🏌🏾‍♀️
return

;1F3CC 1F3FE 200D 2640
:*::woman-golfing:-medium-dark-skin-tone::
Send,🏌🏾‍♀
return

;1F3CC 1F3FF 200D 2640 FE0F
:*::woman-golfing:-dark-skin-tone::
Send,🏌🏿‍♀️
return

;1F3CC 1F3FF 200D 2640
:*::woman-golfing:-dark-skin-tone::
Send,🏌🏿‍♀
return

;1F3C4
:*::person-surfing::
Send,🏄
return

;1F3C4 1F3FB
:*::person-surfing:-light-skin-tone::
Send,🏄🏻
return

;1F3C4 1F3FC
:*::person-surfing:-medium-light-skin-tone::
Send,🏄🏼
return

;1F3C4 1F3FD
:*::person-surfing:-medium-skin-tone::
Send,🏄🏽
return

;1F3C4 1F3FE
:*::person-surfing:-medium-dark-skin-tone::
Send,🏄🏾
return

;1F3C4 1F3FF
:*::person-surfing:-dark-skin-tone::
Send,🏄🏿
return

;1F3C4 200D 2642 FE0F
:*::man-surfing::
Send,🏄‍♂️
return

;1F3C4 200D 2642
:*::man-surfing::
Send,🏄‍♂
return

;1F3C4 1F3FB 200D 2642 FE0F
:*::man-surfing:-light-skin-tone::
Send,🏄🏻‍♂️
return

;1F3C4 1F3FB 200D 2642
:*::man-surfing:-light-skin-tone::
Send,🏄🏻‍♂
return

;1F3C4 1F3FC 200D 2642 FE0F
:*::man-surfing:-medium-light-skin-tone::
Send,🏄🏼‍♂️
return

;1F3C4 1F3FC 200D 2642
:*::man-surfing:-medium-light-skin-tone::
Send,🏄🏼‍♂
return

;1F3C4 1F3FD 200D 2642 FE0F
:*::man-surfing:-medium-skin-tone::
Send,🏄🏽‍♂️
return

;1F3C4 1F3FD 200D 2642
:*::man-surfing:-medium-skin-tone::
Send,🏄🏽‍♂
return

;1F3C4 1F3FE 200D 2642 FE0F
:*::man-surfing:-medium-dark-skin-tone::
Send,🏄🏾‍♂️
return

;1F3C4 1F3FE 200D 2642
:*::man-surfing:-medium-dark-skin-tone::
Send,🏄🏾‍♂
return

;1F3C4 1F3FF 200D 2642 FE0F
:*::man-surfing:-dark-skin-tone::
Send,🏄🏿‍♂️
return

;1F3C4 1F3FF 200D 2642
:*::man-surfing:-dark-skin-tone::
Send,🏄🏿‍♂
return

;1F3C4 200D 2640 FE0F
:*::woman-surfing::
Send,🏄‍♀️
return

;1F3C4 200D 2640
:*::woman-surfing::
Send,🏄‍♀
return

;1F3C4 1F3FB 200D 2640 FE0F
:*::woman-surfing:-light-skin-tone::
Send,🏄🏻‍♀️
return

;1F3C4 1F3FB 200D 2640
:*::woman-surfing:-light-skin-tone::
Send,🏄🏻‍♀
return

;1F3C4 1F3FC 200D 2640 FE0F
:*::woman-surfing:-medium-light-skin-tone::
Send,🏄🏼‍♀️
return

;1F3C4 1F3FC 200D 2640
:*::woman-surfing:-medium-light-skin-tone::
Send,🏄🏼‍♀
return

;1F3C4 1F3FD 200D 2640 FE0F
:*::woman-surfing:-medium-skin-tone::
Send,🏄🏽‍♀️
return

;1F3C4 1F3FD 200D 2640
:*::woman-surfing:-medium-skin-tone::
Send,🏄🏽‍♀
return

;1F3C4 1F3FE 200D 2640 FE0F
:*::woman-surfing:-medium-dark-skin-tone::
Send,🏄🏾‍♀️
return

;1F3C4 1F3FE 200D 2640
:*::woman-surfing:-medium-dark-skin-tone::
Send,🏄🏾‍♀
return

;1F3C4 1F3FF 200D 2640 FE0F
:*::woman-surfing:-dark-skin-tone::
Send,🏄🏿‍♀️
return

;1F3C4 1F3FF 200D 2640
:*::woman-surfing:-dark-skin-tone::
Send,🏄🏿‍♀
return

;1F6A3
:*::person-rowing-boat::
Send,🚣
return

;1F6A3 1F3FB
:*::person-rowing-boat:-light-skin-tone::
Send,🚣🏻
return

;1F6A3 1F3FC
:*::person-rowing-boat:-medium-light-skin-t::
Send,🚣🏼
return

;1F6A3 1F3FD
:*::person-rowing-boat:-medium-skin-tone::
Send,🚣🏽
return

;1F6A3 1F3FE
:*::person-rowing-boat:-medium-dark-skin-to::
Send,🚣🏾
return

;1F6A3 1F3FF
:*::person-rowing-boat:-dark-skin-tone::
Send,🚣🏿
return

;1F6A3 200D 2642 FE0F
:*::man-rowing-boat::
Send,🚣‍♂️
return

;1F6A3 200D 2642
:*::man-rowing-boat::
Send,🚣‍♂
return

;1F6A3 1F3FB 200D 2642 FE0F
:*::man-rowing-boat:-light-skin-tone::
Send,🚣🏻‍♂️
return

;1F6A3 1F3FB 200D 2642
:*::man-rowing-boat:-light-skin-tone::
Send,🚣🏻‍♂
return

;1F6A3 1F3FC 200D 2642 FE0F
:*::man-rowing-boat:-medium-light-skin-tone::
Send,🚣🏼‍♂️
return

;1F6A3 1F3FC 200D 2642
:*::man-rowing-boat:-medium-light-skin-tone::
Send,🚣🏼‍♂
return

;1F6A3 1F3FD 200D 2642 FE0F
:*::man-rowing-boat:-medium-skin-tone::
Send,🚣🏽‍♂️
return

;1F6A3 1F3FD 200D 2642
:*::man-rowing-boat:-medium-skin-tone::
Send,🚣🏽‍♂
return

;1F6A3 1F3FE 200D 2642 FE0F
:*::man-rowing-boat:-medium-dark-skin-tone::
Send,🚣🏾‍♂️
return

;1F6A3 1F3FE 200D 2642
:*::man-rowing-boat:-medium-dark-skin-tone::
Send,🚣🏾‍♂
return

;1F6A3 1F3FF 200D 2642 FE0F
:*::man-rowing-boat:-dark-skin-tone::
Send,🚣🏿‍♂️
return

;1F6A3 1F3FF 200D 2642
:*::man-rowing-boat:-dark-skin-tone::
Send,🚣🏿‍♂
return

;1F6A3 200D 2640 FE0F
:*::woman-rowing-boat::
Send,🚣‍♀️
return

;1F6A3 200D 2640
:*::woman-rowing-boat::
Send,🚣‍♀
return

;1F6A3 1F3FB 200D 2640 FE0F
:*::woman-rowing-boat:-light-skin-tone::
Send,🚣🏻‍♀️
return

;1F6A3 1F3FB 200D 2640
:*::woman-rowing-boat:-light-skin-tone::
Send,🚣🏻‍♀
return

;1F6A3 1F3FC 200D 2640 FE0F
:*::woman-rowing-boat:-medium-light-skin-to::
Send,🚣🏼‍♀️
return

;1F6A3 1F3FC 200D 2640
:*::woman-rowing-boat:-medium-light-skin-to::
Send,🚣🏼‍♀
return

;1F6A3 1F3FD 200D 2640 FE0F
:*::woman-rowing-boat:-medium-skin-tone::
Send,🚣🏽‍♀️
return

;1F6A3 1F3FD 200D 2640
:*::woman-rowing-boat:-medium-skin-tone::
Send,🚣🏽‍♀
return

;1F6A3 1F3FE 200D 2640 FE0F
:*::woman-rowing-boat:-medium-dark-skin-ton::
Send,🚣🏾‍♀️
return

;1F6A3 1F3FE 200D 2640
:*::woman-rowing-boat:-medium-dark-skin-ton::
Send,🚣🏾‍♀
return

;1F6A3 1F3FF 200D 2640 FE0F
:*::woman-rowing-boat:-dark-skin-tone::
Send,🚣🏿‍♀️
return

;1F6A3 1F3FF 200D 2640
:*::woman-rowing-boat:-dark-skin-tone::
Send,🚣🏿‍♀
return

;1F3CA
:*::person-swimming::
Send,🏊
return

;1F3CA 1F3FB
:*::person-swimming:-light-skin-tone::
Send,🏊🏻
return

;1F3CA 1F3FC
:*::person-swimming:-medium-light-skin-tone::
Send,🏊🏼
return

;1F3CA 1F3FD
:*::person-swimming:-medium-skin-tone::
Send,🏊🏽
return

;1F3CA 1F3FE
:*::person-swimming:-medium-dark-skin-tone::
Send,🏊🏾
return

;1F3CA 1F3FF
:*::person-swimming:-dark-skin-tone::
Send,🏊🏿
return

;1F3CA 200D 2642 FE0F
:*::man-swimming::
Send,🏊‍♂️
return

;1F3CA 200D 2642
:*::man-swimming::
Send,🏊‍♂
return

;1F3CA 1F3FB 200D 2642 FE0F
:*::man-swimming:-light-skin-tone::
Send,🏊🏻‍♂️
return

;1F3CA 1F3FB 200D 2642
:*::man-swimming:-light-skin-tone::
Send,🏊🏻‍♂
return

;1F3CA 1F3FC 200D 2642 FE0F
:*::man-swimming:-medium-light-skin-tone::
Send,🏊🏼‍♂️
return

;1F3CA 1F3FC 200D 2642
:*::man-swimming:-medium-light-skin-tone::
Send,🏊🏼‍♂
return

;1F3CA 1F3FD 200D 2642 FE0F
:*::man-swimming:-medium-skin-tone::
Send,🏊🏽‍♂️
return

;1F3CA 1F3FD 200D 2642
:*::man-swimming:-medium-skin-tone::
Send,🏊🏽‍♂
return

;1F3CA 1F3FE 200D 2642 FE0F
:*::man-swimming:-medium-dark-skin-tone::
Send,🏊🏾‍♂️
return

;1F3CA 1F3FE 200D 2642
:*::man-swimming:-medium-dark-skin-tone::
Send,🏊🏾‍♂
return

;1F3CA 1F3FF 200D 2642 FE0F
:*::man-swimming:-dark-skin-tone::
Send,🏊🏿‍♂️
return

;1F3CA 1F3FF 200D 2642
:*::man-swimming:-dark-skin-tone::
Send,🏊🏿‍♂
return

;1F3CA 200D 2640 FE0F
:*::woman-swimming::
Send,🏊‍♀️
return

;1F3CA 200D 2640
:*::woman-swimming::
Send,🏊‍♀
return

;1F3CA 1F3FB 200D 2640 FE0F
:*::woman-swimming:-light-skin-tone::
Send,🏊🏻‍♀️
return

;1F3CA 1F3FB 200D 2640
:*::woman-swimming:-light-skin-tone::
Send,🏊🏻‍♀
return

;1F3CA 1F3FC 200D 2640 FE0F
:*::woman-swimming:-medium-light-skin-tone::
Send,🏊🏼‍♀️
return

;1F3CA 1F3FC 200D 2640
:*::woman-swimming:-medium-light-skin-tone::
Send,🏊🏼‍♀
return

;1F3CA 1F3FD 200D 2640 FE0F
:*::woman-swimming:-medium-skin-tone::
Send,🏊🏽‍♀️
return

;1F3CA 1F3FD 200D 2640
:*::woman-swimming:-medium-skin-tone::
Send,🏊🏽‍♀
return

;1F3CA 1F3FE 200D 2640 FE0F
:*::woman-swimming:-medium-dark-skin-tone::
Send,🏊🏾‍♀️
return

;1F3CA 1F3FE 200D 2640
:*::woman-swimming:-medium-dark-skin-tone::
Send,🏊🏾‍♀
return

;1F3CA 1F3FF 200D 2640 FE0F
:*::woman-swimming:-dark-skin-tone::
Send,🏊🏿‍♀️
return

;1F3CA 1F3FF 200D 2640
:*::woman-swimming:-dark-skin-tone::
Send,🏊🏿‍♀
return

;26F9 FE0F
:*::person-bouncing-ball::
Send,⛹️
return

;26F9
:*::person-bouncing-ball::
Send,⛹
return

;26F9 1F3FB
:*::person-bouncing-ball:-light-skin-tone::
Send,⛹🏻
return

;26F9 1F3FC
:*::person-bouncing-ball:-medium-light-skin::
Send,⛹🏼
return

;26F9 1F3FD
:*::person-bouncing-ball:-medium-skin-tone::
Send,⛹🏽
return

;26F9 1F3FE
:*::person-bouncing-ball:-medium-dark-skin::
Send,⛹🏾
return

;26F9 1F3FF
:*::person-bouncing-ball:-dark-skin-tone::
Send,⛹🏿
return

;26F9 FE0F 200D 2642 FE0F
:*::man-bouncing-ball::
Send,⛹️‍♂️
return

;26F9 200D 2642 FE0F
:*::man-bouncing-ball::
Send,⛹‍♂️
return

;26F9 FE0F 200D 2642
:*::man-bouncing-ball::
Send,⛹️‍♂
return

;26F9 200D 2642
:*::man-bouncing-ball::
Send,⛹‍♂
return

;26F9 1F3FB 200D 2642 FE0F
:*::man-bouncing-ball:-light-skin-tone::
Send,⛹🏻‍♂️
return

;26F9 1F3FB 200D 2642
:*::man-bouncing-ball:-light-skin-tone::
Send,⛹🏻‍♂
return

;26F9 1F3FC 200D 2642 FE0F
:*::man-bouncing-ball:-medium-light-skin-to::
Send,⛹🏼‍♂️
return

;26F9 1F3FC 200D 2642
:*::man-bouncing-ball:-medium-light-skin-to::
Send,⛹🏼‍♂
return

;26F9 1F3FD 200D 2642 FE0F
:*::man-bouncing-ball:-medium-skin-tone::
Send,⛹🏽‍♂️
return

;26F9 1F3FD 200D 2642
:*::man-bouncing-ball:-medium-skin-tone::
Send,⛹🏽‍♂
return

;26F9 1F3FE 200D 2642 FE0F
:*::man-bouncing-ball:-medium-dark-skin-ton::
Send,⛹🏾‍♂️
return

;26F9 1F3FE 200D 2642
:*::man-bouncing-ball:-medium-dark-skin-ton::
Send,⛹🏾‍♂
return

;26F9 1F3FF 200D 2642 FE0F
:*::man-bouncing-ball:-dark-skin-tone::
Send,⛹🏿‍♂️
return

;26F9 1F3FF 200D 2642
:*::man-bouncing-ball:-dark-skin-tone::
Send,⛹🏿‍♂
return

;26F9 FE0F 200D 2640 FE0F
:*::woman-bouncing-ball::
Send,⛹️‍♀️
return

;26F9 200D 2640 FE0F
:*::woman-bouncing-ball::
Send,⛹‍♀️
return

;26F9 FE0F 200D 2640
:*::woman-bouncing-ball::
Send,⛹️‍♀
return

;26F9 200D 2640
:*::woman-bouncing-ball::
Send,⛹‍♀
return

;26F9 1F3FB 200D 2640 FE0F
:*::woman-bouncing-ball:-light-skin-tone::
Send,⛹🏻‍♀️
return

;26F9 1F3FB 200D 2640
:*::woman-bouncing-ball:-light-skin-tone::
Send,⛹🏻‍♀
return

;26F9 1F3FC 200D 2640 FE0F
:*::woman-bouncing-ball:-medium-light-skin::
Send,⛹🏼‍♀️
return

;26F9 1F3FC 200D 2640
:*::woman-bouncing-ball:-medium-light-skin::
Send,⛹🏼‍♀
return

;26F9 1F3FD 200D 2640 FE0F
:*::woman-bouncing-ball:-medium-skin-tone::
Send,⛹🏽‍♀️
return

;26F9 1F3FD 200D 2640
:*::woman-bouncing-ball:-medium-skin-tone::
Send,⛹🏽‍♀
return

;26F9 1F3FE 200D 2640 FE0F
:*::woman-bouncing-ball:-medium-dark-skin-t::
Send,⛹🏾‍♀️
return

;26F9 1F3FE 200D 2640
:*::woman-bouncing-ball:-medium-dark-skin-t::
Send,⛹🏾‍♀
return

;26F9 1F3FF 200D 2640 FE0F
:*::woman-bouncing-ball:-dark-skin-tone::
Send,⛹🏿‍♀️
return

;26F9 1F3FF 200D 2640
:*::woman-bouncing-ball:-dark-skin-tone::
Send,⛹🏿‍♀
return

;1F3CB FE0F
:*::person-lifting-weights::
Send,🏋️
return

;1F3CB
:*::person-lifting-weights::
Send,🏋
return

;1F3CB 1F3FB
:*::person-lifting-weights:-light-skin-tone::
Send,🏋🏻
return

;1F3CB 1F3FC
:*::person-lifting-weights:-medium-light-sk::
Send,🏋🏼
return

;1F3CB 1F3FD
:*::person-lifting-weights:-medium-skin-ton::
Send,🏋🏽
return

;1F3CB 1F3FE
:*::person-lifting-weights:-medium-dark-ski::
Send,🏋🏾
return

;1F3CB 1F3FF
:*::person-lifting-weights:-dark-skin-tone::
Send,🏋🏿
return

;1F3CB FE0F 200D 2642 FE0F
:*::man-lifting-weights::
Send,🏋️‍♂️
return

;1F3CB 200D 2642 FE0F
:*::man-lifting-weights::
Send,🏋‍♂️
return

;1F3CB FE0F 200D 2642
:*::man-lifting-weights::
Send,🏋️‍♂
return

;1F3CB 200D 2642
:*::man-lifting-weights::
Send,🏋‍♂
return

;1F3CB 1F3FB 200D 2642 FE0F
:*::man-lifting-weights:-light-skin-tone::
Send,🏋🏻‍♂️
return

;1F3CB 1F3FB 200D 2642
:*::man-lifting-weights:-light-skin-tone::
Send,🏋🏻‍♂
return

;1F3CB 1F3FC 200D 2642 FE0F
:*::man-lifting-weights:-medium-light-skin::
Send,🏋🏼‍♂️
return

;1F3CB 1F3FC 200D 2642
:*::man-lifting-weights:-medium-light-skin::
Send,🏋🏼‍♂
return

;1F3CB 1F3FD 200D 2642 FE0F
:*::man-lifting-weights:-medium-skin-tone::
Send,🏋🏽‍♂️
return

;1F3CB 1F3FD 200D 2642
:*::man-lifting-weights:-medium-skin-tone::
Send,🏋🏽‍♂
return

;1F3CB 1F3FE 200D 2642 FE0F
:*::man-lifting-weights:-medium-dark-skin-t::
Send,🏋🏾‍♂️
return

;1F3CB 1F3FE 200D 2642
:*::man-lifting-weights:-medium-dark-skin-t::
Send,🏋🏾‍♂
return

;1F3CB 1F3FF 200D 2642 FE0F
:*::man-lifting-weights:-dark-skin-tone::
Send,🏋🏿‍♂️
return

;1F3CB 1F3FF 200D 2642
:*::man-lifting-weights:-dark-skin-tone::
Send,🏋🏿‍♂
return

;1F3CB FE0F 200D 2640 FE0F
:*::woman-lifting-weights::
Send,🏋️‍♀️
return

;1F3CB 200D 2640 FE0F
:*::woman-lifting-weights::
Send,🏋‍♀️
return

;1F3CB FE0F 200D 2640
:*::woman-lifting-weights::
Send,🏋️‍♀
return

;1F3CB 200D 2640
:*::woman-lifting-weights::
Send,🏋‍♀
return

;1F3CB 1F3FB 200D 2640 FE0F
:*::woman-lifting-weights:-light-skin-tone::
Send,🏋🏻‍♀️
return

;1F3CB 1F3FB 200D 2640
:*::woman-lifting-weights:-light-skin-tone::
Send,🏋🏻‍♀
return

;1F3CB 1F3FC 200D 2640 FE0F
:*::woman-lifting-weights:-medium-light-ski::
Send,🏋🏼‍♀️
return

;1F3CB 1F3FC 200D 2640
:*::woman-lifting-weights:-medium-light-ski::
Send,🏋🏼‍♀
return

;1F3CB 1F3FD 200D 2640 FE0F
:*::woman-lifting-weights:-medium-skin-tone::
Send,🏋🏽‍♀️
return

;1F3CB 1F3FD 200D 2640
:*::woman-lifting-weights:-medium-skin-tone::
Send,🏋🏽‍♀
return

;1F3CB 1F3FE 200D 2640 FE0F
:*::woman-lifting-weights:-medium-dark-skin::
Send,🏋🏾‍♀️
return

;1F3CB 1F3FE 200D 2640
:*::woman-lifting-weights:-medium-dark-skin::
Send,🏋🏾‍♀
return

;1F3CB 1F3FF 200D 2640 FE0F
:*::woman-lifting-weights:-dark-skin-tone::
Send,🏋🏿‍♀️
return

;1F3CB 1F3FF 200D 2640
:*::woman-lifting-weights:-dark-skin-tone::
Send,🏋🏿‍♀
return

;1F6B4
:*::person-biking::
Send,🚴
return

;1F6B4 1F3FB
:*::person-biking:-light-skin-tone::
Send,🚴🏻
return

;1F6B4 1F3FC
:*::person-biking:-medium-light-skin-tone::
Send,🚴🏼
return

;1F6B4 1F3FD
:*::person-biking:-medium-skin-tone::
Send,🚴🏽
return

;1F6B4 1F3FE
:*::person-biking:-medium-dark-skin-tone::
Send,🚴🏾
return

;1F6B4 1F3FF
:*::person-biking:-dark-skin-tone::
Send,🚴🏿
return

;1F6B4 200D 2642 FE0F
:*::man-biking::
Send,🚴‍♂️
return

;1F6B4 200D 2642
:*::man-biking::
Send,🚴‍♂
return

;1F6B4 1F3FB 200D 2642 FE0F
:*::man-biking:-light-skin-tone::
Send,🚴🏻‍♂️
return

;1F6B4 1F3FB 200D 2642
:*::man-biking:-light-skin-tone::
Send,🚴🏻‍♂
return

;1F6B4 1F3FC 200D 2642 FE0F
:*::man-biking:-medium-light-skin-tone::
Send,🚴🏼‍♂️
return

;1F6B4 1F3FC 200D 2642
:*::man-biking:-medium-light-skin-tone::
Send,🚴🏼‍♂
return

;1F6B4 1F3FD 200D 2642 FE0F
:*::man-biking:-medium-skin-tone::
Send,🚴🏽‍♂️
return

;1F6B4 1F3FD 200D 2642
:*::man-biking:-medium-skin-tone::
Send,🚴🏽‍♂
return

;1F6B4 1F3FE 200D 2642 FE0F
:*::man-biking:-medium-dark-skin-tone::
Send,🚴🏾‍♂️
return

;1F6B4 1F3FE 200D 2642
:*::man-biking:-medium-dark-skin-tone::
Send,🚴🏾‍♂
return

;1F6B4 1F3FF 200D 2642 FE0F
:*::man-biking:-dark-skin-tone::
Send,🚴🏿‍♂️
return

;1F6B4 1F3FF 200D 2642
:*::man-biking:-dark-skin-tone::
Send,🚴🏿‍♂
return

;1F6B4 200D 2640 FE0F
:*::woman-biking::
Send,🚴‍♀️
return

;1F6B4 200D 2640
:*::woman-biking::
Send,🚴‍♀
return

;1F6B4 1F3FB 200D 2640 FE0F
:*::woman-biking:-light-skin-tone::
Send,🚴🏻‍♀️
return

;1F6B4 1F3FB 200D 2640
:*::woman-biking:-light-skin-tone::
Send,🚴🏻‍♀
return

;1F6B4 1F3FC 200D 2640 FE0F
:*::woman-biking:-medium-light-skin-tone::
Send,🚴🏼‍♀️
return

;1F6B4 1F3FC 200D 2640
:*::woman-biking:-medium-light-skin-tone::
Send,🚴🏼‍♀
return

;1F6B4 1F3FD 200D 2640 FE0F
:*::woman-biking:-medium-skin-tone::
Send,🚴🏽‍♀️
return

;1F6B4 1F3FD 200D 2640
:*::woman-biking:-medium-skin-tone::
Send,🚴🏽‍♀
return

;1F6B4 1F3FE 200D 2640 FE0F
:*::woman-biking:-medium-dark-skin-tone::
Send,🚴🏾‍♀️
return

;1F6B4 1F3FE 200D 2640
:*::woman-biking:-medium-dark-skin-tone::
Send,🚴🏾‍♀
return

;1F6B4 1F3FF 200D 2640 FE0F
:*::woman-biking:-dark-skin-tone::
Send,🚴🏿‍♀️
return

;1F6B4 1F3FF 200D 2640
:*::woman-biking:-dark-skin-tone::
Send,🚴🏿‍♀
return

;1F6B5
:*::person-mountain-biking::
Send,🚵
return

;1F6B5 1F3FB
:*::person-mountain-biking:-light-skin-tone::
Send,🚵🏻
return

;1F6B5 1F3FC
:*::person-mountain-biking:-medium-light-sk::
Send,🚵🏼
return

;1F6B5 1F3FD
:*::person-mountain-biking:-medium-skin-ton::
Send,🚵🏽
return

;1F6B5 1F3FE
:*::person-mountain-biking:-medium-dark-ski::
Send,🚵🏾
return

;1F6B5 1F3FF
:*::person-mountain-biking:-dark-skin-tone::
Send,🚵🏿
return

;1F6B5 200D 2642 FE0F
:*::man-mountain-biking::
Send,🚵‍♂️
return

;1F6B5 200D 2642
:*::man-mountain-biking::
Send,🚵‍♂
return

;1F6B5 1F3FB 200D 2642 FE0F
:*::man-mountain-biking:-light-skin-tone::
Send,🚵🏻‍♂️
return

;1F6B5 1F3FB 200D 2642
:*::man-mountain-biking:-light-skin-tone::
Send,🚵🏻‍♂
return

;1F6B5 1F3FC 200D 2642 FE0F
:*::man-mountain-biking:-medium-light-skin::
Send,🚵🏼‍♂️
return

;1F6B5 1F3FC 200D 2642
:*::man-mountain-biking:-medium-light-skin::
Send,🚵🏼‍♂
return

;1F6B5 1F3FD 200D 2642 FE0F
:*::man-mountain-biking:-medium-skin-tone::
Send,🚵🏽‍♂️
return

;1F6B5 1F3FD 200D 2642
:*::man-mountain-biking:-medium-skin-tone::
Send,🚵🏽‍♂
return

;1F6B5 1F3FE 200D 2642 FE0F
:*::man-mountain-biking:-medium-dark-skin-t::
Send,🚵🏾‍♂️
return

;1F6B5 1F3FE 200D 2642
:*::man-mountain-biking:-medium-dark-skin-t::
Send,🚵🏾‍♂
return

;1F6B5 1F3FF 200D 2642 FE0F
:*::man-mountain-biking:-dark-skin-tone::
Send,🚵🏿‍♂️
return

;1F6B5 1F3FF 200D 2642
:*::man-mountain-biking:-dark-skin-tone::
Send,🚵🏿‍♂
return

;1F6B5 200D 2640 FE0F
:*::woman-mountain-biking::
Send,🚵‍♀️
return

;1F6B5 200D 2640
:*::woman-mountain-biking::
Send,🚵‍♀
return

;1F6B5 1F3FB 200D 2640 FE0F
:*::woman-mountain-biking:-light-skin-tone::
Send,🚵🏻‍♀️
return

;1F6B5 1F3FB 200D 2640
:*::woman-mountain-biking:-light-skin-tone::
Send,🚵🏻‍♀
return

;1F6B5 1F3FC 200D 2640 FE0F
:*::woman-mountain-biking:-medium-light-ski::
Send,🚵🏼‍♀️
return

;1F6B5 1F3FC 200D 2640
:*::woman-mountain-biking:-medium-light-ski::
Send,🚵🏼‍♀
return

;1F6B5 1F3FD 200D 2640 FE0F
:*::woman-mountain-biking:-medium-skin-tone::
Send,🚵🏽‍♀️
return

;1F6B5 1F3FD 200D 2640
:*::woman-mountain-biking:-medium-skin-tone::
Send,🚵🏽‍♀
return

;1F6B5 1F3FE 200D 2640 FE0F
:*::woman-mountain-biking:-medium-dark-skin::
Send,🚵🏾‍♀️
return

;1F6B5 1F3FE 200D 2640
:*::woman-mountain-biking:-medium-dark-skin::
Send,🚵🏾‍♀
return

;1F6B5 1F3FF 200D 2640 FE0F
:*::woman-mountain-biking:-dark-skin-tone::
Send,🚵🏿‍♀️
return

;1F6B5 1F3FF 200D 2640
:*::woman-mountain-biking:-dark-skin-tone::
Send,🚵🏿‍♀
return

;1F938
:*::person-cartwheeling::
Send,🤸
return

;1F938 1F3FB
:*::person-cartwheeling:-light-skin-tone::
Send,🤸🏻
return

;1F938 1F3FC
:*::person-cartwheeling:-medium-light-skin::
Send,🤸🏼
return

;1F938 1F3FD
:*::person-cartwheeling:-medium-skin-tone::
Send,🤸🏽
return

;1F938 1F3FE
:*::person-cartwheeling:-medium-dark-skin-t::
Send,🤸🏾
return

;1F938 1F3FF
:*::person-cartwheeling:-dark-skin-tone::
Send,🤸🏿
return

;1F938 200D 2642 FE0F
:*::man-cartwheeling::
Send,🤸‍♂️
return

;1F938 200D 2642
:*::man-cartwheeling::
Send,🤸‍♂
return

;1F938 1F3FB 200D 2642 FE0F
:*::man-cartwheeling:-light-skin-tone::
Send,🤸🏻‍♂️
return

;1F938 1F3FB 200D 2642
:*::man-cartwheeling:-light-skin-tone::
Send,🤸🏻‍♂
return

;1F938 1F3FC 200D 2642 FE0F
:*::man-cartwheeling:-medium-light-skin-ton::
Send,🤸🏼‍♂️
return

;1F938 1F3FC 200D 2642
:*::man-cartwheeling:-medium-light-skin-ton::
Send,🤸🏼‍♂
return

;1F938 1F3FD 200D 2642 FE0F
:*::man-cartwheeling:-medium-skin-tone::
Send,🤸🏽‍♂️
return

;1F938 1F3FD 200D 2642
:*::man-cartwheeling:-medium-skin-tone::
Send,🤸🏽‍♂
return

;1F938 1F3FE 200D 2642 FE0F
:*::man-cartwheeling:-medium-dark-skin-tone::
Send,🤸🏾‍♂️
return

;1F938 1F3FE 200D 2642
:*::man-cartwheeling:-medium-dark-skin-tone::
Send,🤸🏾‍♂
return

;1F938 1F3FF 200D 2642 FE0F
:*::man-cartwheeling:-dark-skin-tone::
Send,🤸🏿‍♂️
return

;1F938 1F3FF 200D 2642
:*::man-cartwheeling:-dark-skin-tone::
Send,🤸🏿‍♂
return

;1F938 200D 2640 FE0F
:*::woman-cartwheeling::
Send,🤸‍♀️
return

;1F938 200D 2640
:*::woman-cartwheeling::
Send,🤸‍♀
return

;1F938 1F3FB 200D 2640 FE0F
:*::woman-cartwheeling:-light-skin-tone::
Send,🤸🏻‍♀️
return

;1F938 1F3FB 200D 2640
:*::woman-cartwheeling:-light-skin-tone::
Send,🤸🏻‍♀
return

;1F938 1F3FC 200D 2640 FE0F
:*::woman-cartwheeling:-medium-light-skin-t::
Send,🤸🏼‍♀️
return

;1F938 1F3FC 200D 2640
:*::woman-cartwheeling:-medium-light-skin-t::
Send,🤸🏼‍♀
return

;1F938 1F3FD 200D 2640 FE0F
:*::woman-cartwheeling:-medium-skin-tone::
Send,🤸🏽‍♀️
return

;1F938 1F3FD 200D 2640
:*::woman-cartwheeling:-medium-skin-tone::
Send,🤸🏽‍♀
return

;1F938 1F3FE 200D 2640 FE0F
:*::woman-cartwheeling:-medium-dark-skin-to::
Send,🤸🏾‍♀️
return

;1F938 1F3FE 200D 2640
:*::woman-cartwheeling:-medium-dark-skin-to::
Send,🤸🏾‍♀
return

;1F938 1F3FF 200D 2640 FE0F
:*::woman-cartwheeling:-dark-skin-tone::
Send,🤸🏿‍♀️
return

;1F938 1F3FF 200D 2640
:*::woman-cartwheeling:-dark-skin-tone::
Send,🤸🏿‍♀
return

;1F93C
:*::people-wrestling::
Send,🤼
return

;1F93C 200D 2642 FE0F
:*::men-wrestling::
Send,🤼‍♂️
return

;1F93C 200D 2642
:*::men-wrestling::
Send,🤼‍♂
return

;1F93C 200D 2640 FE0F
:*::women-wrestling::
Send,🤼‍♀️
return

;1F93C 200D 2640
:*::women-wrestling::
Send,🤼‍♀
return

;1F93D
:*::person-playing-water-polo::
Send,🤽
return

;1F93D 1F3FB
:*::person-playing-water-polo:-light-skin-t::
Send,🤽🏻
return

;1F93D 1F3FC
:*::person-playing-water-polo:-medium-light::
Send,🤽🏼
return

;1F93D 1F3FD
:*::person-playing-water-polo:-medium-skin::
Send,🤽🏽
return

;1F93D 1F3FE
:*::person-playing-water-polo:-medium-dark::
Send,🤽🏾
return

;1F93D 1F3FF
:*::person-playing-water-polo:-dark-skin-to::
Send,🤽🏿
return

;1F93D 200D 2642 FE0F
:*::man-playing-water-polo::
Send,🤽‍♂️
return

;1F93D 200D 2642
:*::man-playing-water-polo::
Send,🤽‍♂
return

;1F93D 1F3FB 200D 2642 FE0F
:*::man-playing-water-polo:-light-skin-tone::
Send,🤽🏻‍♂️
return

;1F93D 1F3FB 200D 2642
:*::man-playing-water-polo:-light-skin-tone::
Send,🤽🏻‍♂
return

;1F93D 1F3FC 200D 2642 FE0F
:*::man-playing-water-polo:-medium-light-sk::
Send,🤽🏼‍♂️
return

;1F93D 1F3FC 200D 2642
:*::man-playing-water-polo:-medium-light-sk::
Send,🤽🏼‍♂
return

;1F93D 1F3FD 200D 2642 FE0F
:*::man-playing-water-polo:-medium-skin-ton::
Send,🤽🏽‍♂️
return

;1F93D 1F3FD 200D 2642
:*::man-playing-water-polo:-medium-skin-ton::
Send,🤽🏽‍♂
return

;1F93D 1F3FE 200D 2642 FE0F
:*::man-playing-water-polo:-medium-dark-ski::
Send,🤽🏾‍♂️
return

;1F93D 1F3FE 200D 2642
:*::man-playing-water-polo:-medium-dark-ski::
Send,🤽🏾‍♂
return

;1F93D 1F3FF 200D 2642 FE0F
:*::man-playing-water-polo:-dark-skin-tone::
Send,🤽🏿‍♂️
return

;1F93D 1F3FF 200D 2642
:*::man-playing-water-polo:-dark-skin-tone::
Send,🤽🏿‍♂
return

;1F93D 200D 2640 FE0F
:*::woman-playing-water-polo::
Send,🤽‍♀️
return

;1F93D 200D 2640
:*::woman-playing-water-polo::
Send,🤽‍♀
return

;1F93D 1F3FB 200D 2640 FE0F
:*::woman-playing-water-polo:-light-skin-to::
Send,🤽🏻‍♀️
return

;1F93D 1F3FB 200D 2640
:*::woman-playing-water-polo:-light-skin-to::
Send,🤽🏻‍♀
return

;1F93D 1F3FC 200D 2640 FE0F
:*::woman-playing-water-polo:-medium-light::
Send,🤽🏼‍♀️
return

;1F93D 1F3FC 200D 2640
:*::woman-playing-water-polo:-medium-light::
Send,🤽🏼‍♀
return

;1F93D 1F3FD 200D 2640 FE0F
:*::woman-playing-water-polo:-medium-skin-t::
Send,🤽🏽‍♀️
return

;1F93D 1F3FD 200D 2640
:*::woman-playing-water-polo:-medium-skin-t::
Send,🤽🏽‍♀
return

;1F93D 1F3FE 200D 2640 FE0F
:*::woman-playing-water-polo:-medium-dark-s::
Send,🤽🏾‍♀️
return

;1F93D 1F3FE 200D 2640
:*::woman-playing-water-polo:-medium-dark-s::
Send,🤽🏾‍♀
return

;1F93D 1F3FF 200D 2640 FE0F
:*::woman-playing-water-polo:-dark-skin-ton::
Send,🤽🏿‍♀️
return

;1F93D 1F3FF 200D 2640
:*::woman-playing-water-polo:-dark-skin-ton::
Send,🤽🏿‍♀
return

;1F93E
:*::person-playing-handball::
Send,🤾
return

;1F93E 1F3FB
:*::person-playing-handball:-light-skin-ton::
Send,🤾🏻
return

;1F93E 1F3FC
:*::person-playing-handball:-medium-light-s::
Send,🤾🏼
return

;1F93E 1F3FD
:*::person-playing-handball:-medium-skin-to::
Send,🤾🏽
return

;1F93E 1F3FE
:*::person-playing-handball:-medium-dark-sk::
Send,🤾🏾
return

;1F93E 1F3FF
:*::person-playing-handball:-dark-skin-tone::
Send,🤾🏿
return

;1F93E 200D 2642 FE0F
:*::man-playing-handball::
Send,🤾‍♂️
return

;1F93E 200D 2642
:*::man-playing-handball::
Send,🤾‍♂
return

;1F93E 1F3FB 200D 2642 FE0F
:*::man-playing-handball:-light-skin-tone::
Send,🤾🏻‍♂️
return

;1F93E 1F3FB 200D 2642
:*::man-playing-handball:-light-skin-tone::
Send,🤾🏻‍♂
return

;1F93E 1F3FC 200D 2642 FE0F
:*::man-playing-handball:-medium-light-skin::
Send,🤾🏼‍♂️
return

;1F93E 1F3FC 200D 2642
:*::man-playing-handball:-medium-light-skin::
Send,🤾🏼‍♂
return

;1F93E 1F3FD 200D 2642 FE0F
:*::man-playing-handball:-medium-skin-tone::
Send,🤾🏽‍♂️
return

;1F93E 1F3FD 200D 2642
:*::man-playing-handball:-medium-skin-tone::
Send,🤾🏽‍♂
return

;1F93E 1F3FE 200D 2642 FE0F
:*::man-playing-handball:-medium-dark-skin::
Send,🤾🏾‍♂️
return

;1F93E 1F3FE 200D 2642
:*::man-playing-handball:-medium-dark-skin::
Send,🤾🏾‍♂
return

;1F93E 1F3FF 200D 2642 FE0F
:*::man-playing-handball:-dark-skin-tone::
Send,🤾🏿‍♂️
return

;1F93E 1F3FF 200D 2642
:*::man-playing-handball:-dark-skin-tone::
Send,🤾🏿‍♂
return

;1F93E 200D 2640 FE0F
:*::woman-playing-handball::
Send,🤾‍♀️
return

;1F93E 200D 2640
:*::woman-playing-handball::
Send,🤾‍♀
return

;1F93E 1F3FB 200D 2640 FE0F
:*::woman-playing-handball:-light-skin-tone::
Send,🤾🏻‍♀️
return

;1F93E 1F3FB 200D 2640
:*::woman-playing-handball:-light-skin-tone::
Send,🤾🏻‍♀
return

;1F93E 1F3FC 200D 2640 FE0F
:*::woman-playing-handball:-medium-light-sk::
Send,🤾🏼‍♀️
return

;1F93E 1F3FC 200D 2640
:*::woman-playing-handball:-medium-light-sk::
Send,🤾🏼‍♀
return

;1F93E 1F3FD 200D 2640 FE0F
:*::woman-playing-handball:-medium-skin-ton::
Send,🤾🏽‍♀️
return

;1F93E 1F3FD 200D 2640
:*::woman-playing-handball:-medium-skin-ton::
Send,🤾🏽‍♀
return

;1F93E 1F3FE 200D 2640 FE0F
:*::woman-playing-handball:-medium-dark-ski::
Send,🤾🏾‍♀️
return

;1F93E 1F3FE 200D 2640
:*::woman-playing-handball:-medium-dark-ski::
Send,🤾🏾‍♀
return

;1F93E 1F3FF 200D 2640 FE0F
:*::woman-playing-handball:-dark-skin-tone::
Send,🤾🏿‍♀️
return

;1F93E 1F3FF 200D 2640
:*::woman-playing-handball:-dark-skin-tone::
Send,🤾🏿‍♀
return

;1F939
:*::person-juggling::
Send,🤹
return

;1F939 1F3FB
:*::person-juggling:-light-skin-tone::
Send,🤹🏻
return

;1F939 1F3FC
:*::person-juggling:-medium-light-skin-tone::
Send,🤹🏼
return

;1F939 1F3FD
:*::person-juggling:-medium-skin-tone::
Send,🤹🏽
return

;1F939 1F3FE
:*::person-juggling:-medium-dark-skin-tone::
Send,🤹🏾
return

;1F939 1F3FF
:*::person-juggling:-dark-skin-tone::
Send,🤹🏿
return

;1F939 200D 2642 FE0F
:*::man-juggling::
Send,🤹‍♂️
return

;1F939 200D 2642
:*::man-juggling::
Send,🤹‍♂
return

;1F939 1F3FB 200D 2642 FE0F
:*::man-juggling:-light-skin-tone::
Send,🤹🏻‍♂️
return

;1F939 1F3FB 200D 2642
:*::man-juggling:-light-skin-tone::
Send,🤹🏻‍♂
return

;1F939 1F3FC 200D 2642 FE0F
:*::man-juggling:-medium-light-skin-tone::
Send,🤹🏼‍♂️
return

;1F939 1F3FC 200D 2642
:*::man-juggling:-medium-light-skin-tone::
Send,🤹🏼‍♂
return

;1F939 1F3FD 200D 2642 FE0F
:*::man-juggling:-medium-skin-tone::
Send,🤹🏽‍♂️
return

;1F939 1F3FD 200D 2642
:*::man-juggling:-medium-skin-tone::
Send,🤹🏽‍♂
return

;1F939 1F3FE 200D 2642 FE0F
:*::man-juggling:-medium-dark-skin-tone::
Send,🤹🏾‍♂️
return

;1F939 1F3FE 200D 2642
:*::man-juggling:-medium-dark-skin-tone::
Send,🤹🏾‍♂
return

;1F939 1F3FF 200D 2642 FE0F
:*::man-juggling:-dark-skin-tone::
Send,🤹🏿‍♂️
return

;1F939 1F3FF 200D 2642
:*::man-juggling:-dark-skin-tone::
Send,🤹🏿‍♂
return

;1F939 200D 2640 FE0F
:*::woman-juggling::
Send,🤹‍♀️
return

;1F939 200D 2640
:*::woman-juggling::
Send,🤹‍♀
return

;1F939 1F3FB 200D 2640 FE0F
:*::woman-juggling:-light-skin-tone::
Send,🤹🏻‍♀️
return

;1F939 1F3FB 200D 2640
:*::woman-juggling:-light-skin-tone::
Send,🤹🏻‍♀
return

;1F939 1F3FC 200D 2640 FE0F
:*::woman-juggling:-medium-light-skin-tone::
Send,🤹🏼‍♀️
return

;1F939 1F3FC 200D 2640
:*::woman-juggling:-medium-light-skin-tone::
Send,🤹🏼‍♀
return

;1F939 1F3FD 200D 2640 FE0F
:*::woman-juggling:-medium-skin-tone::
Send,🤹🏽‍♀️
return

;1F939 1F3FD 200D 2640
:*::woman-juggling:-medium-skin-tone::
Send,🤹🏽‍♀
return

;1F939 1F3FE 200D 2640 FE0F
:*::woman-juggling:-medium-dark-skin-tone::
Send,🤹🏾‍♀️
return

;1F939 1F3FE 200D 2640
:*::woman-juggling:-medium-dark-skin-tone::
Send,🤹🏾‍♀
return

;1F939 1F3FF 200D 2640 FE0F
:*::woman-juggling:-dark-skin-tone::
Send,🤹🏿‍♀️
return

;1F939 1F3FF 200D 2640
:*::woman-juggling:-dark-skin-tone::
Send,🤹🏿‍♀
return

;1F9D8
:*::person-in-lotus-position::
Send,🧘
return

;1F9D8 1F3FB
:*::person-in-lotus-position:-light-skin-to::
Send,🧘🏻
return

;1F9D8 1F3FC
:*::person-in-lotus-position:-medium-light::
Send,🧘🏼
return

;1F9D8 1F3FD
:*::person-in-lotus-position:-medium-skin-t::
Send,🧘🏽
return

;1F9D8 1F3FE
:*::person-in-lotus-position:-medium-dark-s::
Send,🧘🏾
return

;1F9D8 1F3FF
:*::person-in-lotus-position:-dark-skin-ton::
Send,🧘🏿
return

;1F9D8 200D 2642 FE0F
:*::man-in-lotus-position::
Send,🧘‍♂️
return

;1F9D8 200D 2642
:*::man-in-lotus-position::
Send,🧘‍♂
return

;1F9D8 1F3FB 200D 2642 FE0F
:*::man-in-lotus-position:-light-skin-tone::
Send,🧘🏻‍♂️
return

;1F9D8 1F3FB 200D 2642
:*::man-in-lotus-position:-light-skin-tone::
Send,🧘🏻‍♂
return

;1F9D8 1F3FC 200D 2642 FE0F
:*::man-in-lotus-position:-medium-light-ski::
Send,🧘🏼‍♂️
return

;1F9D8 1F3FC 200D 2642
:*::man-in-lotus-position:-medium-light-ski::
Send,🧘🏼‍♂
return

;1F9D8 1F3FD 200D 2642 FE0F
:*::man-in-lotus-position:-medium-skin-tone::
Send,🧘🏽‍♂️
return

;1F9D8 1F3FD 200D 2642
:*::man-in-lotus-position:-medium-skin-tone::
Send,🧘🏽‍♂
return

;1F9D8 1F3FE 200D 2642 FE0F
:*::man-in-lotus-position:-medium-dark-skin::
Send,🧘🏾‍♂️
return

;1F9D8 1F3FE 200D 2642
:*::man-in-lotus-position:-medium-dark-skin::
Send,🧘🏾‍♂
return

;1F9D8 1F3FF 200D 2642 FE0F
:*::man-in-lotus-position:-dark-skin-tone::
Send,🧘🏿‍♂️
return

;1F9D8 1F3FF 200D 2642
:*::man-in-lotus-position:-dark-skin-tone::
Send,🧘🏿‍♂
return

;1F9D8 200D 2640 FE0F
:*::woman-in-lotus-position::
Send,🧘‍♀️
return

;1F9D8 200D 2640
:*::woman-in-lotus-position::
Send,🧘‍♀
return

;1F9D8 1F3FB 200D 2640 FE0F
:*::woman-in-lotus-position:-light-skin-ton::
Send,🧘🏻‍♀️
return

;1F9D8 1F3FB 200D 2640
:*::woman-in-lotus-position:-light-skin-ton::
Send,🧘🏻‍♀
return

;1F9D8 1F3FC 200D 2640 FE0F
:*::woman-in-lotus-position:-medium-light-s::
Send,🧘🏼‍♀️
return

;1F9D8 1F3FC 200D 2640
:*::woman-in-lotus-position:-medium-light-s::
Send,🧘🏼‍♀
return

;1F9D8 1F3FD 200D 2640 FE0F
:*::woman-in-lotus-position:-medium-skin-to::
Send,🧘🏽‍♀️
return

;1F9D8 1F3FD 200D 2640
:*::woman-in-lotus-position:-medium-skin-to::
Send,🧘🏽‍♀
return

;1F9D8 1F3FE 200D 2640 FE0F
:*::woman-in-lotus-position:-medium-dark-sk::
Send,🧘🏾‍♀️
return

;1F9D8 1F3FE 200D 2640
:*::woman-in-lotus-position:-medium-dark-sk::
Send,🧘🏾‍♀
return

;1F9D8 1F3FF 200D 2640 FE0F
:*::woman-in-lotus-position:-dark-skin-tone::
Send,🧘🏿‍♀️
return

;1F9D8 1F3FF 200D 2640
:*::woman-in-lotus-position:-dark-skin-tone::
Send,🧘🏿‍♀
return

;1F6C0
:*::person-taking-bath::
Send,🛀
return

;1F6C0 1F3FB
:*::person-taking-bath:-light-skin-tone::
Send,🛀🏻
return

;1F6C0 1F3FC
:*::person-taking-bath:-medium-light-skin-t::
Send,🛀🏼
return

;1F6C0 1F3FD
:*::person-taking-bath:-medium-skin-tone::
Send,🛀🏽
return

;1F6C0 1F3FE
:*::person-taking-bath:-medium-dark-skin-to::
Send,🛀🏾
return

;1F6C0 1F3FF
:*::person-taking-bath:-dark-skin-tone::
Send,🛀🏿
return

;1F6CC
:*::person-in-bed::
Send,🛌
return

;1F6CC 1F3FB
:*::person-in-bed:-light-skin-tone::
Send,🛌🏻
return

;1F6CC 1F3FC
:*::person-in-bed:-medium-light-skin-tone::
Send,🛌🏼
return

;1F6CC 1F3FD
:*::person-in-bed:-medium-skin-tone::
Send,🛌🏽
return

;1F6CC 1F3FE
:*::person-in-bed:-medium-dark-skin-tone::
Send,🛌🏾
return

;1F6CC 1F3FF
:*::person-in-bed:-dark-skin-tone::
Send,🛌🏿
return

;1F9D1 200D 1F91D 200D 1F9D1
:*::people-holding-hands::
Send,🧑‍🤝‍🧑
return

;1F9D1 1F3FB 200D 1F91D 200D 1F9D1 1F3FB
:*::people-holding-hands:-light-skin-tone::
Send,🧑🏻‍🤝‍🧑🏻
return

;1F9D1 1F3FC 200D 1F91D 200D 1F9D1 1F3FB
:*::people-holding-hands:-medium-light-skin::
Send,🧑🏼‍🤝‍🧑🏻
return

;1F9D1 1F3FC 200D 1F91D 200D 1F9D1 1F3FC
:*::people-holding-hands:-medium-light-skin::
Send,🧑🏼‍🤝‍🧑🏼
return

;1F9D1 1F3FD 200D 1F91D 200D 1F9D1 1F3FB
:*::people-holding-hands:-medium-skin-tone,::
Send,🧑🏽‍🤝‍🧑🏻
return

;1F9D1 1F3FD 200D 1F91D 200D 1F9D1 1F3FC
:*::people-holding-hands:-medium-skin-tone,::
Send,🧑🏽‍🤝‍🧑🏼
return

;1F9D1 1F3FD 200D 1F91D 200D 1F9D1 1F3FD
:*::people-holding-hands:-medium-skin-tone::
Send,🧑🏽‍🤝‍🧑🏽
return

;1F9D1 1F3FE 200D 1F91D 200D 1F9D1 1F3FB
:*::people-holding-hands:-medium-dark-skin::
Send,🧑🏾‍🤝‍🧑🏻
return

;1F9D1 1F3FE 200D 1F91D 200D 1F9D1 1F3FC
:*::people-holding-hands:-medium-dark-skin::
Send,🧑🏾‍🤝‍🧑🏼
return

;1F9D1 1F3FE 200D 1F91D 200D 1F9D1 1F3FD
:*::people-holding-hands:-medium-dark-skin::
Send,🧑🏾‍🤝‍🧑🏽
return

;1F9D1 1F3FE 200D 1F91D 200D 1F9D1 1F3FE
:*::people-holding-hands:-medium-dark-skin::
Send,🧑🏾‍🤝‍🧑🏾
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FB
:*::people-holding-hands:-dark-skin-tone,-l::
Send,🧑🏿‍🤝‍🧑🏻
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FC
:*::people-holding-hands:-dark-skin-tone,-m::
Send,🧑🏿‍🤝‍🧑🏼
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FD
:*::people-holding-hands:-dark-skin-tone,-m::
Send,🧑🏿‍🤝‍🧑🏽
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FE
:*::people-holding-hands:-dark-skin-tone,-m::
Send,🧑🏿‍🤝‍🧑🏾
return

;1F9D1 1F3FF 200D 1F91D 200D 1F9D1 1F3FF
:*::people-holding-hands:-dark-skin-tone::
Send,🧑🏿‍🤝‍🧑🏿
return

;1F46D
:*::women-holding-hands::
Send,👭
return

;1F46D 1F3FB
:*::women-holding-hands:-light-skin-tone::
Send,👭🏻
return

;1F469 1F3FC 200D 1F91D 200D 1F469 1F3FB
:*::women-holding-hands:-medium-light-skin::
Send,👩🏼‍🤝‍👩🏻
return

;1F46D 1F3FC
:*::women-holding-hands:-medium-light-skin::
Send,👭🏼
return

;1F469 1F3FD 200D 1F91D 200D 1F469 1F3FB
:*::women-holding-hands:-medium-skin-tone,::
Send,👩🏽‍🤝‍👩🏻
return

;1F469 1F3FD 200D 1F91D 200D 1F469 1F3FC
:*::women-holding-hands:-medium-skin-tone,::
Send,👩🏽‍🤝‍👩🏼
return

;1F46D 1F3FD
:*::women-holding-hands:-medium-skin-tone::
Send,👭🏽
return

;1F469 1F3FE 200D 1F91D 200D 1F469 1F3FB
:*::women-holding-hands:-medium-dark-skin-t::
Send,👩🏾‍🤝‍👩🏻
return

;1F469 1F3FE 200D 1F91D 200D 1F469 1F3FC
:*::women-holding-hands:-medium-dark-skin-t::
Send,👩🏾‍🤝‍👩🏼
return

;1F469 1F3FE 200D 1F91D 200D 1F469 1F3FD
:*::women-holding-hands:-medium-dark-skin-t::
Send,👩🏾‍🤝‍👩🏽
return

;1F46D 1F3FE
:*::women-holding-hands:-medium-dark-skin-t::
Send,👭🏾
return

;1F469 1F3FF 200D 1F91D 200D 1F469 1F3FB
:*::women-holding-hands:-dark-skin-tone,-li::
Send,👩🏿‍🤝‍👩🏻
return

;1F469 1F3FF 200D 1F91D 200D 1F469 1F3FC
:*::women-holding-hands:-dark-skin-tone,-me::
Send,👩🏿‍🤝‍👩🏼
return

;1F469 1F3FF 200D 1F91D 200D 1F469 1F3FD
:*::women-holding-hands:-dark-skin-tone,-me::
Send,👩🏿‍🤝‍👩🏽
return

;1F469 1F3FF 200D 1F91D 200D 1F469 1F3FE
:*::women-holding-hands:-dark-skin-tone,-me::
Send,👩🏿‍🤝‍👩🏾
return

;1F46D 1F3FF
:*::women-holding-hands:-dark-skin-tone::
Send,👭🏿
return

;1F46B
:*::woman-and-man-holding-hands::
Send,👫
return

;1F46B 1F3FB
:*::woman-and-man-holding-hands:-light-skin::
Send,👫🏻
return

;1F469 1F3FB 200D 1F91D 200D 1F468 1F3FC
:*::woman-and-man-holding-hands:-light-skin::
Send,👩🏻‍🤝‍👨🏼
return

;1F469 1F3FB 200D 1F91D 200D 1F468 1F3FD
:*::woman-and-man-holding-hands:-light-skin::
Send,👩🏻‍🤝‍👨🏽
return

;1F469 1F3FB 200D 1F91D 200D 1F468 1F3FE
:*::woman-and-man-holding-hands:-light-skin::
Send,👩🏻‍🤝‍👨🏾
return

;1F469 1F3FB 200D 1F91D 200D 1F468 1F3FF
:*::woman-and-man-holding-hands:-light-skin::
Send,👩🏻‍🤝‍👨🏿
return

;1F469 1F3FC 200D 1F91D 200D 1F468 1F3FB
:*::woman-and-man-holding-hands:-medium-lig::
Send,👩🏼‍🤝‍👨🏻
return

;1F46B 1F3FC
:*::woman-and-man-holding-hands:-medium-lig::
Send,👫🏼
return

;1F469 1F3FC 200D 1F91D 200D 1F468 1F3FD
:*::woman-and-man-holding-hands:-medium-lig::
Send,👩🏼‍🤝‍👨🏽
return

;1F469 1F3FC 200D 1F91D 200D 1F468 1F3FE
:*::woman-and-man-holding-hands:-medium-lig::
Send,👩🏼‍🤝‍👨🏾
return

;1F469 1F3FC 200D 1F91D 200D 1F468 1F3FF
:*::woman-and-man-holding-hands:-medium-lig::
Send,👩🏼‍🤝‍👨🏿
return

;1F469 1F3FD 200D 1F91D 200D 1F468 1F3FB
:*::woman-and-man-holding-hands:-medium-ski::
Send,👩🏽‍🤝‍👨🏻
return

;1F469 1F3FD 200D 1F91D 200D 1F468 1F3FC
:*::woman-and-man-holding-hands:-medium-ski::
Send,👩🏽‍🤝‍👨🏼
return

;1F46B 1F3FD
:*::woman-and-man-holding-hands:-medium-ski::
Send,👫🏽
return

;1F469 1F3FD 200D 1F91D 200D 1F468 1F3FE
:*::woman-and-man-holding-hands:-medium-ski::
Send,👩🏽‍🤝‍👨🏾
return

;1F469 1F3FD 200D 1F91D 200D 1F468 1F3FF
:*::woman-and-man-holding-hands:-medium-ski::
Send,👩🏽‍🤝‍👨🏿
return

;1F469 1F3FE 200D 1F91D 200D 1F468 1F3FB
:*::woman-and-man-holding-hands:-medium-dar::
Send,👩🏾‍🤝‍👨🏻
return

;1F469 1F3FE 200D 1F91D 200D 1F468 1F3FC
:*::woman-and-man-holding-hands:-medium-dar::
Send,👩🏾‍🤝‍👨🏼
return

;1F469 1F3FE 200D 1F91D 200D 1F468 1F3FD
:*::woman-and-man-holding-hands:-medium-dar::
Send,👩🏾‍🤝‍👨🏽
return

;1F46B 1F3FE
:*::woman-and-man-holding-hands:-medium-dar::
Send,👫🏾
return

;1F469 1F3FE 200D 1F91D 200D 1F468 1F3FF
:*::woman-and-man-holding-hands:-medium-dar::
Send,👩🏾‍🤝‍👨🏿
return

;1F469 1F3FF 200D 1F91D 200D 1F468 1F3FB
:*::woman-and-man-holding-hands:-dark-skin::
Send,👩🏿‍🤝‍👨🏻
return

;1F469 1F3FF 200D 1F91D 200D 1F468 1F3FC
:*::woman-and-man-holding-hands:-dark-skin::
Send,👩🏿‍🤝‍👨🏼
return

;1F469 1F3FF 200D 1F91D 200D 1F468 1F3FD
:*::woman-and-man-holding-hands:-dark-skin::
Send,👩🏿‍🤝‍👨🏽
return

;1F469 1F3FF 200D 1F91D 200D 1F468 1F3FE
:*::woman-and-man-holding-hands:-dark-skin::
Send,👩🏿‍🤝‍👨🏾
return

;1F46B 1F3FF
:*::woman-and-man-holding-hands:-dark-skin::
Send,👫🏿
return

;1F46C
:*::men-holding-hands::
Send,👬
return

;1F46C 1F3FB
:*::men-holding-hands:-light-skin-tone::
Send,👬🏻
return

;1F468 1F3FC 200D 1F91D 200D 1F468 1F3FB
:*::men-holding-hands:-medium-light-skin-to::
Send,👨🏼‍🤝‍👨🏻
return

;1F46C 1F3FC
:*::men-holding-hands:-medium-light-skin-to::
Send,👬🏼
return

;1F468 1F3FD 200D 1F91D 200D 1F468 1F3FB
:*::men-holding-hands:-medium-skin-tone,-li::
Send,👨🏽‍🤝‍👨🏻
return

;1F468 1F3FD 200D 1F91D 200D 1F468 1F3FC
:*::men-holding-hands:-medium-skin-tone,-me::
Send,👨🏽‍🤝‍👨🏼
return

;1F46C 1F3FD
:*::men-holding-hands:-medium-skin-tone::
Send,👬🏽
return

;1F468 1F3FE 200D 1F91D 200D 1F468 1F3FB
:*::men-holding-hands:-medium-dark-skin-ton::
Send,👨🏾‍🤝‍👨🏻
return

;1F468 1F3FE 200D 1F91D 200D 1F468 1F3FC
:*::men-holding-hands:-medium-dark-skin-ton::
Send,👨🏾‍🤝‍👨🏼
return

;1F468 1F3FE 200D 1F91D 200D 1F468 1F3FD
:*::men-holding-hands:-medium-dark-skin-ton::
Send,👨🏾‍🤝‍👨🏽
return

;1F46C 1F3FE
:*::men-holding-hands:-medium-dark-skin-ton::
Send,👬🏾
return

;1F468 1F3FF 200D 1F91D 200D 1F468 1F3FB
:*::men-holding-hands:-dark-skin-tone,-ligh::
Send,👨🏿‍🤝‍👨🏻
return

;1F468 1F3FF 200D 1F91D 200D 1F468 1F3FC
:*::men-holding-hands:-dark-skin-tone,-medi::
Send,👨🏿‍🤝‍👨🏼
return

;1F468 1F3FF 200D 1F91D 200D 1F468 1F3FD
:*::men-holding-hands:-dark-skin-tone,-medi::
Send,👨🏿‍🤝‍👨🏽
return

;1F468 1F3FF 200D 1F91D 200D 1F468 1F3FE
:*::men-holding-hands:-dark-skin-tone,-medi::
Send,👨🏿‍🤝‍👨🏾
return

;1F46C 1F3FF
:*::men-holding-hands:-dark-skin-tone::
Send,👬🏿
return

;1F48F
:*::kiss::
Send,💏
return

;1F469 200D 2764 FE0F 200D 1F48B 200D 1F468
:*::kiss:-woman,-man::
Send,👩‍❤️‍💋‍👨
return

;1F469 200D 2764 200D 1F48B 200D 1F468
:*::kiss:-woman,-man::
Send,👩‍❤‍💋‍👨
return

;1F468 200D 2764 FE0F 200D 1F48B 200D 1F468
:*::kiss:-man,-man::
Send,👨‍❤️‍💋‍👨
return

;1F468 200D 2764 200D 1F48B 200D 1F468
:*::kiss:-man,-man::
Send,👨‍❤‍💋‍👨
return

;1F469 200D 2764 FE0F 200D 1F48B 200D 1F469
:*::kiss:-woman,-woman::
Send,👩‍❤️‍💋‍👩
return

;1F469 200D 2764 200D 1F48B 200D 1F469
:*::kiss:-woman,-woman::
Send,👩‍❤‍💋‍👩
return

;1F491
:*::couple-with-heart::
Send,💑
return

;1F469 200D 2764 FE0F 200D 1F468
:*::couple-with-heart:-woman,-man::
Send,👩‍❤️‍👨
return

;1F469 200D 2764 200D 1F468
:*::couple-with-heart:-woman,-man::
Send,👩‍❤‍👨
return

;1F468 200D 2764 FE0F 200D 1F468
:*::couple-with-heart:-man,-man::
Send,👨‍❤️‍👨
return

;1F468 200D 2764 200D 1F468
:*::couple-with-heart:-man,-man::
Send,👨‍❤‍👨
return

;1F469 200D 2764 FE0F 200D 1F469
:*::couple-with-heart:-woman,-woman::
Send,👩‍❤️‍👩
return

;1F469 200D 2764 200D 1F469
:*::couple-with-heart:-woman,-woman::
Send,👩‍❤‍👩
return

;1F46A
:*::family::
Send,👪
return

;1F468 200D 1F469 200D 1F466
:*::family:-man,-woman,-boy::
Send,👨‍👩‍👦
return

;1F468 200D 1F469 200D 1F467
:*::family:-man,-woman,-girl::
Send,👨‍👩‍👧
return

;1F468 200D 1F469 200D 1F467 200D 1F466
:*::family:-man,-woman,-girl,-boy::
Send,👨‍👩‍👧‍👦
return

;1F468 200D 1F469 200D 1F466 200D 1F466
:*::family:-man,-woman,-boy,-boy::
Send,👨‍👩‍👦‍👦
return

;1F468 200D 1F469 200D 1F467 200D 1F467
:*::family:-man,-woman,-girl,-girl::
Send,👨‍👩‍👧‍👧
return

;1F468 200D 1F468 200D 1F466
:*::family:-man,-man,-boy::
Send,👨‍👨‍👦
return

;1F468 200D 1F468 200D 1F467
:*::family:-man,-man,-girl::
Send,👨‍👨‍👧
return

;1F468 200D 1F468 200D 1F467 200D 1F466
:*::family:-man,-man,-girl,-boy::
Send,👨‍👨‍👧‍👦
return

;1F468 200D 1F468 200D 1F466 200D 1F466
:*::family:-man,-man,-boy,-boy::
Send,👨‍👨‍👦‍👦
return

;1F468 200D 1F468 200D 1F467 200D 1F467
:*::family:-man,-man,-girl,-girl::
Send,👨‍👨‍👧‍👧
return

;1F469 200D 1F469 200D 1F466
:*::family:-woman,-woman,-boy::
Send,👩‍👩‍👦
return

;1F469 200D 1F469 200D 1F467
:*::family:-woman,-woman,-girl::
Send,👩‍👩‍👧
return

;1F469 200D 1F469 200D 1F467 200D 1F466
:*::family:-woman,-woman,-girl,-boy::
Send,👩‍👩‍👧‍👦
return

;1F469 200D 1F469 200D 1F466 200D 1F466
:*::family:-woman,-woman,-boy,-boy::
Send,👩‍👩‍👦‍👦
return

;1F469 200D 1F469 200D 1F467 200D 1F467
:*::family:-woman,-woman,-girl,-girl::
Send,👩‍👩‍👧‍👧
return

;1F468 200D 1F466
:*::family:-man,-boy::
Send,👨‍👦
return

;1F468 200D 1F466 200D 1F466
:*::family:-man,-boy,-boy::
Send,👨‍👦‍👦
return

;1F468 200D 1F467
:*::family:-man,-girl::
Send,👨‍👧
return

;1F468 200D 1F467 200D 1F466
:*::family:-man,-girl,-boy::
Send,👨‍👧‍👦
return

;1F468 200D 1F467 200D 1F467
:*::family:-man,-girl,-girl::
Send,👨‍👧‍👧
return

;1F469 200D 1F466
:*::family:-woman,-boy::
Send,👩‍👦
return

;1F469 200D 1F466 200D 1F466
:*::family:-woman,-boy,-boy::
Send,👩‍👦‍👦
return

;1F469 200D 1F467
:*::family:-woman,-girl::
Send,👩‍👧
return

;1F469 200D 1F467 200D 1F466
:*::family:-woman,-girl,-boy::
Send,👩‍👧‍👦
return

;1F469 200D 1F467 200D 1F467
:*::family:-woman,-girl,-girl::
Send,👩‍👧‍👧
return

;1F5E3 FE0F
:*::speaking-head::
Send,🗣️
return

;1F5E3
:*::speaking-head::
Send,🗣
return

;1F464
:*::bust-in-silhouette::
Send,👤
return

;1F465
:*::busts-in-silhouette::
Send,👥
return

;1F463
:*::footprints::
Send,👣
return

;1F3FB
:*::light-skin-tone::
Send,🏻
return

;1F3FC
:*::medium-light-skin-tone::
Send,🏼
return

;1F3FD
:*::medium-skin-tone::
Send,🏽
return

;1F3FE
:*::medium-dark-skin-tone::
Send,🏾
return

;1F3FF
:*::dark-skin-tone::
Send,🏿
return

;1F9B0
:*::red-hair::
Send,🦰
return

;1F9B1
:*::curly-hair::
Send,🦱
return

;1F9B3
:*::white-hair::
Send,🦳
return

;1F9B2
:*::bald::
Send,🦲
return

;1F435
:*::monkey-face::
Send,🐵
return

;1F412
:*::monkey::
Send,🐒
return

;1F98D
:*::gorilla::
Send,🦍
return

;1F9A7
:*::orangutan::
Send,🦧
return

;1F436
:*::dog-face::
Send,🐶
return

;1F415
:*::dog::
Send,🐕
return

;1F9AE
:*::guide-dog::
Send,🦮
return

;1F415 200D 1F9BA
:*::service-dog::
Send,🐕‍🦺
return

;1F429
:*::poodle::
Send,🐩
return

;1F43A
:*::wolf::
Send,🐺
return

;1F98A
:*::fox::
Send,🦊
return

;1F99D
:*::raccoon::
Send,🦝
return

;1F431
:*::cat-face::
Send,🐱
return

;1F408
:*::cat::
Send,🐈
return

;1F981
:*::lion::
Send,🦁
return

;1F42F
:*::tiger-face::
Send,🐯
return

;1F405
:*::tiger::
Send,🐅
return

;1F406
:*::leopard::
Send,🐆
return

;1F434
:*::horse-face::
Send,🐴
return

;1F40E
:*::horse::
Send,🐎
return

;1F984
:*::unicorn::
Send,🦄
return

;1F993
:*::zebra::
Send,🦓
return

;1F98C
:*::deer::
Send,🦌
return

;1F42E
:*::cow-face::
Send,🐮
return

;1F402
:*::ox::
Send,🐂
return

;1F403
:*::water-buffalo::
Send,🐃
return

;1F404
:*::cow::
Send,🐄
return

;1F437
:*::pig-face::
Send,🐷
return

;1F416
:*::pig::
Send,🐖
return

;1F417
:*::boar::
Send,🐗
return

;1F43D
:*::pig-nose::
Send,🐽
return

;1F40F
:*::ram::
Send,🐏
return

;1F411
:*::ewe::
Send,🐑
return

;1F410
:*::goat::
Send,🐐
return

;1F42A
:*::camel::
Send,🐪
return

;1F42B
:*::two-hump-camel::
Send,🐫
return

;1F999
:*::llama::
Send,🦙
return

;1F992
:*::giraffe::
Send,🦒
return

;1F418
:*::elephant::
Send,🐘
return

;1F98F
:*::rhinoceros::
Send,🦏
return

;1F99B
:*::hippopotamus::
Send,🦛
return

;1F42D
:*::mouse-face::
Send,🐭
return

;1F401
:*::mouse::
Send,🐁
return

;1F400
:*::rat::
Send,🐀
return

;1F439
:*::hamster::
Send,🐹
return

;1F430
:*::rabbit-face::
Send,🐰
return

;1F407
:*::rabbit::
Send,🐇
return

;1F43F FE0F
:*::chipmunk::
Send,🐿️
return

;1F43F
:*::chipmunk::
Send,🐿
return

;1F994
:*::hedgehog::
Send,🦔
return

;1F987
:*::bat::
Send,🦇
return

;1F43B
:*::bear::
Send,🐻
return

;1F428
:*::koala::
Send,🐨
return

;1F43C
:*::panda::
Send,🐼
return

;1F9A5
:*::sloth::
Send,🦥
return

;1F9A6
:*::otter::
Send,🦦
return

;1F9A8
:*::skunk::
Send,🦨
return

;1F998
:*::kangaroo::
Send,🦘
return

;1F9A1
:*::badger::
Send,🦡
return

;1F43E
:*::paw-prints::
Send,🐾
return

;1F983
:*::turkey::
Send,🦃
return

;1F414
:*::chicken::
Send,🐔
return

;1F413
:*::rooster::
Send,🐓
return

;1F423
:*::hatching-chick::
Send,🐣
return

;1F424
:*::baby-chick::
Send,🐤
return

;1F425
:*::front-facing-baby-chick::
Send,🐥
return

;1F426
:*::bird::
Send,🐦
return

;1F427
:*::penguin::
Send,🐧
return

;1F54A FE0F
:*::dove::
Send,🕊️
return

;1F54A
:*::dove::
Send,🕊
return

;1F985
:*::eagle::
Send,🦅
return

;1F986
:*::duck::
Send,🦆
return

;1F9A2
:*::swan::
Send,🦢
return

;1F989
:*::owl::
Send,🦉
return

;1F9A9
:*::flamingo::
Send,🦩
return

;1F99A
:*::peacock::
Send,🦚
return

;1F99C
:*::parrot::
Send,🦜
return

;1F438
:*::frog::
Send,🐸
return

;1F40A
:*::crocodile::
Send,🐊
return

;1F422
:*::turtle::
Send,🐢
return

;1F98E
:*::lizard::
Send,🦎
return

;1F40D
:*::snake::
Send,🐍
return

;1F432
:*::dragon-face::
Send,🐲
return

;1F409
:*::dragon::
Send,🐉
return

;1F995
:*::sauropod::
Send,🦕
return

;1F996
:*::T-Rex::
Send,🦖
return

;1F433
:*::spouting-whale::
Send,🐳
return

;1F40B
:*::whale::
Send,🐋
return

;1F42C
:*::dolphin::
Send,🐬
return

;1F41F
:*::fish::
Send,🐟
return

;1F420
:*::tropical-fish::
Send,🐠
return

;1F421
:*::blowfish::
Send,🐡
return

;1F988
:*::shark::
Send,🦈
return

;1F419
:*::octopus::
Send,🐙
return

;1F41A
:*::spiral-shell::
Send,🐚
return

;1F40C
:*::snail::
Send,🐌
return

;1F98B
:*::butterfly::
Send,🦋
return

;1F41B
:*::bug::
Send,🐛
return

;1F41C
:*::ant::
Send,🐜
return

;1F41D
:*::honeybee::
Send,🐝
return

;1F41E
:*::lady-beetle::
Send,🐞
return

;1F997
:*::cricket::
Send,🦗
return

;1F577 FE0F
:*::spider::
Send,🕷️
return

;1F577
:*::spider::
Send,🕷
return

;1F578 FE0F
:*::spider-web::
Send,🕸️
return

;1F578
:*::spider-web::
Send,🕸
return

;1F982
:*::scorpion::
Send,🦂
return

;1F99F
:*::mosquito::
Send,🦟
return

;1F9A0
:*::microbe::
Send,🦠
return

;1F490
:*::bouquet::
Send,💐
return

;1F338
:*::cherry-blossom::
Send,🌸
return

;1F4AE
:*::white-flower::
Send,💮
return

;1F3F5 FE0F
:*::rosette::
Send,🏵️
return

;1F3F5
:*::rosette::
Send,🏵
return

;1F339
:*::rose::
Send,🌹
return

;1F940
:*::wilted-flower::
Send,🥀
return

;1F33A
:*::hibiscus::
Send,🌺
return

;1F33B
:*::sunflower::
Send,🌻
return

;1F33C
:*::blossom::
Send,🌼
return

;1F337
:*::tulip::
Send,🌷
return

;1F331
:*::seedling::
Send,🌱
return

;1F332
:*::evergreen-tree::
Send,🌲
return

;1F333
:*::deciduous-tree::
Send,🌳
return

;1F334
:*::palm-tree::
Send,🌴
return

;1F335
:*::cactus::
Send,🌵
return

;1F33E
:*::sheaf-of-rice::
Send,🌾
return

;1F33F
:*::herb::
Send,🌿
return

;2618 FE0F
:*::shamrock::
Send,☘️
return

;2618
:*::shamrock::
Send,☘
return

;1F340
:*::four-leaf-clover::
Send,🍀
return

;1F341
:*::maple-leaf::
Send,🍁
return

;1F342
:*::fallen-leaf::
Send,🍂
return

;1F343
:*::leaf-fluttering-in-wind::
Send,🍃
return

;1F347
:*::grapes::
Send,🍇
return

;1F348
:*::melon::
Send,🍈
return

;1F349
:*::watermelon::
Send,🍉
return

;1F34A
:*::tangerine::
Send,🍊
return

;1F34B
:*::lemon::
Send,🍋
return

;1F34C
:*::banana::
Send,🍌
return

;1F34D
:*::pineapple::
Send,🍍
return

;1F96D
:*::mango::
Send,🥭
return

;1F34E
:*::red-apple::
Send,🍎
return

;1F34F
:*::green-apple::
Send,🍏
return

;1F350
:*::pear::
Send,🍐
return

;1F351
:*::peach::
Send,🍑
return

;1F352
:*::cherries::
Send,🍒
return

;1F353
:*::strawberry::
Send,🍓
return

;1F95D
:*::kiwi-fruit::
Send,🥝
return

;1F345
:*::tomato::
Send,🍅
return

;1F965
:*::coconut::
Send,🥥
return

;1F951
:*::avocado::
Send,🥑
return

;1F346
:*::eggplant::
Send,🍆
return

;1F954
:*::potato::
Send,🥔
return

;1F955
:*::carrot::
Send,🥕
return

;1F33D
:*::ear-of-corn::
Send,🌽
return

;1F336 FE0F
:*::hot-pepper::
Send,🌶️
return

;1F336
:*::hot-pepper::
Send,🌶
return

;1F952
:*::cucumber::
Send,🥒
return

;1F96C
:*::leafy-green::
Send,🥬
return

;1F966
:*::broccoli::
Send,🥦
return

;1F9C4
:*::garlic::
Send,🧄
return

;1F9C5
:*::onion::
Send,🧅
return

;1F344
:*::mushroom::
Send,🍄
return

;1F95C
:*::peanuts::
Send,🥜
return

;1F330
:*::chestnut::
Send,🌰
return

;1F35E
:*::bread::
Send,🍞
return

;1F950
:*::croissant::
Send,🥐
return

;1F956
:*::baguette-bread::
Send,🥖
return

;1F968
:*::pretzel::
Send,🥨
return

;1F96F
:*::bagel::
Send,🥯
return

;1F95E
:*::pancakes::
Send,🥞
return

;1F9C7
:*::waffle::
Send,🧇
return

;1F9C0
:*::cheese-wedge::
Send,🧀
return

;1F356
:*::meat-on-bone::
Send,🍖
return

;1F357
:*::poultry-leg::
Send,🍗
return

;1F969
:*::cut-of-meat::
Send,🥩
return

;1F953
:*::bacon::
Send,🥓
return

;1F354
:*::hamburger::
Send,🍔
return

;1F35F
:*::french-fries::
Send,🍟
return

;1F355
:*::pizza::
Send,🍕
return

;1F32D
:*::hot-dog::
Send,🌭
return

;1F96A
:*::sandwich::
Send,🥪
return

;1F32E
:*::taco::
Send,🌮
return

;1F32F
:*::burrito::
Send,🌯
return

;1F959
:*::stuffed-flatbread::
Send,🥙
return

;1F9C6
:*::falafel::
Send,🧆
return

;1F95A
:*::egg::
Send,🥚
return

;1F373
:*::cooking::
Send,🍳
return

;1F958
:*::shallow-pan-of-food::
Send,🥘
return

;1F372
:*::pot-of-food::
Send,🍲
return

;1F963
:*::bowl-with-spoon::
Send,🥣
return

;1F957
:*::green-salad::
Send,🥗
return

;1F37F
:*::popcorn::
Send,🍿
return

;1F9C8
:*::butter::
Send,🧈
return

;1F9C2
:*::salt::
Send,🧂
return

;1F96B
:*::canned-food::
Send,🥫
return

;1F371
:*::bento-box::
Send,🍱
return

;1F358
:*::rice-cracker::
Send,🍘
return

;1F359
:*::rice-ball::
Send,🍙
return

;1F35A
:*::cooked-rice::
Send,🍚
return

;1F35B
:*::curry-rice::
Send,🍛
return

;1F35C
:*::steaming-bowl::
Send,🍜
return

;1F35D
:*::spaghetti::
Send,🍝
return

;1F360
:*::roasted-sweet-potato::
Send,🍠
return

;1F362
:*::oden::
Send,🍢
return

;1F363
:*::sushi::
Send,🍣
return

;1F364
:*::fried-shrimp::
Send,🍤
return

;1F365
:*::fish-cake-with-swirl::
Send,🍥
return

;1F96E
:*::moon-cake::
Send,🥮
return

;1F361
:*::dango::
Send,🍡
return

;1F95F
:*::dumpling::
Send,🥟
return

;1F960
:*::fortune-cookie::
Send,🥠
return

;1F961
:*::takeout-box::
Send,🥡
return

;1F980
:*::crab::
Send,🦀
return

;1F99E
:*::lobster::
Send,🦞
return

;1F990
:*::shrimp::
Send,🦐
return

;1F991
:*::squid::
Send,🦑
return

;1F9AA
:*::oyster::
Send,🦪
return

;1F366
:*::soft-ice-cream::
Send,🍦
return

;1F367
:*::shaved-ice::
Send,🍧
return

;1F368
:*::ice-cream::
Send,🍨
return

;1F369
:*::doughnut::
Send,🍩
return

;1F36A
:*::cookie::
Send,🍪
return

;1F382
:*::birthday-cake::
Send,🎂
return

;1F370
:*::shortcake::
Send,🍰
return

;1F9C1
:*::cupcake::
Send,🧁
return

;1F967
:*::pie::
Send,🥧
return

;1F36B
:*::chocolate-bar::
Send,🍫
return

;1F36C
:*::candy::
Send,🍬
return

;1F36D
:*::lollipop::
Send,🍭
return

;1F36E
:*::custard::
Send,🍮
return

;1F36F
:*::honey-pot::
Send,🍯
return

;1F37C
:*::baby-bottle::
Send,🍼
return

;1F95B
:*::glass-of-milk::
Send,🥛
return

;2615
:*::hot-beverage::
Send,☕
return

;1F375
:*::teacup-without-handle::
Send,🍵
return

;1F376
:*::sake::
Send,🍶
return

;1F37E
:*::bottle-with-popping-cork::
Send,🍾
return

;1F377
:*::wine-glass::
Send,🍷
return

;1F378
:*::cocktail-glass::
Send,🍸
return

;1F379
:*::tropical-drink::
Send,🍹
return

;1F37A
:*::beer-mug::
Send,🍺
return

;1F37B
:*::clinking-beer-mugs::
Send,🍻
return

;1F942
:*::clinking-glasses::
Send,🥂
return

;1F943
:*::tumbler-glass::
Send,🥃
return

;1F964
:*::cup-with-straw::
Send,🥤
return

;1F9C3
:*::beverage-box::
Send,🧃
return

;1F9C9
:*::mate::
Send,🧉
return

;1F9CA
:*::ice-cube::
Send,🧊
return

;1F962
:*::chopsticks::
Send,🥢
return

;1F37D FE0F
:*::fork-and-knife-with-plate::
Send,🍽️
return

;1F37D
:*::fork-and-knife-with-plate::
Send,🍽
return

;1F374
:*::fork-and-knife::
Send,🍴
return

;1F944
:*::spoon::
Send,🥄
return

;1F52A
:*::kitchen-knife::
Send,🔪
return

;1F3FA
:*::amphora::
Send,🏺
return

;1F30D
:*::globe-showing-Europe-Africa::
Send,🌍
return

;1F30E
:*::globe-showing-Americas::
Send,🌎
return

;1F30F
:*::globe-showing-Asia-Australia::
Send,🌏
return

;1F310
:*::globe-with-meridians::
Send,🌐
return

;1F5FA FE0F
:*::world-map::
Send,🗺️
return

;1F5FA
:*::world-map::
Send,🗺
return

;1F5FE
:*::map-of-Japan::
Send,🗾
return

;1F9ED
:*::compass::
Send,🧭
return

;1F3D4 FE0F
:*::snow-capped-mountain::
Send,🏔️
return

;1F3D4
:*::snow-capped-mountain::
Send,🏔
return

;26F0 FE0F
:*::mountain::
Send,⛰️
return

;26F0
:*::mountain::
Send,⛰
return

;1F30B
:*::volcano::
Send,🌋
return

;1F5FB
:*::mount-fuji::
Send,🗻
return

;1F3D5 FE0F
:*::camping::
Send,🏕️
return

;1F3D5
:*::camping::
Send,🏕
return

;1F3D6 FE0F
:*::beach-with-umbrella::
Send,🏖️
return

;1F3D6
:*::beach-with-umbrella::
Send,🏖
return

;1F3DC FE0F
:*::desert::
Send,🏜️
return

;1F3DC
:*::desert::
Send,🏜
return

;1F3DD FE0F
:*::desert-island::
Send,🏝️
return

;1F3DD
:*::desert-island::
Send,🏝
return

;1F3DE FE0F
:*::national-park::
Send,🏞️
return

;1F3DE
:*::national-park::
Send,🏞
return

;1F3DF FE0F
:*::stadium::
Send,🏟️
return

;1F3DF
:*::stadium::
Send,🏟
return

;1F3DB FE0F
:*::classical-building::
Send,🏛️
return

;1F3DB
:*::classical-building::
Send,🏛
return

;1F3D7 FE0F
:*::building-construction::
Send,🏗️
return

;1F3D7
:*::building-construction::
Send,🏗
return

;1F9F1
:*::brick::
Send,🧱
return

;1F3D8 FE0F
:*::houses::
Send,🏘️
return

;1F3D8
:*::houses::
Send,🏘
return

;1F3DA FE0F
:*::derelict-house::
Send,🏚️
return

;1F3DA
:*::derelict-house::
Send,🏚
return

;1F3E0
:*::house::
Send,🏠
return

;1F3E1
:*::house-with-garden::
Send,🏡
return

;1F3E2
:*::office-building::
Send,🏢
return

;1F3E3
:*::Japanese-post-office::
Send,🏣
return

;1F3E4
:*::post-office::
Send,🏤
return

;1F3E5
:*::hospital::
Send,🏥
return

;1F3E6
:*::bank::
Send,🏦
return

;1F3E8
:*::hotel::
Send,🏨
return

;1F3E9
:*::love-hotel::
Send,🏩
return

;1F3EA
:*::convenience-store::
Send,🏪
return

;1F3EB
:*::school::
Send,🏫
return

;1F3EC
:*::department-store::
Send,🏬
return

;1F3ED
:*::factory::
Send,🏭
return

;1F3EF
:*::Japanese-castle::
Send,🏯
return

;1F3F0
:*::castle::
Send,🏰
return

;1F492
:*::wedding::
Send,💒
return

;1F5FC
:*::Tokyo-tower::
Send,🗼
return

;1F5FD
:*::Statue-of-Liberty::
Send,🗽
return

;26EA
:*::church::
Send,⛪
return

;1F54C
:*::mosque::
Send,🕌
return

;1F6D5
:*::hindu-temple::
Send,🛕
return

;1F54D
:*::synagogue::
Send,🕍
return

;26E9 FE0F
:*::shinto-shrine::
Send,⛩️
return

;26E9
:*::shinto-shrine::
Send,⛩
return

;1F54B
:*::kaaba::
Send,🕋
return

;26F2
:*::fountain::
Send,⛲
return

;26FA
:*::tent::
Send,⛺
return

;1F301
:*::foggy::
Send,🌁
return

;1F303
:*::night-with-stars::
Send,🌃
return

;1F3D9 FE0F
:*::cityscape::
Send,🏙️
return

;1F3D9
:*::cityscape::
Send,🏙
return

;1F304
:*::sunrise-over-mountains::
Send,🌄
return

;1F305
:*::sunrise::
Send,🌅
return

;1F306
:*::cityscape-at-dusk::
Send,🌆
return

;1F307
:*::sunset::
Send,🌇
return

;1F309
:*::bridge-at-night::
Send,🌉
return

;2668 FE0F
:*::hot-springs::
Send,♨️
return

;2668
:*::hot-springs::
Send,♨
return

;1F3A0
:*::carousel-horse::
Send,🎠
return

;1F3A1
:*::ferris-wheel::
Send,🎡
return

;1F3A2
:*::roller-coaster::
Send,🎢
return

;1F488
:*::barber-pole::
Send,💈
return

;1F3AA
:*::circus-tent::
Send,🎪
return

;1F682
:*::locomotive::
Send,🚂
return

;1F683
:*::railway-car::
Send,🚃
return

;1F684
:*::high-speed-train::
Send,🚄
return

;1F685
:*::bullet-train::
Send,🚅
return

;1F686
:*::train::
Send,🚆
return

;1F687
:*::metro::
Send,🚇
return

;1F688
:*::light-rail::
Send,🚈
return

;1F689
:*::station::
Send,🚉
return

;1F68A
:*::tram::
Send,🚊
return

;1F69D
:*::monorail::
Send,🚝
return

;1F69E
:*::mountain-railway::
Send,🚞
return

;1F68B
:*::tram-car::
Send,🚋
return

;1F68C
:*::bus::
Send,🚌
return

;1F68D
:*::oncoming-bus::
Send,🚍
return

;1F68E
:*::trolleybus::
Send,🚎
return

;1F690
:*::minibus::
Send,🚐
return

;1F691
:*::ambulance::
Send,🚑
return

;1F692
:*::fire-engine::
Send,🚒
return

;1F693
:*::police-car::
Send,🚓
return

;1F694
:*::oncoming-police-car::
Send,🚔
return

;1F695
:*::taxi::
Send,🚕
return

;1F696
:*::oncoming-taxi::
Send,🚖
return

;1F697
:*::automobile::
Send,🚗
return

;1F698
:*::oncoming-automobile::
Send,🚘
return

;1F699
:*::sport-utility-vehicle::
Send,🚙
return

;1F69A
:*::delivery-truck::
Send,🚚
return

;1F69B
:*::articulated-lorry::
Send,🚛
return

;1F69C
:*::tractor::
Send,🚜
return

;1F3CE FE0F
:*::racing-car::
Send,🏎️
return

;1F3CE
:*::racing-car::
Send,🏎
return

;1F3CD FE0F
:*::motorcycle::
Send,🏍️
return

;1F3CD
:*::motorcycle::
Send,🏍
return

;1F6F5
:*::motor-scooter::
Send,🛵
return

;1F9BD
:*::manual-wheelchair::
Send,🦽
return

;1F9BC
:*::motorized-wheelchair::
Send,🦼
return

;1F6FA
:*::auto-rickshaw::
Send,🛺
return

;1F6B2
:*::bicycle::
Send,🚲
return

;1F6F4
:*::kick-scooter::
Send,🛴
return

;1F6F9
:*::skateboard::
Send,🛹
return

;1F68F
:*::bus-stop::
Send,🚏
return

;1F6E3 FE0F
:*::motorway::
Send,🛣️
return

;1F6E3
:*::motorway::
Send,🛣
return

;1F6E4 FE0F
:*::railway-track::
Send,🛤️
return

;1F6E4
:*::railway-track::
Send,🛤
return

;1F6E2 FE0F
:*::oil-drum::
Send,🛢️
return

;1F6E2
:*::oil-drum::
Send,🛢
return

;26FD
:*::fuel-pump::
Send,⛽
return

;1F6A8
:*::police-car-light::
Send,🚨
return

;1F6A5
:*::horizontal-traffic-light::
Send,🚥
return

;1F6A6
:*::vertical-traffic-light::
Send,🚦
return

;1F6D1
:*::stop-sign::
Send,🛑
return

;1F6A7
:*::construction::
Send,🚧
return

;2693
:*::anchor::
Send,⚓
return

;26F5
:*::sailboat::
Send,⛵
return

;1F6F6
:*::canoe::
Send,🛶
return

;1F6A4
:*::speedboat::
Send,🚤
return

;1F6F3 FE0F
:*::passenger-ship::
Send,🛳️
return

;1F6F3
:*::passenger-ship::
Send,🛳
return

;26F4 FE0F
:*::ferry::
Send,⛴️
return

;26F4
:*::ferry::
Send,⛴
return

;1F6E5 FE0F
:*::motor-boat::
Send,🛥️
return

;1F6E5
:*::motor-boat::
Send,🛥
return

;1F6A2
:*::ship::
Send,🚢
return

;2708 FE0F
:*::airplane::
Send,✈️
return

;2708
:*::airplane::
Send,✈
return

;1F6E9 FE0F
:*::small-airplane::
Send,🛩️
return

;1F6E9
:*::small-airplane::
Send,🛩
return

;1F6EB
:*::airplane-departure::
Send,🛫
return

;1F6EC
:*::airplane-arrival::
Send,🛬
return

;1FA82
:*::parachute::
Send,🪂
return

;1F4BA
:*::seat::
Send,💺
return

;1F681
:*::helicopter::
Send,🚁
return

;1F69F
:*::suspension-railway::
Send,🚟
return

;1F6A0
:*::mountain-cableway::
Send,🚠
return

;1F6A1
:*::aerial-tramway::
Send,🚡
return

;1F6F0 FE0F
:*::satellite::
Send,🛰️
return

;1F6F0
:*::satellite::
Send,🛰
return

;1F680
:*::rocket::
Send,🚀
return

;1F6F8
:*::flying-saucer::
Send,🛸
return

;1F6CE FE0F
:*::bellhop-bell::
Send,🛎️
return

;1F6CE
:*::bellhop-bell::
Send,🛎
return

;1F9F3
:*::luggage::
Send,🧳
return

;231B
:*::hourglass-done::
Send,⌛
return

;23F3
:*::hourglass-not-done::
Send,⏳
return

;231A
:*::watch::
Send,⌚
return

;23F0
:*::alarm-clock::
Send,⏰
return

;23F1 FE0F
:*::stopwatch::
Send,⏱️
return

;23F1
:*::stopwatch::
Send,⏱
return

;23F2 FE0F
:*::timer-clock::
Send,⏲️
return

;23F2
:*::timer-clock::
Send,⏲
return

;1F570 FE0F
:*::mantelpiece-clock::
Send,🕰️
return

;1F570
:*::mantelpiece-clock::
Send,🕰
return

;1F55B
:*::twelve-o’clock::
Send,🕛
return

;1F567
:*::twelve-thirty::
Send,🕧
return

;1F550
:*::one-o’clock::
Send,🕐
return

;1F55C
:*::one-thirty::
Send,🕜
return

;1F551
:*::two-o’clock::
Send,🕑
return

;1F55D
:*::two-thirty::
Send,🕝
return

;1F552
:*::three-o’clock::
Send,🕒
return

;1F55E
:*::three-thirty::
Send,🕞
return

;1F553
:*::four-o’clock::
Send,🕓
return

;1F55F
:*::four-thirty::
Send,🕟
return

;1F554
:*::five-o’clock::
Send,🕔
return

;1F560
:*::five-thirty::
Send,🕠
return

;1F555
:*::six-o’clock::
Send,🕕
return

;1F561
:*::six-thirty::
Send,🕡
return

;1F556
:*::seven-o’clock::
Send,🕖
return

;1F562
:*::seven-thirty::
Send,🕢
return

;1F557
:*::eight-o’clock::
Send,🕗
return

;1F563
:*::eight-thirty::
Send,🕣
return

;1F558
:*::nine-o’clock::
Send,🕘
return

;1F564
:*::nine-thirty::
Send,🕤
return

;1F559
:*::ten-o’clock::
Send,🕙
return

;1F565
:*::ten-thirty::
Send,🕥
return

;1F55A
:*::eleven-o’clock::
Send,🕚
return

;1F566
:*::eleven-thirty::
Send,🕦
return

;1F311
:*::new-moon::
Send,🌑
return

;1F312
:*::waxing-crescent-moon::
Send,🌒
return

;1F313
:*::first-quarter-moon::
Send,🌓
return

;1F314
:*::waxing-gibbous-moon::
Send,🌔
return

;1F315
:*::full-moon::
Send,🌕
return

;1F316
:*::waning-gibbous-moon::
Send,🌖
return

;1F317
:*::last-quarter-moon::
Send,🌗
return

;1F318
:*::waning-crescent-moon::
Send,🌘
return

;1F319
:*::crescent-moon::
Send,🌙
return

;1F31A
:*::new-moon-face::
Send,🌚
return

;1F31B
:*::first-quarter-moon-face::
Send,🌛
return

;1F31C
:*::last-quarter-moon-face::
Send,🌜
return

;1F321 FE0F
:*::thermometer::
Send,🌡️
return

;1F321
:*::thermometer::
Send,🌡
return

;2600 FE0F
:*::sun::
Send,☀️
return

;2600
:*::sun::
Send,☀
return

;1F31D
:*::full-moon-face::
Send,🌝
return

;1F31E
:*::sun-with-face::
Send,🌞
return

;1FA90
:*::ringed-planet::
Send,🪐
return

;2B50
:*::star::
Send,⭐
return

;1F31F
:*::glowing-star::
Send,🌟
return

;1F320
:*::shooting-star::
Send,🌠
return

;1F30C
:*::milky-way::
Send,🌌
return

;2601 FE0F
:*::cloud::
Send,☁️
return

;2601
:*::cloud::
Send,☁
return

;26C5
:*::sun-behind-cloud::
Send,⛅
return

;26C8 FE0F
:*::cloud-with-lightning-and-rain::
Send,⛈️
return

;26C8
:*::cloud-with-lightning-and-rain::
Send,⛈
return

;1F324 FE0F
:*::sun-behind-small-cloud::
Send,🌤️
return

;1F324
:*::sun-behind-small-cloud::
Send,🌤
return

;1F325 FE0F
:*::sun-behind-large-cloud::
Send,🌥️
return

;1F325
:*::sun-behind-large-cloud::
Send,🌥
return

;1F326 FE0F
:*::sun-behind-rain-cloud::
Send,🌦️
return

;1F326
:*::sun-behind-rain-cloud::
Send,🌦
return

;1F327 FE0F
:*::cloud-with-rain::
Send,🌧️
return

;1F327
:*::cloud-with-rain::
Send,🌧
return

;1F328 FE0F
:*::cloud-with-snow::
Send,🌨️
return

;1F328
:*::cloud-with-snow::
Send,🌨
return

;1F329 FE0F
:*::cloud-with-lightning::
Send,🌩️
return

;1F329
:*::cloud-with-lightning::
Send,🌩
return

;1F32A FE0F
:*::tornado::
Send,🌪️
return

;1F32A
:*::tornado::
Send,🌪
return

;1F32B FE0F
:*::fog::
Send,🌫️
return

;1F32B
:*::fog::
Send,🌫
return

;1F32C FE0F
:*::wind-face::
Send,🌬️
return

;1F32C
:*::wind-face::
Send,🌬
return

;1F300
:*::cyclone::
Send,🌀
return

;1F308
:*::rainbow::
Send,🌈
return

;1F302
:*::closed-umbrella::
Send,🌂
return

;2602 FE0F
:*::umbrella::
Send,☂️
return

;2602
:*::umbrella::
Send,☂
return

;2614
:*::umbrella-with-rain-drops::
Send,☔
return

;26F1 FE0F
:*::umbrella-on-ground::
Send,⛱️
return

;26F1
:*::umbrella-on-ground::
Send,⛱
return

;26A1
:*::high-voltage::
Send,⚡
return

;2744 FE0F
:*::snowflake::
Send,❄️
return

;2744
:*::snowflake::
Send,❄
return

;2603 FE0F
:*::snowman::
Send,☃️
return

;2603
:*::snowman::
Send,☃
return

;26C4
:*::snowman-without-snow::
Send,⛄
return

;2604 FE0F
:*::comet::
Send,☄️
return

;2604
:*::comet::
Send,☄
return

;1F525
:*::fire::
Send,🔥
return

;1F4A7
:*::droplet::
Send,💧
return

;1F30A
:*::water-wave::
Send,🌊
return

;1F383
:*::jack-o-lantern::
Send,🎃
return

;1F384
:*::Christmas-tree::
Send,🎄
return

;1F386
:*::fireworks::
Send,🎆
return

;1F387
:*::sparkler::
Send,🎇
return

;1F9E8
:*::firecracker::
Send,🧨
return

;2728
:*::sparkles::
Send,✨
return

;1F388
:*::balloon::
Send,🎈
return

;1F389
:*::party-popper::
Send,🎉
return

;1F38A
:*::confetti-ball::
Send,🎊
return

;1F38B
:*::tanabata-tree::
Send,🎋
return

;1F38D
:*::pine-decoration::
Send,🎍
return

;1F38E
:*::Japanese-dolls::
Send,🎎
return

;1F38F
:*::carp-streamer::
Send,🎏
return

;1F390
:*::wind-chime::
Send,🎐
return

;1F391
:*::moon-viewing-ceremony::
Send,🎑
return

;1F9E7
:*::red-envelope::
Send,🧧
return

;1F380
:*::ribbon::
Send,🎀
return

;1F381
:*::wrapped-gift::
Send,🎁
return

;1F397 FE0F
:*::reminder-ribbon::
Send,🎗️
return

;1F397
:*::reminder-ribbon::
Send,🎗
return

;1F39F FE0F
:*::admission-tickets::
Send,🎟️
return

;1F39F
:*::admission-tickets::
Send,🎟
return

;1F3AB
:*::ticket::
Send,🎫
return

;1F396 FE0F
:*::military-medal::
Send,🎖️
return

;1F396
:*::military-medal::
Send,🎖
return

;1F3C6
:*::trophy::
Send,🏆
return

;1F3C5
:*::sports-medal::
Send,🏅
return

;1F947
:*::1st-place-medal::
Send,🥇
return

;1F948
:*::2nd-place-medal::
Send,🥈
return

;1F949
:*::3rd-place-medal::
Send,🥉
return

;26BD
:*::soccer-ball::
Send,⚽
return

;26BE
:*::baseball::
Send,⚾
return

;1F94E
:*::softball::
Send,🥎
return

;1F3C0
:*::basketball::
Send,🏀
return

;1F3D0
:*::volleyball::
Send,🏐
return

;1F3C8
:*::american-football::
Send,🏈
return

;1F3C9
:*::rugby-football::
Send,🏉
return

;1F3BE
:*::tennis::
Send,🎾
return

;1F94F
:*::flying-disc::
Send,🥏
return

;1F3B3
:*::bowling::
Send,🎳
return

;1F3CF
:*::cricket-game::
Send,🏏
return

;1F3D1
:*::field-hockey::
Send,🏑
return

;1F3D2
:*::ice-hockey::
Send,🏒
return

;1F94D
:*::lacrosse::
Send,🥍
return

;1F3D3
:*::ping-pong::
Send,🏓
return

;1F3F8
:*::badminton::
Send,🏸
return

;1F94A
:*::boxing-glove::
Send,🥊
return

;1F94B
:*::martial-arts-uniform::
Send,🥋
return

;1F945
:*::goal-net::
Send,🥅
return

;26F3
:*::flag-in-hole::
Send,⛳
return

;26F8 FE0F
:*::ice-skate::
Send,⛸️
return

;26F8
:*::ice-skate::
Send,⛸
return

;1F3A3
:*::fishing-pole::
Send,🎣
return

;1F93F
:*::diving-mask::
Send,🤿
return

;1F3BD
:*::running-shirt::
Send,🎽
return

;1F3BF
:*::skis::
Send,🎿
return

;1F6F7
:*::sled::
Send,🛷
return

;1F94C
:*::curling-stone::
Send,🥌
return

;1F3AF
:*::direct-hit::
Send,🎯
return

;1FA80
:*::yo-yo::
Send,🪀
return

;1FA81
:*::kite::
Send,🪁
return

;1F3B1
:*::pool-8-ball::
Send,🎱
return

;1F52E
:*::crystal-ball::
Send,🔮
return

;1F9FF
:*::nazar-amulet::
Send,🧿
return

;1F3AE
:*::video-game::
Send,🎮
return

;1F579 FE0F
:*::joystick::
Send,🕹️
return

;1F579
:*::joystick::
Send,🕹
return

;1F3B0
:*::slot-machine::
Send,🎰
return

;1F3B2
:*::game-die::
Send,🎲
return

;1F9E9
:*::puzzle-piece::
Send,🧩
return

;1F9F8
:*::teddy-bear::
Send,🧸
return

;2660 FE0F
:*::spade-suit::
Send,♠️
return

;2660
:*::spade-suit::
Send,♠
return

;2665 FE0F
:*::heart-suit::
Send,♥️
return

;2665
:*::heart-suit::
Send,♥
return

;2666 FE0F
:*::diamond-suit::
Send,♦️
return

;2666
:*::diamond-suit::
Send,♦
return

;2663 FE0F
:*::club-suit::
Send,♣️
return

;2663
:*::club-suit::
Send,♣
return

;265F FE0F
:*::chess-pawn::
Send,♟️
return

;265F
:*::chess-pawn::
Send,♟
return

;1F0CF
:*::joker::
Send,🃏
return

;1F004
:*::mahjong-red-dragon::
Send,🀄
return

;1F3B4
:*::flower-playing-cards::
Send,🎴
return

;1F3AD
:*::performing-arts::
Send,🎭
return

;1F5BC FE0F
:*::framed-picture::
Send,🖼️
return

;1F5BC
:*::framed-picture::
Send,🖼
return

;1F3A8
:*::artist-palette::
Send,🎨
return

;1F9F5
:*::thread::
Send,🧵
return

;1F9F6
:*::yarn::
Send,🧶
return

;1F453
:*::glasses::
Send,👓
return

;1F576 FE0F
:*::sunglasses::
Send,🕶️
return

;1F576
:*::sunglasses::
Send,🕶
return

;1F97D
:*::goggles::
Send,🥽
return

;1F97C
:*::lab-coat::
Send,🥼
return

;1F9BA
:*::safety-vest::
Send,🦺
return

;1F454
:*::necktie::
Send,👔
return

;1F455
:*::t-shirt::
Send,👕
return

;1F456
:*::jeans::
Send,👖
return

;1F9E3
:*::scarf::
Send,🧣
return

;1F9E4
:*::gloves::
Send,🧤
return

;1F9E5
:*::coat::
Send,🧥
return

;1F9E6
:*::socks::
Send,🧦
return

;1F457
:*::dress::
Send,👗
return

;1F458
:*::kimono::
Send,👘
return

;1F97B
:*::sari::
Send,🥻
return

;1FA71
:*::one-piece-swimsuit::
Send,🩱
return

;1FA72
:*::swim-brief::
Send,🩲
return

;1FA73
:*::shorts::
Send,🩳
return

;1F459
:*::bikini::
Send,👙
return

;1F45A
:*::woman’s-clothes::
Send,👚
return

;1F45B
:*::purse::
Send,👛
return

;1F45C
:*::handbag::
Send,👜
return

;1F45D
:*::clutch-bag::
Send,👝
return

;1F6CD FE0F
:*::shopping-bags::
Send,🛍️
return

;1F6CD
:*::shopping-bags::
Send,🛍
return

;1F392
:*::backpack::
Send,🎒
return

;1F45E
:*::man’s-shoe::
Send,👞
return

;1F45F
:*::running-shoe::
Send,👟
return

;1F97E
:*::hiking-boot::
Send,🥾
return

;1F97F
:*::flat-shoe::
Send,🥿
return

;1F460
:*::high-heeled-shoe::
Send,👠
return

;1F461
:*::woman’s-sandal::
Send,👡
return

;1FA70
:*::ballet-shoes::
Send,🩰
return

;1F462
:*::woman’s-boot::
Send,👢
return

;1F451
:*::crown::
Send,👑
return

;1F452
:*::woman’s-hat::
Send,👒
return

;1F3A9
:*::top-hat::
Send,🎩
return

;1F393
:*::graduation-cap::
Send,🎓
return

;1F9E2
:*::billed-cap::
Send,🧢
return

;26D1 FE0F
:*::rescue-worker’s-helmet::
Send,⛑️
return

;26D1
:*::rescue-worker’s-helmet::
Send,⛑
return

;1F4FF
:*::prayer-beads::
Send,📿
return

;1F484
:*::lipstick::
Send,💄
return

;1F48D
:*::ring::
Send,💍
return

;1F48E
:*::gem-stone::
Send,💎
return

;1F507
:*::muted-speaker::
Send,🔇
return

;1F508
:*::speaker-low-volume::
Send,🔈
return

;1F509
:*::speaker-medium-volume::
Send,🔉
return

;1F50A
:*::speaker-high-volume::
Send,🔊
return

;1F4E2
:*::loudspeaker::
Send,📢
return

;1F4E3
:*::megaphone::
Send,📣
return

;1F4EF
:*::postal-horn::
Send,📯
return

;1F514
:*::bell::
Send,🔔
return

;1F515
:*::bell-with-slash::
Send,🔕
return

;1F3BC
:*::musical-score::
Send,🎼
return

;1F3B5
:*::musical-note::
Send,🎵
return

;1F3B6
:*::musical-notes::
Send,🎶
return

;1F399 FE0F
:*::studio-microphone::
Send,🎙️
return

;1F399
:*::studio-microphone::
Send,🎙
return

;1F39A FE0F
:*::level-slider::
Send,🎚️
return

;1F39A
:*::level-slider::
Send,🎚
return

;1F39B FE0F
:*::control-knobs::
Send,🎛️
return

;1F39B
:*::control-knobs::
Send,🎛
return

;1F3A4
:*::microphone::
Send,🎤
return

;1F3A7
:*::headphone::
Send,🎧
return

;1F4FB
:*::radio::
Send,📻
return

;1F3B7
:*::saxophone::
Send,🎷
return

;1F3B8
:*::guitar::
Send,🎸
return

;1F3B9
:*::musical-keyboard::
Send,🎹
return

;1F3BA
:*::trumpet::
Send,🎺
return

;1F3BB
:*::violin::
Send,🎻
return

;1FA95
:*::banjo::
Send,🪕
return

;1F941
:*::drum::
Send,🥁
return

;1F4F1
:*::mobile-phone::
Send,📱
return

;1F4F2
:*::mobile-phone-with-arrow::
Send,📲
return

;260E FE0F
:*::telephone::
Send,☎️
return

;260E
:*::telephone::
Send,☎
return

;1F4DE
:*::telephone-receiver::
Send,📞
return

;1F4DF
:*::pager::
Send,📟
return

;1F4E0
:*::fax-machine::
Send,📠
return

;1F50B
:*::battery::
Send,🔋
return

;1F50C
:*::electric-plug::
Send,🔌
return

;1F4BB
:*::laptop-computer::
Send,💻
return

;1F5A5 FE0F
:*::desktop-computer::
Send,🖥️
return

;1F5A5
:*::desktop-computer::
Send,🖥
return

;1F5A8 FE0F
:*::printer::
Send,🖨️
return

;1F5A8
:*::printer::
Send,🖨
return

;2328 FE0F
:*::keyboard::
Send,⌨️
return

;2328
:*::keyboard::
Send,⌨
return

;1F5B1 FE0F
:*::computer-mouse::
Send,🖱️
return

;1F5B1
:*::computer-mouse::
Send,🖱
return

;1F5B2 FE0F
:*::trackball::
Send,🖲️
return

;1F5B2
:*::trackball::
Send,🖲
return

;1F4BD
:*::computer-disk::
Send,💽
return

;1F4BE
:*::floppy-disk::
Send,💾
return

;1F4BF
:*::optical-disk::
Send,💿
return

;1F4C0
:*::dvd::
Send,📀
return

;1F9EE
:*::abacus::
Send,🧮
return

;1F3A5
:*::movie-camera::
Send,🎥
return

;1F39E FE0F
:*::film-frames::
Send,🎞️
return

;1F39E
:*::film-frames::
Send,🎞
return

;1F4FD FE0F
:*::film-projector::
Send,📽️
return

;1F4FD
:*::film-projector::
Send,📽
return

;1F3AC
:*::clapper-board::
Send,🎬
return

;1F4FA
:*::television::
Send,📺
return

;1F4F7
:*::camera::
Send,📷
return

;1F4F8
:*::camera-with-flash::
Send,📸
return

;1F4F9
:*::video-camera::
Send,📹
return

;1F4FC
:*::videocassette::
Send,📼
return

;1F50D
:*::magnifying-glass-tilted-left::
Send,🔍
return

;1F50E
:*::magnifying-glass-tilted-right::
Send,🔎
return

;1F56F FE0F
:*::candle::
Send,🕯️
return

;1F56F
:*::candle::
Send,🕯
return

;1F4A1
:*::light-bulb::
Send,💡
return

;1F526
:*::flashlight::
Send,🔦
return

;1F3EE
:*::red-paper-lantern::
Send,🏮
return

;1FA94
:*::diya-lamp::
Send,🪔
return

;1F4D4
:*::notebook-with-decorative-cover::
Send,📔
return

;1F4D5
:*::closed-book::
Send,📕
return

;1F4D6
:*::open-book::
Send,📖
return

;1F4D7
:*::green-book::
Send,📗
return

;1F4D8
:*::blue-book::
Send,📘
return

;1F4D9
:*::orange-book::
Send,📙
return

;1F4DA
:*::books::
Send,📚
return

;1F4D3
:*::notebook::
Send,📓
return

;1F4D2
:*::ledger::
Send,📒
return

;1F4C3
:*::page-with-curl::
Send,📃
return

;1F4DC
:*::scroll::
Send,📜
return

;1F4C4
:*::page-facing-up::
Send,📄
return

;1F4F0
:*::newspaper::
Send,📰
return

;1F5DE FE0F
:*::rolled-up-newspaper::
Send,🗞️
return

;1F5DE
:*::rolled-up-newspaper::
Send,🗞
return

;1F4D1
:*::bookmark-tabs::
Send,📑
return

;1F516
:*::bookmark::
Send,🔖
return

;1F3F7 FE0F
:*::label::
Send,🏷️
return

;1F3F7
:*::label::
Send,🏷
return

;1F4B0
:*::money-bag::
Send,💰
return

;1F4B4
:*::yen-banknote::
Send,💴
return

;1F4B5
:*::dollar-banknote::
Send,💵
return

;1F4B6
:*::euro-banknote::
Send,💶
return

;1F4B7
:*::pound-banknote::
Send,💷
return

;1F4B8
:*::money-with-wings::
Send,💸
return

;1F4B3
:*::credit-card::
Send,💳
return

;1F9FE
:*::receipt::
Send,🧾
return

;1F4B9
:*::chart-increasing-with-yen::
Send,💹
return

;1F4B1
:*::currency-exchange::
Send,💱
return

;1F4B2
:*::heavy-dollar-sign::
Send,💲
return

;2709 FE0F
:*::envelope::
Send,✉️
return

;2709
:*::envelope::
Send,✉
return

;1F4E7
:*::e-mail::
Send,📧
return

;1F4E8
:*::incoming-envelope::
Send,📨
return

;1F4E9
:*::envelope-with-arrow::
Send,📩
return

;1F4E4
:*::outbox-tray::
Send,📤
return

;1F4E5
:*::inbox-tray::
Send,📥
return

;1F4E6
:*::package::
Send,📦
return

;1F4EB
:*::closed-mailbox-with-raised-flag::
Send,📫
return

;1F4EA
:*::closed-mailbox-with-lowered-flag::
Send,📪
return

;1F4EC
:*::open-mailbox-with-raised-flag::
Send,📬
return

;1F4ED
:*::open-mailbox-with-lowered-flag::
Send,📭
return

;1F4EE
:*::postbox::
Send,📮
return

;1F5F3 FE0F
:*::ballot-box-with-ballot::
Send,🗳️
return

;1F5F3
:*::ballot-box-with-ballot::
Send,🗳
return

;270F FE0F
:*::pencil::
Send,✏️
return

;270F
:*::pencil::
Send,✏
return

;2712 FE0F
:*::black-nib::
Send,✒️
return

;2712
:*::black-nib::
Send,✒
return

;1F58B FE0F
:*::fountain-pen::
Send,🖋️
return

;1F58B
:*::fountain-pen::
Send,🖋
return

;1F58A FE0F
:*::pen::
Send,🖊️
return

;1F58A
:*::pen::
Send,🖊
return

;1F58C FE0F
:*::paintbrush::
Send,🖌️
return

;1F58C
:*::paintbrush::
Send,🖌
return

;1F58D FE0F
:*::crayon::
Send,🖍️
return

;1F58D
:*::crayon::
Send,🖍
return

;1F4DD
:*::memo::
Send,📝
return

;1F4BC
:*::briefcase::
Send,💼
return

;1F4C1
:*::file-folder::
Send,📁
return

;1F4C2
:*::open-file-folder::
Send,📂
return

;1F5C2 FE0F
:*::card-index-dividers::
Send,🗂️
return

;1F5C2
:*::card-index-dividers::
Send,🗂
return

;1F4C5
:*::calendar::
Send,📅
return

;1F4C6
:*::tear-off-calendar::
Send,📆
return

;1F5D2 FE0F
:*::spiral-notepad::
Send,🗒️
return

;1F5D2
:*::spiral-notepad::
Send,🗒
return

;1F5D3 FE0F
:*::spiral-calendar::
Send,🗓️
return

;1F5D3
:*::spiral-calendar::
Send,🗓
return

;1F4C7
:*::card-index::
Send,📇
return

;1F4C8
:*::chart-increasing::
Send,📈
return

;1F4C9
:*::chart-decreasing::
Send,📉
return

;1F4CA
:*::bar-chart::
Send,📊
return

;1F4CB
:*::clipboard::
Send,📋
return

;1F4CC
:*::pushpin::
Send,📌
return

;1F4CD
:*::round-pushpin::
Send,📍
return

;1F4CE
:*::paperclip::
Send,📎
return

;1F587 FE0F
:*::linked-paperclips::
Send,🖇️
return

;1F587
:*::linked-paperclips::
Send,🖇
return

;1F4CF
:*::straight-ruler::
Send,📏
return

;1F4D0
:*::triangular-ruler::
Send,📐
return

;2702 FE0F
:*::scissors::
Send,✂️
return

;2702
:*::scissors::
Send,✂
return

;1F5C3 FE0F
:*::card-file-box::
Send,🗃️
return

;1F5C3
:*::card-file-box::
Send,🗃
return

;1F5C4 FE0F
:*::file-cabinet::
Send,🗄️
return

;1F5C4
:*::file-cabinet::
Send,🗄
return

;1F5D1 FE0F
:*::wastebasket::
Send,🗑️
return

;1F5D1
:*::wastebasket::
Send,🗑
return

;1F512
:*::locked::
Send,🔒
return

;1F513
:*::unlocked::
Send,🔓
return

;1F50F
:*::locked-with-pen::
Send,🔏
return

;1F510
:*::locked-with-key::
Send,🔐
return

;1F511
:*::key::
Send,🔑
return

;1F5DD FE0F
:*::old-key::
Send,🗝️
return

;1F5DD
:*::old-key::
Send,🗝
return

;1F528
:*::hammer::
Send,🔨
return

;1FA93
:*::axe::
Send,🪓
return

;26CF FE0F
:*::pick::
Send,⛏️
return

;26CF
:*::pick::
Send,⛏
return

;2692 FE0F
:*::hammer-and-pick::
Send,⚒️
return

;2692
:*::hammer-and-pick::
Send,⚒
return

;1F6E0 FE0F
:*::hammer-and-wrench::
Send,🛠️
return

;1F6E0
:*::hammer-and-wrench::
Send,🛠
return

;1F5E1 FE0F
:*::dagger::
Send,🗡️
return

;1F5E1
:*::dagger::
Send,🗡
return

;2694 FE0F
:*::crossed-swords::
Send,⚔️
return

;2694
:*::crossed-swords::
Send,⚔
return

;1F52B
:*::pistol::
Send,🔫
return

;1F3F9
:*::bow-and-arrow::
Send,🏹
return

;1F6E1 FE0F
:*::shield::
Send,🛡️
return

;1F6E1
:*::shield::
Send,🛡
return

;1F527
:*::wrench::
Send,🔧
return

;1F529
:*::nut-and-bolt::
Send,🔩
return

;2699 FE0F
:*::gear::
Send,⚙️
return

;2699
:*::gear::
Send,⚙
return

;1F5DC FE0F
:*::clamp::
Send,🗜️
return

;1F5DC
:*::clamp::
Send,🗜
return

;2696 FE0F
:*::balance-scale::
Send,⚖️
return

;2696
:*::balance-scale::
Send,⚖
return

;1F9AF
:*::probing-cane::
Send,🦯
return

;1F517
:*::link::
Send,🔗
return

;26D3 FE0F
:*::chains::
Send,⛓️
return

;26D3
:*::chains::
Send,⛓
return

;1F9F0
:*::toolbox::
Send,🧰
return

;1F9F2
:*::magnet::
Send,🧲
return

;2697 FE0F
:*::alembic::
Send,⚗️
return

;2697
:*::alembic::
Send,⚗
return

;1F9EA
:*::test-tube::
Send,🧪
return

;1F9EB
:*::petri-dish::
Send,🧫
return

;1F9EC
:*::dna::
Send,🧬
return

;1F52C
:*::microscope::
Send,🔬
return

;1F52D
:*::telescope::
Send,🔭
return

;1F4E1
:*::satellite-antenna::
Send,📡
return

;1F489
:*::syringe::
Send,💉
return

;1FA78
:*::drop-of-blood::
Send,🩸
return

;1F48A
:*::pill::
Send,💊
return

;1FA79
:*::adhesive-bandage::
Send,🩹
return

;1FA7A
:*::stethoscope::
Send,🩺
return

;1F6AA
:*::door::
Send,🚪
return

;1F6CF FE0F
:*::bed::
Send,🛏️
return

;1F6CF
:*::bed::
Send,🛏
return

;1F6CB FE0F
:*::couch-and-lamp::
Send,🛋️
return

;1F6CB
:*::couch-and-lamp::
Send,🛋
return

;1FA91
:*::chair::
Send,🪑
return

;1F6BD
:*::toilet::
Send,🚽
return

;1F6BF
:*::shower::
Send,🚿
return

;1F6C1
:*::bathtub::
Send,🛁
return

;1FA92
:*::razor::
Send,🪒
return

;1F9F4
:*::lotion-bottle::
Send,🧴
return

;1F9F7
:*::safety-pin::
Send,🧷
return

;1F9F9
:*::broom::
Send,🧹
return

;1F9FA
:*::basket::
Send,🧺
return

;1F9FB
:*::roll-of-paper::
Send,🧻
return

;1F9FC
:*::soap::
Send,🧼
return

;1F9FD
:*::sponge::
Send,🧽
return

;1F9EF
:*::fire-extinguisher::
Send,🧯
return

;1F6D2
:*::shopping-cart::
Send,🛒
return

;1F6AC
:*::cigarette::
Send,🚬
return

;26B0 FE0F
:*::coffin::
Send,⚰️
return

;26B0
:*::coffin::
Send,⚰
return

;26B1 FE0F
:*::funeral-urn::
Send,⚱️
return

;26B1
:*::funeral-urn::
Send,⚱
return

;1F5FF
:*::moai::
Send,🗿
return

;1F3E7
:*::ATM-sign::
Send,🏧
return

;1F6AE
:*::litter-in-bin-sign::
Send,🚮
return

;1F6B0
:*::potable-water::
Send,🚰
return

;267F
:*::wheelchair-symbol::
Send,♿
return

;1F6B9
:*::men’s-room::
Send,🚹
return

;1F6BA
:*::women’s-room::
Send,🚺
return

;1F6BB
:*::restroom::
Send,🚻
return

;1F6BC
:*::baby-symbol::
Send,🚼
return

;1F6BE
:*::water-closet::
Send,🚾
return

;1F6C2
:*::passport-control::
Send,🛂
return

;1F6C3
:*::customs::
Send,🛃
return

;1F6C4
:*::baggage-claim::
Send,🛄
return

;1F6C5
:*::left-luggage::
Send,🛅
return

;26A0 FE0F
:*::warning::
Send,⚠️
return

;26A0
:*::warning::
Send,⚠
return

;1F6B8
:*::children-crossing::
Send,🚸
return

;26D4
:*::no-entry::
Send,⛔
return

;1F6AB
:*::prohibited::
Send,🚫
return

;1F6B3
:*::no-bicycles::
Send,🚳
return

;1F6AD
:*::no-smoking::
Send,🚭
return

;1F6AF
:*::no-littering::
Send,🚯
return

;1F6B1
:*::non-potable-water::
Send,🚱
return

;1F6B7
:*::no-pedestrians::
Send,🚷
return

;1F4F5
:*::no-mobile-phones::
Send,📵
return

;1F51E
:*::no-one-under-eighteen::
Send,🔞
return

;2622 FE0F
:*::radioactive::
Send,☢️
return

;2622
:*::radioactive::
Send,☢
return

;2623 FE0F
:*::biohazard::
Send,☣️
return

;2623
:*::biohazard::
Send,☣
return

;2B06 FE0F
:*::up-arrow::
Send,⬆️
return

;2B06
:*::up-arrow::
Send,⬆
return

;2197 FE0F
:*::up-right-arrow::
Send,↗️
return

;2197
:*::up-right-arrow::
Send,↗
return

;27A1 FE0F
:*::right-arrow::
Send,➡️
return

;27A1
:*::right-arrow::
Send,➡
return

;2198 FE0F
:*::down-right-arrow::
Send,↘️
return

;2198
:*::down-right-arrow::
Send,↘
return

;2B07 FE0F
:*::down-arrow::
Send,⬇️
return

;2B07
:*::down-arrow::
Send,⬇
return

;2199 FE0F
:*::down-left-arrow::
Send,↙️
return

;2199
:*::down-left-arrow::
Send,↙
return

;2B05 FE0F
:*::left-arrow::
Send,⬅️
return

;2B05
:*::left-arrow::
Send,⬅
return

;2196 FE0F
:*::up-left-arrow::
Send,↖️
return

;2196
:*::up-left-arrow::
Send,↖
return

;2195 FE0F
:*::up-down-arrow::
Send,↕️
return

;2195
:*::up-down-arrow::
Send,↕
return

;2194 FE0F
:*::left-right-arrow::
Send,↔️
return

;2194
:*::left-right-arrow::
Send,↔
return

;21A9 FE0F
:*::right-arrow-curving-left::
Send,↩️
return

;21A9
:*::right-arrow-curving-left::
Send,↩
return

;21AA FE0F
:*::left-arrow-curving-right::
Send,↪️
return

;21AA
:*::left-arrow-curving-right::
Send,↪
return

;2934 FE0F
:*::right-arrow-curving-up::
Send,⤴️
return

;2934
:*::right-arrow-curving-up::
Send,⤴
return

;2935 FE0F
:*::right-arrow-curving-down::
Send,⤵️
return

;2935
:*::right-arrow-curving-down::
Send,⤵
return

;1F503
:*::clockwise-vertical-arrows::
Send,🔃
return

;1F504
:*::counterclockwise-arrows-button::
Send,🔄
return

;1F519
:*::BACK-arrow::
Send,🔙
return

;1F51A
:*::END-arrow::
Send,🔚
return

;1F51B
:*::ON!-arrow::
Send,🔛
return

;1F51C
:*::SOON-arrow::
Send,🔜
return

;1F51D
:*::TOP-arrow::
Send,🔝
return

;1F6D0
:*::place-of-worship::
Send,🛐
return

;269B FE0F
:*::atom-symbol::
Send,⚛️
return

;269B
:*::atom-symbol::
Send,⚛
return

;1F549 FE0F
:*::om::
Send,🕉️
return

;1F549
:*::om::
Send,🕉
return

;2721 FE0F
:*::star-of-David::
Send,✡️
return

;2721
:*::star-of-David::
Send,✡
return

;2638 FE0F
:*::wheel-of-dharma::
Send,☸️
return

;2638
:*::wheel-of-dharma::
Send,☸
return

;262F FE0F
:*::yin-yang::
Send,☯️
return

;262F
:*::yin-yang::
Send,☯
return

;271D FE0F
:*::latin-cross::
Send,✝️
return

;271D
:*::latin-cross::
Send,✝
return

;2626 FE0F
:*::orthodox-cross::
Send,☦️
return

;2626
:*::orthodox-cross::
Send,☦
return

;262A FE0F
:*::star-and-crescent::
Send,☪️
return

;262A
:*::star-and-crescent::
Send,☪
return

;262E FE0F
:*::peace-symbol::
Send,☮️
return

;262E
:*::peace-symbol::
Send,☮
return

;1F54E
:*::menorah::
Send,🕎
return

;1F52F
:*::dotted-six-pointed-star::
Send,🔯
return

;2648
:*::Aries::
Send,♈
return

;2649
:*::Taurus::
Send,♉
return

;264A
:*::Gemini::
Send,♊
return

;264B
:*::Cancer::
Send,♋
return

;264C
:*::Leo::
Send,♌
return

;264D
:*::Virgo::
Send,♍
return

;264E
:*::Libra::
Send,♎
return

;264F
:*::Scorpio::
Send,♏
return

;2650
:*::Sagittarius::
Send,♐
return

;2651
:*::Capricorn::
Send,♑
return

;2652
:*::Aquarius::
Send,♒
return

;2653
:*::Pisces::
Send,♓
return

;26CE
:*::Ophiuchus::
Send,⛎
return

;1F500
:*::shuffle-tracks-button::
Send,🔀
return

;1F501
:*::repeat-button::
Send,🔁
return

;1F502
:*::repeat-single-button::
Send,🔂
return

;25B6 FE0F
:*::play-button::
Send,▶️
return

;25B6
:*::play-button::
Send,▶
return

;23E9
:*::fast-forward-button::
Send,⏩
return

;23ED FE0F
:*::next-track-button::
Send,⏭️
return

;23ED
:*::next-track-button::
Send,⏭
return

;23EF FE0F
:*::play-or-pause-button::
Send,⏯️
return

;23EF
:*::play-or-pause-button::
Send,⏯
return

;25C0 FE0F
:*::reverse-button::
Send,◀️
return

;25C0
:*::reverse-button::
Send,◀
return

;23EA
:*::fast-reverse-button::
Send,⏪
return

;23EE FE0F
:*::last-track-button::
Send,⏮️
return

;23EE
:*::last-track-button::
Send,⏮
return

;1F53C
:*::upwards-button::
Send,🔼
return

;23EB
:*::fast-up-button::
Send,⏫
return

;1F53D
:*::downwards-button::
Send,🔽
return

;23EC
:*::fast-down-button::
Send,⏬
return

;23F8 FE0F
:*::pause-button::
Send,⏸️
return

;23F8
:*::pause-button::
Send,⏸
return

;23F9 FE0F
:*::stop-button::
Send,⏹️
return

;23F9
:*::stop-button::
Send,⏹
return

;23FA FE0F
:*::record-button::
Send,⏺️
return

;23FA
:*::record-button::
Send,⏺
return

;23CF FE0F
:*::eject-button::
Send,⏏️
return

;23CF
:*::eject-button::
Send,⏏
return

;1F3A6
:*::cinema::
Send,🎦
return

;1F505
:*::dim-button::
Send,🔅
return

;1F506
:*::bright-button::
Send,🔆
return

;1F4F6
:*::antenna-bars::
Send,📶
return

;1F4F3
:*::vibration-mode::
Send,📳
return

;1F4F4
:*::mobile-phone-off::
Send,📴
return

;2640 FE0F
:*::female-sign::
Send,♀️
return

;2640
:*::female-sign::
Send,♀
return

;2642 FE0F
:*::male-sign::
Send,♂️
return

;2642
:*::male-sign::
Send,♂
return

;2695 FE0F
:*::medical-symbol::
Send,⚕️
return

;2695
:*::medical-symbol::
Send,⚕
return

;267E FE0F
:*::infinity::
Send,♾️
return

;267E
:*::infinity::
Send,♾
return

;267B FE0F
:*::recycling-symbol::
Send,♻️
return

;267B
:*::recycling-symbol::
Send,♻
return

;269C FE0F
:*::fleur-de-lis::
Send,⚜️
return

;269C
:*::fleur-de-lis::
Send,⚜
return

;1F531
:*::trident-emblem::
Send,🔱
return

;1F4DB
:*::name-badge::
Send,📛
return

;1F530
:*::Japanese-symbol-for-beginner::
Send,🔰
return

;2B55
:*::hollow-red-circle::
Send,⭕
return

;2705
:*::check-mark-button::
Send,✅
return

;2611 FE0F
:*::check-box-with-check::
Send,☑️
return

;2611
:*::check-box-with-check::
Send,☑
return

;2714 FE0F
:*::check-mark::
Send,✔️
return

;2714
:*::check-mark::
Send,✔
return

;2716 FE0F
:*::multiplication-sign::
Send,✖️
return

;2716
:*::multiplication-sign::
Send,✖
return

;274C
:*::cross-mark::
Send,❌
return

;274E
:*::cross-mark-button::
Send,❎
return

;2795
:*::plus-sign::
Send,➕
return

;2796
:*::minus-sign::
Send,➖
return

;2797
:*::division-sign::
Send,➗
return

;27B0
:*::curly-loop::
Send,➰
return

;27BF
:*::double-curly-loop::
Send,➿
return

;303D FE0F
:*::part-alternation-mark::
Send,〽️
return

;303D
:*::part-alternation-mark::
Send,〽
return

;2733 FE0F
:*::eight-spoked-asterisk::
Send,✳️
return

;2733
:*::eight-spoked-asterisk::
Send,✳
return

;2734 FE0F
:*::eight-pointed-star::
Send,✴️
return

;2734
:*::eight-pointed-star::
Send,✴
return

;2747 FE0F
:*::sparkle::
Send,❇️
return

;2747
:*::sparkle::
Send,❇
return

;203C FE0F
:*::double-exclamation-mark::
Send,‼️
return

;203C
:*::double-exclamation-mark::
Send,‼
return

;2049 FE0F
:*::exclamation-question-mark::
Send,⁉️
return

;2049
:*::exclamation-question-mark::
Send,⁉
return

;2753
:*::question-mark::
Send,❓
return

;2754
:*::white-question-mark::
Send,❔
return

;2755
:*::white-exclamation-mark::
Send,❕
return

;2757
:*::exclamation-mark::
Send,❗
return

;3030 FE0F
:*::wavy-dash::
Send,〰️
return

;3030
:*::wavy-dash::
Send,〰
return

;00A9 FE0F
:*::copyright::
Send,©️
return

;00A9
:*::copyright::
Send,©
return

;00AE FE0F
:*::registered::
Send,®️
return

;00AE
:*::registered::
Send,®
return

;2122 FE0F
:*::trade-mark::
Send,™️
return

;2122
:*::trade-mark::
Send,™
return

;0023 FE0F 20E3
:*::keycap:-#::
Send,#️⃣
return

;0023 20E3
:*::keycap:-#::
Send,#⃣
return

;002A FE0F 20E3
:*::keycap:-*::
Send,*️⃣
return

;002A 20E3
:*::keycap:-*::
Send,*⃣
return

;0030 FE0F 20E3
:*::keycap:-0::
Send,0️⃣
return

;0030 20E3
:*::keycap:-0::
Send,0⃣
return

;0031 FE0F 20E3
:*::keycap:-1::
Send,1️⃣
return

;0031 20E3
:*::keycap:-1::
Send,1⃣
return

;0032 FE0F 20E3
:*::keycap:-2::
Send,2️⃣
return

;0032 20E3
:*::keycap:-2::
Send,2⃣
return

;0033 FE0F 20E3
:*::keycap:-3::
Send,3️⃣
return

;0033 20E3
:*::keycap:-3::
Send,3⃣
return

;0034 FE0F 20E3
:*::keycap:-4::
Send,4️⃣
return

;0034 20E3
:*::keycap:-4::
Send,4⃣
return

;0035 FE0F 20E3
:*::keycap:-5::
Send,5️⃣
return

;0035 20E3
:*::keycap:-5::
Send,5⃣
return

;0036 FE0F 20E3
:*::keycap:-6::
Send,6️⃣
return

;0036 20E3
:*::keycap:-6::
Send,6⃣
return

;0037 FE0F 20E3
:*::keycap:-7::
Send,7️⃣
return

;0037 20E3
:*::keycap:-7::
Send,7⃣
return

;0038 FE0F 20E3
:*::keycap:-8::
Send,8️⃣
return

;0038 20E3
:*::keycap:-8::
Send,8⃣
return

;0039 FE0F 20E3
:*::keycap:-9::
Send,9️⃣
return

;0039 20E3
:*::keycap:-9::
Send,9⃣
return

;1F51F
:*::keycap:-10::
Send,🔟
return

;1F520
:*::input-latin-uppercase::
Send,🔠
return

;1F521
:*::input-latin-lowercase::
Send,🔡
return

;1F522
:*::input-numbers::
Send,🔢
return

;1F523
:*::input-symbols::
Send,🔣
return

;1F524
:*::input-latin-letters::
Send,🔤
return

;1F170 FE0F
:*::A-button-(blood-type)::
Send,🅰️
return

;1F170
:*::A-button-(blood-type)::
Send,🅰
return

;1F18E
:*::AB-button-(blood-type)::
Send,🆎
return

;1F171 FE0F
:*::B-button-(blood-type)::
Send,🅱️
return

;1F171
:*::B-button-(blood-type)::
Send,🅱
return

;1F191
:*::CL-button::
Send,🆑
return

;1F192
:*::COOL-button::
Send,🆒
return

;1F193
:*::FREE-button::
Send,🆓
return

;2139 FE0F
:*::information::
Send,ℹ️
return

;2139
:*::information::
Send,ℹ
return

;1F194
:*::ID-button::
Send,🆔
return

;24C2 FE0F
:*::circled-M::
Send,Ⓜ️
return

;24C2
:*::circled-M::
Send,Ⓜ
return

;1F195
:*::NEW-button::
Send,🆕
return

;1F196
:*::NG-button::
Send,🆖
return

;1F17E FE0F
:*::O-button-(blood-type)::
Send,🅾️
return

;1F17E
:*::O-button-(blood-type)::
Send,🅾
return

;1F197
:*::OK-button::
Send,🆗
return

;1F17F FE0F
:*::P-button::
Send,🅿️
return

;1F17F
:*::P-button::
Send,🅿
return

;1F198
:*::SOS-button::
Send,🆘
return

;1F199
:*::UP!-button::
Send,🆙
return

;1F19A
:*::VS-button::
Send,🆚
return

;1F201
:*::Japanese-“here”-button::
Send,🈁
return

;1F202 FE0F
:*::Japanese-“service-charge”-button::
Send,🈂️
return

;1F202
:*::Japanese-“service-charge”-button::
Send,🈂
return

;1F237 FE0F
:*::Japanese-“monthly-amount”-button::
Send,🈷️
return

;1F237
:*::Japanese-“monthly-amount”-button::
Send,🈷
return

;1F236
:*::Japanese-“not-free-of-charge”-button::
Send,🈶
return

;1F22F
:*::Japanese-“reserved”-button::
Send,🈯
return

;1F250
:*::Japanese-“bargain”-button::
Send,🉐
return

;1F239
:*::Japanese-“discount”-button::
Send,🈹
return

;1F21A
:*::Japanese-“free-of-charge”-button::
Send,🈚
return

;1F232
:*::Japanese-“prohibited”-button::
Send,🈲
return

;1F251
:*::Japanese-“acceptable”-button::
Send,🉑
return

;1F238
:*::Japanese-“application”-button::
Send,🈸
return

;1F234
:*::Japanese-“passing-grade”-button::
Send,🈴
return

;1F233
:*::Japanese-“vacancy”-button::
Send,🈳
return

;3297 FE0F
:*::Japanese-“congratulations”-button::
Send,㊗️
return

;3297
:*::Japanese-“congratulations”-button::
Send,㊗
return

;3299 FE0F
:*::Japanese-“secret”-button::
Send,㊙️
return

;3299
:*::Japanese-“secret”-button::
Send,㊙
return

;1F23A
:*::Japanese-“open-for-business”-button::
Send,🈺
return

;1F235
:*::Japanese-“no-vacancy”-button::
Send,🈵
return

;1F534
:*::red-circle::
Send,🔴
return

;1F7E0
:*::orange-circle::
Send,🟠
return

;1F7E1
:*::yellow-circle::
Send,🟡
return

;1F7E2
:*::green-circle::
Send,🟢
return

;1F535
:*::blue-circle::
Send,🔵
return

;1F7E3
:*::purple-circle::
Send,🟣
return

;1F7E4
:*::brown-circle::
Send,🟤
return

;26AB
:*::black-circle::
Send,⚫
return

;26AA
:*::white-circle::
Send,⚪
return

;1F7E5
:*::red-square::
Send,🟥
return

;1F7E7
:*::orange-square::
Send,🟧
return

;1F7E8
:*::yellow-square::
Send,🟨
return

;1F7E9
:*::green-square::
Send,🟩
return

;1F7E6
:*::blue-square::
Send,🟦
return

;1F7EA
:*::purple-square::
Send,🟪
return

;1F7EB
:*::brown-square::
Send,🟫
return

;2B1B
:*::black-large-square::
Send,⬛
return

;2B1C
:*::white-large-square::
Send,⬜
return

;25FC FE0F
:*::black-medium-square::
Send,◼️
return

;25FC
:*::black-medium-square::
Send,◼
return

;25FB FE0F
:*::white-medium-square::
Send,◻️
return

;25FB
:*::white-medium-square::
Send,◻
return

;25FE
:*::black-medium-small-square::
Send,◾
return

;25FD
:*::white-medium-small-square::
Send,◽
return

;25AA FE0F
:*::black-small-square::
Send,▪️
return

;25AA
:*::black-small-square::
Send,▪
return

;25AB FE0F
:*::white-small-square::
Send,▫️
return

;25AB
:*::white-small-square::
Send,▫
return

;1F536
:*::large-orange-diamond::
Send,🔶
return

;1F537
:*::large-blue-diamond::
Send,🔷
return

;1F538
:*::small-orange-diamond::
Send,🔸
return

;1F539
:*::small-blue-diamond::
Send,🔹
return

;1F53A
:*::red-triangle-pointed-up::
Send,🔺
return

;1F53B
:*::red-triangle-pointed-down::
Send,🔻
return

;1F4A0
:*::diamond-with-a-dot::
Send,💠
return

;1F518
:*::radio-button::
Send,🔘
return

;1F533
:*::white-square-button::
Send,🔳
return

;1F532
:*::black-square-button::
Send,🔲
return

;1F3C1
:*::chequered-flag::
Send,🏁
return

;1F6A9
:*::triangular-flag::
Send,🚩
return

;1F38C
:*::crossed-flags::
Send,🎌
return

;1F3F4
:*::black-flag::
Send,🏴
return

;1F3F3 FE0F
:*::white-flag::
Send,🏳️
return

;1F3F3
:*::white-flag::
Send,🏳
return

;1F3F3 FE0F 200D 1F308
:*::rainbow-flag::
Send,🏳️‍🌈
return

;1F3F3 200D 1F308
:*::rainbow-flag::
Send,🏳‍🌈
return

;1F3F4 200D 2620 FE0F
:*::pirate-flag::
Send,🏴‍☠️
return

;1F3F4 200D 2620
:*::pirate-flag::
Send,🏴‍☠
return

;1F1E6 1F1E8
:*::flag:-Ascension-Island::
Send,🇦🇨
return

;1F1E6 1F1E9
:*::flag:-Andorra::
Send,🇦🇩
return

;1F1E6 1F1EA
:*::flag:-United-Arab-Emirates::
Send,🇦🇪
return

;1F1E6 1F1EB
:*::flag:-Afghanistan::
Send,🇦🇫
return

;1F1E6 1F1EC
:*::flag:-Antigua-&-Barbuda::
Send,🇦🇬
return

;1F1E6 1F1EE
:*::flag:-Anguilla::
Send,🇦🇮
return

;1F1E6 1F1F1
:*::flag:-Albania::
Send,🇦🇱
return

;1F1E6 1F1F2
:*::flag:-Armenia::
Send,🇦🇲
return

;1F1E6 1F1F4
:*::flag:-Angola::
Send,🇦🇴
return

;1F1E6 1F1F6
:*::flag:-Antarctica::
Send,🇦🇶
return

;1F1E6 1F1F7
:*::flag:-Argentina::
Send,🇦🇷
return

;1F1E6 1F1F8
:*::flag:-American-Samoa::
Send,🇦🇸
return

;1F1E6 1F1F9
:*::flag:-Austria::
Send,🇦🇹
return

;1F1E6 1F1FA
:*::flag:-Australia::
Send,🇦🇺
return

;1F1E6 1F1FC
:*::flag:-Aruba::
Send,🇦🇼
return

;1F1E6 1F1FD
:*::flag:-Åland-Islands::
Send,🇦🇽
return

;1F1E6 1F1FF
:*::flag:-Azerbaijan::
Send,🇦🇿
return

;1F1E7 1F1E6
:*::flag:-Bosnia-&-Herzegovina::
Send,🇧🇦
return

;1F1E7 1F1E7
:*::flag:-Barbados::
Send,🇧🇧
return

;1F1E7 1F1E9
:*::flag:-Bangladesh::
Send,🇧🇩
return

;1F1E7 1F1EA
:*::flag:-Belgium::
Send,🇧🇪
return

;1F1E7 1F1EB
:*::flag:-Burkina-Faso::
Send,🇧🇫
return

;1F1E7 1F1EC
:*::flag:-Bulgaria::
Send,🇧🇬
return

;1F1E7 1F1ED
:*::flag:-Bahrain::
Send,🇧🇭
return

;1F1E7 1F1EE
:*::flag:-Burundi::
Send,🇧🇮
return

;1F1E7 1F1EF
:*::flag:-Benin::
Send,🇧🇯
return

;1F1E7 1F1F1
:*::flag:-St.-Barthélemy::
Send,🇧🇱
return

;1F1E7 1F1F2
:*::flag:-Bermuda::
Send,🇧🇲
return

;1F1E7 1F1F3
:*::flag:-Brunei::
Send,🇧🇳
return

;1F1E7 1F1F4
:*::flag:-Bolivia::
Send,🇧🇴
return

;1F1E7 1F1F6
:*::flag:-Caribbean-Netherlands::
Send,🇧🇶
return

;1F1E7 1F1F7
:*::flag:-Brazil::
Send,🇧🇷
return

;1F1E7 1F1F8
:*::flag:-Bahamas::
Send,🇧🇸
return

;1F1E7 1F1F9
:*::flag:-Bhutan::
Send,🇧🇹
return

;1F1E7 1F1FB
:*::flag:-Bouvet-Island::
Send,🇧🇻
return

;1F1E7 1F1FC
:*::flag:-Botswana::
Send,🇧🇼
return

;1F1E7 1F1FE
:*::flag:-Belarus::
Send,🇧🇾
return

;1F1E7 1F1FF
:*::flag:-Belize::
Send,🇧🇿
return

;1F1E8 1F1E6
:*::flag:-Canada::
Send,🇨🇦
return

;1F1E8 1F1E8
:*::flag:-Cocos-(Keeling)-Islands::
Send,🇨🇨
return

;1F1E8 1F1E9
:*::flag:-Congo---Kinshasa::
Send,🇨🇩
return

;1F1E8 1F1EB
:*::flag:-Central-African-Republic::
Send,🇨🇫
return

;1F1E8 1F1EC
:*::flag:-Congo---Brazzaville::
Send,🇨🇬
return

;1F1E8 1F1ED
:*::flag:-Switzerland::
Send,🇨🇭
return

;1F1E8 1F1EE
:*::flag:-Côte-d’Ivoire::
Send,🇨🇮
return

;1F1E8 1F1F0
:*::flag:-Cook-Islands::
Send,🇨🇰
return

;1F1E8 1F1F1
:*::flag:-Chile::
Send,🇨🇱
return

;1F1E8 1F1F2
:*::flag:-Cameroon::
Send,🇨🇲
return

;1F1E8 1F1F3
:*::flag:-China::
Send,🇨🇳
return

;1F1E8 1F1F4
:*::flag:-Colombia::
Send,🇨🇴
return

;1F1E8 1F1F5
:*::flag:-Clipperton-Island::
Send,🇨🇵
return

;1F1E8 1F1F7
:*::flag:-Costa-Rica::
Send,🇨🇷
return

;1F1E8 1F1FA
:*::flag:-Cuba::
Send,🇨🇺
return

;1F1E8 1F1FB
:*::flag:-Cape-Verde::
Send,🇨🇻
return

;1F1E8 1F1FC
:*::flag:-Curaçao::
Send,🇨🇼
return

;1F1E8 1F1FD
:*::flag:-Christmas-Island::
Send,🇨🇽
return

;1F1E8 1F1FE
:*::flag:-Cyprus::
Send,🇨🇾
return

;1F1E8 1F1FF
:*::flag:-Czechia::
Send,🇨🇿
return

;1F1E9 1F1EA
:*::flag:-Germany::
Send,🇩🇪
return

;1F1E9 1F1EC
:*::flag:-Diego-Garcia::
Send,🇩🇬
return

;1F1E9 1F1EF
:*::flag:-Djibouti::
Send,🇩🇯
return

;1F1E9 1F1F0
:*::flag:-Denmark::
Send,🇩🇰
return

;1F1E9 1F1F2
:*::flag:-Dominica::
Send,🇩🇲
return

;1F1E9 1F1F4
:*::flag:-Dominican-Republic::
Send,🇩🇴
return

;1F1E9 1F1FF
:*::flag:-Algeria::
Send,🇩🇿
return

;1F1EA 1F1E6
:*::flag:-Ceuta-&-Melilla::
Send,🇪🇦
return

;1F1EA 1F1E8
:*::flag:-Ecuador::
Send,🇪🇨
return

;1F1EA 1F1EA
:*::flag:-Estonia::
Send,🇪🇪
return

;1F1EA 1F1EC
:*::flag:-Egypt::
Send,🇪🇬
return

;1F1EA 1F1ED
:*::flag:-Western-Sahara::
Send,🇪🇭
return

;1F1EA 1F1F7
:*::flag:-Eritrea::
Send,🇪🇷
return

;1F1EA 1F1F8
:*::flag:-Spain::
Send,🇪🇸
return

;1F1EA 1F1F9
:*::flag:-Ethiopia::
Send,🇪🇹
return

;1F1EA 1F1FA
:*::flag:-European-Union::
Send,🇪🇺
return

;1F1EB 1F1EE
:*::flag:-Finland::
Send,🇫🇮
return

;1F1EB 1F1EF
:*::flag:-Fiji::
Send,🇫🇯
return

;1F1EB 1F1F0
:*::flag:-Falkland-Islands::
Send,🇫🇰
return

;1F1EB 1F1F2
:*::flag:-Micronesia::
Send,🇫🇲
return

;1F1EB 1F1F4
:*::flag:-Faroe-Islands::
Send,🇫🇴
return

;1F1EB 1F1F7
:*::flag:-France::
Send,🇫🇷
return

;1F1EC 1F1E6
:*::flag:-Gabon::
Send,🇬🇦
return

;1F1EC 1F1E7
:*::flag:-United-Kingdom::
Send,🇬🇧
return

;1F1EC 1F1E9
:*::flag:-Grenada::
Send,🇬🇩
return

;1F1EC 1F1EA
:*::flag:-Georgia::
Send,🇬🇪
return

;1F1EC 1F1EB
:*::flag:-French-Guiana::
Send,🇬🇫
return

;1F1EC 1F1EC
:*::flag:-Guernsey::
Send,🇬🇬
return

;1F1EC 1F1ED
:*::flag:-Ghana::
Send,🇬🇭
return

;1F1EC 1F1EE
:*::flag:-Gibraltar::
Send,🇬🇮
return

;1F1EC 1F1F1
:*::flag:-Greenland::
Send,🇬🇱
return

;1F1EC 1F1F2
:*::flag:-Gambia::
Send,🇬🇲
return

;1F1EC 1F1F3
:*::flag:-Guinea::
Send,🇬🇳
return

;1F1EC 1F1F5
:*::flag:-Guadeloupe::
Send,🇬🇵
return

;1F1EC 1F1F6
:*::flag:-Equatorial-Guinea::
Send,🇬🇶
return

;1F1EC 1F1F7
:*::flag:-Greece::
Send,🇬🇷
return

;1F1EC 1F1F8
:*::flag:-South-Georgia-&-South-Sandwich-Is::
Send,🇬🇸
return

;1F1EC 1F1F9
:*::flag:-Guatemala::
Send,🇬🇹
return

;1F1EC 1F1FA
:*::flag:-Guam::
Send,🇬🇺
return

;1F1EC 1F1FC
:*::flag:-Guinea-Bissau::
Send,🇬🇼
return

;1F1EC 1F1FE
:*::flag:-Guyana::
Send,🇬🇾
return

;1F1ED 1F1F0
:*::flag:-Hong-Kong-SAR-China::
Send,🇭🇰
return

;1F1ED 1F1F2
:*::flag:-Heard-&-McDonald-Islands::
Send,🇭🇲
return

;1F1ED 1F1F3
:*::flag:-Honduras::
Send,🇭🇳
return

;1F1ED 1F1F7
:*::flag:-Croatia::
Send,🇭🇷
return

;1F1ED 1F1F9
:*::flag:-Haiti::
Send,🇭🇹
return

;1F1ED 1F1FA
:*::flag:-Hungary::
Send,🇭🇺
return

;1F1EE 1F1E8
:*::flag:-Canary-Islands::
Send,🇮🇨
return

;1F1EE 1F1E9
:*::flag:-Indonesia::
Send,🇮🇩
return

;1F1EE 1F1EA
:*::flag:-Ireland::
Send,🇮🇪
return

;1F1EE 1F1F1
:*::flag:-Israel::
Send,🇮🇱
return

;1F1EE 1F1F2
:*::flag:-Isle-of-Man::
Send,🇮🇲
return

;1F1EE 1F1F3
:*::flag:-India::
Send,🇮🇳
return

;1F1EE 1F1F4
:*::flag:-British-Indian-Ocean-Territory::
Send,🇮🇴
return

;1F1EE 1F1F6
:*::flag:-Iraq::
Send,🇮🇶
return

;1F1EE 1F1F7
:*::flag:-Iran::
Send,🇮🇷
return

;1F1EE 1F1F8
:*::flag:-Iceland::
Send,🇮🇸
return

;1F1EE 1F1F9
:*::flag:-Italy::
Send,🇮🇹
return

;1F1EF 1F1EA
:*::flag:-Jersey::
Send,🇯🇪
return

;1F1EF 1F1F2
:*::flag:-Jamaica::
Send,🇯🇲
return

;1F1EF 1F1F4
:*::flag:-Jordan::
Send,🇯🇴
return

;1F1EF 1F1F5
:*::flag:-Japan::
Send,🇯🇵
return

;1F1F0 1F1EA
:*::flag:-Kenya::
Send,🇰🇪
return

;1F1F0 1F1EC
:*::flag:-Kyrgyzstan::
Send,🇰🇬
return

;1F1F0 1F1ED
:*::flag:-Cambodia::
Send,🇰🇭
return

;1F1F0 1F1EE
:*::flag:-Kiribati::
Send,🇰🇮
return

;1F1F0 1F1F2
:*::flag:-Comoros::
Send,🇰🇲
return

;1F1F0 1F1F3
:*::flag:-St.-Kitts-&-Nevis::
Send,🇰🇳
return

;1F1F0 1F1F5
:*::flag:-North-Korea::
Send,🇰🇵
return

;1F1F0 1F1F7
:*::flag:-South-Korea::
Send,🇰🇷
return

;1F1F0 1F1FC
:*::flag:-Kuwait::
Send,🇰🇼
return

;1F1F0 1F1FE
:*::flag:-Cayman-Islands::
Send,🇰🇾
return

;1F1F0 1F1FF
:*::flag:-Kazakhstan::
Send,🇰🇿
return

;1F1F1 1F1E6
:*::flag:-Laos::
Send,🇱🇦
return

;1F1F1 1F1E7
:*::flag:-Lebanon::
Send,🇱🇧
return

;1F1F1 1F1E8
:*::flag:-St.-Lucia::
Send,🇱🇨
return

;1F1F1 1F1EE
:*::flag:-Liechtenstein::
Send,🇱🇮
return

;1F1F1 1F1F0
:*::flag:-Sri-Lanka::
Send,🇱🇰
return

;1F1F1 1F1F7
:*::flag:-Liberia::
Send,🇱🇷
return

;1F1F1 1F1F8
:*::flag:-Lesotho::
Send,🇱🇸
return

;1F1F1 1F1F9
:*::flag:-Lithuania::
Send,🇱🇹
return

;1F1F1 1F1FA
:*::flag:-Luxembourg::
Send,🇱🇺
return

;1F1F1 1F1FB
:*::flag:-Latvia::
Send,🇱🇻
return

;1F1F1 1F1FE
:*::flag:-Libya::
Send,🇱🇾
return

;1F1F2 1F1E6
:*::flag:-Morocco::
Send,🇲🇦
return

;1F1F2 1F1E8
:*::flag:-Monaco::
Send,🇲🇨
return

;1F1F2 1F1E9
:*::flag:-Moldova::
Send,🇲🇩
return

;1F1F2 1F1EA
:*::flag:-Montenegro::
Send,🇲🇪
return

;1F1F2 1F1EB
:*::flag:-St.-Martin::
Send,🇲🇫
return

;1F1F2 1F1EC
:*::flag:-Madagascar::
Send,🇲🇬
return

;1F1F2 1F1ED
:*::flag:-Marshall-Islands::
Send,🇲🇭
return

;1F1F2 1F1F0
:*::flag:-Macedonia::
Send,🇲🇰
return

;1F1F2 1F1F1
:*::flag:-Mali::
Send,🇲🇱
return

;1F1F2 1F1F2
:*::flag:-Myanmar-(Burma)::
Send,🇲🇲
return

;1F1F2 1F1F3
:*::flag:-Mongolia::
Send,🇲🇳
return

;1F1F2 1F1F4
:*::flag:-Macao-SAR-China::
Send,🇲🇴
return

;1F1F2 1F1F5
:*::flag:-Northern-Mariana-Islands::
Send,🇲🇵
return

;1F1F2 1F1F6
:*::flag:-Martinique::
Send,🇲🇶
return

;1F1F2 1F1F7
:*::flag:-Mauritania::
Send,🇲🇷
return

;1F1F2 1F1F8
:*::flag:-Montserrat::
Send,🇲🇸
return

;1F1F2 1F1F9
:*::flag:-Malta::
Send,🇲🇹
return

;1F1F2 1F1FA
:*::flag:-Mauritius::
Send,🇲🇺
return

;1F1F2 1F1FB
:*::flag:-Maldives::
Send,🇲🇻
return

;1F1F2 1F1FC
:*::flag:-Malawi::
Send,🇲🇼
return

;1F1F2 1F1FD
:*::flag:-Mexico::
Send,🇲🇽
return

;1F1F2 1F1FE
:*::flag:-Malaysia::
Send,🇲🇾
return

;1F1F2 1F1FF
:*::flag:-Mozambique::
Send,🇲🇿
return

;1F1F3 1F1E6
:*::flag:-Namibia::
Send,🇳🇦
return

;1F1F3 1F1E8
:*::flag:-New-Caledonia::
Send,🇳🇨
return

;1F1F3 1F1EA
:*::flag:-Niger::
Send,🇳🇪
return

;1F1F3 1F1EB
:*::flag:-Norfolk-Island::
Send,🇳🇫
return

;1F1F3 1F1EC
:*::flag:-Nigeria::
Send,🇳🇬
return

;1F1F3 1F1EE
:*::flag:-Nicaragua::
Send,🇳🇮
return

;1F1F3 1F1F1
:*::flag:-Netherlands::
Send,🇳🇱
return

;1F1F3 1F1F4
:*::flag:-Norway::
Send,🇳🇴
return

;1F1F3 1F1F5
:*::flag:-Nepal::
Send,🇳🇵
return

;1F1F3 1F1F7
:*::flag:-Nauru::
Send,🇳🇷
return

;1F1F3 1F1FA
:*::flag:-Niue::
Send,🇳🇺
return

;1F1F3 1F1FF
:*::flag:-New-Zealand::
Send,🇳🇿
return

;1F1F4 1F1F2
:*::flag:-Oman::
Send,🇴🇲
return

;1F1F5 1F1E6
:*::flag:-Panama::
Send,🇵🇦
return

;1F1F5 1F1EA
:*::flag:-Peru::
Send,🇵🇪
return

;1F1F5 1F1EB
:*::flag:-French-Polynesia::
Send,🇵🇫
return

;1F1F5 1F1EC
:*::flag:-Papua-New-Guinea::
Send,🇵🇬
return

;1F1F5 1F1ED
:*::flag:-Philippines::
Send,🇵🇭
return

;1F1F5 1F1F0
:*::flag:-Pakistan::
Send,🇵🇰
return

;1F1F5 1F1F1
:*::flag:-Poland::
Send,🇵🇱
return

;1F1F5 1F1F2
:*::flag:-St.-Pierre-&-Miquelon::
Send,🇵🇲
return

;1F1F5 1F1F3
:*::flag:-Pitcairn-Islands::
Send,🇵🇳
return

;1F1F5 1F1F7
:*::flag:-Puerto-Rico::
Send,🇵🇷
return

;1F1F5 1F1F8
:*::flag:-Palestinian-Territories::
Send,🇵🇸
return

;1F1F5 1F1F9
:*::flag:-Portugal::
Send,🇵🇹
return

;1F1F5 1F1FC
:*::flag:-Palau::
Send,🇵🇼
return

;1F1F5 1F1FE
:*::flag:-Paraguay::
Send,🇵🇾
return

;1F1F6 1F1E6
:*::flag:-Qatar::
Send,🇶🇦
return

;1F1F7 1F1EA
:*::flag:-Réunion::
Send,🇷🇪
return

;1F1F7 1F1F4
:*::flag:-Romania::
Send,🇷🇴
return

;1F1F7 1F1F8
:*::flag:-Serbia::
Send,🇷🇸
return

;1F1F7 1F1FA
:*::flag:-Russia::
Send,🇷🇺
return

;1F1F7 1F1FC
:*::flag:-Rwanda::
Send,🇷🇼
return

;1F1F8 1F1E6
:*::flag:-Saudi-Arabia::
Send,🇸🇦
return

;1F1F8 1F1E7
:*::flag:-Solomon-Islands::
Send,🇸🇧
return

;1F1F8 1F1E8
:*::flag:-Seychelles::
Send,🇸🇨
return

;1F1F8 1F1E9
:*::flag:-Sudan::
Send,🇸🇩
return

;1F1F8 1F1EA
:*::flag:-Sweden::
Send,🇸🇪
return

;1F1F8 1F1EC
:*::flag:-Singapore::
Send,🇸🇬
return

;1F1F8 1F1ED
:*::flag:-St.-Helena::
Send,🇸🇭
return

;1F1F8 1F1EE
:*::flag:-Slovenia::
Send,🇸🇮
return

;1F1F8 1F1EF
:*::flag:-Svalbard-&-Jan-Mayen::
Send,🇸🇯
return

;1F1F8 1F1F0
:*::flag:-Slovakia::
Send,🇸🇰
return

;1F1F8 1F1F1
:*::flag:-Sierra-Leone::
Send,🇸🇱
return

;1F1F8 1F1F2
:*::flag:-San-Marino::
Send,🇸🇲
return

;1F1F8 1F1F3
:*::flag:-Senegal::
Send,🇸🇳
return

;1F1F8 1F1F4
:*::flag:-Somalia::
Send,🇸🇴
return

;1F1F8 1F1F7
:*::flag:-Suriname::
Send,🇸🇷
return

;1F1F8 1F1F8
:*::flag:-South-Sudan::
Send,🇸🇸
return

;1F1F8 1F1F9
:*::flag:-São-Tomé-&-Príncipe::
Send,🇸🇹
return

;1F1F8 1F1FB
:*::flag:-El-Salvador::
Send,🇸🇻
return

;1F1F8 1F1FD
:*::flag:-Sint-Maarten::
Send,🇸🇽
return

;1F1F8 1F1FE
:*::flag:-Syria::
Send,🇸🇾
return

;1F1F8 1F1FF
:*::flag:-Eswatini::
Send,🇸🇿
return

;1F1F9 1F1E6
:*::flag:-Tristan-da-Cunha::
Send,🇹🇦
return

;1F1F9 1F1E8
:*::flag:-Turks-&-Caicos-Islands::
Send,🇹🇨
return

;1F1F9 1F1E9
:*::flag:-Chad::
Send,🇹🇩
return

;1F1F9 1F1EB
:*::flag:-French-Southern-Territories::
Send,🇹🇫
return

;1F1F9 1F1EC
:*::flag:-Togo::
Send,🇹🇬
return

;1F1F9 1F1ED
:*::flag:-Thailand::
Send,🇹🇭
return

;1F1F9 1F1EF
:*::flag:-Tajikistan::
Send,🇹🇯
return

;1F1F9 1F1F0
:*::flag:-Tokelau::
Send,🇹🇰
return

;1F1F9 1F1F1
:*::flag:-Timor-Leste::
Send,🇹🇱
return

;1F1F9 1F1F2
:*::flag:-Turkmenistan::
Send,🇹🇲
return

;1F1F9 1F1F3
:*::flag:-Tunisia::
Send,🇹🇳
return

;1F1F9 1F1F4
:*::flag:-Tonga::
Send,🇹🇴
return

;1F1F9 1F1F7
:*::flag:-Turkey::
Send,🇹🇷
return

;1F1F9 1F1F9
:*::flag:-Trinidad-&-Tobago::
Send,🇹🇹
return

;1F1F9 1F1FB
:*::flag:-Tuvalu::
Send,🇹🇻
return

;1F1F9 1F1FC
:*::flag:-Taiwan::
Send,🇹🇼
return

;1F1F9 1F1FF
:*::flag:-Tanzania::
Send,🇹🇿
return

;1F1FA 1F1E6
:*::flag:-Ukraine::
Send,🇺🇦
return

;1F1FA 1F1EC
:*::flag:-Uganda::
Send,🇺🇬
return

;1F1FA 1F1F2
:*::flag:-U.S.-Outlying-Islands::
Send,🇺🇲
return

;1F1FA 1F1F3
:*::flag:-United-Nations::
Send,🇺🇳
return

;1F1FA 1F1F8
:*::flag:-United-States::
Send,🇺🇸
return

;1F1FA 1F1FE
:*::flag:-Uruguay::
Send,🇺🇾
return

;1F1FA 1F1FF
:*::flag:-Uzbekistan::
Send,🇺🇿
return

;1F1FB 1F1E6
:*::flag:-Vatican-City::
Send,🇻🇦
return

;1F1FB 1F1E8
:*::flag:-St.-Vincent-&-Grenadines::
Send,🇻🇨
return

;1F1FB 1F1EA
:*::flag:-Venezuela::
Send,🇻🇪
return

;1F1FB 1F1EC
:*::flag:-British-Virgin-Islands::
Send,🇻🇬
return

;1F1FB 1F1EE
:*::flag:-U.S.-Virgin-Islands::
Send,🇻🇮
return

;1F1FB 1F1F3
:*::flag:-Vietnam::
Send,🇻🇳
return

;1F1FB 1F1FA
:*::flag:-Vanuatu::
Send,🇻🇺
return

;1F1FC 1F1EB
:*::flag:-Wallis-&-Futuna::
Send,🇼🇫
return

;1F1FC 1F1F8
:*::flag:-Samoa::
Send,🇼🇸
return

;1F1FD 1F1F0
:*::flag:-Kosovo::
Send,🇽🇰
return

;1F1FE 1F1EA
:*::flag:-Yemen::
Send,🇾🇪
return

;1F1FE 1F1F9
:*::flag:-Mayotte::
Send,🇾🇹
return

;1F1FF 1F1E6
:*::flag:-South-Africa::
Send,🇿🇦
return

;1F1FF 1F1F2
:*::flag:-Zambia::
Send,🇿🇲
return

;1F1FF 1F1FC
:*::flag:-Zimbabwe::
Send,🇿🇼
return

;1F3F4 E0067 E0062 E0065 E006E E0067 E007F
:*::flag:-England::
Send,🏴󠁧󠁢󠁥󠁮󠁧󠁿
return

;1F3F4 E0067 E0062 E0073 E0063 E0074 E007F
:*::flag:-Scotland::
Send,🏴󠁧󠁢󠁳󠁣󠁴󠁿
return

;1F3F4 E0067 E0062 E0077 E006C E0073 E007F
:*::flag:-Wales::
Send,🏴󠁧󠁢󠁷󠁬󠁳󠁿
return
