import {getEmojiList, normalizeNames, saveEmojiList} from "./src/emojiList";
import {getCommonNames, saveCommonNames} from "./src/commonNames";
import {buildScript, combineOgNameWithCommonName, saveScript} from "./src/ahk";
import {useOverrideList} from "./src/overrides";

const fs = require('fs');
const path = require('path');

const FORCE_GET_EMOJI_LIST = true;
const FORCE_GET_COMMON_NAMES = true;

const init = async () => {
    const emojiList = await getEmojiList(FORCE_GET_EMOJI_LIST)
        .catch(e => {
            console.error(e);
            const d = [
                {
                    codes: '1F621',
                    char: '😡',
                    name: 'pouting face',
                    category: 'Smileys & Emotion (face-negative)'
                }

            ];

            return d;
        });

    const normEmojiList = normalizeNames(emojiList);


    saveEmojiList(normEmojiList);

    const commonNames = await getCommonNames(normEmojiList.map(e => e.name), FORCE_GET_COMMON_NAMES);
    saveCommonNames(commonNames);

    const combined = combineOgNameWithCommonName(normEmojiList, commonNames);

    saveJsonFile(combined, 'combined.json')

    const withOverrides = useOverrideList(combined);

    const scriptContent = buildScript(withOverrides);

    saveScript(scriptContent);

    return 'done';
};


init()
    .then(console.log)
    .catch(console.error);


const saveJsonFile = (content: any, fileName: string): void => {
    const p = path.resolve(__dirname, `./assets/${fileName}`)
    fs.writeFileSync(p, JSON.stringify(content, null, 4), {encoding: 'utf8'})
}
