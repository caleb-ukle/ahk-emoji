const axios = require('axios');
const c = require('cheerio');
const fs = require('fs');
const path = require('path');

export const getCommonNames = async (emojiNames: string[], force: boolean = false): Promise<ICommonEmoji[]> => {
    if (force) {
        return getNames(emojiNames)
    }

    const p = path.resolve(__dirname, '../assets/commonNames.json');

    if (fs.existsSync(p)) {
        const file = fs.readFileSync(p, {encoding: 'utf8'})
        return JSON.parse(file);

    } else {
        return getNames(emojiNames)
    }
};

export const saveCommonNames = (list: ICommonEmoji[]): void => {
    const p = path.resolve(__dirname, '../assets/commonNames.json')
    fs.writeFileSync(p, JSON.stringify(list, null, 4), {encoding: 'utf8'});
};

const getNames = async (list: string[]): Promise<ICommonEmoji[]> => {
    const commonList: ICommonEmoji[] = [];

    for (let i = 0; i < list.length; i++) {
        await delay(Math.random() * 100);
        const commonNames = await getCommonNameByLongName(
            list[i].trim().toLowerCase().split(' ').join('-'));

        commonList.push(commonNames)
    }

    return commonList;
};

const getCommonNameByLongName = async (ogName: string): Promise<ICommonEmoji> => {
    const {data: html} = await axios.get(`https://emojipedia.org/${ogName}/`)
        .catch(e => {
            fs.appendFile(path.resolve(__dirname, '../assets/notFound.txt'),`${ogName}\n`, (err) => {
                if (!!err) {
                    throw err
                }
                console.warn('error for', ogName, 'wrote to notFound.txt');
            });
            return {
                data: null,
            }
        });

    let parsed = [];

    if (!!html) {
        parsed = parseRequest(html);
    }

    return {
        originalName: ogName,
        commonNames: parsed,
    }
}

const parseRequest = (html: string): string[] => {
    const $ = c.load(html);

    const listItems = $('section.aliases')
        .find('ul');

    const parsed = [];


    for (let i = 0; i < listItems.length; i++) {
        const html = $(listItems[i]).html().replace(/<span.+span>\s/g, '')
        const names = $(html).text().trim().toLowerCase().split('\n').map(item => item.split(' ').join('-'))
        parsed.push(...names);
    }
    return parsed;
};

const delay = ms => {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
};

export interface ICommonEmoji {
    originalName: string;
    commonNames: string[]
}
