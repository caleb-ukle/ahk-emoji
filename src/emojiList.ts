const axios = require('axios');
const fs = require('fs');
const path = require('path');

export const getEmojiList = async (force: boolean = false, url: string = 'https://raw.githubusercontent.com/amio/emoji.json/master/emoji.json'): Promise<IEmoji[]> => {
    if (force) {
        const {data} = await axios.get(url)
            .catch(e => console.error(e));
        return data as IEmoji[];

    }
    const p = path.resolve(__dirname, '../assets/emojiList.json');

    if (fs.existsSync(p)) {
        const buffer = fs.readFileSync(p, 'utf8');
        return JSON.parse(buffer) as IEmoji[];

    } else {
        const {data} = await axios.get(url)
            .catch(e => console.error(e));
        return data as IEmoji[]
    }
};

export const normalizeNames = (list: IEmoji[]): IEmoji[] => {
    return list.map(e => {
        const b = e.name.trim().toLowerCase().split(' ').join('-');
        return {
            ...e,
            name: b
        };
    })
}

export const saveEmojiList = (emojis: IEmoji[]): void => {
    const p = path.resolve(__dirname, '../assets/emojiList.json');
    fs.writeFileSync(p, `${JSON.stringify(emojis, null, 4)}`, {encoding: 'utf8'});
};


export interface IEmoji {
    codes: string;
    name: string;
    char: string;
    category: string;
}
