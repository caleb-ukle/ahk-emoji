import {ICombinedEmoji} from "./ahk";

const overrideList: IOverrideEmoji[] = require('../assets/overrides.json');

export const useOverrideList = (emojiList: ICombinedEmoji[]): ICombinedEmoji[] => {
    const overridden: ICombinedEmoji[] = [];

    overrideList.forEach(o => {
        const found = emojiList.find(e => e.name === o.name);

        if (!!found) {
            if (o.append) {
                const names = new Set(found.commonNames);
                o.altNames.forEach(n => names.add(n));
                found.commonNames = Array.from(names);
            } else {
                found.commonNames = o.altNames;
            }

            overridden.push(found);
        }
    });
    return [...emojiList, ...overridden];
}


export interface IOverrideEmoji {
    name: string,
    altNames: string[];
    append: boolean;
}
