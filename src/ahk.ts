import {IEmoji} from "./emojiList";
import {ICommonEmoji} from "./commonNames";

const fs = require('fs');
const path = require('path');

export const buildScript = (emojiList: ICombinedEmoji[]): string[] => {
    return emojiList.map(emoji => {
        return `;${emoji.codes}\n${getCommonNameSelectors(emoji.commonNames)}\n${getOgNameSelector(emoji.name)}\nSend,${emoji.char}\nreturn`
    });
}

export const combineOgNameWithCommonName = (og: IEmoji[], cn: ICommonEmoji[]): ICombinedEmoji[] => {
    if (og.length >= cn.length) {
        return og.map(e => {
            const item = cn.find(n => n.originalName === e.name);
            return {
                commonNames: item?.commonNames ?? [],
                name: e?.name ?? '',
                char: e?.char ?? '',
                category: e?.category ?? '',
                codes: e?.codes ?? ''
            }
        })
    } else {
        return cn.map(e => {
            const item = og.find(n => n.name === e.originalName);

            return {
                commonNames: e?.commonNames ?? [],
                name: item?.name ?? '',
                char: item?.char ?? '',
                category: item?.category ?? '',
                codes: item?.codes ?? ''
            }
        })
    }
}

const getCommonNameSelectors = (names: string[]): string => {
    if (names.length > 0) {

        return names.map(cn => `:*::${cn.substring(0, 39)}::`).join('\n');
    }

    return '';
}

const getOgNameSelector = (name: string): string => {
    if (!!name) {

        return `:*::${name.substring(0, 39).trim().split(' ').join('-')}::`;
    }
    return ''
}

export const saveScript = (content: string[]): void => {
    const data = `\ufeff${content.join(`\n\n`)}`;
    const options = {
        encoding: 'utf8'
    };

    const p = path.resolve(__dirname, '../assets/emoji.ahk');

    fs.writeFileSync(p, data, options);
};

export interface ICombinedEmoji extends IEmoji {
    commonNames: string[];
}
